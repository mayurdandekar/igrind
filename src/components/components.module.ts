import { NgModule } from '@angular/core';
import { CommonConverterComponent } from './common-converter/common-converter';
import { CalcComponent } from './calc/calc';
import { ArithCalcComponent } from './arith-calc/arith-calc';
import { CalculatorButtonComponent } from './calculator-button/calculator-button';
@NgModule({
	declarations: [CommonConverterComponent,
    CalcComponent,
    ArithCalcComponent,
    CalculatorButtonComponent],
	imports: [],
	exports: [CommonConverterComponent,
    CalcComponent,
    ArithCalcComponent,
    CalculatorButtonComponent]
})
export class ComponentsModule {}
