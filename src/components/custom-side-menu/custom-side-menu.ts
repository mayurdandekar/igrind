import { Component, Input } from '@angular/core';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { NavController } from 'ionic-angular';
import * as FirebaseConstants from '../../utils/analytics-constants';

/**
 * Generated class for the CustomSideMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-side-menu',
  templateUrl: 'custom-side-menu.html'
})
export class CustomSideMenuComponent {
  @Input() calculatorName: String = "Name";
  @Input() visible: boolean = false;
  @Input() calculatorKey: string = "coolant";

  constructor(public firebaseAnalytics: FirebaseAnalytics,
    public navCtrl: NavController) {
    console.log('Hello CustomSideMenuComponent Component');
    if (this.calculatorKey == "home") {
      // alert(123);
    }
  }

  info() {
    console.log(123);
    if (this.calculatorKey == "coolant") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.COOLANT_CALC_INFO_TOP_HITCOUNT, {});
      this.navCtrl.push('InfoPage', {
        data: this.calculatorKey
      });
    } else if (this.calculatorKey == "dressing") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.DRESSING_CALC_INFO_TOP_HITCOUNT, {});
      this.navCtrl.push('InfoPage', {
        data: this.calculatorKey
      });
    } else if (this.calculatorKey == "wheel_speed") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.WHEEL_SPEED_CALC_INFO_TOP_HITCOUNT, {});
      this.navCtrl.push('InfoPage', {
        data: this.calculatorKey
      });
    } else if (this.calculatorKey == "plunge_dressing") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_INFO_TOP_HITCOUNT, {});
      this.navCtrl.push('InfoPage', {
        data: this.calculatorKey
      });
    } else if (this.calculatorKey == "stationary_dressing") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.STATIONARY_TOOL_CALC_INFO_TOP_HITCOUNT, {});
      this.navCtrl.push('InfoPage', {
        data: this.calculatorKey
      });
    }
    else if (this.calculatorKey == "qprime") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.QPRIME_CALC_INFO_TOP_HITCOUNT, {});
      this.navCtrl.push('InfoPage', {
        data: this.calculatorKey
      });
    }
    else if (this.calculatorKey == "chip_thickness") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.CHIP_THICKNESS_CALC_INFO_TOP_HITCOUNT, {});
      this.navCtrl.push('InfoPage', {
        data: this.calculatorKey
      });
    } else if (this.calculatorKey == "area"){
      this.firebaseAnalytics.logEvent(FirebaseConstants.AREA_CONVERSION_CALC_INFO_TOP_HITCOUNT, {});
      this.navCtrl.push('InfoPage', {
        data: this.calculatorKey
      });
    }

    
  }

}
