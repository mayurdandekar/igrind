import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  ViewController,
  NavParams,
  Slides,
  MenuController,
} from "ionic-angular";
/**
 * Generated class for the CalcComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "calc",
  templateUrl: "calc.html",
})
export class CalcComponent {
  SwipedTabsIndicator: any = null;
  tabs: any = [];
  userInput: any;
  calcType: any = 0;
  @ViewChild("SwipedTabsSlider") SwipedTabsSlider: Slides;
  result: any;
  constructor(
    public navCtrl: NavController,
    private viewCtrl: ViewController,
    private _navParams: NavParams,
    private menuCtrl: MenuController
  ) {
    this.userInput = this._navParams.get("userInput");
    // this.tabs=["Arithmetic","Algebra","Trigo","Calculus","Power","Fraction"];
    this.tabs = ["Arithmetic", "Trigo", "History"];
  }

  ionViewDidEnter() {
    this.SwipedTabsIndicator = document.getElementById("indicator");
    this.menuCtrl.enable(true, "main_menu");
  }

  selectTab(index) {
    this.SwipedTabsIndicator.style.webkitTransform =
      "translate3d(" + 100 * index + "%,0,0)";
    this.SwipedTabsSlider.slideTo(index, 500);
  }

  updateIndicatorPosition() {
    // this condition is to avoid passing to incorrect index
    if (
      this.SwipedTabsSlider.length() > this.SwipedTabsSlider.getActiveIndex()
    ) {
      this.SwipedTabsIndicator.style.webkitTransform =
        "translate3d(" +
        this.SwipedTabsSlider.getActiveIndex() * 100 +
        "%,0,0)";
    }
  }

  animateIndicator($event) {
    if (this.SwipedTabsIndicator)
      this.SwipedTabsIndicator.style.webkitTransform =
        "translate3d(" +
        $event.progress * (this.SwipedTabsSlider.length() - 1) * 100 +
        "%,0,0)";
  }

  onCalculation(result) {
    this.result = result;
  }
  onSelectcalc(calcType) {
    this.calcType = calcType;
  }

  dismiss(isCalc) {
    let calculatedData = { result: this.result };
    if (isCalc) this.viewCtrl.dismiss(calculatedData);
    else this.viewCtrl.dismiss();
  }
}
