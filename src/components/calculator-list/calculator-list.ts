import { Component, Input } from '@angular/core';
import { CalcListProvider } from '../../providers/calc-list/calc-list';
import { NavController, Platform } from 'ionic-angular';


/**
 * Generated class for the CalculatorListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'calculator-list',
  templateUrl: 'calculator-list.html'
})
export class CalculatorListComponent {
  @Input() calcType;
  calculatorsList: Array<{ name: any, desc: any, logo: any, key: any }>;

  constructor(public calcModel: CalcListProvider,
    public navCtrl: NavController,
    public platform: Platform) {
    this.calculatorsList = new Array();
  }
  ngOnInit() {
    let calculatorMethod = this.calcModel.getConversionCalculators()
    if (this.calcType == 'grind') {
      calculatorMethod = this.calcModel.getGrindParamCalculators();
    }
    else if (this.calcType == 'dress') {
      calculatorMethod = this.calcModel.getDressParamCalculators()
    }
    else if (this.calcType == 'cut') {
      calculatorMethod = this.calcModel.getCutParamCalculators()
    }
    calculatorMethod.then((calcList) => {
      if (calcList != null)
        this.calculatorsList = calcList;
    });
  }

  navigateToCalculator(pageName) {
    var nextPage: any;
    nextPage = pageName.component;

    this.calcModel.setActiveCalculator(pageName);
    if (this.platform.is('android')) {
      this.navCtrl.push(nextPage);
    } else if (this.platform.is('ios')) {
      this.navCtrl.setRoot(nextPage);
    } else {
      this.navCtrl.push(nextPage);
    }
  }
}
