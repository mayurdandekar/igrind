import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ViewController, Events } from "ionic-angular";

/**
 * Generated class for the ArithCalcComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "arith-calc",
  templateUrl: "arith-calc.html",
})
export class ArithCalcComponent {
  myPara: any = "";
  myAns: any = 0;
  result: any = "";
  isInverse: boolean = false;
  appendResultString: boolean = false;
  PowArray = [];
  allOperators = ["+", "-", "*", "/"];
  prevHistory = [];
  ansUnit = "DEG";
  @Input("userInput") userInput;
  @Input("calcType") calcType;
  @Output() calculatedValue: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectedCalc: EventEmitter<any> = new EventEmitter<any>();
  isExp: boolean;
  inputString: string = "";
  isRoot: boolean;
  showEqual: boolean;
  showResult: boolean = true;
  isRestore: boolean;
  constructor(private viewCtrl: ViewController, private events: Events) {}
  ngOnInit() {
    if (this.userInput) {
      this.myPara = this.inputString = this.result = this.userInput;
      this.appendResultString = true;
    }
    let historyString = localStorage.getItem("myHistory");
    if (historyString && historyString.length) {
      this.prevHistory = JSON.parse(historyString);
    }
  }

  // splitPower(res)
  // {
  //     /****** Include  */
  //   if(res.includes("^"))
  //   {

  //     let copy = this.myPara;
  //     // this.myPara=
  //     this.myPara = this.myPara.replace(/[0-9]+/g, "#").replace(/[\(|\|\.)]/g, "");
  //     let numbers = copy.split(/[^0-9\.]+/);
  //     let operators = this.myPara.split("#").filter((n)=>{return n});
  //     console.log(operators);

  // //     let bodmasOperators=operators.reverse();
  // //     let operatorsArray = [];
  // //     let result:any='';
  // //     numbers.reverse().forEach((num,i)=> {

  // // console.log(num);

  // //         // operatorsArray.push(num);
  // //         if (i>=2)
  // //         {
  // //             if(bodmasOperators[i-2]=='^')
  // //             {
  // //                 if(result=='')
  // //                 {
  // //                     result +=Math.pow(num,numbers[i-1])
  // //                 }
  // //                 else{
  // //                     if( eval(result))
  // //                     {
  // //                         let calculatedResult= eval(result);
  // //                         result=Math.pow(num,calculatedResult);
  // //                     }
  // //                     else
  // //                     return 'Error!';

  // //                 }
  // //                 // result==''?result +=Math.pow(num,numbers[i-1]):Math.pow(num,eval(result));
  // //             }
  // //             else
  // //             {
  // //                 bodmasOperators[i-2]?result +=bodmasOperators[i-2]+num:result +=num;
  // //             }

  // //             // operatorsArray.push(operators[i]);
  // //         }
  // //     });
  // //     alert(result);
  // //     return result;
  //     // operatorsArray.reverse().forEach((operator,index)=> {
  //     //     if(operator)
  //     //     {
  //     //         if()
  //     //     }
  //     //  });
  //     // console.log(numbers.reverse(),operators,result);

  //     // let rawArray=res.match(/(\(?[^(]*\)?)\^(\(?.*\)?)/);
  //     // console.log(rawArray);

  //     // this.PowArray.push(rawArray[1]);
  //     // let powFirstNum= rawArray[1].toString().split('*');
  //     // powFirstNum= powFirstNum.pop().split('(');
  //     // powFirstNum= powFirstNum.pop().split('+');
  //     // powFirstNum= powFirstNum.pop().split('-');
  //     // powFirstNum= powFirstNum.pop().split('/');

  //     // if(isFirst)
  //     // {
  //         // let powSecondExpr=this.splitPower(rawArray[2]);
  //         // let powVal=Math.pow(powFirstNum,this.splitPower(rawArray[2]));
  //         // let strToBeReplaced=powFirstNum+'^'+powSecondExpr;alert(strToBeReplaced)
  //         // this.myPara.replace()
  //         // alert(Math.pow(powFirstNum,this.splitPower(rawArray[2])));
  //         // return Math.pow(powFirstNum,this.splitPower(rawArray[2]));
  //     // }
  //     // else{
  //     //
  //     //     rawArray[2].replace()
  //     //     return
  //     // }

  //     // alert(eval(rawArray[2]));
  //     // let powerArray=this.myPara.split('^');
  //     // let revPowerArray=powerArray.reverse();
  //     // revPowerArray.forEach((revPowElem,index)=> {
  //     //     if(index==0)
  //     //     {
  //     //         revPowerArray[0]=eval(revPowElem);
  //     //     }
  //     //     else{
  //     //         let powFirstNum= revPowElem.split('*');
  //     //         powFirstNum= powFirstNum.pop().split('(');
  //     //         powFirstNum= powFirstNum.pop().split('+');
  //     //         powFirstNum= powFirstNum.pop().split('-');
  //     //         powFirstNum= powFirstNum.pop().split('/');

  //     //         let calculatedPow=
  //     //     }
  //     // });
  //     // console.log(revPowerArray);

  //     }
  //   return res;
  //   }
}
