import { Component, ViewChild, Input } from '@angular/core';
import { NavController, Platform, Events, App } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';


/**
 * Generated class for the CommonConverterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'common-converter',
  templateUrl: 'common-converter.html'
})
export class CommonConverterComponent {
  @Input('converterData') wheelData1: any;
  @Input('converterName') converterType: any;
  @ViewChild('slides1') slides1: Slides;
  @ViewChild('slides2') slides2: Slides;
  @ViewChild('focusInput') myInput;
  @ViewChild('workPieceInput') RpmInput;


  toggledValue: boolean;
  myCart: any = [];
  totalPrice: any = 0;
  selected: string;
  selection: any;
  inp: any;
  outp: any;
  input: any = 1;
  output: any;
  wheelData: any = [];
  currentTab: any;
  wheelOutputData: any = [];
  slide1CurrentIndex: number;
  slide2CurrentIndex: number;
  currentWheel: any;
  showWorkPieceInput: boolean = false;
  workPieceValue: any = 0;
  isInputInvalid: boolean = false;
  isDirtyWorkPiece: boolean=false;

  constructor(public navCtrl: NavController,
    public keyboard: Keyboard,
    public platform: Platform,
    public events: Events,
    public app: App, ) {
    this.myCart;
    this.input;
    this.output;

  }


  ngOnInit() {
    if (!event.cancelable) { event.preventDefault(); }
    this.events.subscribe('menu:close', menuResponse => {
      console.log("ngoninit opening keyboard");

      this.slideChanged1(1);
    });

    window.addEventListener('focusin', (e) => {
    //  alert(this.slides2.getActiveIndex())
     setTimeout(function () {
      //  this.slides2.slideTo(this.slides2.getActiveIndex());
      // this.getConvertedOutput();
      }, 1000);
    });

    if (this.platform.is('ios')) {
      window.addEventListener('keyboardWillShow', (e) => {
        setTimeout(function () {
          this.slideChanged1(1);
        }, 1000);

      });
      window.addEventListener('keyboardWillHide', () => {
        //_this.slideChanged1(1);
        setTimeout(function () {
          this.slideChanged1(1);
        }, 1000);
      });
    }

    this.wheelData = this.wheelData1;
    setTimeout(() => {
      // this.keyboard.show() // for android
      // this.myInput.setFocus();
      this.slideChanged1(1);
    }, 500);

//on Touch end Slide1
    this.slides1.ionSlideTouchEnd.subscribe(()=>{
      let slide1Elm = <HTMLElement>document.querySelector(".slide1Class .swiper-wrapper");
      let transformValPx=slide1Elm['style']['transform'].split(', ');
      let transformValExist=transformValPx[1].replace('px', '');
      let currentSlideVal=Math.abs(Math.ceil(parseInt(transformValExist)/-60))
      console.log(currentSlideVal,currentSlideVal>this.wheelData.length-4)
      if(currentSlideVal>this.wheelData.length-4)
      {
        currentSlideVal=this.wheelData.length-4;
      }
      else if(this.slide1CurrentIndex==0 && parseInt(transformValExist)>-60)
      {
        currentSlideVal=0;

      }
      this.slides1.slideTo(currentSlideVal)
      this.slide1CurrentIndex = this.slides1.getActiveIndex();
      this.wheelOutputData = this.wheelData[this.slide1CurrentIndex + 1].outp;
      console.log(this.wheelData,transformValExist,currentSlideVal);
      slide1Elm.style.transform = "translate3d(0px, " + (currentSlideVal)*-60 + "px, 0px)";      
    });
//on Touch end Slide2

    this.slides2.ionSlideTouchEnd.subscribe(() => {
      let elm = <HTMLElement>document.querySelector(".slide2Class .swiper-wrapper");
      let transformValPx=elm['style']['transform'].split(', ');
      let transformValExist=transformValPx[1].replace('px', '');
      let currentSlideVal=Math.abs(Math.ceil(parseInt(transformValExist)/-60))
      
      this.wheelOutputData = this.wheelData[this.slide1CurrentIndex + 1].outp;
      let outputDataSlideLength = this.wheelOutputData.length - 3;
      if (outputDataSlideLength == 1) {
        currentSlideVal=1
        this.slides2.lockSwipes(true);
        this.slides2.slideTo(1);
      }
      else {
        this.slides2.lockSwipes(false);
        if (currentSlideVal <= 1) {
          currentSlideVal=1
          this.slides2.slideTo(1);
          this.slides2.lockSwipeToPrev(true);
        }
        else if (currentSlideVal >= outputDataSlideLength) {
          currentSlideVal=outputDataSlideLength
          this.slides2.slideTo(outputDataSlideLength);
          this.slides2.lockSwipeToNext(true);
        }
        else
        {
          this.slides2.slideTo(currentSlideVal);
        }
      }
      // 
    this.getConvertedOutput();
      elm.style.transform = "translate3d(0px, " + (currentSlideVal)*-60 + "px, 0px)";      
    });
  }


  slideChanged() {
    this.slide1CurrentIndex = this.slides1.getActiveIndex();
    let inputDataSlideLength = this.wheelData.length - 4;
    if (this.slide1CurrentIndex >= inputDataSlideLength) {
      this.slides1.slideTo(inputDataSlideLength);
      this.slides1.lockSwipeToNext(true);
      this.wheelOutputData = this.wheelData[inputDataSlideLength + 1].outp;
      var a = this.wheelData[inputDataSlideLength + 1].data
      this.inp = a.substr(0, a.indexOf(' to'));
      this.slide1CurrentIndex = inputDataSlideLength;   
    // console.log(this.slide1CurrentIndex,inputDataSlideLength, this.wheelOutputData, "current slide ka index after entered in if condition");
    }

    else {
      this.wheelOutputData = this.wheelData[this.slide1CurrentIndex + 1].outp
      this.slides1.lockSwipeToNext(false);
    }

    var a = this.wheelData[this.slide1CurrentIndex + 1].data
    this.inp = a.substr(0, a.indexOf(' to'));

    if (this.wheelData[this.slide1CurrentIndex + 1].outp.length == 4) {
      this.slideChanged1(1);
    }
    else {
      this.slideChanged1(2);
    }

  }

  slideChanged1(val) {
    let elm = <HTMLElement>document.querySelector(".slide2Class .swiper-wrapper");
    let slide2CurrentIndex: any = this.slides2.getActiveIndex();

    this.wheelOutputData = this.wheelData[this.slide1CurrentIndex + 1].outp;

    if (val == 1) {
      this.slides2.slideTo(1);
    }
    let outputDataSlideLength = this.wheelOutputData.length - 3;
    console.log("output inside slideChanged1 function", outputDataSlideLength, slide2CurrentIndex);
    if (outputDataSlideLength == 1) {
      this.slides2.lockSwipes(true);
      this.slides2.slideTo(1);
    }
    else {
      this.slides2.lockSwipes(false);
      if (slide2CurrentIndex <= 1) {
        this.slides2.slideTo(1);
        this.slides2.lockSwipeToPrev(true);
      }
      else if (slide2CurrentIndex >= outputDataSlideLength) {
        this.slides2.slideTo(outputDataSlideLength);
        this.slides2.lockSwipeToNext(true);
      }
    }
    // setTimeout(()=>{alert(this.slides2.getActiveIndex())
    //   if (slide2CurrentIndex >= outputDataSlideLength) {
    //   this.slides2.slideTo(outputDataSlideLength);
    //     }//if
    //   },2000)
    elm.style.transform = "translate3d(0px, " + (this.slides2.getActiveIndex())*-60 + "px, 0px)";
    this.getConvertedOutput();
  }


  getConvertedOutput() {
    let currentConverter = this.getActiveSlide2();
    this.currentWheel = this.wheelData[this.slide1CurrentIndex + 1].outp[currentConverter + 1];
    let toFixedVal=this.currentWheel.decimal;
    console.log(this.wheelData[this.slide1CurrentIndex + 1].outp[currentConverter + 1]);

    if (this.currentWheel && this.currentWheel.isWorkPiece) {
      if(this.showWorkPieceInput)
      {
        
      }
      else{
        this.showWorkPieceInput = true;
        this.workPieceValue=0;
        this.output = '';
      }
      
    }
    else {
      this.showWorkPieceInput = false;
    }
   
    (!this.showWorkPieceInput && this.currentWheel && this.input) ? this.output = (this.input * this.currentWheel.value).toFixed(toFixedVal) : this.getWorkPieceOutput();
    console.log(this.output, "after output");
    if (this.input && !this.output) {
      this.isInputInvalid = true;
    }

  }

  getWorkPieceOutput() {
    let toFixedVal=this.currentWheel.decimal;
    (this.showWorkPieceInput && this.currentWheel && this.input != "" && this.input != 0 && this.workPieceValue != "" && this.workPieceValue != 0) ? (this.currentWheel.workPieceType == '*' ? this.output = (this.input * this.workPieceValue).toFixed(toFixedVal) : this.output = (this.input / this.workPieceValue).toFixed(toFixedVal)) : this.output = '';
  }

  accessInput(data) {
   
  this.forceSlideChangedOnBlur();
    // this.keyboard.show() // for android
    this.myInput.setFocus();
  }

  onBlurWorkPiece()
  {
    this.isDirtyWorkPiece=true;
    this.forceSlideChangedOnBlur();
  }

  forceSlideChangedOnFocus()
  {
    setTimeout(()=>{
      this.slideChanged1(2);
     },750)

    //checks if still it doesn't get active slide
     setTimeout(()=>{
      let nextSlide = <HTMLElement>document.querySelector(" .slide2Class .swiper-slide-next");
      if(!nextSlide)
        this.slideChanged1(2);
     },1550)
  }

  forceSlideChangedOnBlur()
  {
    setTimeout(()=>{
      this.slideChanged1(2);
   },600);
    //checks if still it doesn't get active slide
   setTimeout(()=>{
    let nextSlide = <HTMLElement>document.querySelector(".slide2Class .swiper-slide-next");
    if(!nextSlide)
      this.slideChanged1(2);
   },800)
  }

  getActiveSlide2() {
    return this.slides2.getActiveIndex()
  }

  ionViewDidLeave() {
    console.log("did leave");
    // this.keyboard.hide();
  }

  inputKeypressValidation(ev,val) {
    console.log(val, "last added keypress val")
    if (ev.keyCode == 46 || (ev.keyCode >= 48 && ev.keyCode <= 57)) {
      var x = val.toString().split('.');
      if (x[1].length >= 2) {
        console.log(x[1].length >= 2, x[1].length)
        
        return false;
        }
    }
    else {
      ev.preventDefault()
      }
    // let val = this.input;
    // if (val) {
    //   var x = val.split('.');

    //   if (parseFloat(val.toString() + ev.key.toString()) > 10000000) {
    //     ev.preventDefault();
    //   }

    //   if (x[1]) {
    //     if (x[1].length == 8) {
    //       ev.preventDefault();
    //     }
    //   }
    //   else if (x[0] && x[1]) {
    //     if (x[0].length >= 2) {
    //       ev.preventDefault();
    //     }
    //   }
    //   if (ev.key == '.') {
    //     if (val) {
    //       if (val.toString().indexOf('.') != -1) {
    //         ev.preventDefault();
    //       }
    //     }
    //   } else if (!this.isNumeric(ev.key)) {
    //     ev.preventDefault();
    //   }
    // }//if value
  }

  checkInputValidation(ev,isRPM) {

    if((isRPM && this.workPieceValue.length>9) || (isRPM && this.workPieceValue>100000000))
    {
      this.workPieceValue = this.workPieceValue.slice(0, -1);
    }
    else if(isRPM && this.workPieceValue.split(".").length>=3)
    {
      this.workPieceValue = this.workPieceValue.slice(0,this.workPieceValue.lastIndexOf('.') );
    }
    else if(!isRPM && this.input.length>9 || (!isRPM && this.input>100000000))
    {
      this.input = this.input.slice(0, -1);
    }
    else if(!isRPM && this.input.split(".").length>=3)
    {
      this.input = this.input.slice(0,this.input.lastIndexOf('.') );
    }
  }

  checkNumber(ev) {
    let inp = ev.key;
    if (!(this.mobilenoValidationRunTime(inp))) {
      ev.preventDefault();
    }

  }//checkNumber


  mobilenoValidationRunTime(val) {
    return /^\d{1}$/.test(val);
  }
  floatnoValidationRunTime(val) {
    return /[\.0-9]{1}/.test(val);
  }
  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
 
}
