import { Component, EventEmitter, Input, Output } from "@angular/core";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";

/**
 * Generated class for the CalculatorButtonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "calculator-button",
  templateUrl: "calculator-button.html",
})
export class CalculatorButtonComponent {
  @Input("pageRef") pageRef;
  @Input("inputRef") inputRef;
  @Input("formRef") formRef;

  constructor(public common: CommonHelperProvider) {

  }
}
