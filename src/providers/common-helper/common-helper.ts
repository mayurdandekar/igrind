import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ModalController, Platform } from "ionic-angular";
import { CalcAllPage } from "../../pages/calc-all/calc-all";

/*
  Generated class for the CommonHelperProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommonHelperProvider {
  constructor(
    public http: HttpClient,
    public modalCtrl: ModalController,
    public platform: Platform
  ) {
    console.log("Hello CommonHelperProvider Provider");
  }
  getNonFloatIndexFromNumericStr(val) {
    var x = val.match(/[.]/);
    if (x && x[0] && x[0].index != 0) {
      let floatValue = val.split(".");
      if (floatValue[1] != null && floatValue[1] != "")
        return this.getNonNumericIndexFromNumericStr(
          floatValue[0].length + 1,
          val.substring(val.indexOf(".") + 1)
        );
      else if (floatValue[1] == "" && floatValue[2] == "")
        return floatValue[0].length + 1;
      else return false;
    } else return this.getNonNumericIndexFromNumericStr(0, val);

    // return x;
  }
  // non numeric char index in input tel string
  getNonNumericIndexFromNumericStr(numLngth, val) {
    var x = val.match(/\D/);
    return x ? numLngth + x.index : "false";
  }

  floatnoValidationRunTime(val) {
    // return /[\.0-9]{1}/.test(val);
    return /^\d{1}$/.test(val);
  }

  getCalculatedValue(pageRef, inputLabel, formRef) {
    let userInput = formRef ? formRef.value[inputLabel] : pageRef[inputLabel];
    let calcModal = this.modalCtrl.create(CalcAllPage, {
      userInput: userInput,
    });
    calcModal.present();
    calcModal.onDidDismiss((data) => {
      if (data !== null && data != undefined) {
        isFinite(data.result)
          ? formRef
            ? formRef.get(inputLabel).setValue(data.result)
            : (pageRef[inputLabel] = data.result)
          : "";
        if (!formRef) {
          pageRef.onChangeInput(data.result);
          console.log(inputLabel);
          if (inputLabel == "inputRPMValue" || inputLabel == "inputValue") {
            pageRef.onChange(data.result);
            if (inputLabel == "inputRPMValue") {
              pageRef.error2 = data.result ? false : true;
              pageRef.error3 = data.result ? false : true;
            }

            pageRef.error1 = pageRef["inputValue"] ? false : true;
          }
        }
      }
    });
  }

  //
  setFocusInput(pageRef, currentInputRef, currentFormRef) {
    //
    // uncomment for custom calculator
    //
    setTimeout(() => {
      pageRef.isKeyBoard = true;
      pageRef.currentInputRef = currentInputRef;
      if (currentFormRef) {
        pageRef.currentFormRef = currentFormRef;
      }
    }, 250);
  }

  setBlur(pageRef) {
    //
    // uncomment for custom calculator
    //
    setTimeout(() => {
      pageRef["isKeyBoard"] = false;
    }, 100);
  }

  getCalc(pageRef, inputLabel, formRef) {}

  telkeypress(ev) {
    var inp = ev.key;
    if (ev.keyCode == 13) {
      let activeElement = <HTMLElement>document.activeElement;
      activeElement.blur();
    }
    if (ev.target.value.length >= 9 || !this.floatnoValidationRunTime(inp)) {
      ev.target.value.split(".").length > 1 && ev.preventDefault();
    }
  }

  getCalcButtonHeight() {
    return !this.platform.is("ios") ? "66px" : "92px";
  }
}
