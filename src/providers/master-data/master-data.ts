import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import * as Constants from "../../utils/constants";
/*
  Generated class for the MasterDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MasterDataProvider {
  constructor(private storage: Storage) {
    console.log("Hello MasterDataProvider Provider");
  }

  getWheelSpeedDropDowns(): Promise<any> {
    return this.storage.get(
      Constants.KEY_WHEEL_SPEED + Constants.DATABASE_VERSION
    );
  }

  getCoolantTypeDropDowns(): Promise<any> {
    return this.storage.get(
      Constants.KEY_COOLANT_TYPE + Constants.DATABASE_VERSION
    );
  }

  getProductTypeDropDowns(): Promise<any> {
    return this.storage.get(
      Constants.KEY_PRODUCT_TYPE + Constants.DATABASE_VERSION
    );
  }

  getSpeedRatioRecommendedValues(): Promise<any> {
    return this.storage.get(
      Constants.KEY_SPEED_RATIO_RECOMMENDED + Constants.DATABASE_VERSION
    );
  }

  getOverlapRatioRecommendedValues(): Promise<any> {
    return this.storage.get(
      Constants.KEY_OVERLAP_RATIO_RECOMMENDED + Constants.DATABASE_VERSION
    );
  }

  getMaxDressDepthRecommendedValues(): Promise<any> {
    return this.storage.get(
      Constants.KEY_MAX_DRESS_DEPTH_RECOMMENDED + Constants.DATABASE_VERSION
    );
  }

  getWheelTechTypeDropDowns(isImperial, isCDnotNCD): Promise<any> {
    console.log(isImperial);
    console.log(isCDnotNCD);
    if (isImperial) {
      if (isCDnotNCD) {
        return this.storage.get(
          Constants.KEY_WHEEL_TECH_TYPE_IMPERIAL_CD + Constants.DATABASE_VERSION
        );
      } else {
        return this.storage.get(
          Constants.KEY_WHEEL_TECH_TYPE_IMPERIAL + Constants.DATABASE_VERSION
        );
      }
    } else {
      if (isCDnotNCD) {
        return this.storage.get(
          Constants.KEY_WHEEL_TECH_TYPE_METRIC_CD + Constants.DATABASE_VERSION
        );
      } else {
        return this.storage.get(
          Constants.KEY_WHEEL_TECH_TYPE_METRIC + Constants.DATABASE_VERSION
        );
      }
    }
  }
  getTotalInfeedRateRecommendedValues(isImperial): Promise<any> {
    // console.log(this.storage.get(Constants.KEY_TOTAL_INFEED_RATE_IMPERIAL + Constants.DATABASE_VERSION))
    if (isImperial) {
      return this.storage.get(
        Constants.KEY_TOTAL_INFEED_RATE_IMPERIAL + Constants.DATABASE_VERSION
      );
    } else {
      return this.storage.get(
        Constants.KEY_TOTAL_INFEED_RATE_METRIC + Constants.DATABASE_VERSION
      );
    }
  }
  getWheelSpeedSFPMRecommendedValues(isImperial, isCDnotNCD): Promise<any> {
    // console.log(this.storage.get(Constants.KEY_TOTAL_INFEED_RATE_IMPERIAL + Constants.DATABASE_VERSION))
    if (isImperial) {
      if (isCDnotNCD) {
        return this.storage.get(
          Constants.KEY_WHEEL_SPEED_CD_ENG_RECOMMENDED +
            Constants.DATABASE_VERSION
        );
      } else {
        return this.storage.get(
          Constants.KEY_WHEEL_SPEED_NCD_ENG_RECOMMENDED +
            Constants.DATABASE_VERSION
        );
      }
    } else {
      if (isCDnotNCD) {
        return this.storage.get(
          Constants.KEY_WHEEL_SPEED_CD_MET_RECOMMENDED +
            Constants.DATABASE_VERSION
        );
      } else {
        return this.storage.get(
          Constants.KEY_WHEEL_SPEED_NCD_MET_RECOMMENDED +
            Constants.DATABASE_VERSION
        );
      }
    }
  }
  getSpeedRationRecommended(isImperial): Promise<any> {
    if (isImperial) {
      return this.storage.get(
        Constants.KEY_DRESSING_SPEED_RATIO_RECOMMENDED +
          Constants.DATABASE_VERSION
      );
    } else {
      return this.storage.get(
        Constants.KEY_DRESSING_SPEED_RATIO_METRIC_RECOMMENDED +
          Constants.DATABASE_VERSION
      );
    }
  }

  //Stationary Tool Calculator

  getProductTypeDropDownsST(isImperial): Promise<any> {
    // console.log(isImperial);
    if (isImperial) {
      return this.storage.get(
        Constants.KEY_PRODUCT_TYPE_STATIONARY_IMPERIAL +
          Constants.DATABASE_VERSION
      );
    } else {
      return this.storage.get(
        Constants.KEY_PRODUCT_TYPE_STATIONARY_METRIC +
          Constants.DATABASE_VERSION
      );
    }
  }

  getRecommendedLeadST(isImperial, isSinglePoint): Promise<any> {
    if (isImperial) {
      if (isSinglePoint) {
        return this.storage.get(
          Constants.KEY_RECOMMENDED_LEAD_SINGLE_IMPERIAL +
            Constants.DATABASE_VERSION
        );
      } else {
        return this.storage.get(
          Constants.KEY_RECOMMENDED_LEAD_MULTIPLE_IMPERIAL +
            Constants.DATABASE_VERSION
        );
      }
    } else {
      if (isSinglePoint) {
        return this.storage.get(
          Constants.KEY_RECOMMENDED_LEAD_SINGLE_METRIC +
            Constants.DATABASE_VERSION
        );
      } else {
        return this.storage.get(
          Constants.KEY_RECOMMENDED_LEAD_MULTIPLE_METRIC +
            Constants.DATABASE_VERSION
        );
      }
    }
  }
  getLengthConverterData(): Promise<any> {
    return this.storage.get(
      Constants.LENGTH_CONVERTER_DATA + Constants.DATABASE_VERSION
    );
  }
  getVelocityConverterData(): Promise<any> {
    return this.storage.get(
      Constants.VELOCITY_CONVERTER_DATA + Constants.DATABASE_VERSION
    );
  }

  getInputTypeDropDowns(): Promise<any> {
    return this.storage.get(
      Constants.KEY_INPUT_TYPE + Constants.DATABASE_VERSION
    );
  }

  getWheelThicknessDropDowns(): Promise<any> {
    return this.storage.get(
      Constants.KEY_WHEEL_THICKNESS + Constants.DATABASE_VERSION
    );
  }

  getSurfaceAreaDropDowns(): Promise<any> {
    return this.storage.get(
      Constants.KEY_SURFACE_AREA + Constants.DATABASE_VERSION
    );
  }
}
