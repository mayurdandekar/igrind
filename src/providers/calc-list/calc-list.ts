import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {TranslateService} from '@ngx-translate/core';
import * as Constants from '../../utils/constants';
import 'rxjs/add/operator/toPromise';
import {WheelSpeedConverterPage} from '../../pages/wheel-speed-converter/wheel-speed-converter';
import {DiamondRollDressingCalculatorPage} from '../../pages/diamond-roll-dressing-calculator/diamond-roll-dressing-calculator';
import {CoolantCalculatorPage} from '../../pages/coolant-calculator/coolant-calculator';
import {PlungeDressingCalculatorPage} from '../../pages/plunge-dressing-calculator/plunge-dressing-calculator';
import {StationaryDressingCalculatorPage} from '../../pages/stationary-dressing-calculator/stationary-dressing-calculator';
import {CalculatorSubListPage} from '../../pages/calculator-sub-list/calculator-sub-list';
import {LengthConverterNewDesignPage} from '../../pages/length-converter-new-design/length-converter-new-design';
import {VelocityConverterNewDesignPage} from '../../pages/velocity-converter-new-design/velocity-converter-new-design';
import {ChipThicknessCalculatorPage} from '../../pages/chip-thickness-calculator/chip-thickness-calculator';
import {QPrimeCalculatorPage} from '../../pages/q-prime-calculator/q-prime-calculator';
import {AreaConversionPage} from '../../pages/area-conversion/area-conversion';
import {KerfLossCalculatorPage} from '../../pages/kerf-loss-calculator/kerf-loss-calculator';
/*
  Generated class for the CalcListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CalcListProvider {
	landingPageItems: Array<{
		name: any;
		desc: any;
		logo: any;
		calcKey: any;
		blackLogo: any;
		whiteLogo: any;
		isWhiteLogo: boolean;
		isinvert: boolean;
	}>;
	conversionCalcItems: Array<{
		name: any;
		desc: any;
		logo: any;
		calcKey: any;
		blackLogo: any;
		whiteLogo: any;
		component: any;
		isWhiteLogo: boolean;
		isinvert: boolean;
	}>;
	grindParamCalcItems: Array<{
		name: any;
		desc: any;
		logo: any;
		calcKey: any;
		blackLogo: any;
		whiteLogo: any;
		component: any;
		isWhiteLogo: boolean;
		isinvert: boolean;
	}>;
	dressParamCalcItems: Array<{
		name: any;
		desc: any;
		logo: any;
		calcKey: any;
		blackLogo: any;
		whiteLogo: any;
		component: any;
		isWhiteLogo: boolean;
		isinvert: boolean;
	}>;
	cutParamCalcItems: Array<{
		name: any;
		desc: any;
		logo: any;
		calcKey: any;
		blackLogo: any;
		whiteLogo: any;
		component: any;
		isWhiteLogo: boolean;
		isinvert: boolean;
	}>;

	activePage: any;

	constructor(private translate: TranslateService, private storage: Storage) {
		this.landingPageItems = new Array();
		this.conversionCalcItems = new Array();
		this.grindParamCalcItems = new Array();
		this.dressParamCalcItems = new Array();
		this.cutParamCalcItems = new Array();
	}
	getCalculators(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage
				.get(Constants.KEY_CALC_LIST + Constants.DATABASE_VERSION)
				.then(val => {
					let i = 0;
					this.landingPageItems.length = 0;
					for (let calc of val) {
						var promises = [];
						promises.push(
							this.translate
								.get(calc.calcKey + '_title')
								.toPromise()
						);
						promises.push(
							this.translate
								.get(calc.calcKey + '_desc')
								.toPromise()
						);
						Promise.all(promises).then(values => {
							i++;
							this.landingPageItems.push({
								name: values[0],
								desc: values[1],
								logo: calc.calcLogo,
								calcKey: calc.calcKey,
								blackLogo: calc.blackLogo,
								whiteLogo: calc.whiteLogo,
								isWhiteLogo: calc.isWhiteLogo,
								// component: CalculatorSubListPage,
								isinvert: calc.isInvert,
							});

							if (i == val.length) {
								resolve(this.landingPageItems);
							}
						});
					}
				});
		});
	}

	getConversionCalculators(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage
				.get(
					Constants.KEY_CONVERSION +
						Constants.KEY_CALC_LIST +
						Constants.DATABASE_VERSION
				)
				.then(val => {
					console.log('valvalval 111', val);
					if (val) {
						let i = 0;
						this.conversionCalcItems.length = 0;
						for (let calc of val) {
							var promises = [];
							promises.push(
								this.translate
									.get(calc.calcKey + '_title')
									.toPromise()
							);
							promises.push(
								this.translate
									.get(calc.calcKey + '_desc')
									.toPromise()
							);
							Promise.all(promises).then(values => {
								i++;
								console.log('calc key', calc.calcKey);

								if (calc.calcKey == Constants.KEY_LENGTH) {
									this.conversionCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component: LengthConverterNewDesignPage,
										isinvert: calc.isInvert,
									});
								} else if (
									calc.calcKey == Constants.KEY_VELOCITY
								) {
									this.conversionCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component:
											VelocityConverterNewDesignPage,
										isinvert: calc.isInvert,
									});
								} else if (calc.calcKey == Constants.KEY_AREA) {
									this.conversionCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component: AreaConversionPage,
										isinvert: calc.isInvert,
									});
								} else if (
									calc.calcKey == Constants.KEY_WHEEL
								) {
									this.conversionCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component: WheelSpeedConverterPage,
										isinvert: calc.isInvert,
									});
								}

								if (i == val.length) {
									resolve(this.conversionCalcItems);
								}
							});
						}
					} //if val
				});
		});
	}

	getGrindParamCalculators(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage
				.get(
					Constants.KEY_GRIND_PARAM +
						Constants.KEY_CALC_LIST +
						Constants.DATABASE_VERSION
				)
				.then(val => {
					if (val) {
						let i = 0;
						this.grindParamCalcItems.length = 0;
						for (let calc of val) {
							var promises = [];
							promises.push(
								this.translate
									.get(calc.calcKey + '_title')
									.toPromise()
							);
							promises.push(
								this.translate
									.get(calc.calcKey + '_desc')
									.toPromise()
							);
							Promise.all(promises).then(values => {
								i++;
								if (calc.calcKey == Constants.KEY_COOLANT) {
									this.grindParamCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component: CoolantCalculatorPage,
										isinvert: calc.isInvert,
									});
								} else if (
									calc.calcKey == Constants.KEY_CHIPTHICKNESS
								) {
									this.grindParamCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component: ChipThicknessCalculatorPage,
										isinvert: calc.isInvert,
									});
								} else if (
									calc.calcKey == Constants.KEY_QPRIME
								) {
									this.grindParamCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component: QPrimeCalculatorPage,
										isinvert: calc.isInvert,
									});
								}

								if (i == val.length) {
									resolve(this.grindParamCalcItems);
								}
							});
						}
					} //if val
				});
		});
	}

	getDressParamCalculators(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage
				.get(
					Constants.KEY_DRESS_PARAM +
						Constants.KEY_CALC_LIST +
						Constants.DATABASE_VERSION
				)
				.then(val => {
					if (val) {
						let i = 0;
						this.dressParamCalcItems.length = 0;
						for (let calc of val) {
							var promises = [];
							promises.push(
								this.translate
									.get(calc.calcKey + '_title')
									.toPromise()
							);
							promises.push(
								this.translate
									.get(calc.calcKey + '_desc')
									.toPromise()
							);
							Promise.all(promises).then(values => {
								// console.log(values);

								i++;
								if (calc.calcKey == Constants.KEY_DIAMOND) {
									this.dressParamCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component:
											DiamondRollDressingCalculatorPage,
										isinvert: calc.isInvert,
									});
								} else if (
									calc.calcKey == Constants.KEY_STATIONARY
								) {
									this.dressParamCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component:
											StationaryDressingCalculatorPage,
										isinvert: calc.isInvert,
									});
								} else {
									this.dressParamCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component: PlungeDressingCalculatorPage,
										isinvert: calc.isInvert,
									});
								}

								if (i == val.length) {
									resolve(this.dressParamCalcItems);
								}
							});

							// console.log(promises, "val of dress");
						}
					} //if val
				});
		});
	}

	getCutParamCalculators(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.storage
				.get(
					Constants.KEY_CUT_PARAM +
						Constants.KEY_CALC_LIST +
						Constants.DATABASE_VERSION
				)
				.then(val => {
					console.log(val);

					if (val) {
						let i = 0;
						this.cutParamCalcItems.length = 0;
						for (let calc of val) {
							var promises = [];
							promises.push(
								this.translate
									.get(calc.calcKey + '_title')
									.toPromise()
							);
							promises.push(
								this.translate
									.get(calc.calcKey + '_desc')
									.toPromise()
							);
							Promise.all(promises).then(values => {
								// console.log(values);

								i++;
								if (calc.calcKey == Constants.KEY_KERF_LOSS) {
									this.cutParamCalcItems.push({
										name: values[0],
										desc: values[1],
										logo: calc.calcLogo,
										calcKey: calc.calcKey,
										blackLogo: calc.blackLogo,
										whiteLogo: calc.whiteLogo,
										isWhiteLogo: calc.isWhiteLogo,
										component: KerfLossCalculatorPage,
										isinvert: calc.isInvert,
									});
								}

								if (i == val.length) {
									resolve(this.cutParamCalcItems);
								}
							});

							// console.log(promises, "val of dress");
						}
					} //if val
				});
		});
	}

	setActiveCalculator(calcObj: any) {
		this.activePage = calcObj;
	}

	getActiveCalculator() {
		return this.activePage;
	}

	checkActiveCalculator(calcObj: any) {
		if (this.activePage) {
			return calcObj.calcKey == this.activePage.calcKey;
		} else {
			return false;
		}
	}
}
