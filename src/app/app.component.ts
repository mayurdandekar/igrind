import {Component, ViewChild} from '@angular/core';
import {Nav, Platform, App, Events} from 'ionic-angular';
import {Keyboard} from '@ionic-native/keyboard';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import * as Constants from '../utils/constants';
import {SeedClass} from '../utils/SeedClass';
import {Storage} from '@ionic/storage';
import {TranslateService} from '@ngx-translate/core';

import uuid from 'uuid/v1';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics';
import {CalcListProvider} from '../providers/calc-list/calc-list';
import {MasterDataProvider} from '../providers/master-data/master-data';
import {HomePage} from '../pages/home/home';
import {Globalization} from '@ionic-native/globalization';

import {SettingsPage} from '../pages/settings/settings';
import {CalcAllPage} from '../pages/calc-all/calc-all';

declare var $: any;

@Component({
	templateUrl: 'app.html',
})
export class NortonMobileApp {
	@ViewChild(Nav) nav: Nav;

	rootPage: any;
	activePage: any;
	pages: Array<any>;
	// Selected Side Menu
	// selectedMenu: any;
	currentMenu: string;

	constructor(
		public platform: Platform,
		public statusBar: StatusBar,
		public splashScreen: SplashScreen,
		private translate: TranslateService,
		private storage: Storage,
		public calcModel: CalcListProvider,
		public masterData: MasterDataProvider,
		private firebaseAnalytics: FirebaseAnalytics,
		private keyboard: Keyboard,
		public app: App,
		private events: Events,
		private globalization: Globalization
	) {
		// this.platform.ready().then(() => {
		this.initializeApp();
		// })
	}
	hideFormAccessoryBar;
	initializeApp() {
		this.platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.

			// remove after italian
			// this.translate.setDefaultLang("en");
			// this.translate.use("en");
			//

			//
			// KEEP FOR ITALIAN START
			//

			var appLanguage = localStorage.getItem('globalLanguage');
			if (!appLanguage) {
				// this language will be used as a fallback when a translation isn't found in the current language
				this.translate.setDefaultLang('en');

				// the lang to use, if the lang isn't available, it will use the current loader to get them
				this.translate.use('en');

				try {
					var globalLanguage = navigator.language.split('-')[0];
					this.translate.use(globalLanguage);
					localStorage.setItem('globalLanguage', globalLanguage);
				} catch (e) {
					var globalLanguage = 'en';
					this.translate.use(globalLanguage);
					localStorage.setItem('globalLanguage', globalLanguage);
				}

				// this.globalization
				//   .getPreferredLanguage()
				//   .then((res) => {
				//     // alert(JSON.stringify(res));

				//     console.log(res.value.split("-"));

				//     var globalLanguage = res.value.split("-")[0];
				//     console.log(globalLanguage);

				//     this.translate.use(globalLanguage);

				//     localStorage.setItem("globalLanguage", globalLanguage);
				//   })
				//   .catch((e) => {
				//     // alert("e");
				//     // alert(JSON.stringify(e));

				//     console.log(e);
				//     var globalLanguage = "en";
				//     this.translate.use(globalLanguage);
				//     localStorage.setItem("globalLanguage", globalLanguage);
				//   });
			} else {
				this.translate.setDefaultLang(appLanguage);
				this.translate.use(appLanguage);
			}

			//
			// KEEP FOR ITALIAN END
			//

			setTimeout(() => {
				this.statusBar.styleDefault();
				this.statusBar.styleLightContent();
				this.keyboard.hideFormAccessoryBar(false);
				this.keyboard.setResizeMode('ionic');
				const seeder = new SeedClass(this.storage, this.translate);

				seeder.checkSeeding().then(status => {
					if (status) {
						this.launchSetup();
					} else {
						seeder
							.seed()
							.then(res => {
								if (res) {
									this.launchSetup();
								}
							})
							.catch(err => {
								console.log(err);
							});
					}

					if (this.platform.is('android')) {
						// margin - top: -27px;
					}
				});
			}, 1500);

			this.events.subscribe('setup:launch', () => {
				this.launchSetup();
			});
		});
	}

	openPage(page, index) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		// console.log(page);

		if (page.component) {
			if (page.name == 'calc') {
				this.nav.push(page.component);
			} else {
				this.nav.setRoot(page.component);
				this.calcModel.setActiveCalculator(page);
			}
		} else {
			let ispagemenuclick = page.ismenuclick;

			this.pages.forEach(page => {
				page.ismenuclick = false;
			});

			page.ismenuclick = !ispagemenuclick;
		}
	}

	menuOpened() {
		this.events.publish('menu:open');
	}
	menuClosed() {
		console.log(123);

		this.events.publish('menu:close');
	}
	checkPageCalcKey(page) {
		let calcFun;

		if (page.calcKey == Constants.KEY_CONVERSION) {
			calcFun = this.calcModel.getConversionCalculators();
		} else if (page.calcKey == Constants.KEY_GRIND_PARAM) {
			calcFun = this.calcModel.getGrindParamCalculators();
		} else if (page.calcKey == Constants.KEY_DRESS_PARAM) {
			calcFun = this.calcModel.getDressParamCalculators();
		} else if (page.calcKey == Constants.KEY_CUT_PARAM) {
			calcFun = this.calcModel.getCutParamCalculators();
		}

		return calcFun;
	}
	goToHomePage() {
		this.nav.setRoot(HomePage);
	}
	checkActive(page) {
		return this.calcModel.checkActiveCalculator(page);
	}

	getLogo(page) {
		// if (this.checkActive(page)) {
		//   return page.whiteLogo;
		// } else {
		return page.blackLogo ? page.blackLogo : '../assets/imgs/home.png';
		// }
	}

	launchSetup() {
		let homePageItem = {
			name: 'home',
			desc: '',
			logo: '',
			calcKey: '',
			blackLogo: '',
			whiteLogo: '',
			component: HomePage,
		};
		let settingsPageItem = {
			name: 'settings',
			desc: '',
			logo: '',
			calcKey: '',
			blackLogo: '../assets/imgs/settings-22-64.png',
			whiteLogo: '',
			component: SettingsPage,
		};
		let calcPageItem = {
			name: 'calc',
			desc: '',
			logo: '',
			calcKey: '',
			blackLogo: '../assets/imgs/Button_Calculator.jpg',
			whiteLogo: '',
			component: CalcAllPage,
		};
		this.calcModel.getCalculators().then(calcList => {
			let pagesArrayObject = calcList;
			/*converting object into array*/
			console.log(pagesArrayObject);
			this.pages = Object.keys(pagesArrayObject).map(
				i => pagesArrayObject[i]
			);
			console.log(this.pages);

			this.pages.forEach(page => {
				this.checkPageCalcKey(page).then(calcList => {
					page.subPages = calcList;
				});
			});

			this.pages.unshift(homePageItem);

			this.pages.push(calcPageItem);

			//
			// keep for italian
			this.pages.push(settingsPageItem);
			//

			console.log(this.pages);
		});

		this.storage.get(Constants.KEY_TERMS).then(termsStatus => {
			this.splashScreen.hide();
			this.storage.get(Constants.APP_UUID).then(uuid_val => {
				if (uuid_val === null || uuid_val === undefined) {
					let uuidData = uuid();
					this.storage.set(Constants.APP_UUID, uuidData);
					this.firebaseAnalytics.setUserId(uuidData);
				} else {
					this.firebaseAnalytics.setUserId(uuid_val);
				}
			});
			this.firebaseAnalytics.setUserProperty(
				'app_selected_language',
				localStorage.getItem('globalLanguage')
			);
			if (termsStatus !== null) {
				this.rootPage = HomePage;
			} else {
				this.rootPage = 'TermsPage';
			}
		});
	}

	showTerms() {
		this.nav.push('TermsPage');
	}
}
