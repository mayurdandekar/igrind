import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { Keyboard } from "@ionic-native/keyboard";
import { NortonMobileApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { WheelSpeedConverterPage } from "../pages/wheel-speed-converter/wheel-speed-converter";
import { CoolantCalculatorPage } from "../pages/coolant-calculator/coolant-calculator";
import { DiamondRollDressingCalculatorPage } from "../pages/diamond-roll-dressing-calculator/diamond-roll-dressing-calculator";
import { ChipThicknessCalculatorPage } from "../pages/chip-thickness-calculator/chip-thickness-calculator";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import { CustomSideMenuComponent } from "../components/custom-side-menu/custom-side-menu";

import { IonicStorageModule } from "@ionic/storage";

import { HttpClientModule, HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { CalcListProvider } from "../providers/calc-list/calc-list";
import { MasterDataProvider } from "../providers/master-data/master-data";
import { HttpModule } from "@angular/http";
import { PlungeDressingCalculatorPage } from "../pages/plunge-dressing-calculator/plunge-dressing-calculator";
import { StationaryDressingCalculatorPage } from "../pages/stationary-dressing-calculator/stationary-dressing-calculator";
import { CommonHelperProvider } from "../providers/common-helper/common-helper";
import { CalculatorSubListPage } from "../pages/calculator-sub-list/calculator-sub-list";
import { CalculatorListComponent } from "../components/calculator-list/calculator-list";
import { CommonConverterComponent } from "../components/common-converter/common-converter";
import { VelocityConverterNewDesignPage } from "../pages/velocity-converter-new-design/velocity-converter-new-design";
import { LengthConverterNewDesignPage } from "../pages/length-converter-new-design/length-converter-new-design";
import { QPrimeCalculatorPage } from "../pages/q-prime-calculator/q-prime-calculator";
// import { IonicImageViewerModule } from 'ionic-img-viewer';
import { AreaConversionPage } from "../pages/area-conversion/area-conversion";
import { Globalization } from "@ionic-native/globalization";
import { SettingsPage } from "../pages/settings/settings";
import { KerfLossCalculatorPage } from "../pages/kerf-loss-calculator/kerf-loss-calculator";
import { CalcComponent } from "../components/calc/calc";
import { ArithCalcComponent } from "../components/arith-calc/arith-calc";
import { CalcAllPage } from "../pages/calc-all/calc-all";
import { CalculatorButtonComponent } from "../components/calculator-button/calculator-button";

@NgModule({
  declarations: [
    NortonMobileApp,
    HomePage,
    CalculatorSubListPage,
    WheelSpeedConverterPage,
    ChipThicknessCalculatorPage,
    CoolantCalculatorPage,
    DiamondRollDressingCalculatorPage,
    CustomSideMenuComponent,
    CalculatorListComponent,
    CommonConverterComponent,
    PlungeDressingCalculatorPage,
    StationaryDressingCalculatorPage,
    LengthConverterNewDesignPage,
    VelocityConverterNewDesignPage,
    QPrimeCalculatorPage,
    AreaConversionPage,
    SettingsPage,
    KerfLossCalculatorPage,
    CalcComponent,
    ArithCalcComponent,
    CalcAllPage,
    CalculatorButtonComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,

    // IonicImageViewerModule,
    IonicModule.forRoot(NortonMobileApp, {
      platform: {
        ios: {
          scrollPadding: false,
          scrollAssist: false,
          autoFocusAssist: false,
        },
      },
    }),
    IonicStorageModule.forRoot({
      name: "__mydb",
      driverOrder: ["sqlite", "websql"],
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    NortonMobileApp,
    HomePage,
    CalculatorSubListPage,
    WheelSpeedConverterPage,
    ChipThicknessCalculatorPage,
    CoolantCalculatorPage,
    DiamondRollDressingCalculatorPage,
    CustomSideMenuComponent,
    CalculatorListComponent,
    CommonConverterComponent,
    PlungeDressingCalculatorPage,
    StationaryDressingCalculatorPage,
    LengthConverterNewDesignPage,
    VelocityConverterNewDesignPage,
    QPrimeCalculatorPage,
    AreaConversionPage,
    SettingsPage,
    KerfLossCalculatorPage,
    CalcComponent,
    ArithCalcComponent,
    CalcAllPage,
    CalculatorButtonComponent,
  ],
  providers: [
    Keyboard,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    FirebaseAnalytics,
    CalcListProvider,
    MasterDataProvider,
    CommonHelperProvider,
    Globalization,
  ],
})
export class AppModule {}
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}
