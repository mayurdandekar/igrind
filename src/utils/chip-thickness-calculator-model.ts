import * as Constants from './constants';
import { FormGroup } from '@angular/forms';
export class ChipThicknessModel {
  resultObj: { [s: string]: string };
  roundHec = 0;
  roundVc = 0;
  roundVw = 0;
  roundAe = 0;
  constructor(private storage: any) {
  }

  chipThicknessValidator(fg: FormGroup) {

    // let chipThickType = fg.get('chipThickType').value;
    // let inp_2 = fg.get('inp_2').value;
    // let isEnglish = !(fg.get('unitPref').value);
    let inp_1 = fg.get('inp_1').value;
    let inp_2 = fg.get('inp_2').value;
    let inp_3 = fg.get('inp_3').value;

    if (inp_1 != "" && inp_1 != undefined && (inp_1.toString().length > 9)) {
      fg.get('inp_1').setErrors({ 'inp1_eng': true });
    }
    if (inp_2 != "" && inp_2 != undefined && (inp_2.toString().length > 9)) {
      fg.get('inp_2').setErrors({ 'inp2_eng': true });
    }
    if (inp_3 != "" && inp_3 != undefined && (inp_3.toString().length > 9)) {
      fg.get('inp_3').setErrors({ 'inp2_eng': true });
    }
    if ((inp_1 == "" || inp_1 == undefined) && (inp_2 == "" || inp_2 == undefined) && (inp_3 == "" || inp_3 == undefined)) {
      fg.get('inp_1').setErrors({ 'inp_1_empty': true });
      fg.get('inp_2').setErrors({ 'inp_2_empty': true });
      fg.get('inp_3').setErrors({ 'inp_3_empty': true });
    }
    else if (inp_1 == "" || inp_1 == undefined || inp_1 == null) {
      fg.get('inp_1').setErrors({ 'inp_1_empty': true });
    }
    else if (inp_2 == "" || inp_2 == undefined || inp_2 == null) {
      fg.get('inp_2').setErrors({ 'inp_2_empty': true });
    }
    else if (inp_3 == "" || inp_3 == undefined || inp_3 == null) {
      fg.get('inp_3').setErrors({ 'inp_3_empty': true });
    }
    return null;
  }


  calculateChipThickness(isEnglish: boolean, chipThicknessType: string, chipOutputType: string, inp_1_num: number, inp_2_num: number, inp_3_num: number) {

    var chipThicknessResult: {} = {};
    chipThicknessResult['unitPref'] = isEnglish;
    chipThicknessResult['chipThicknessType'] = chipThicknessType;
    chipThicknessResult['chipOutputType'] = chipOutputType;
    chipThicknessResult['inp_1'] = inp_1_num;
    chipThicknessResult['inp_2'] = inp_2_num;
    chipThicknessResult['inp_3'] = inp_3_num;
    var calculatedValue;
    switch (chipOutputType) {

      case "hec":
        calculatedValue = this.calculateHEC(isEnglish, chipThicknessType, inp_1_num, inp_2_num, inp_3_num);
        chipThicknessResult['output'] = calculatedValue;
        break;
      case "vw":
        calculatedValue = this.calculateVW(isEnglish, inp_1_num, inp_2_num, inp_3_num);
        chipThicknessResult['output'] = calculatedValue;
        break;
      case "ae":
        calculatedValue = this.calculateAE(isEnglish, inp_1_num, inp_2_num, inp_3_num);
        chipThicknessResult['output'] = calculatedValue;
        break;
      case "vfr":
        calculatedValue = this.calculateVFR(isEnglish, inp_1_num, inp_2_num, inp_3_num);
        chipThicknessResult['output'] = calculatedValue;
        break;
      case "dw":
        calculatedValue = this.calculateDW(isEnglish, inp_1_num, inp_2_num, inp_3_num);
        chipThicknessResult['output'] = calculatedValue;
        break;
      case "vc":
        calculatedValue = this.calculateVC(isEnglish, chipThicknessType, inp_1_num, inp_2_num, inp_3_num);
        chipThicknessResult['output'] = calculatedValue;
        break;

      default:
        break;
    }
    this.storage.set(Constants.KEY_CHIPTHICKNESS, chipThicknessResult);
    return calculatedValue;
  }

  calculateHEC(isEnglish, chipThickType, inp_1_num, inp_2_num, inp_3_num) {
    var hecOutput;
    var n;
    if (chipThickType == 'peripheral') {
      if (isEnglish) {
        n = 1;
        hecOutput = ((inp_2_num * inp_3_num) / (inp_1_num * 12)) * 1000000;
        hecOutput = this.round(hecOutput, n);
        return hecOutput;
      }
      else {
        n = 3;
        hecOutput = ((inp_2_num * inp_3_num) / (inp_1_num * 60));
        hecOutput = this.round(hecOutput, n);
        return hecOutput;
      }
    }
    else if (chipThickType == 'circumferal') {
      if (isEnglish) {
        n = 2;
        hecOutput = (inp_3_num * 3.141592654 * inp_2_num) / (inp_1_num * 12) * 1000000;
        hecOutput = this.round(hecOutput, n);
        return hecOutput;
      }
      else {
        n = 3;
        hecOutput = (inp_3_num * 3.141592654 * inp_2_num) / (inp_1_num * 60);
        hecOutput = this.round(hecOutput, n);
        return hecOutput;
      }
    }
    else { }
  }

  calculateVW(isEnglish, inp_1_num, inp_2_num, inp_3_num) {
    var vwOutput;
    var n;
    if (isEnglish) {
      n = 1;
      vwOutput = (inp_1_num * 12 * inp_2_num / inp_3_num) / 1000000;
      vwOutput = this.round(vwOutput, n);
      return vwOutput;
    }
    else {
      n = 1;
      vwOutput = (inp_1_num * inp_2_num * 60 / inp_3_num);
      vwOutput = this.round(vwOutput, n);
      return vwOutput;
    }
  }

  calculateAE(isEnglish, inp_1_num, inp_2_num, inp_3_num) {
    var aeOutput;
    var n;
    if (isEnglish) {
      n = 3;
      aeOutput = (inp_1_num * (inp_2_num * 12) / inp_3_num) / 1000000;
      aeOutput = this.round(aeOutput, n);
      return aeOutput;
    }
    else {
      n = 1;
      aeOutput = (inp_1_num * inp_2_num * 60 / inp_3_num);
      aeOutput = this.round(aeOutput, n);
      return aeOutput;
    }
  }

  calculateVFR(isEnglish, inp_1_num, inp_2_num, inp_3_num) {
    var vfrOutput;
    var n;
    if (isEnglish) {
      n = 1;
      vfrOutput = (inp_1_num * 12 * inp_2_num / (inp_3_num * 3.141592654)) / 1000000;
      vfrOutput = this.round(vfrOutput, n);
      return vfrOutput;
    }
    else {
      n = 1;
      vfrOutput = (inp_1_num * inp_2_num * 60 / (inp_3_num * 3.141592654));
      vfrOutput = this.round(vfrOutput, n);
      return vfrOutput;
    }
  }

  calculateDW(isEnglish, inp_1_num, inp_2_num, inp_3_num) {
    var dwOutput;
    var n;
    if (isEnglish) {
      n = 3;
      dwOutput = (inp_1_num * inp_2_num * 12 / (inp_3_num * 3.141592654)) / 1000000;
      dwOutput = this.round(dwOutput, n);
      return dwOutput;
    }
    else {
      n = 1;
      dwOutput = (inp_1_num * inp_2_num * 60 / (inp_3_num * 3.141592654));
      dwOutput = this.round(dwOutput, n);
      return dwOutput;
    }
  }


  calculateVC(isEnglish, chipThickType, inp_1_num, inp_2_num, inp_3_num) {
    var vcOutput;
    var n;
    if (chipThickType == 'peripheral') {
      if (isEnglish) {
        n = 1;
        vcOutput = ((inp_2_num * inp_3_num) / (inp_1_num * 12)) * 1000000;
        // vcOutput = this.round(vcOutput, n);
        vcOutput = Math.round(vcOutput);
        return vcOutput;
      }
      else {
        n = 1;
        vcOutput = ((inp_2_num * inp_3_num) / (inp_1_num * 60));
        vcOutput = this.round(vcOutput, n);
        return vcOutput;
      }
    }
    else if (chipThickType == 'circumferal') {
      if (isEnglish) {
        n = 0;
        vcOutput = (inp_3_num * 3.141592654 * inp_2_num) / (inp_1_num * 12) * 1000000;
        vcOutput = this.round(vcOutput, n);
        return vcOutput;
      }
      else {
        n = 1;
        vcOutput = (inp_3_num * 3.141592654 * inp_2_num) / (inp_1_num * 60);
        vcOutput = this.round(vcOutput, n);
        return vcOutput;
      }
    }
    else { }
  }





  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  }




  calculateThicknessIfPeripheral(isEnglish, chipOutputType, inp_1_num, inp_2_num, inp_3_num) {
    let calculatedHEC;
    let calculatedVC;
    let calculatedVW;
    let calculatedAE;
    let chipThicknessResult: {} = {};
    chipThicknessResult['unitPref'] = !isEnglish;
    chipThicknessResult['chipThicknessType'] = Constants.KEY_WH_PERIPHERAL;
    chipThicknessResult['inp_1_num'] = inp_1_num;
    chipThicknessResult['inp_2_num'] = inp_2_num;
    chipThicknessResult['inp_3_num'] = inp_3_num;
    if (isEnglish) {
      this.roundHec = 1;
      this.roundVc = 0;
      this.roundVw = 1;
      this.roundAe = 3;
    }
    else {
      this.roundHec = 3;
      this.roundVc = 1;
      this.roundVw = 1;
      this.roundAe = 1;
    }
    switch (chipOutputType) {
      case Constants.KEY_HEC:
        // type :HEC
        calculatedHEC = this.calculateHECIfPeripheral(isEnglish, inp_1_num, inp_2_num, inp_3_num);
        calculatedVC = inp_1_num;
        calculatedVW = inp_2_num;
        calculatedAE = inp_3_num;
        break;
      case Constants.KEY_VC:
        calculatedHEC = inp_1_num;
        calculatedVC = this.calculateVCIfPeripheral(isEnglish, inp_1_num, inp_2_num, inp_3_num);
        calculatedVW = inp_2_num;
        calculatedAE = inp_3_num;
        break;
      case Constants.KEY_VW:
        calculatedHEC = inp_1_num;
        calculatedVC = inp_2_num;
        calculatedVW = this.calculateVWIfPeripheral(isEnglish, inp_1_num, inp_2_num, inp_3_num);
        calculatedAE = inp_3_num;
        break;
      case Constants.KEY_AE:
        calculatedHEC = inp_1_num;
        calculatedVC = inp_2_num;
        calculatedVW = inp_3_num;
        calculatedAE = this.calculateAEIfPeripheral(isEnglish, inp_1_num, inp_2_num, inp_3_num);
        break;

      default:
        break;
    }


    // chipThicknessResult[Constants.KEY_HEC] = this.round(calculatedHEC, this.roundHec);
    // chipThicknessResult[Constants.KEY_VC] = this.round(calculatedVC, this.roundVc);
    // chipThicknessResult[Constants.KEY_VW] = this.round(calculatedVW, this.roundVw);
    // chipThicknessResult[Constants.KEY_AE] = this.round(calculatedAE, this.roundAe);

    // this.storage.set(Constants.KEY_CHIPTHICKNESS, chipThicknessResult);
    // this.storage.set(Constants.KEY_UNIT_PREF_CHIP_THICK, !isEnglish);
    // this.storage.set(Constants.KEY_TYPE_PREF_CHIP_THICK, Constants.KEY_WH_PERIPHERAL);

    return chipThicknessResult;
  }

  calculateThicknessIfCircumferal(isEnglish, chipOutputType, circumferalOrChipThick, feedRateOrCircumferalSpeed, diameterOrFeedRate) {
    let calculatedHEC;
    let calculatedVC;
    let calculatedVW;
    let calculatedAE;
    let chipThicknessResult: {} = {};
    chipThicknessResult['unitPref'] = !isEnglish;
    chipThicknessResult['chipThicknessType'] = Constants.KEY_WH_PERIPHERAL;
    chipThicknessResult['peripheral'] = circumferalOrChipThick;
    chipThicknessResult['inp_2_num'] = feedRateOrCircumferalSpeed;
    chipThicknessResult['inp_3_num'] = diameterOrFeedRate;
    if (isEnglish) {
      this.roundHec = 2;
      this.roundVc = 0;
      this.roundVw = 1;
      this.roundAe = 3;
    }
    else {
      this.roundHec = 3;
      this.roundVc = 0;
      this.roundVw = 1;
      this.roundAe = 1;
    }
    switch (chipOutputType) {
      case Constants.KEY_HEC:
        // type :HEC
        calculatedHEC = this.calculateHECIfCircumferal(isEnglish, circumferalOrChipThick, feedRateOrCircumferalSpeed, diameterOrFeedRate);
        calculatedVC = circumferalOrChipThick;
        calculatedVW = feedRateOrCircumferalSpeed;
        calculatedAE = diameterOrFeedRate;
        break;

      case Constants.KEY_VC:
        calculatedHEC = circumferalOrChipThick;
        calculatedVC = this.calculateVCIfCircumferal(isEnglish, circumferalOrChipThick, feedRateOrCircumferalSpeed, diameterOrFeedRate);
        calculatedVW = feedRateOrCircumferalSpeed;
        calculatedAE = diameterOrFeedRate;
        break;

      case Constants.KEY_VW:
        calculatedHEC = circumferalOrChipThick;
        calculatedVC = feedRateOrCircumferalSpeed;
        calculatedVW = this.calculateVfrIfCircumferal(isEnglish, circumferalOrChipThick, feedRateOrCircumferalSpeed, diameterOrFeedRate);
        calculatedAE = diameterOrFeedRate;
        break;

      case Constants.KEY_AE:
        calculatedHEC = circumferalOrChipThick;
        calculatedVC = feedRateOrCircumferalSpeed;
        calculatedVW = diameterOrFeedRate;
        calculatedAE = this.calculateDWIfCircumferal(isEnglish, circumferalOrChipThick, feedRateOrCircumferalSpeed, diameterOrFeedRate);
        break;

      default:
        break;
    }


    // chipThicknessResult[Constants.KEY_HEC] = this.round(calculatedHEC, this.roundHec);
    // chipThicknessResult[Constants.KEY_VC] = this.round(calculatedVC, this.roundVc);
    // chipThicknessResult[Constants.KEY_VW] = this.round(calculatedVW, this.roundVw);
    // chipThicknessResult[Constants.KEY_AE] = this.round(calculatedAE, this.roundAe);

    // this.storage.set(Constants.KEY_CHIPTHICKNESS, chipThicknessResult);
    // this.storage.set(Constants.KEY_UNIT_PREF_CHIP_THICK, !isEnglish);
    // this.storage.set(Constants.KEY_TYPE_PREF_CHIP_THICK, Constants.KEY_WH_PERIPHERAL);

    return chipThicknessResult;
  }


  /*************************************** HEC Calculation ***************************************/
  calculateHECIfPeripheral(isEnglish: boolean, peripheralSpeed: number, pieceSpeed: number, infeed: number) {

    /**
     *         1. HEC = If(isEnglish) {
				hec=(𝑄^′ 𝑤)/𝑉𝑐=  (𝑉𝑤[𝑖𝑛/𝑚𝑖𝑛]∗𝑎𝑒 [𝑖𝑛])/(𝑉𝑐 [𝑓𝑒𝑒𝑡/𝑚𝑖𝑛] )
				}else {
				(hec=(𝑄^′ 𝑤)/𝑉𝑐=  (𝑉𝑤[𝑚𝑚/𝑚𝑖𝑛]∗𝑎𝑒 [𝑚𝑚])/(𝑉𝑐 [𝑚/𝑠]∗60 [𝑠/𝑚𝑖𝑛]))
				}
     */
    var hecIfPeripheralSelected;
    if (isEnglish) {
      // English calculations go here
      hecIfPeripheralSelected = ((pieceSpeed * infeed) / (peripheralSpeed * 12)) * 1000000;

    } else {
      // Metric calculations go here
      hecIfPeripheralSelected = (pieceSpeed * infeed) / (peripheralSpeed * 60);
    }
    console.log(hecIfPeripheralSelected, "hec selected ka value");

    return hecIfPeripheralSelected;
  }

  calculateHECIfCircumferal(isEnglish: boolean, circumferalSpeeed: number, feedRate: number, diameter: number) {

    /**
     *         1. HEC = If(isEnglish) {
				hec=(𝑄^′ 𝑤)/𝑉𝑐=  (𝑑𝑤[𝑖𝑛]  ∗ 3.141592654 ∗ 𝑉𝑓𝑟[𝑖𝑛/𝑚𝑖𝑛]  )/(𝑉𝑐 [𝑓𝑒𝑒𝑡/𝑚𝑖𝑛] )
				}else {
				(hec=(𝑄^′ 𝑤)/𝑉𝑐=  (𝑑𝑤[𝑚𝑚]  ∗ 3.141592654 ∗ 𝑉𝑓𝑟[𝑚𝑚/𝑚𝑖𝑛]  )/(𝑉𝑐 [𝑚/𝑠]∗60 [𝑠/𝑚𝑖𝑛]))
				}
     */
    var hecIfCircumferalSelected;
    if (isEnglish) {
      // English calculations go here
      hecIfCircumferalSelected = 3.141592654 * (diameter * feedRate) / (circumferalSpeeed * 12) * 1000000;

    } else {
      // Metric calculations go here
      hecIfCircumferalSelected = 3.141592654 * (diameter * feedRate) / (circumferalSpeeed * 60);
    }
    return hecIfCircumferalSelected;
  }

  /*************************************** VC Calculation ***************************************/
  calculateVCIfPeripheral(isEnglish: boolean, chipThick: number, pieceSpeed: number, infeed: number) {
    /*	𝑉𝑐 [𝑓𝑒𝑒𝑡/𝑚𝑖𝑛] = (𝑉𝑤[𝑖𝑛/𝑚𝑖𝑛]∗𝑎𝑒 [𝑖𝑛])/(hec) */
    var vcIfPeripheralSelected;
    if (isEnglish) {
      // English calculations go here
      vcIfPeripheralSelected = ((pieceSpeed * infeed) / (chipThick * 12)) * 1000000;

    } else {
      // Metric calculations go here
      vcIfPeripheralSelected = ((pieceSpeed * infeed) / (chipThick * 60));
    }
    return vcIfPeripheralSelected;
  }
  calculateVCIfCircumferal(isEnglish: boolean, chipThick: number, feedRate: number, diameter: number) {
    /*	hec =  (𝑑𝑤[𝑖𝑛]  ∗ 𝜋 ∗ 𝑉𝑓𝑟[𝑖𝑛/𝑚𝑖𝑛]  )/(𝑉𝑐 [𝑓𝑒𝑒𝑡/𝑚𝑖𝑛] ) */
    var vcIfCircumferalSelected;
    if (isEnglish) {
      // English calculations go here
      vcIfCircumferalSelected = ((diameter * feedRate * 3.141592654) / (chipThick * 12)) * 1000000;

    } else {
      // Metric calculations go here
      vcIfCircumferalSelected = ((diameter * feedRate * 3.141592654) / (chipThick * 60));
    }
    return vcIfCircumferalSelected;
  }

  /*************************************** VW Calculation ***************************************/
  calculateVWIfPeripheral(isEnglish: boolean, chipThick: number, peripheralSpeed: number, infeed: number) {
    /*	𝑉𝑤[𝑖𝑛/𝑚𝑖𝑛] = (𝑉𝑐 [𝑓𝑒𝑒𝑡/𝑚𝑖𝑛] * (hec))/𝑎𝑒 [𝑖𝑛] */
    var vwIfPeripheralSelected;
    if (isEnglish) {
      // English calculations go here
      vwIfPeripheralSelected = ((chipThick * 12 * peripheralSpeed) / infeed) / 1000000;

    } else {
      // Metric calculations go here
      vwIfPeripheralSelected = ((chipThick * peripheralSpeed * 60) / infeed);
    }
    return vwIfPeripheralSelected;
  }
  calculateVfrIfCircumferal(isEnglish: boolean, chipThick: number, circumferalSpeed: number, diameter: number) {
    /*	𝑉𝑓𝑟[𝑖𝑛/𝑚𝑖𝑛] = (𝑉𝑐 [𝑓𝑒𝑒𝑡/𝑚𝑖𝑛] * (hec))/𝑑𝑤 [𝑖𝑛] */
    var vfrIfCircumferalSelected;
    if (isEnglish) {
      // English calculations go here
      vfrIfCircumferalSelected = ((chipThick * 12 * circumferalSpeed) / (diameter * 3.141592654)) / 1000000;

    } else {
      // Metric calculations go here
      vfrIfCircumferalSelected = ((chipThick * circumferalSpeed * 60) / (diameter * 3.141592654));
    }
    return vfrIfCircumferalSelected;
  }

  /*************************************** AE Calculation ***************************************/
  calculateAEIfPeripheral(isEnglish: boolean, chipThick: number, peripheralSpeed: number, pieceSpeed: number) {
    /*	𝑎𝑒 [𝑖𝑛]  = (𝑉𝑐 [𝑓𝑒𝑒𝑡/𝑚𝑖𝑛] * (hec))/ 𝑉𝑤[𝑖𝑛/𝑚𝑖𝑛] */
    var aeIfPeripheralSelected;
    if (isEnglish) {
      // English calculations go here
      aeIfPeripheralSelected = ((chipThick * peripheralSpeed * 12) / (pieceSpeed)) / 1000000;

    } else {
      // Metric calculations go here
      aeIfPeripheralSelected = ((chipThick * peripheralSpeed * 60) / (pieceSpeed));
    }
    return aeIfPeripheralSelected;
  }
  calculateDWIfCircumferal(isEnglish: boolean, chipThick: number, circumferalSpeed: number, feedRate: number) {
    /* 𝑑𝑤 [𝑖𝑛]  = (𝑉𝑐 [𝑓𝑒𝑒𝑡/𝑚𝑖𝑛] * (hec))/	𝑉𝑓𝑟[𝑖𝑛/𝑚𝑖𝑛]*/
    var vcIfCircumferalSelected;
    if (isEnglish) {
      // English calculations go here
      vcIfCircumferalSelected = ((chipThick * circumferalSpeed * 12) / (feedRate * 3.141592654)) / 1000000;

    } else {
      // Metric calculations go here
      vcIfCircumferalSelected = ((chipThick * circumferalSpeed * 60) / (feedRate));
    }
    return vcIfCircumferalSelected;
  }
}
