import { TranslateService } from '@ngx-translate/core';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import * as Constants from '../utils/constants';
import * as FirebaseConstants from '../utils/analytics-constants';
import { FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
export class plungeDressingCalculator {

    coolantType: string;
    resultCalculatedWheelRPM: any;
    totalInfeedRateRecommendedValues: any;
    wheelTechType: any;
    resultCalculatedWheelSFPM: any;
    resultCalculatedDressRollSFPM: number;
    wheelSFPM: any;
    wheelSFPMRecommended: any;
    speedRatioRecommended: any;
    wheelTechTypekey: any;

    constructor(private storage: any, private firebaseAnalytics: FirebaseAnalytics) {
    }

    calculate(isImperialNotMetric: boolean, productType: string, dressRollParameters: {}, wheelParameters: {}, totalInfeedRateRecommendedValues, wheelTechnTypeListRefs, isUDnotCD: boolean, isUinNotIndp: boolean, wheelSFPMRecommendedValues, speedRatioRecommendedValues, isunitPrefCD) {
        // dressRollDiameter: "12", dressRollRPM: "12", dressRollIFR: "12"
        // wpWheelSFPM: "12", wpDiameter: "12"
        var dressRollDiameter: number = dressRollParameters["dressRollDiameter"];
        var dressRollRPM: number = dressRollParameters["dressRollRPM"];
        var dressRollIFR: number = dressRollParameters["dressRollIFR"];
        var wpWheelSFPM: number = wheelParameters["wpWheelSFPM"];
        var wpDiameter: number = wheelParameters["wpDiameter"];
        console.log(productType);
        console.log(totalInfeedRateRecommendedValues)
        totalInfeedRateRecommendedValues.forEach(totalInfeedRatio => {
            if (totalInfeedRatio.key == productType) {
                this.totalInfeedRateRecommendedValues = totalInfeedRatio.value;
            }
        });
        console.log(wheelTechnTypeListRefs);
        wheelTechnTypeListRefs.forEach(wheelTechnType => {
            if (wheelTechnType.key == productType) {
                this.wheelTechType = wheelTechnType.value;
                this.wheelTechTypekey = wheelTechnType.key;
            }
        });

        wheelSFPMRecommendedValues.forEach(wheelSFPM => {
            if (wheelSFPM.key == productType) {
                this.wheelSFPMRecommended = wheelSFPM.value;
            }
        });
        console.log(speedRatioRecommendedValues);
        speedRatioRecommendedValues.forEach(speedRatio => {
            if (speedRatio.key == productType) {
                this.speedRatioRecommended = speedRatio.value;
            }
        });

        console.log(this.wheelTechType);
        var calculateAndFetchWheelSFPM: {} = this.calculateAndFetchWheelSFPM(isImperialNotMetric, wpWheelSFPM, wpDiameter, this.wheelSFPMRecommended);
        var calculateAndFetchDressRollSFPM: {} = this.calculateAndFetchDressRollSFPM(isImperialNotMetric, dressRollDiameter, dressRollRPM, isUDnotCD, this.speedRatioRecommended, this.wheelSFPMRecommended);
        var calculateAndFetchSpeedRatio: {} = this.calculateAndFetchSpeedRatio(this.speedRatioRecommended);
        var calculateAndFetchInfeedRateinmin: {} = this.calculateAndFetchInfeedRateinmin(isImperialNotMetric, isUinNotIndp, wpWheelSFPM, dressRollIFR, this.wheelTechType, isunitPrefCD);
        var calculateAndFetchInfeedRateuinrev: {} = this.calculateAndFetchInfeedRateuinrev(isImperialNotMetric, isUinNotIndp, wpWheelSFPM, dressRollIFR, this.wheelTechType, isunitPrefCD);



        var resultWheelDressing: {} = {
            "calculatedAndRecommendedWheelSFPM": calculateAndFetchWheelSFPM,
            "recommendedTotalInfeedRate": this.totalInfeedRateRecommendedValues,
            "calculatedAndRecommendedDressRollSFPM": calculateAndFetchDressRollSFPM,
            "calculatedAndRecommendedSpeedRatio": calculateAndFetchSpeedRatio,
            "calculatedAndRecommendedInfeedRateinmin": calculateAndFetchInfeedRateinmin,
            "calculatedAndRecommendedInfeedRateuinrev": calculateAndFetchInfeedRateuinrev,
            "productType": this.wheelTechTypekey,
            "inputWheelParameters": wheelParameters,
            "inputDressRollParameters": dressRollParameters,
            "isImperialNotMetric": isImperialNotMetric,
            "isUDnotCD": isUDnotCD,
            "isUinNotIndp": isUinNotIndp,
            "isunitPrefCD": isunitPrefCD
        };
        var resultOfToggles: {} = {
            "isImperialNotMetric": isImperialNotMetric,
            "isUDnotCD": isUDnotCD,
            "isUinNotIndp": isUinNotIndp,
            "isunitPrefCD": isunitPrefCD
        }
        this.storage.set(Constants.KEY_DRESSING, resultWheelDressing);
        this.storage.set(Constants.KEY_UNIT_PREF_PLUNGE_ROLL_DRESSING, resultOfToggles);

        return resultWheelDressing;
    };

    calculateAndFetchWheelSFPM(isImperialNotMetric, wpWheelSFPM, wpDiameter, wheelSFPMRecommended) {
        var resultCalculatedAndRecommendedWheelSFPM: {};
        this.resultCalculatedWheelSFPM;
        var resultRecommendedWheelSFPM: number;
        console.log(isImperialNotMetric);
        if (isImperialNotMetric) {
            this.resultCalculatedWheelSFPM = wpWheelSFPM * wpDiameter * 0.2618;
            resultRecommendedWheelSFPM = wheelSFPMRecommended;
        } else {
            this.resultCalculatedWheelSFPM = (3.14159 * wpDiameter * wpWheelSFPM) / 60000;
            resultRecommendedWheelSFPM = wheelSFPMRecommended;
        }
        resultCalculatedAndRecommendedWheelSFPM = {
            "resultCalculatedWheelSFPM": this.resultCalculatedWheelSFPM,
            "resultRecommendedWheelSFPM": Math.round(resultRecommendedWheelSFPM)
        };
        console.log(resultCalculatedAndRecommendedWheelSFPM);
        return resultCalculatedAndRecommendedWheelSFPM;
    }

    round(value) {
        var value1: any = (value >= 0 || -1);
        return value1 * Math.round(Math.abs(value));
    }
    // E8*F8*(PI()/12)*G8
    calculateAndFetchDressRollSFPM(isImperialNotMetric, dressRollDiameter, dressRollRPM, isUDnotCD, speedRatioRecommended, wheelSFPMRecommended) {
        var resultCalculatedAndRecommendedDressRollSFPM: {};
        this.resultCalculatedDressRollSFPM;
        var resultRecommendedDressRollSFPM: number;
        var UDorCD;
        console.log(isImperialNotMetric);
        if (isUDnotCD) {
            UDorCD = 1;
        } else {
            UDorCD = -1;
        }
        if (isImperialNotMetric) {
            this.resultCalculatedDressRollSFPM = dressRollDiameter * dressRollRPM * (3.14159 / 12) * UDorCD
            resultRecommendedDressRollSFPM = wheelSFPMRecommended * speedRatioRecommended;
        } else {
            this.resultCalculatedDressRollSFPM = (3.14159 * dressRollDiameter * dressRollRPM) / 60000 * UDorCD;
            resultRecommendedDressRollSFPM = wheelSFPMRecommended * speedRatioRecommended;
        }
        resultCalculatedAndRecommendedDressRollSFPM = {
            "resultCalculatedDressRollSFPM": this.resultCalculatedDressRollSFPM,
            "resultRecommendedDressRollSFPM": this.round(resultRecommendedDressRollSFPM)
        };
        console.log(resultCalculatedAndRecommendedDressRollSFPM);
        return resultCalculatedAndRecommendedDressRollSFPM;
    }

    calculateAndFetchSpeedRatio(speedRatioRecommended) {
        var resultCalculatedAndRecommendedSpeedRatio: {};
        var resultRecommendedDressRollSFPM: number = 1;
        resultCalculatedAndRecommendedSpeedRatio = {
            "resultCalculatedDressSpeedRatio": this.resultCalculatedDressRollSFPM / this.resultCalculatedWheelSFPM,
            "resultRecommendedDressSpeedRatio": speedRatioRecommended
        };
        console.log(resultCalculatedAndRecommendedSpeedRatio);
        return resultCalculatedAndRecommendedSpeedRatio;
    }
    // IF(I8<>"",I8,(H8*0.000001*D8))
    calculateAndFetchInfeedRateinmin(isImperialNotMetric, isUinNotIndp, wpWheelSFPM, dressRollIFR, wheelTechType, isunitPrefCD) {
        console.log(wheelTechType);
        var resultCalculatedAndRecommendedInfeedRateinmin: {};
        var resultRecommendedInfeedRateinmin: number;
        var resultCalculatedInfeedRateinmin: number;
        var UDorCD;
        // S8*D8/1000000
        console.log(isImperialNotMetric);
        if (isImperialNotMetric) {
            resultRecommendedInfeedRateinmin = wheelTechType * wpWheelSFPM / 1000000;
            if (isUinNotIndp) {
                resultCalculatedInfeedRateinmin = dressRollIFR;
            } else {
                resultCalculatedInfeedRateinmin = dressRollIFR * 0.000001 * wpWheelSFPM
            }

        } else {
            resultRecommendedInfeedRateinmin = wheelTechType * wpWheelSFPM / 60000;
            if (isUinNotIndp) {
                resultCalculatedInfeedRateinmin = dressRollIFR;
            } else {
                resultCalculatedInfeedRateinmin = (dressRollIFR * wpWheelSFPM) / 60000;
            }

        }
        resultCalculatedAndRecommendedInfeedRateinmin = {
            "resultCalculatedInfeedRateinmin": resultCalculatedInfeedRateinmin,
            "resultRecommendedInfeedRateinmin": resultRecommendedInfeedRateinmin
        };
        console.log(resultCalculatedAndRecommendedInfeedRateinmin);
        return resultCalculatedAndRecommendedInfeedRateinmin;

    }
    // IF(I9<>"",I9/(D9/1000000),H9)
    calculateAndFetchInfeedRateuinrev(isImperialNotMetric, isUinNotIndp, wpWheelSFPM, dressRollIFR, wheelTechType, isunitPrefCD) {
        var resultCalculatedAndRecommendedInfeedRateuinrev: {};
        var resultRecommendedInfeedRateuinrev: number;
        var resultCalculatedInfeedRateuinrev: number;
        console.log(isImperialNotMetric);
        if (isImperialNotMetric) {
            if (isUinNotIndp) {
                // I9*60000/D9
                if (isunitPrefCD) {
                    resultCalculatedInfeedRateuinrev = dressRollIFR / (wpWheelSFPM / 1000000);
                } else {
                    // ,I8/(D8/1000000)
                    resultCalculatedInfeedRateuinrev = dressRollIFR / (wpWheelSFPM / 1000000);
                }
                resultRecommendedInfeedRateuinrev = wheelTechType;
            } else {
                resultCalculatedInfeedRateuinrev = dressRollIFR;
                resultRecommendedInfeedRateuinrev = wheelTechType;
            }

        } else {
            // (R9*D9)/60000
            if (isUinNotIndp) {
                // I22*60000/D22
                if (isunitPrefCD) {
                    resultCalculatedInfeedRateuinrev = (dressRollIFR * 60000) / wpWheelSFPM;
                } else {
                    resultCalculatedInfeedRateuinrev = (dressRollIFR * 60000) / wpWheelSFPM;
                }
                resultRecommendedInfeedRateuinrev = wheelTechType;
            } else {
                resultCalculatedInfeedRateuinrev = dressRollIFR;
                resultRecommendedInfeedRateuinrev = wheelTechType;
            }

        }
        resultCalculatedAndRecommendedInfeedRateuinrev = {
            "resultCalculatedInfeedRateuinrev": resultCalculatedInfeedRateuinrev,
            "resultRecommendedInfeedRateuinrev": resultRecommendedInfeedRateuinrev
        };
        console.log(resultCalculatedAndRecommendedInfeedRateuinrev);
        return resultCalculatedAndRecommendedInfeedRateuinrev;

    }
    productTypeEvents(productType) {
        console.log(productType);
        switch (productType) {
            case Constants.KEY_CONVENTIONAL_VITRIFIED:
                this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_PRODUCTTYPE_CONVENTIONAL_VITRIFIED_HITCOUNT, {});
                break;
            case Constants.KEY_CERAMIC_VITRIFIED:
                this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_PRODUCTTYPE_CERAMICL_VITRIFIED_HITCOUNT, {});
                break;
            case Constants.KEY_VIT_CBN:
                this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_PRODUCTTYPE_VIT_CBN_HITCOUNT, {});
                break;
            case Constants.KEY_VIT_DIAMOND:
                this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_PRODUCTTYPE_VIT_DIAMOND_HITCOUNT, {});
                break;
            case Constants.KEY_PARADIGM_DIAMOND:
                this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_PRODUCTTYPE_PARADIGM_DIAMOND_HITCOUNT, {});
                break;
            default:
                break;
        }
    }
    triggerFirebaseEvent(keyError) {
        console.log(keyError);
        if (keyError === "ncdDiameter_eng" || keyError === "ncdDiameter_metric") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_DRESSROLL_DIAMETER_HITCOUNT, {});
        } else if (keyError === "ncdWPWheel_eng" || keyError === "ncdWPWheel_metric") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_WHEEL_RPM_HITCOUNT, {});
        } else if (keyError === "ncdWPDiameter_eng" || keyError === "ncdWPDiameter_metric") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_WHEEL_DIAMETER_HITCOUNT, {});
        } else if (keyError === "rpmDiamRoll_eng" || keyError === "rpmDiamRoll_metric") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_DRESSROLL_RPM_HITCOUNT, {});
        } else if (keyError === "infeedRateInmin_eng" || keyError === "infeedRateMms_metric") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.PLUNGE_DRESSING_CALC_DRESSROLL_IR_HITCOUNT, {});
        }
    }


}
