export const DIRECTORY_ADD_EVENT = 'addDir';
export const DATABASE_VERSION = '_v3.40';
export const KEY_TERMS = 'terms';
export const KEY_SEED = 'seeding';
export const KEY_CALC_LIST = 'calculators';
export const KEY_WHEEL = 'wheel';
export const KEY_DIAMOND = 'diamond';
export const KEY_COOLANT = 'coolant';
export const KEY_CHIPTHICKNESS = 'chipThickness';
export const KEY_QPRIME = 'qPrime';
export const KEY_CONVERSION = 'conversions';
export const KEY_CONVERSION_DESIGN2 = 'conversions2';
export const KEY_GRIND_PARAM = 'grindingParameters';
export const KEY_DRESS_PARAM = 'dressingParameters';
export const KEY_CUT_PARAM = 'cuttingParameters';

export const KEY_LENGTH = 'length';
export const KEY_VELOCITY = 'velocity';
export const KEY_AREA = 'area';

export const KEY_DRESSING = 'dressing';
export const KEY_STATIONARY = 'stationary';

export const KEY_KERF_LOSS = 'kerfloss';

export const KEY_TOTAL_INFEED_RATE_METRIC = 'totalInfeedRateMetric';
export const KEY_TOTAL_INFEED_RATE_IMPERIAL = 'totalInfeedRateImperial';
export const KEY_WHEEL_TECH_TYPE = 'wtType';
export const KEY_WHEEL_TECH_TYPE_METRIC = 'wtTypeMetric';
export const KEY_WHEEL_TECH_TYPE_METRIC_CD = 'wtTypeMetriccd';
export const KEY_WHEEL_TECH_TYPE_IMPERIAL = 'wtTypeImperial';
export const KEY_WHEEL_TECH_TYPE_IMPERIAL_CD = 'wtTypeImperialcd';

export const KEY_PRODUCT_TYPE_STATIONARY_METRIC = 'productTypeStationaryMetric';
export const KEY_PRODUCT_TYPE_STATIONARY_IMPERIAL =
	'productTypeStationaryImperial';

export const KEY_COOLANT_TYPE = 'coolantType';
export const KEY_SYNTHETIC_OIL = 'synthetic_oil';
export const KEY_MINERAL_OIL = 'mineral_oil';
export const KEY_WATER_SYN_OIL = 'water_synthetic_oil';
export const KEY_WATER_SOL_OIL = 'water_synthetic_oil';
export const KEY_PRODUCT_TYPE = 'productType';
export const KEY_INPUT_TYPE = 'inputType';
export const KEY_RESIN = 'resin';
export const KEY_VITRIFIED = 'vitrified';
export const KEY_GFORCE_DIAMOND = 'gforce_diamond';
export const KEY_GFORCE_CBN = 'gforce_cbn';
export const KEY_PARADIGM = 'paradigm';
export const KEY_VIT_CBN = 'vit_cbn';
export const KEY_VIT_DIAMOND = 'vit_diamond';

export const KEY_IN = 'in';
export const KEY_CM = 'cm';
export const KEY_MM = 'mm';
export const KEY_FT = 'ft';
export const KEY_M = 'm';

export const KEY_IN2 = 'in2';
export const KEY_CM2 = 'cm2';
export const KEY_MM2 = 'mm2';

export const KEY_CONVENTIONAL_VITRIFIED = 'conventional_vitrified';
export const KEY_CERAMIC_VITRIFIED = 'ceramic_vitrified';
export const KEY_PARADIGM_DIAMOND = 'paradigm_diamond';

export const KEY_CONVENTIONAL_ABRASIVES = 'conventional_abrasives';
export const KEY_CERAMIC_ABRASIVES = 'ceramic_abrasives';

export const KEY_WHEEL_UNIT = 'grinding_wheel_unit';
export const KEY_WS_MPS = 'wheel_speed_mps';
export const KEY_WS_SFPM = 'wheel_speed_sfpm';
export const KEY_WHEEL_SPEED = 'wheel_speed';
export const KEY_WHEEL_THICKNESS = 'wheel_thickness';
export const KEY_SURFACE_AREA = 'surface_area';
export const KEY_RPM = 'rpm';
export const KEY_SFPM = 'sfpm';
export const KEY_MPS = 'mps';
export const KEY_COOLANT_FLOWRATE = 'coolant_flowrate';
export const KEY_IPM = 'ipm';
export const KEY_GPM = 'gpm';
export const KEY_DIAMOND_ROLL_SFPM = 'sfpmDiamondRoll';
export const KEY_DIAMOND_ROLL_WHEEL_SFPM = 'resultCalculatedWheelSFPM';
export const KEY_DIAMOND_ROLL_RECOMMENDED_WHEEL_SFPM =
	'resultCalculatedWheelSFPM';
export const KEY_SPEED_RATIO = 'resultCalculatedSpeedRatio';
export const KEY_RECOMMENDED_SPEED_RATIO = 'resultRecommendedSpeedRatio';
export const KEY_TRAVERSE_RATE = 'resultCalculatedTraverseRate';
export const KEY_RECOMMENDED_TRAVERSE_RATE = 'resultRecommendedTraverseRate';
export const KEY_OVERLAP_RATIO = 'resultCalculatedOverlapRatio';
export const KEY_RECOMMENDED_OVERLAP_RATIO =
	'resultRecommendedWheelOverlapRatio';
export const KEY_MAX_DRESS_DEPTH = 'resultRecommendedMaxDressDepth';

export const APP_UUID = 'app_uuid';
//DATABASE OBJECTS
export const KEY_UNIT_PREF_WHEEL_SPEED = 'wheelSpeedUnit';
export const KEY_UNIT_PREF_DIAMOND_ROLL_DRESSING = 'resultRecommendedWheelSFPM';

export const KEY_UNIT_PREF_PLUNGE_ROLL_DRESSING = 'resultToggleOFDressing';
export const KEY_UNIT_PREF_STATIONARY_TOOL = 'resultToggleOFStationary';

//Validation Ranges for Wheel speed
// TCS
// export const WHEEL_SPEED_DIA_MIN_ENG = 0.375;
// export const WHEEL_SPEED_DIA_MAX_ENG = 20;
// export const WHEEL_SPEED_DIA_MIN_METRIC = 9.5;
// export const WHEEL_SPEED_DIA_MAX_METRIC = 508;

// SG
/**/
// export const WHEEL_SPEED_DIA_MIN_ENG = 0.005;
// export const WHEEL_SPEED_DIA_MAX_ENG = 200;
// export const WHEEL_SPEED_DIA_MIN_METRIC = 0.127;
// export const WHEEL_SPEED_DIA_MAX_METRIC = 5080;
/**/
// SG New Ruchi
/**/
export const MIN_VALUE = 0.0;
export const MAX_VALUE = 999999999;
// export const WHEEL_SPEED_DIA_MIN_ENG = 0.001;
// export const WHEEL_SPEED_DIA_MAX_ENG = ;
// export const WHEEL_SPEED_DIA_MIN_METRIC = 0.127;
// export const WHEEL_SPEED_DIA_MAX_METRIC = 5080;
/**/

// TCS
// export const WHEEL_SPEED_RPM_MIN_ENG = 10;
// export const WHEEL_SPEED_RPM_MAX_ENG = 305000;
// export const WHEEL_SPEED_SFPM_MIN_ENG = 10;
// export const WHEEL_SPEED_SFPM_MAX_ENG = 1600000;
// export const WHEEL_SPEED_MPS_MIN_ENG = .05;
// export const WHEEL_SPEED_MPS_MAX_ENG = 9000;
//

// SG
// export const WHEEL_SPEED_RPM_MIN_ENG = 1;
// export const WHEEL_SPEED_RPM_MAX_ENG = 305000;
// export const WHEEL_SPEED_SFPM_MIN_ENG = 1;
// export const WHEEL_SPEED_SFPM_MAX_ENG = 1600000;
// export const WHEEL_SPEED_MPS_MIN_ENG = 0.01;
// export const WHEEL_SPEED_MPS_MAX_ENG = 10000;
//

//Validation Ragnes for Diamond dressing
// TCS
// export const DIAMOND_ROLL_DIAMETER_MIN_ENG = 2;
// export const DIAMOND_ROLL_DIAMETER_MAX_ENG = 8;
// export const DIAMOND_ROLL_DIAMETER_MIN_METRIC = 50;
// export const DIAMOND_ROLL_DIAMETER_MAX_METRIC = 205;
// export const DIAMOND_ROLL_WIDTH_MIN_ENG = .005;
// export const DIAMOND_ROLL_WIDTH_MAX_ENG = .05;
// export const DIAMOND_ROLL_WIDTH_MIN_METRIC = .200;
// export const DIAMOND_ROLL_WIDTH_MAX_METRIC = 1.30;
// export const DIMAOND_ROLL_RPM_MIN_ENG = 100;
// export const DIMAOND_ROLL_RPM_MAX_ENG = 12000;
// export const DIAMOND_ROLL_WHEEL_DIAMETER_MIN_ENG = .375;
// export const DIAMOND_ROLL_WHEEL_DIAMETER_MAX_ENG = 20;
// export const DIAMOND_ROLL_WHEEL_DIAMETER_MIN_METRIC = 9.5;
// export const DIAMOND_ROLL_WHEEL_DIAMETER_MAX_METRIC = 508;
// export const DIAMOND_ROLL_WHEEL_RPM_MIN_ENG = 10;
// export const DIAMOND_ROLL_WHEEL_RPM_MAX_ENG = 305000;
// export const DIAMOND_ROLL_TRAVERSE_RATE_MIN_ENG = 2;
// export const DIAMOND_ROLL_TRAVERSE_RATE_MAX_ENG = 50;
// export const DIAMOND_ROLL_TRAVERSE_RATE_MIN_METRIC = 50.0;
// export const DIAMOND_ROLL_TRAVERSE_RATE_MAX_METRIC = 1270;

// SG
/**/
// export const DIAMOND_ROLL_DIAMETER_MIN_ENG = 1;
// export const DIAMOND_ROLL_DIAMETER_MAX_ENG = 20;
// export const DIAMOND_ROLL_DIAMETER_MIN_METRIC = 25.4;
// export const DIAMOND_ROLL_DIAMETER_MAX_METRIC = 508;
// export const DIAMOND_ROLL_WIDTH_MIN_ENG = 0.005;
// export const DIAMOND_ROLL_WIDTH_MAX_ENG = 2.0;
// export const DIAMOND_ROLL_WIDTH_MIN_METRIC = 0.200;
// export const DIAMOND_ROLL_WIDTH_MAX_METRIC = 50.8;
// export const DIMAOND_ROLL_RPM_MIN_ENG = 50;
// export const DIMAOND_ROLL_RPM_MAX_ENG = 20000;
// export const DIAMOND_ROLL_WHEEL_DIAMETER_MIN_ENG = 0.050;
// export const DIAMOND_ROLL_WHEEL_DIAMETER_MAX_ENG = 100;
// export const DIAMOND_ROLL_WHEEL_DIAMETER_MIN_METRIC = 1.27;
// export const DIAMOND_ROLL_WHEEL_DIAMETER_MAX_METRIC = 2540;
// export const DIAMOND_ROLL_WHEEL_RPM_MIN_ENG = 10;
// export const DIAMOND_ROLL_WHEEL_RPM_MAX_ENG = 305000;
// export const DIAMOND_ROLL_TRAVERSE_RATE_MIN_ENG = 0.01;
// export const DIAMOND_ROLL_TRAVERSE_RATE_MAX_ENG = 600;
// export const DIAMOND_ROLL_TRAVERSE_RATE_MIN_METRIC = 0.254;
// export const DIAMOND_ROLL_TRAVERSE_RATE_MAX_METRIC = 15240;
/**/

//Coolant Validation
export const VAL_COOLANT_TYPE_EMPTY = 'coolant_type_empty';
export const VAL_GWP_SFPM = 'gwp_sfpm';
export const VAL_GWP_MSEC = 'gwp_msec';
export const VAL_GWP_BAR = 'gwp_bar';
export const VAL_GWP_PSI = 'gwp_psi';
export const VAL_GWP_EMPTY = 'gwp_empty';

export const VAL_GPCF_KW = 'gpcf_kw';
export const VAL_GPCF_HP = 'gpcf_hp';
export const VAL_GPCF_LMIN = 'gpcf_lmin';
export const VAL_GPCF_GPM = 'gpcf_gpm';
export const VAL_GPCF_EMPTY = 'gpcf_empty';

export const VAL_GPCW_MM = 'gpcw_mm';
export const VAL_GPCW_IN = 'gpcw_in';
export const VAL_GPCW_EMPTY = 'gpcw_empty';

// TCS
// export const COOLANT_SFPM_MIN = 500;
// export const COOLANT_SFPM_MAX = 60000;
// export const COOLANT_MSEC_MIN = 2.5;
// export const COOLANT_MSEC_MAX = 305;
//

// SG
// export const COOLANT_SFPM_MIN = 50;
// export const COOLANT_SFPM_MAX = 60000;
// export const COOLANT_MSEC_MIN = 0.25;
// export const COOLANT_MSEC_MAX = 305;
// //

// export const COOLANT_BAR_MIN = 0.028;
// export const COOLANT_BAR_MAX = 404;
// export const COOLANT_PSI_MIN = 0.4;
// export const COOLANT_PSI_MAX = 6000;

// TCS
// export const COOLANT_KW_MIN = 3;
// export const COOLANT_KW_MAX = 40;
// export const COOLANT_HP_MIN = 5;
// export const COOLANT_HP_MAX = 50;
//

// SG
// export const COOLANT_KW_MIN = 0.75;
// export const COOLANT_KW_MAX = 150;
// export const COOLANT_HP_MIN = 1;
// export const COOLANT_HP_MAX = 200;
//

// TCS
// export const COOLANT_LMIN_MIN = 30;
// export const COOLANT_LMIN_MAX = 400;
// export const COOLANT_GPM_MIN = 10;
// export const COOLANT_GPM_MAX = 100;
//

// SG
// export const COOLANT_LMIN_MIN = 2;
// export const COOLANT_LMIN_MAX = 200;
// export const COOLANT_GPM_MIN = 3.8;
// export const COOLANT_GPM_MAX = 750;
//

// TCS
// export const COOLANT_MM_MIN = 2.5;
// export const COOLANT_MM_MAX = 76.2;
// export const COOLANT_IN_MIN = 0.1;
// export const COOLANT_IN_MAX = 3;
//

// SG
// export const COOLANT_MM_MIN = 0.127;
// export const COOLANT_MM_MAX = 508;
// export const COOLANT_IN_MIN = 0.005;
// export const COOLANT_IN_MAX = 20;
//

//Coolant DB
export const COOLANT_INPUT = 'coolant_input';
export const COOLANT_OUTPUT = 'coolant_output';

export const KEY_SPEED_RATIO_RECOMMENDED = 'speed_ratio_recommended';

export const KEY_DRESSING_SPEED_RATIO_RECOMMENDED =
	'dressing_speed_ratio_recommended';
export const KEY_DRESSING_SPEED_RATIO_METRIC_RECOMMENDED =
	'dressing_speed_ratio_metric_recommended';

export const KEY_OVERLAP_RATIO_RECOMMENDED = 'overlap_ratio_recommended';
export const KEY_MAX_DRESS_DEPTH_RECOMMENDED = 'max_dress_depth';

export const KEY_WHEEL_SPEED_NCD_ENG_RECOMMENDED =
	'wheel_speed_ncd_eng_recommended';
export const KEY_WHEEL_SPEED_CD_ENG_RECOMMENDED =
	'wheel_speed_cd_eng_recommended';
export const KEY_WHEEL_SPEED_NCD_MET_RECOMMENDED =
	'wheel_speed_ncd_met_recommended';
export const KEY_WHEEL_SPEED_CD_MET_RECOMMENDED =
	'wheel_speed_cd_met_recommended';

//stationary tool

export const KEY_RECOMMENDED_LEAD_SINGLE_IMPERIAL =
	'recommended_lead_single_imeprial';
export const KEY_RECOMMENDED_LEAD_SINGLE_METRIC =
	'recommended_lead_single_metric';
export const KEY_RECOMMENDED_LEAD_MULTIPLE_IMPERIAL =
	'recommended_lead_multiple_imeprial';
export const KEY_RECOMMENDED_LEAD_MULTIPLE_METRIC =
	'recommended_lead_multiple_metric';

//dressing validation
export const VAL_WHEEL_TECHNOLOGY_TYPE_EMPTY = 'wheeltech_type_empty';
export const LENGTH_CONVERTER_DATA = 'lengthConverterData';
export const VELOCITY_CONVERTER_DATA = 'velocityConverterData';

//dressing constant
// export const DRESS_ROLL_DIAMETER_MIN_IMPERIAL = 1;
// export const DRESS_ROLL_DIAMETER_MAX_IMPERIAL = 20;
// export const DRESS_ROLL_DIAMETER_MIN_MATRIC = 0.127;
// export const DRESS_ROLL_DIAMETER_MAX_MATRIC = 508;
// export const DRESS_INFEED_RATE_MIN_IN_MIN_IMPERIAL = 0.00001;
// export const DRESS_INFEED_RATE_MAX_IN_MIN_IMPERIAL = 100;
// export const DRESS_INFEED_RATE_MIN_UIN_REV_IMPERIAL = 0.05;
// export const DRESS_INFEED_RATE_MAX_UIN_REV_IMPERIAL = 100;
// export const DRESS_INFEED_RATE_MIN_MM_S_MATRIC = 0.0000042;
// export const DRESS_INFEED_RATE_MAX_MM_S_MATRIC = 42.3;
// export const DRESS_INFEED_RATE_MIN_UM_REV_MATRIC = 0.0127;
// export const DRESS_INFEED_RATE_MAX_UM_REV_MATRIC = 2.54;
// export const WHEEL_PARAMETER_SPEED_RATIO_MIN_IMPERIAL = -10;
// export const WHEEL_PARAMETER_SPEED_RATIO_MAX_IMPERIAL = 10;
// export const WHEEL_PARAMETER_SPEED_RATIO_MIN_MATRIC = -10;
// export const WHEEL_PARAMETER_SPEED_RATIO_MAX_MATRIC = 10;
// export const WHEEL_PARAMETER_WHEEL_MIN_IMPERIAL = 1;
// export const WHEEL_PARAMETER_WHEEL_MAX_IMPERIAL = 300000;
// export const WHEEL_PARAMETER_WHEEL_MIN_MATRIC = 1;
// export const WHEEL_PARAMETER_WHEEL_MAX_MATRIC = 300000;
// export const WHEEL_PARAMETER_DIAMETER_MIN_IMPERIAL = 0.005;
// export const WHEEL_PARAMETER_DIAMETER_MAX_IMPERIAL = 200;
// export const WHEEL_PARAMETER_DIAMETER_MIN_MATRIC = 0.127;
// export const WHEEL_PARAMETER_DIAMETER_MAX_MATRIC = 508;
// export const DRESS_ROLL_RPM_MIN_IMPERIAL = 1;
// export const DRESS_ROLL_RPM_MAX_IMPERIAL = 20000;
// export const DRESS_ROLL_RPM_MIN_MATRIC = 1;
// export const DRESS_ROLL_RPM_MAX_MATRIC = 300000;

//dressing constant
// export const DRESSING_PARAMETERS_CONTACT_WIDTH_MIN_IMPERIAL = 0.001;
// export const DRESSING_PARAMETERS_CONTACT_WIDTH_MAX_IMPERIAL = 1.000;
// export const DRESSING_PARAMETERS_CONTACT_WIDTH_MIN_METRIC = 0.025;
// export const DRESSING_PARAMETERS_CONTACT_WIDTH_MAX_METRIC = 25.4;

// export const DRESSING_PARAMETERS_RPM_MIN_IMPERIAL = 1;
// export const DRESSING_PARAMETERS_RPM_MAX_IMPERIAL = 75000;
// export const DRESSING_PARAMETERS_RPM_MIN_METRIC = 1;
// export const DRESSING_PARAMETERS_RPM_MAX_METRIC = 75000;

// export const DRESSING_PARAMETERS_TR_MIN_IMPERIAL = 0.01;
// export const DRESSING_PARAMETERS_TR_MAX_IMPERIAL = 600;
// export const DRESSING_PARAMETERS_TR_MIN_METRIC = 0.25;
// export const DRESSING_PARAMETERS_TR_MAX_METRIC = 15250;
// export const DRESSING_PARAMETERS_DL_MIN_IMPERIAL = 0.00;
// export const DRESSING_PARAMETERS_DL_MAX_IMPERIAL = 0.68;
// export const DRESSING_PARAMETERS_DL_MIN_METRIC = 0.000;
// export const DRESSING_PARAMETERS_DL_MAX_METRIC = 17.232;

// common qprime and chip

export const KEY_HEC = 'h<sub>ec</sub>';
export const KEY_VC = 'V<sub>c</sub>';
export const KEY_VFR = 'V<sub>fr</sub>';
export const KEY_VW = 'V<sub>w</sub>';
export const KEY_AE = 'a<sub>e</sub>';
export const KEY_DW = 'd<sub>w</sub>';

// Chip constans

export const KEY_UNIT_PREF_CHIP_THICK = 'chipThicknessUnit';
export const KEY_TYPE_PREF_CHIP_THICK = 'chipThicknessType';
export const KEY_WH_PERIPHERAL = 'peripheral';
export const KEY_WH_CIRCUMFERAL = 'circumferal';

export const KEY_CHIP_INFO = 'chipInfo';
export const KEY_CHIP_INFO_RANGE_IMPERIAL =
	'(Imperial)<br/>&#8226; Precision Finishing : 0.39 - 4.0µin <br/> &#8226; Precision Grinding : 4.0 - 27.0µin <br/> &#8226; Rough Grinding :    27.0 - 137.0µin<br/>';
export const KEY_CHIP_INFO_RANGE_METRIC =
	'<br/>(Metric)<br/>&#8226; Precision Finishing : 0.01 - 0.1µm<br/> &#8226; Precision Grinding : 0.01 - 0.7µm <br/> &#8226; Rough Grinding :    0.7 - 3.5µm';
export const KEY_CHIP_INFO_NOTE =
	"&#8226; h<sub>ec</sub> too small : Risk of Burn, Low surface roughness<br/> &#8226; h<sub>ec</sub> too large : Damage of the wheel, Rough Surface<br/><br/>  The 'real' chip thickness depends on other parameters:<br/> &#8226; Coolants: Higher with oil than with Emulsion<br/> &#8226; Grit quality: Higher with high performing grits such as TGX or even TQX<br/> &#8226; Wheel Size: Small with larger diameter<br/> &#8226; Abrasive Concentration: Smaller with higher concentration";
export const KEY_CHIP_INFO_DESC =
	'The equivelent chip thickness h<sub>ec</sub> is an important value in the grinding process.In many processes, the chip thickness is in a similar range, so it can be used to define the process parameters.';

//Qprime Constants

export const KEY_QPRIME_UNITS = 'qprime_units';

export const KEY_QW = "Q'<sub>w</sub>";

export const KEY_QP_VAL_QW = 'QW';
export const KEY_QP_VAL_VFR = 'VFR';
export const KEY_QP_VAL_DW = 'DW';
export const KEY_QP_VAL_AE = 'AE';
export const KEY_QP_VAL_VW = 'VW';

export const KEY_UNIT_PREF_QPRIME = 'qPrimeUnit';
export const KEY_TYPE_PREF_QPRIME = 'qPrimeType';

export const KEY_QP_INFO = 'qPrimeInfo';
export const KEY_QP_INFO_DESC =
	"The specific material removal rate Q'<sub>w</sub> can vary in a wide range, which depends on the grinding process, the wheel specification, the workpiece,machine..etc";
export const KEY_QP_INFO_DESC1 =
	"The Q'<sub>w</sub> must be reduced for the finishing process, often to a value of 10 - 50%";
export const KEY_QP_INFO_RANGE_IMPERIAL =
	'(Imperial)<br/>&#8226; Creep Feed Grinding BA : 0.5 - 2.8  in3/min/in<br/> &#8226; Creep Feed Grinding SA : 0.3 - 2.8 in3/min/in<br/> &#8226; Surface Grinding : 0.2 - .09 in3/min/in<br/> &#8226; ID Grinding : .05 - .47 in3/min/in<br/> &#8226; Centerless Grinding : 5.5 - 9.0 in3/min/in<br/>';
export const KEY_QP_INFO_RANGE_METRIC =
	'<br/>(Metric)<br/>&#8226; Creep Feed Grinding BA : 5 - 30  mm3/sec/mm<br/> &#8226; Creep Feed Grinding SA : 3 - 30 mm3/sec/mm<br/> &#8226; Surface Grinding : 2 - 10 mm3/sec/mm<br/> &#8226; ID Grinding : 0.5 - 5 mm3/sec/mm<br/> &#8226; Centerless Grinding : 60 - 100 mm3/sec/mm';
