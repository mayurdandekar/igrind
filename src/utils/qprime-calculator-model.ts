import * as Constants from './constants';
import { FormGroup } from '@angular/forms';
export class QPrimeModel {
  resultObj: { [s: string]: string };
  constructor(private storage: any) {
  }

  qPrimeValidator(fg: FormGroup) {

    // let QPType = fg.get('QPType').value;
    let inp_1 = fg.get('inp_1').value;
    let isEnglish = !(fg.get('unitPref').value);
    let inp_2 = fg.get('inp_2').value;
    // let qPrimeOutputType = !(fg.get('qPrimeOutputType').value);
    if (inp_1 != "" && inp_1 != undefined && (inp_1.toString().length > 9)) {
      fg.get('inp_1').setErrors({ 'inp1_eng': true });
    }
    if (inp_2 != "" && inp_2 != undefined && (inp_2.toString().length > 9)) {
      fg.get('inp_2').setErrors({ 'inp2_eng': true });
    }
    if ((inp_1 == "" || inp_1 == undefined) && (inp_2 == "" || inp_2 == undefined)) {
      fg.get('inp_1').setErrors({ 'inp_1_empty': true });
      fg.get('inp_2').setErrors({ 'inp_2_empty': true });
    }
    else if (inp_1 == "" || inp_1 == undefined || inp_1 == null) {
      fg.get('inp_1').setErrors({ 'inp_1_empty': true });
    }
    else if (inp_2 == "" || inp_2 == undefined || inp_2 == null) {
      fg.get('inp_2').setErrors({ 'inp_2_empty': true });
    }

    return null;
  }

  calculateQprime(isEnglish: boolean, inp_1: number, inp_2: number, QPType: string, qPrimeOutputType: string) {
    var qprimeResult: {} = {};
    qprimeResult['unitPref'] = isEnglish;
    qprimeResult['QPType'] = QPType;
    qprimeResult['qPrimeOutputType'] = qPrimeOutputType;
    qprimeResult['inp_1'] = inp_1;
    qprimeResult['inp_2'] = inp_2;
    var calculatedValue;

    console.log('Z', qPrimeOutputType);

    switch (qPrimeOutputType) {
      case "qw":
        calculatedValue = this.calculateQW(isEnglish, QPType, inp_1, inp_2);
        qprimeResult['output'] = calculatedValue;
        break;
      case "vw":
        calculatedValue = this.calculateVW(isEnglish, inp_1, inp_2);
        qprimeResult['output'] = calculatedValue;
        break;
      case "ae":
        calculatedValue = this.calculateAE(isEnglish, inp_1, inp_2);
        qprimeResult['output'] = calculatedValue;
        break;
      case "vfr":
        calculatedValue = this.calculateVFR(isEnglish, inp_1, inp_2);
        qprimeResult['output'] = calculatedValue;
        break;
      case "dw":
        calculatedValue = this.calculateDW(isEnglish, inp_1, inp_2);
        qprimeResult['output'] = calculatedValue;
        break;
    }

    // qprimeResult[Constants.KEY_QP_VAL_QW] = calculatedValue;
    // qprimeResult[Constants.KEY_QP_VAL_VW] = calculatedValue
    // qprimeResult[Constants.KEY_QP_VAL_AE] = calculatedValue;
    this.storage.set(Constants.KEY_QPRIME, qprimeResult);
    return calculatedValue;
  }

  /* 
    
  */

  calculateQW(isEnglish, QPType, inp_1, inp_2) {
    var qwOutput;
    var n;
    if (QPType == 'workpiece') {
      if (isEnglish) {
        n = 2;
        qwOutput = inp_1 * inp_2;
        qwOutput = this.round(qwOutput, n);
        return qwOutput;
      }
      else {
        n = 1;
        qwOutput = (inp_1 * inp_2) / 60;
        qwOutput = this.round(qwOutput, n);
        return qwOutput;
      }
    }
    else if (QPType == 'radial') {
      if (isEnglish) {
        n = 1;
        qwOutput = inp_1 * 3.141592654 * inp_2;
        qwOutput = this.round(qwOutput, n);
        return qwOutput;
      }
      else {
        n = 1;
        qwOutput = (inp_1 * 3.141592654 * inp_2) / 60;
        qwOutput = this.round(qwOutput, n);
        return qwOutput;
      }
    }
    else { }
  }

  calculateVW(isEnglish, inp_1, inp_2) {
    var vwOutput;
    var n;
    if (isEnglish) {
      n = 1;
      vwOutput = inp_1 / inp_2;
      vwOutput = this.round(vwOutput, n);
      return vwOutput;
    }
    else {
      n = 1;
      vwOutput = (inp_1 * 60) / inp_2;
      vwOutput = this.round(vwOutput, n);
      return vwOutput;
    }

  }

  calculateAE(isEnglish, inp_1, inp_2) {
    var aeOutput;
    var n;
    if (isEnglish) {
      n = 3;
      aeOutput = inp_1 / inp_2;
      aeOutput = this.round(aeOutput, n);
      return aeOutput;
    }
    else {
      n = 3;
      aeOutput = (inp_1 * 60) / inp_2;
      aeOutput = this.round(aeOutput, n);
      return aeOutput;
    }

  }

  calculateVFR(isEnglish, inp_1, inp_2) {
    var vfrOutput;
    var n;
    if (isEnglish) {
      n = 2;
      vfrOutput = inp_1 / (inp_2 * 3.141592654);
      vfrOutput = this.round(vfrOutput, n);
      return vfrOutput;
    }
    else {
      n = 1;
      vfrOutput = (inp_1 * 60) / (inp_2 * 3.141592654);
      vfrOutput = this.round(vfrOutput, n);
      return vfrOutput;
    }

  }

  calculateDW(isEnglish, inp_1, inp_2) {
    var dwOutput;
    var n;
    if (isEnglish) {
      n = 1;
      dwOutput = inp_1 / (inp_2 * 3.141592654);
      dwOutput = this.round(dwOutput, n);
      return dwOutput;
    }
    else {
      n = 1;
      dwOutput = (inp_1 * 60) / (inp_2 * 3.141592654);
      dwOutput = this.round(dwOutput, n);
      return dwOutput;
    }

  }

  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  }
}
