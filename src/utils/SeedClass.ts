import * as Constants from '../utils/constants';

export class SeedClass {
	constructor(private storage: any, private translate: any) {}

	public seed(): Promise<any> {
		return this.storage.ready().then(() => {
			return this.storage
				.get(Constants.KEY_SEED + Constants.DATABASE_VERSION)
				.then(seedVal => {
					return new Promise((resolve, reject) => {
						if (seedVal !== null) {
							console.log('seeding done');
							//seeding done, call load UI method
							resolve(true);
						} else {
							console.log('seeding  not done');
							//seeding not done,start seeding

							this.storage.set(
								Constants.KEY_CALC_LIST +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										calcKey: Constants.KEY_CONVERSION,
										calcLogo:
											'assets/imgs/conversions_calc.png',
										blackLogo:
											'assets/imgs/sidebar_conversion_black.png',
										whiteLogo:
											'assets/imgs/sidebar_wheel_white.png',
										isWhiteLogo: true,
										isInvert: true,
									},
									{
										id: 2,
										calcKey: Constants.KEY_GRIND_PARAM,
										calcLogo:
											'assets/imgs/grind_param_calc.png',
										blackLogo:
											'assets/imgs/sidebar_grind_param_black.png',
										whiteLogo:
											'assets/imgs/sidebar_diamond_white.png',
										isWhiteLogo: true,
									},
									{
										id: 3,
										calcKey: Constants.KEY_DRESS_PARAM,
										calcLogo:
											'assets/imgs/landing_page_diamond.png',
										blackLogo:
											'assets/imgs/sidebar_diamond_black.png',
										whiteLogo:
											'assets/imgs/sidebar_diamond_white.png',
										isWhiteLogo: false,
									},
									{
										id: 4,
										calcKey: Constants.KEY_CUT_PARAM,
										calcLogo:
											'assets/imgs/Illustrations-iGrindapp-CuttingParametersIcon.jpg',
										blackLogo:
											'assets/imgs/Illustrations-iGrindapp-CuttingParametersIcon.png',
										whiteLogo:
											'assets/imgs/Illustrations-iGrindapp-CuttingParametersIcon.png',
										isWhiteLogo: true,
									},
								]
							);

							this.storage.set(
								Constants.KEY_CONVERSION +
									Constants.KEY_CALC_LIST +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										calcKey: Constants.KEY_LENGTH,
										calcLogo: 'assets/imgs/length_calc.png',
										blackLogo:
											'assets/imgs/sidebar_length_black.png',
										whiteLogo:
											'assets/imgs/sidebar_wheel_white.png',
										isWhiteLogo: true,
										isInvert: true,
									},
									{
										id: 2,
										calcKey: Constants.KEY_VELOCITY,
										calcLogo:
											'assets/imgs/velocity_calc.png',
										blackLogo:
											'assets/imgs/sidebar_velocity_black.png',
										whiteLogo:
											'assets/imgs/sidebar_diamond_white.png',
										isWhiteLogo: true,
										isInvert: true,
									},
									{
										id: 3,
										calcKey: Constants.KEY_AREA,
										calcLogo: 'assets/imgs/area_calc.png',
										blackLogo:
											'assets/imgs/sidebar_area_black.png',
										whiteLogo:
											'assets/imgs/sidebar_area_black.png',
										isWhiteLogo: true,
									},
									{
										id: 4,
										calcKey: Constants.KEY_WHEEL,
										calcLogo:
											'assets/imgs/landing_page_wheel.png',
										blackLogo:
											'assets/imgs/sidebar_wheel_black.png',
										whiteLogo:
											'assets/imgs/sidebar_wheel_white.png',
										isWhiteLogo: false,
									},
								]
							);

							this.storage.set(
								Constants.KEY_GRIND_PARAM +
									Constants.KEY_CALC_LIST +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										calcKey: Constants.KEY_COOLANT,
										calcLogo:
											'assets/imgs/landing_page_coolant.png',
										blackLogo:
											'assets/imgs/sidebar_coolant_black.png',
										whiteLogo:
											'assets/imgs/sidebar_coolant_white.png',
										isWhiteLogo: false,
									},
									{
										id: 2,
										calcKey: Constants.KEY_CHIPTHICKNESS,
										calcLogo:
											'assets/imgs/grind_param_calc.png',
										blackLogo:
											'assets/imgs/sidebar_grind_param_black.png',
										whiteLogo:
											'assets/imgs/sidebar_grind_param_white.png',
										isWhiteLogo: true,
									},
									{
										id: 3,
										calcKey: Constants.KEY_QPRIME,
										calcLogo:
											'assets/imgs/grind_param_calc.png',
										blackLogo:
											'assets/imgs/sidebar_grind_param_black.png',
										whiteLogo:
											'assets/imgs/sidebar_grind_param_white.png',
										isWhiteLogo: true,
									},
								]
							);

							this.storage.set(
								Constants.KEY_DRESS_PARAM +
									Constants.KEY_CALC_LIST +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										calcKey: Constants.KEY_DIAMOND,
										calcLogo:
											'assets/imgs/landing_page_diamond.png',
										blackLogo:
											'assets/imgs/sidebar_diamond_black.png',
										whiteLogo:
											'assets/imgs/sidebar_diamond_white.png',
										isWhiteLogo: false,
									},
									{
										id: 2,
										calcKey: Constants.KEY_STATIONARY,
										calcLogo:
											'assets/imgs/landing_page_stationary_tool.png',
										blackLogo:
											'assets/imgs/sidebar_stationary_black.png',
										whiteLogo:
											'assets/imgs/sidebar_stationary_white.png',
										isWhiteLogo: false,
									},
									{
										id: 3,
										calcKey: Constants.KEY_DRESSING,
										calcLogo:
											'assets/imgs/landing_page_plunge_dressing.png',
										blackLogo:
											'assets/imgs/sidebar_plunge_black.png',
										whiteLogo:
											'assets/imgs/sidebar_plunge_white.png',
										isWhiteLogo: false,
									},
								]
							);
							this.storage.set(
								Constants.KEY_CUT_PARAM +
									Constants.KEY_CALC_LIST +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										calcKey: Constants.KEY_KERF_LOSS,
										calcLogo:
											'assets/imgs/Illustrations-iGrindapp-KerfLossIcon.jpg',
										blackLogo:
											'assets/imgs/Illustrations-iGrindapp-KerfLossIcon.png',
										whiteLogo:
											'assets/imgs/Illustrations-iGrindapp-KerfLossIcon.png',
										isWhiteLogo: false,
									},
								]
							);
							this.storage.set(
								Constants.KEY_INPUT_TYPE +
									Constants.DATABASE_VERSION,
								[
									{id: 1, key: Constants.KEY_IN, alias: 'in'},
									{id: 2, key: Constants.KEY_CM, alias: 'cm'},
									{id: 3, key: Constants.KEY_MM, alias: 'mm'},
									{id: 4, key: Constants.KEY_FT, alias: 'ft'},
									{id: 4, key: Constants.KEY_M, alias: 'm'},
								]
							);
							this.storage.set(
								Constants.KEY_COOLANT_TYPE +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_SYNTHETIC_OIL,
										alias: this.translate.get(
											'synthetic_oil'
										).value,
										value: 0.93,
									},
									{
										id: 2,
										key: Constants.KEY_MINERAL_OIL,
										alias: this.translate.get('mineral_oil')
											.value,
										value: 0.87,
									},
									{
										id: 3,
										key: Constants.KEY_WATER_SYN_OIL,
										alias: this.translate.get(
											'water_synthetic_oil'
										).value,
										value: 1,
									},
									{
										id: 4,
										key: Constants.KEY_WATER_SOL_OIL,
										alias: this.translate.get(
											'water_soluble_synthetic'
										).value,
										value: 1.05,
									},
								]
							);
							this.storage.set(
								Constants.KEY_PRODUCT_TYPE +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_RESIN,
										alias: this.translate.get('resin')
											.value,
									},
									{
										id: 2,
										key: Constants.KEY_VITRIFIED,
										alias: this.translate.get('vitrified')
											.value,
									},
									{
										id: 3,
										key: Constants.KEY_GFORCE_DIAMOND,
										alias: this.translate.get(
											'norton_winter_gforce'
										).value,
									},
									{
										id: 5,
										key: Constants.KEY_PARADIGM,
										alias: this.translate.get(
											'norton_winter_paradigm'
										).value,
									},
									{
										id: 6,
										key: Constants.KEY_VIT_CBN,
										alias: this.translate.get('vit_cbn')
											.value,
									},
									{
										id: 7,
										key: Constants.KEY_VIT_DIAMOND,
										alias: this.translate.get('vit_diamond')
											.value,
									},
								]
							);
							this.storage.set(
								Constants.KEY_WHEEL_TECH_TYPE_IMPERIAL +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										alias: this.translate.get(
											'conventional_vitrified'
										).value,
										value: 40,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										alias: this.translate.get(
											'ceramic_vitrified'
										).value,
										value: 30,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										alias: this.translate.get('vit_cbn')
											.value,
										value: 5,
									},
									{
										id: 4,
										key: Constants.KEY_VIT_DIAMOND,
										alias: this.translate.get('vit_diamond')
											.value,
										value: 4,
									},
									{
										id: 5,
										key: Constants.KEY_PARADIGM_DIAMOND,
										alias: this.translate.get(
											'paradigm_diamond'
										).value,
										value: 3,
									},
								]
							);
							this.storage.set(
								Constants.KEY_WHEEL_TECH_TYPE_IMPERIAL_CD +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										alias: this.translate.get('vitrified')
											.value,
										value: 20,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										alias: this.translate.get(
											'ceramic_vitrified'
										).value,
										value: 15,
									},
								]
							);
							this.storage.set(
								Constants.KEY_WHEEL_TECH_TYPE_METRIC +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										alias: this.translate.get(
											'conventional_vitrified'
										).value,
										value: 1.0,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										alias: this.translate.get(
											'ceramic_vitrified'
										).value,
										value: 0.75,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										alias: this.translate.get('vit_cbn')
											.value,
										value: 0.13,
									},
									{
										id: 4,
										key: Constants.KEY_VIT_DIAMOND,
										alias: this.translate.get('vit_diamond')
											.value,
										value: 0.1,
									},
									{
										id: 5,
										key: Constants.KEY_PARADIGM_DIAMOND,
										alias: this.translate.get(
											'paradigm_diamond'
										).value,
										value: 0.08,
									},
								]
							);
							this.storage.set(
								Constants.KEY_WHEEL_TECH_TYPE_METRIC_CD +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										alias: this.translate.get(
											'conventional_vitrified'
										).value,
										value: 0.5,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										alias: this.translate.get(
											'ceramic_vitrified'
										).value,
										value: 0.38,
									},
								]
							);
							this.storage.set(
								Constants.KEY_TOTAL_INFEED_RATE_IMPERIAL +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										alias: this.translate.get(
											'conventional_vitrified'
										).value,
										value: 0.002,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										alias: this.translate.get(
											'ceramic_vitrified'
										).value,
										value: 0.001,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										alias: this.translate.get('vit_cbn')
											.value,
										value: 0.0002,
									},
									{
										id: 4,
										key: Constants.KEY_VIT_DIAMOND,
										alias: this.translate.get('vit_diamond')
											.value,
										value: 0.0002,
									},
									{
										id: 5,
										key: Constants.KEY_PARADIGM_DIAMOND,
										alias: this.translate.get(
											'paradigm_diamond'
										).value,
										value: 0.0002,
									},
								]
							);
							this.storage.set(
								Constants.KEY_TOTAL_INFEED_RATE_METRIC +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										alias: this.translate.get(
											'conventional_vitrified'
										).value,
										value: 0.0508,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										alias: this.translate.get(
											'ceramic_vitrified'
										).value,
										value: 0.0254,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										alias: this.translate.get('vit_cbn')
											.value,
										value: 0.0051,
									},
									{
										id: 4,
										key: Constants.KEY_VIT_DIAMOND,
										alias: this.translate.get('vit_diamond')
											.value,
										value: 0.0051,
									},
									{
										id: 5,
										key: Constants.KEY_PARADIGM_DIAMOND,
										alias: this.translate.get(
											'paradigm_diamond'
										).value,
										value: 0.0051,
									},
								]
							);
							this.storage.set(
								Constants.KEY_SPEED_RATIO_RECOMMENDED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_RESIN,
										value: -0.3,
									},
									{
										id: 1,
										key: Constants.KEY_VITRIFIED,
										value: 0.75,
									},
									{
										id: 1,
										key: Constants.KEY_GFORCE_DIAMOND,
										value: -8.0,
									},
									{
										id: 1,
										key: Constants.KEY_VIT_CBN,
										value: 0.75,
									},
									{
										id: 1,
										key: Constants.KEY_PARADIGM,
										value: -3.0,
									},
									{
										id: 1,
										key: Constants.KEY_VIT_DIAMOND,
										value: -0.3,
									},
								]
							);
							this.storage.set(
								Constants.KEY_DRESSING_SPEED_RATIO_RECOMMENDED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										value: 0.8,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										value: 0.8,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										value: 0.8,
									},
									{
										id: 4,
										key: Constants.KEY_VIT_DIAMOND,
										value: -0.3,
									},
									{
										id: 5,
										key: Constants.KEY_PARADIGM_DIAMOND,
										value: -3.0,
									},
								]
							);

							this.storage.set(
								Constants.KEY_DRESSING_SPEED_RATIO_METRIC_RECOMMENDED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										value: 0.8,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										value: 0.8,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										value: 0.8,
									},
									{
										id: 4,
										key: Constants.KEY_VIT_DIAMOND,
										value: -0.5,
									},
									{
										id: 5,
										key: Constants.KEY_PARADIGM_DIAMOND,
										value: -3.0,
									},
								]
							);

							this.storage.set(
								Constants.KEY_WHEEL_SPEED_NCD_ENG_RECOMMENDED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										value: 5000,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										value: 4000,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										value: 3000,
									},
									{
										id: 4,
										key: Constants.KEY_VIT_DIAMOND,
										value: 3000,
									},
									{
										id: 5,
										key: Constants.KEY_PARADIGM_DIAMOND,
										value: 2000,
									},
								]
							);
							this.storage.set(
								Constants.KEY_WHEEL_SPEED_CD_ENG_RECOMMENDED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										value: 4000,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										value: 3000,
									},
								]
							);
							this.storage.set(
								Constants.KEY_WHEEL_SPEED_NCD_MET_RECOMMENDED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										value: 25,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										value: 20,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										value: 15,
									},
									{
										id: 4,
										key: Constants.KEY_VIT_DIAMOND,
										value: 15,
									},
									{
										id: 5,
										key: Constants.KEY_PARADIGM_DIAMOND,
										value: 10,
									},
								]
							);
							this.storage.set(
								Constants.KEY_WHEEL_SPEED_CD_MET_RECOMMENDED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_VITRIFIED,
										value: 20,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_VITRIFIED,
										value: 15,
									},
								]
							);
							this.storage.set(
								Constants.KEY_OVERLAP_RATIO_RECOMMENDED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_RESIN,
										value: 10,
									},
									{
										id: 1,
										key: Constants.KEY_VITRIFIED,
										value: 12,
									},
									{
										id: 1,
										key: Constants.KEY_GFORCE_DIAMOND,
										value: 1,
									},
									{
										id: 1,
										key: Constants.KEY_VIT_CBN,
										value: 3,
									},
									{
										id: 1,
										key: Constants.KEY_PARADIGM,
										value: 1,
									},
									{
										id: 1,
										key: Constants.KEY_VIT_DIAMOND,
										value: 2,
									},
								]
							);

							this.storage.set(
								Constants.KEY_MAX_DRESS_DEPTH_RECOMMENDED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_RESIN + 'english',
										value: 0.0005,
									},
									{
										id: 1,
										key:
											Constants.KEY_VITRIFIED + 'english',
										value: 0.001,
									},
									{
										id: 1,
										key:
											Constants.KEY_GFORCE_DIAMOND +
											'english',
										value: 0.0001,
									},
									{
										id: 1,
										key: Constants.KEY_VIT_CBN + 'english',
										value: 0.0002,
									},
									{
										id: 1,
										key: Constants.KEY_PARADIGM + 'english',
										value: 0.0001,
									},
									{
										id: 1,
										key:
											Constants.KEY_VIT_DIAMOND +
											'english',
										value: 0.0001,
									},
									{
										id: 1,
										key: Constants.KEY_RESIN + 'metric',
										value: 12,
									},
									{
										id: 1,
										key: Constants.KEY_VITRIFIED + 'metric',
										value: 25,
									},
									{
										id: 1,
										key:
											Constants.KEY_GFORCE_DIAMOND +
											'metric',
										value: 2,
									},
									{
										id: 1,
										key: Constants.KEY_VIT_CBN + 'metric',
										value: 5,
									},
									{
										id: 1,
										key: Constants.KEY_PARADIGM + 'metric',
										value: 2,
									},
									{
										id: 1,
										key:
											Constants.KEY_VIT_DIAMOND +
											'metric',
										value: 3,
									},
								]
							);

							this.storage.set(
								Constants.KEY_WHEEL_UNIT +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_WS_MPS,
										alias: this.translate.get(
											'wheel_speed_m_sec'
										).value,
									},
									{
										id: 2,
										key: Constants.KEY_WS_SFPM,
										alias: this.translate.get(
											'wheel_speed_sfpm'
										).value,
									},
								]
							);
							this.storage.set(
								Constants.KEY_WHEEL_SPEED +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_RPM,
										alias: this.translate.get('rpm').value,
									},
									{
										id: 2,
										key: Constants.KEY_SFPM,
										alias: this.translate.get('sfpm').value,
									},
									{
										id: 3,
										key: Constants.KEY_MPS,
										alias: this.translate.get('m_sec')
											.value,
									},
								]
							);
							this.storage.set(
								Constants.KEY_COOLANT_FLOWRATE +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_IPM,
										alias: this.translate.get(
											'coolant_flow_i_min'
										).value,
									},
									{
										id: 2,
										key: Constants.KEY_GPM,
										alias: this.translate.get(
											'coolant_flow_gpm'
										).value,
									},
								]
							);

							//Qprime

							this.storage.set(
								Constants.KEY_QPRIME_UNITS +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_VW,
										alias: this.translate.get(
											'feed_rate_wp_speed'
										).value,
									},
									{
										id: 2,
										key: Constants.KEY_VFR,
										alias: this.translate.get(
											'radial_feed_rate'
										).value,
									},
									{
										id: 3,
										key: Constants.KEY_DW,
										alias: this.translate.get(
											'workpiece_diameter'
										).value,
									},
									{
										id: 4,
										key: Constants.KEY_AE,
										alias: this.translate.get('infeed')
											.value,
									},
									{
										id: 5,
										key: Constants.KEY_QW,
										alias: this.translate.get(
											'removal_rate'
										).value,
									},
								]
							);

							this.storage.set(
								Constants.KEY_QP_INFO +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_QP_INFO_RANGE_IMPERIAL,
										alias: this.translate.get(
											'qp_range_imperial'
										).value,
									},
									{
										id: 2,
										key: Constants.KEY_QP_INFO_RANGE_METRIC,
										alias: this.translate.get(
											'qp_range_metric'
										).value,
									},
									{
										id: 3,
										key: Constants.KEY_QP_INFO_DESC,
										alias: this.translate.get(
											'qp_description'
										).value,
									},
									{
										id: 4,
										key: Constants.KEY_QP_INFO_DESC,
										alias: this.translate.get(
											'qp_decription1'
										).value,
									},
								]
							);

							//CHIP
							this.storage.set(
								Constants.KEY_CHIP_INFO +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CHIP_INFO_RANGE_IMPERIAL,
										alias: this.translate.get(
											'chip_range_imperial'
										).value,
									},
									{
										id: 2,
										key: Constants.KEY_CHIP_INFO_RANGE_METRIC,
										alias: this.translate.get(
											'chip_range_metric'
										).value,
									},
									{
										id: 3,
										key: Constants.KEY_CHIP_INFO_NOTE,
										alias: this.translate.get('chip_note')
											.value,
									},
									{
										id: 4,
										key: Constants.KEY_CHIP_INFO_DESC,
										alias: this.translate.get(
											'chip_description'
										).value,
									},
								]
							);

							// Stationary tool calculator
							this.storage.set(
								Constants.KEY_PRODUCT_TYPE_STATIONARY_IMPERIAL +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_ABRASIVES,
										alias: this.translate.get(
											'conventional_abrasives'
										).value,
										value: 0.002,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_ABRASIVES,
										alias: this.translate.get(
											'ceramic_abrasives'
										).value,
										value: 0.001,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										alias: this.translate.get(
											'vit_cbn_blade_tools_only'
										).value,
										value: 0.0001,
									},
								]
							);
							this.storage.set(
								Constants.KEY_PRODUCT_TYPE_STATIONARY_METRIC +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_ABRASIVES,
										alias: this.translate.get(
											'conventional_abrasives'
										).value,
										value: 0.051,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_ABRASIVES,
										alias: this.translate.get(
											'ceramic_abrasives'
										).value,
										value: 0.025,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										alias: this.translate.get(
											'vit_cbn_blade_tools_only'
										).value,
										value: 0.003,
									},
								]
							);

							this.storage.set(
								Constants.KEY_RECOMMENDED_LEAD_SINGLE_IMPERIAL +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_ABRASIVES,
										value: 0.009,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_ABRASIVES,
										value: 0.007,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										value: 0.003,
									},
								]
							);
							this.storage.set(
								Constants.KEY_RECOMMENDED_LEAD_SINGLE_METRIC +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_ABRASIVES,
										value: 0.225,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_ABRASIVES,
										value: 0.18,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										value: 0.075,
									},
								]
							);
							this.storage.set(
								Constants.KEY_RECOMMENDED_LEAD_MULTIPLE_IMPERIAL +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_ABRASIVES,
										value: 0.0265,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_ABRASIVES,
										value: 0.0175,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										value: 0.009,
									},
								]
							);
							this.storage.set(
								Constants.KEY_RECOMMENDED_LEAD_MULTIPLE_METRIC +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_CONVENTIONAL_ABRASIVES,
										value: 0.67,
									},
									{
										id: 2,
										key: Constants.KEY_CERAMIC_ABRASIVES,
										value: 0.45,
									},
									{
										id: 3,
										key: Constants.KEY_VIT_CBN,
										value: 0.24,
									},
								]
							);
							//Author:Ruchi
							this.storage.set(
								Constants.LENGTH_CONVERTER_DATA +
									Constants.DATABASE_VERSION,
								[
									{
										id: '1',
										data: 'uin',
										outp: [
											{
												id: '100',
												data: 'um',
												outp: 0,
												decimal: 3,
												value: 0.0254,
												clss: '',
											},
										],
									},
									{
										id: '3',
										data: 'in',
										outp: [
											{
												id: '101',
												data: 'um',
												outp: 0,
												decimal: 0,
												value: 25400,
												clss: '',
											},
											{
												id: '102',
												data: 'mm',
												outp: 0,
												decimal: 3,
												value: 1 / 0.03936996,
												clss: '',
											},
											{
												id: '103',
												data: 'm',
												outp: 0,
												decimal: 3,
												value: 1 / 39.36996,
												clss: '',
											},
											{
												id: '104',
												data: 'cm',
												outp: 0,
												decimal: 3,
												value: 1 / 0.3936996,
												clss: '',
											},
										],
									},
									{
										id: '7',
										data: 'ft',
										outp: [
											{
												id: '107',
												data: 'm',
												outp: 0,
												decimal: 3,
												value: 0.3048,
												clss: '',
											},
										],
									},
									{
										id: '2',
										data: 'um',
										outp: [
											{
												id: '101',
												data: 'uin',
												outp: 0,
												decimal: 2,
												value: 1 / 0.0254,
												clss: '',
											},
											{
												id: '102',
												data: 'in',
												outp: 0,
												decimal: 6,
												value: 1 / 25400,
												clss: '',
											},
										],
									},

									{
										id: '4',
										data: 'mm',
										outp: [
											{
												id: '103',
												data: 'in',
												outp: 0,
												decimal: 3,
												value: 0.03936996,
												clss: '',
											},
										],
									},
									{
										id: '6',
										data: 'cm',
										outp: [
											{
												id: '106',
												data: 'in',
												outp: 0,
												decimal: 3,
												value: 0.3936996,
												clss: '',
											},
										],
									},
									{
										id: '5',
										data: 'm',
										outp: [
											{
												id: '104',
												data: 'in',
												outp: 0,
												decimal: 3,
												value: 39.36996,
												clss: '',
											},
											{
												id: '105',
												data: 'ft',
												outp: 0,
												decimal: 3,
												value: 1 / 0.3048,
												clss: '',
											},
										],
									},
								]
							);
							this.storage.set(
								Constants.VELOCITY_CONVERTER_DATA +
									Constants.DATABASE_VERSION,
								[
									{
										id: '1',
										data: 'SFPM',
										isWorkPiece: false,
										outp: [
											{
												id: '1',
												data: 'm/s',
												outp: 0,
												decimal: 5,
												isWorkPiece: false,
												value: 0.00508,
												clss: '',
											},
											// { "id": "6", "data": "in/min", "outp": 0, "decimal": 3, "isWorkPiece": false, "value": 5, "clss": "" },
										],
									},
									{
										id: '1',
										data: 'in/s',
										isWorkPiece: false,
										outp: [
											{
												id: '6',
												data: 'in/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 60,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 12,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 5,
												clss: '',
											},
											{
												id: '6',
												data: 'm/s',
												outp: 0,
												decimal: 4,
												isWorkPiece: false,
												value: 0.0254,
												clss: '',
											},
											{
												id: '6',
												data: 'm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: (60 * 25.4) / 1000,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 25.4,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 25.4 / 60,
												clss: '',
											},
										],
									},
									{
										id: '2',
										data: 'in/min',
										isWorkPiece: true,
										outp: [
											{
												id: '6',
												data: 'in/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 60,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/s',
												outp: 0,
												decimal: 4,
												isWorkPiece: false,
												value: 1 / (12 * 60),
												clss: '',
											},
											{
												id: '6',
												data: 'ft/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 12,
												clss: '',
											},
											{
												id: '6',
												data: 'm/s',
												outp: 0,
												decimal: 5,
												isWorkPiece: false,
												value: 25.4 / 60000,
												clss: '',
											},
											{
												id: '6',
												data: 'm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 0.0254,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 25.40008 / 60,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 25.40008,
												clss: '',
											},
											{
												id: '6',
												data: 'in/rev',
												outp: 0,
												decimal: 5,
												isWorkPiece: true,
												workPieceType: '/',
												value: 0.00042,
												clss: '',
											},
										],
									},
									{
										id: '3',
										data: 'ft/s',
										isWorkPiece: false,
										outp: [
											{
												id: '6',
												data: 'in/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 12,
												clss: '',
											},
											{
												id: '6',
												data: 'in/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 720,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 60,
												clss: '',
											},
											{
												id: '6',
												data: 'm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: (12 * 25.4) / 1000,
												clss: '',
											},
											{
												id: '6',
												data: 'm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: (12 * 25.4 * 60) / 1000,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 304.8,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 18288,
												clss: '',
											},
										],
									},
									{
										id: '3',
										data: 'ft/min',
										isWorkPiece: false,
										outp: [
											{
												id: '6',
												data: 'in/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 5,
												clss: '',
											},
											{
												id: '6',
												data: 'in/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 12,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/s',
												outp: 0,
												decimal: 2,
												isWorkPiece: false,
												value: 60,
												clss: '',
											},
											{
												id: '6',
												data: 'm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 0.00508,
												clss: '',
											},
											{
												id: '6',
												data: 'm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 0.3048,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: (12 * 25.4) / 60,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 304.8,
												clss: '',
											},
										],
									},
									{
										id: '3',
										data: 'mm/s',
										isWorkPiece: false,
										outp: [
											{
												id: '6',
												data: 'in/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 25.4,
												clss: '',
											},
											{
												id: '6',
												data: 'in/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 60 / 25.40008,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/s',
												outp: 0,
												decimal: 9,
												isWorkPiece: false,
												value: 1 / (12 * 25.4),
												clss: '',
											},
											{
												id: '6',
												data: 'ft/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 60 / (12 * 25.4),
												clss: '',
											},
											{
												id: '6',
												data: 'm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 1000,
												clss: '',
											},
											{
												id: '6',
												data: 'm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 0.06,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 60,
												clss: '',
											},
										],
									},
									{
										id: '3',
										data: 'mm/min',
										isWorkPiece: true,
										outp: [
											{
												id: '6',
												data: 'in/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 60 / 25.4,
												clss: '',
											},
											{
												id: '6',
												data: 'in/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 25.40008,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/s',
												outp: 0,
												decimal: 4,
												isWorkPiece: false,
												value: 1 / 18288,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 304.8,
												clss: '',
											},
											{
												id: '6',
												data: 'm/s',
												outp: 0,
												decimal: 6,
												isWorkPiece: false,
												value: 1 / 60000,
												clss: '',
											},
											{
												id: '6',
												data: 'm/min',
												outp: 0,
												decimal: 4,
												isWorkPiece: false,
												value: 1 / 1000,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 60,
												clss: '',
											},
											{
												id: '75',
												data: 'mm/rev',
												outp: 0,
												decimal: 3,
												isWorkPiece: true,
												workPieceType: '/',
												clss: '',
											},
										],
									},
									{
										id: '2',
										data: 'm/s',
										isWorkPiece: false,
										outp: [
											{
												id: '1',
												data: 'SFPM',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 0.00508,
												clss: '',
											},
											{
												id: '6',
												data: 'in/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												workPieceType: '/',
												value: 1 / 0.0254,
												clss: '',
											},
											{
												id: '6',
												data: 'in/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 60000 / 25.4,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1000 / (12 * 25.4),
												clss: '',
											},
											{
												id: '6',
												data: 'm/min',
												outp: 0,
												decimal: 4,
												isWorkPiece: false,
												value: 1 / 60,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1000,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 60000,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 0.00508,
												clss: '',
											},
										],
									},
									{
										id: '3',
										data: 'm/min',
										isWorkPiece: false,
										outp: [
											{
												id: '6',
												data: 'in/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 0.656168,
												clss: '',
											},
											{
												id: '6',
												data: 'in/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 0.0254,
												clss: '',
											},
											{
												id: '6',
												data: 'ft/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1000 / (12 * 25.4 * 60),
												clss: '',
											},
											{
												id: '6',
												data: 'ft/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 0.3048,
												clss: '',
											},
											{
												id: '6',
												data: 'm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 60,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/s',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1000 / 60,
												clss: '',
											},
											{
												id: '6',
												data: 'mm/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1000,
												clss: '',
											},
										],
									},

									{
										id: '2',
										data: 'in/Rev',
										isWorkPiece: true,
										outp: [
											{
												id: '17',
												data: 'In/Min',
												outp: 0,
												decimal: 3,
												isWorkPiece: true,
												workPieceType: '*',
												clss: '',
											},
										],
									},

									{
										id: '4',
										data: 'mm/rev',
										isWorkPiece: true,
										outp: [
											{
												id: '75',
												data: 'mm/min',
												outp: 0,
												decimal: 0,
												isWorkPiece: true,
												workPieceType: '*',
												clss: '',
											},
										],
									},

									{
										id: '7',
										data: 'gpm',
										isWorkPiece: false,
										outp: [
											{
												id: '84',
												data: 'm^3/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 264.172,
												clss: '',
											},
											{
												id: '85',
												data: 'ft^3/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 7.4805,
												clss: '',
											},
										],
									},
									{
										id: '8',
										data: 'ft^3/min',
										isWorkPiece: false,
										outp: [
											{
												id: '89',
												data: 'm^3/min',
												outp: 0,
												decimal: 2,
												isWorkPiece: false,
												value: 35.3147,
												clss: '',
											},
											{
												id: '90',
												data: 'gpm',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 7.4805,
												clss: '',
											},
										],
									},
									{
										id: '5',
										data: 'm^3/min',
										isWorkPiece: false,
										outp: [
											{
												id: '79',
												data: 'gpm',
												outp: 0,
												decimal: 0,
												isWorkPiece: false,
												value: 264.172,
												clss: '',
											},
											{
												id: '80',
												data: 'ft^3/min',
												outp: 0,
												decimal: 3,
												isWorkPiece: false,
												value: 1 / 35.3147,
												clss: '',
											},
										],
									},
								]
							);

							this.storage.set(
								Constants.KEY_WHEEL_THICKNESS +
									Constants.DATABASE_VERSION,
								[
									{id: 1, key: Constants.KEY_IN, alias: 'in'},
									{id: 2, key: Constants.KEY_CM, alias: 'cm'},
									{id: 3, key: Constants.KEY_MM, alias: 'mm'},
								]
							);

							this.storage.set(
								Constants.KEY_SURFACE_AREA +
									Constants.DATABASE_VERSION,
								[
									{
										id: 1,
										key: Constants.KEY_IN2,
										alias: 'in2',
									},
									{
										id: 2,
										key: Constants.KEY_CM2,
										alias: 'cm2',
									},
									{
										id: 3,
										key: Constants.KEY_MM2,
										alias: 'mm2',
									},
								]
							);

							this.storage.set(
								Constants.KEY_SEED + Constants.DATABASE_VERSION,
								true
							);
							//after successful seeding set seed key as done and call load UI method
							resolve(true);
						}
					});
				});
		});
	}

	checkSeeding(): Promise<any> {
		return this.storage.ready().then(() => {
			return this.storage
				.get(Constants.KEY_SEED + Constants.DATABASE_VERSION)
				.then(seedVal => {
					return new Promise((resolve, reject) => {
						console.log(seedVal);
						if (seedVal !== null) {
							console.log('seeding done');
							//seeding done, call load UI method
							resolve(true);
						} else {
							resolve(false);
						}
					});
				});
		});
	}
}
