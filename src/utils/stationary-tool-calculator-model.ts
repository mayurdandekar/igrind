import { TranslateService } from '@ngx-translate/core';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import * as Constants from '../utils/constants';
import * as FirebaseConstants from '../utils/analytics-constants';
import { FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
export class stationaryToolCalculator {
    resultCalculatedLead: any;
    resultRecommendedLead: number;
    recommendedValue: any;
    resultCalculatedTraverse: number;
    resultRecommendedTraverse: any;
    resultRecommendedOverlapRatio: number;
    resultCalculatedOverlapRatio: any;
    productType: any;
    productTypekey: any;
    constructor(private storage: any, private firebaseAnalytics: FirebaseAnalytics) {

    }

    calculate(unitPref, unitPrefType, unitPrefRate, rpm, contactWidth, rate, productType, recommendedLead, requiredFinish, productTypeListRefs) {
        console.log(productTypeListRefs)
        console.log(unitPref);
        console.log(unitPrefType);
        console.log(unitPrefRate);
        console.log(rpm);
        console.log(contactWidth);
        console.log(rate);
        console.log(productType);

        recommendedLead.forEach(recommendedLead => {
            if (recommendedLead.id == requiredFinish + 1) {
                this.recommendedValue = recommendedLead.value;
            }
        });

        productTypeListRefs.forEach(productType1 => {

            if (productType1.key == productType) {
                this.productType = productType1.value;
                this.productTypekey = productType1.key;
            }
        });

        // console.log()


        var calculateAndFetchLead: {} = this.calculateAndFetchLead(unitPref, rate, rpm, this.recommendedValue, unitPrefRate);
        var calculateAndFetchOverlapRatio: {} = this.calculateAndFetchOverlapRatio(unitPref, rate, rpm, this.recommendedValue, unitPrefRate, contactWidth);
        var calculateAndFetchTraverse: {} = this.calculateAndFetchTraverse(unitPref, rate, rpm, this.recommendedValue, unitPrefRate, contactWidth);






        var resultWheelDressing: {} = {
            "calculatedAndRecommendedLead": calculateAndFetchLead,
            "calculatedAndRecommendedOverlapRatio": calculateAndFetchOverlapRatio,
            "calculatedAndRecommendedTraverse": calculateAndFetchTraverse,
            "productType": this.productType,
            "productTypeKey": this.productTypekey,
            "requiredFinish": requiredFinish,
            "isImperial": unitPref,
            "isSinglePoint": unitPrefType,
            "isRateTraverse": unitPrefRate,
            "rpm":rpm,
            "rate":rate,
            "contactWidth":contactWidth
        };

        var resultOfToggles: {} = {
            "isImperial": unitPref,
            "isSinglePoint": unitPrefType,
            "isRateTraverse": unitPrefRate
        }
        console.log(resultWheelDressing)

        this.storage.set(Constants.KEY_STATIONARY, resultWheelDressing);
        this.storage.set(Constants.KEY_UNIT_PREF_STATIONARY_TOOL, resultOfToggles);

        return resultWheelDressing;
    }
    calculateAndFetchLead(isImperial, rate, rpm, recommendedValue, isRateTraverse) {
        console.log(recommendedValue)
        var resultCalculatedAndRecommendedLead: {};
        var resultRecommendedLead: number;
        if (isImperial) {
            if (isRateTraverse) {
                this.resultCalculatedLead = rate / rpm;
            } else {
                this.resultCalculatedLead = rate * rpm;
            }
            this.resultRecommendedLead = recommendedValue;
        } else {
            if (isRateTraverse) {
                this.resultCalculatedLead = rate / rpm;
            } else {
                this.resultCalculatedLead = rate * rpm;
            }
            this.resultRecommendedLead = recommendedValue;
        }
        resultCalculatedAndRecommendedLead = {
            "resultCalculatedLead": this.resultCalculatedLead,
            "resultRecommendedLead": this.resultRecommendedLead
        };
        console.log(resultCalculatedAndRecommendedLead);
        return resultCalculatedAndRecommendedLead;
    }
    calculateAndFetchOverlapRatio(isImperial, rate, rpm, recommendedValue, isRateTraverse, contactWidth) {
        console.log(recommendedValue)
        var resultCalculatedAndRecommendedOverlapRatio: {};
        var resultRecommendedOverlapRatio: number;
        if (isImperial) {
            if (isRateTraverse) {
                this.resultCalculatedOverlapRatio = contactWidth * rpm / rate;
            } else {
                this.resultCalculatedOverlapRatio = contactWidth / rate;
            }
            this.resultRecommendedOverlapRatio = contactWidth / recommendedValue;
        } else {
            if (isRateTraverse) {
                this.resultCalculatedOverlapRatio = contactWidth * rpm / rate;
            } else {
                this.resultCalculatedOverlapRatio = contactWidth / rate;
            }
            this.resultRecommendedOverlapRatio = contactWidth / recommendedValue;
        }
        resultCalculatedAndRecommendedOverlapRatio = {
            "resultCalculatedOverlapRatio": this.resultCalculatedOverlapRatio,
            "resultRecommendedOverlapRatio": this.resultRecommendedOverlapRatio
        };
        console.log(resultCalculatedAndRecommendedOverlapRatio);
        return resultCalculatedAndRecommendedOverlapRatio;
    }
    calculateAndFetchTraverse(isImperial, rate, rpm, recommendedValue, isRateTraverse, contactWidth) {
        console.log(recommendedValue)
        var resultCalculatedAndRecommendedTraverse: {};
        var resultRecommendedTraverse: number;
        if (isImperial) {
            if (isRateTraverse) {
                this.resultRecommendedTraverse = contactWidth * rpm / this.resultRecommendedOverlapRatio;
            } else {
                this.resultRecommendedTraverse = contactWidth * rpm / this.resultRecommendedOverlapRatio;
            }
            this.resultCalculatedTraverse = rate;
        } else {
            if (isRateTraverse) {
                this.resultRecommendedTraverse = contactWidth * rpm / this.resultRecommendedOverlapRatio;
            } else {
                this.resultRecommendedTraverse = contactWidth * rpm / this.resultRecommendedOverlapRatio;
            }
            this.resultCalculatedTraverse = rate;
        }
        resultCalculatedAndRecommendedTraverse = {
            "resultCalculatedTraverse": this.resultCalculatedTraverse,
            "resultRecommendedTraverse": this.resultRecommendedTraverse
        };
        console.log(resultCalculatedAndRecommendedTraverse);
        return resultCalculatedAndRecommendedTraverse;
    }











    productTypeEvents(productType) {
        console.log(productType);
        switch (productType) {
            case Constants.KEY_CONVENTIONAL_ABRASIVES:
                this.firebaseAnalytics.logEvent(FirebaseConstants.STATIONARY_TOOL_CALC_PRODUCTTYPE_CONVENTIONAL_ABRASIVES_HITCOUNT, {});
                break;
            case Constants.KEY_CERAMIC_ABRASIVES:
                this.firebaseAnalytics.logEvent(FirebaseConstants.STATIONARY_TOOL_CALC_PRODUCTTYPE_CERAMIC_ABRASIVES_HITCOUNT, {});
                break;
            case Constants.KEY_VIT_CBN:
                this.firebaseAnalytics.logEvent(FirebaseConstants.STATIONARY_TOOL_CALC_PRODUCTTYPE_VIT_CBN_HITCOUNT, {});
                break;
            default:
                break;
        }
    }
    triggerFirebaseEvent(keyError) {
        console.log(keyError);
        if (keyError === "rpm_eng" || keyError === "rpm_metric") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.STATIONARY_TOOL_CALC_RPM_HITCOUNT, {});
        } else if (keyError === "contactWidth_eng" || keyError === "contactWidth_metric") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.STATIONARY_TOOL_CALC_CONTACT_WIDTH_HITCOUNT, {});
        } else if (keyError === "rateTR_eng" || keyError === "rateTR_metric" || keyError === "rateDL_eng" || keyError === "rateDL_metric") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.STATIONARY_TOOL_CALC_RATE_HITCOUNT, {});
        }
    }


}
