export const CONVERSION_CALCULATOR = "Conversion Calculators";
export const GRIND_PARAMETERS = "Grinding Parameter Calculator";
export const DRESSING_PARAMETERS = "Dressing Parameter Calculator";
export const CUTTING_PARAMETERS = "Cutting Parameter Calculator";


export const WHEEL_SPEED_CALC_HITCOUNT = "Wheel_Speed_Calc_HitCount";
export const COOLANT_CALC_HITCOUNT = "Coolant_Calc_HitCount";
export const DIAMOND_ROLL_CALC_HITCOUNT = "Diamond_Roll_Calc_HitCount";
export const WHEEL_SPEED_CALC_OOR_SPEED_HITCOUNT = "Wheel_Speed_Calc_OOR_Speed_HitCount";
export const WHEEL_SPEED_CALC_OOR_DIAMETER_HITCOUNT = "Wheel_Speed_Calc_OOR_Diameter_HitCount";
export const WHEEL_SPEED_CALC_INFO_TOP_HITCOUNT = "Wheel_Speed_Calc_Info_Top_HitCount";
export const DIAMOND_ROLL_CALC_OOR_DIAMONDROLL_DIAMETER_HITCOUNT = "D_Roll_Calc_OOR_DR_Diameter_HitCount";
export const DIAMOND_ROLL_CALC_OOR_DIAMONDROLL_WIDTH_HITCOUNT = "D_Roll_Calc_OOR_DR_Width_HitCount";
export const DIAMOND_ROLL_CALC_OOR_DIAMONDROLL_SPEED_HITCOUNT = "D_Roll_Calc_OOR_DR_Speed_HitCount";
export const DIAMOND_ROLL_CALC_OOR_WHEELPARAMETERS_DIAMETER_HITCOUNT = "D_Roll_Calc_OOR_WP_Diameter_HitCount";
export const DIAMOND_ROLL_CALC_OOR_WHEELPARAMETERS_SPEED_HITCOUNT = "D_Roll_Calc_OOR_WP_Speed_HitCount";
export const DIAMOND_ROLL_CALC_OOR_WHEELPARAMETERS_TRAVERSERATE_HITCOUNT = "D_Roll_Calc_OOR_WP_TraverseRate_HitCount";
export const DIAMOND_ROLL_CALC_PRODUCTTYPE_RESIN_HITCOUNT = "D_Roll_Calc_PT_Resin_HitCount";
export const DIAMOND_ROLL_CALC_PRODUCTTYPE_VITRIFIED_HITCOUNT = "D_Roll_Calc_PT_Vitrified_HitCount";
export const DIAMOND_ROLL_CALC_PRODUCTTYPE_GFORCE_HITCOUNT = "D_Roll_Calc_PT_Gforce_HitCount";
export const DIAMOND_ROLL_CALC_PRODUCTTYPE_PARADIGM_HITCOUNT = "D_Roll_Calc_PT_Paradigm_HitCount";
export const DIAMOND_ROLL_CALC_PRODUCTTYPE_VITCBN_HITCOUNT = "D_Roll_Calc_PT_vitcBN_HitCount";
export const DIAMOND_ROLL_CALC_PRODUCTTYPE_VITDIAMOND_HITCOUNT = "D_Roll_Calc_PT_vitDiamond_HitCount";
export const DIAMOND_ROLL_CALC_INFO_TOP_HITCOUNT = "D_Roll_Calc_Info_Top_HitCount";
export const DIAMOND_ROLL_CALC_INFO_RES_SPEEDRATIO_HITCOUNT = "D_Roll_Calc_Info_Res_SRatio_HitCount";
export const DIAMOND_ROLL_CALC_INFO_RES_TRAVERSERATE_HITCOUNT = "D_Roll_Calc_Info_Res_TRate_HitCount";
export const DIAMOND_ROLL_CALC_INFO_RES_SFPMWHEEL_HITCOUNT = "D_Roll_Calc_Info_Res_SfpmWheel_HitCount";
export const DIAMOND_ROLL_CALC_INFO_RES_OVERLAPRATIO_HITCOUNT = "D_Roll_Calc_Info_Res_ORatio_HitCount";
export const DIAMOND_ROLL_CALC_INFO_RES_MAXDRESSDEPTH_HITCOUNT = "D_Roll_Calc_Info_Res_MDDepth_HitCount";
export const COOLANT_CALC_OOR_GRINDINGWHEELVELOCITY_VALUE = "Coolant_Calc_OOR_GWheelVelocity_Value";
export const COOLANT_CALC_OOR_PRESSURE_VALUE = "Coolant_Calc_OOR_Pressure_Value";
export const COOLANT_CALC_OOR_GRINDINGPOWER_VALUE = "Coolant_Calc_OOR_GrindingPower_Value";
export const COOLANT_CALC_OOR_COOLANTFLOWRATE_VALUE = "Coolant_Calc_OOR_CoolantFlowrate_Value";
export const COOLANT_CALC_OOR_GRINDINGWHEELCONTACTWIDTH_VALUE = "Coolant_Calc_OOR_GWContactWidth_Value";
export const COOLANT_CALC_COOLANTTYPE_SYNTHETICOIL_HITCOUNT = "Coolant_Calc_CType_SyntheticOil_HitCount";
export const COOLANT_CALC_COOLANTTYPE_MINERALOIL_HITCOUNT = "Coolant_Calc_CType_MineralOil_HitCount";
export const COOLANT_CALC_COOLANTTYPE_WATERSYNTHETICOIL_HITCOUNT = "Coolant_Calc_CType_WSynthOil_HitCount";
export const COOLANT_CALC_COOLANTTYPE_WATERSOLUBLESYNTHETIC_HITCOUNT = "Coolant_Calc_CType_WSolSynth_HitCount";
export const COOLANT_CALC_GRINDINGWHEELVELOCITY_HITCOUNT = "Coolant_Calc_GWheelVelocity_HitCount";
export const COOLANT_CALC_PRESSURE_HITCOUNT = "Coolant_Calc_Pressure_HitCount";
export const COOLANT_CALC_GRINDINGPOWER_HITCOUNT = "Coolant_Calc_GrindingPower_HitCount";
export const COOLANT_CALC_COOLANTFLOWRATE_HITCOUNT = "Coolant_Calc_CoolantFlowrate_HitCount";
export const COOLANT_CALC_INFO_TOP_HITCOUNT = "Coolant_Calc_Info_Top_HitCount";
export const DRESSING_CALC_INFO_TOP_HITCOUNT = "Dressing_Calc_Info_Top_HitCount";
export const COOLANT_CALCULATOR = "Coolant Calculator";
export const DIAMOND_DRESSING_CALCULATOR = "Diamond Dressing Calculator";
export const WHEEL_SPEED_CONVERTER = "Wheel Speed Converter";
export const KERF_LOSS_CALCULATOR = "Kerf Loss Calculator";
export const HOME_SCREEN = "Home Page";
export const TERMS_AND_CONDITIONS = "Terms and Conditions";

export const PLUNGE_DRESSING_CALCULATOR = "Plunge Dressing Calculator";
export const PLUNGE_DRESSING_CALC_HITCOUNT = "Plunge_Dressing_Calc_HitCount";
export const PLUNGE_DRESSING_CALC_INFO_TOP_HITCOUNT = "Plunge_Dressing_Calc_Info_Top_HitCount";
export const PLUNGE_DRESSING_CALC_PRODUCTTYPE_CONVENTIONAL_VITRIFIED_HITCOUNT = "P_DRESS_Calc_PT_Con_Vit_HitCount";
export const PLUNGE_DRESSING_CALC_PRODUCTTYPE_CERAMICL_VITRIFIED_HITCOUNT = "P_DRESS_Calc_PT_Cer_Vit_HitCount";
export const PLUNGE_DRESSING_CALC_PRODUCTTYPE_VIT_CBN_HITCOUNT = "P_DRESS_Calc_PT_Vit_Cbn_HitCount";
export const PLUNGE_DRESSING_CALC_PRODUCTTYPE_VIT_DIAMOND_HITCOUNT = "P_DRESS_Calc_PT_Vit_Dia_HitCount";
export const PLUNGE_DRESSING_CALC_PRODUCTTYPE_PARADIGM_DIAMOND_HITCOUNT = "P_DRESS_Calc_PT_Par_Dia_HitCount";
export const PLUNGE_DRESSING_CALC_DRESSROLL_DIAMETER_HITCOUNT = "P_DRESS_Calc_DR_Dr_HitCount";
export const PLUNGE_DRESSING_CALC_DRESSROLL_RPM_HITCOUNT = "P_DRESS_Calc_DR_Rpm_HitCount";
export const PLUNGE_DRESSING_CALC_DRESSROLL_IR_HITCOUNT = "P_DRESS_Calc_DR_Ir_HitCount";
export const PLUNGE_DRESSING_CALC_WHEEL_DIAMETER_HITCOUNT = "P_DRESS_Calc_WH_Diameter_HitCount";
export const PLUNGE_DRESSING_CALC_WHEEL_RPM_HITCOUNT = "P_DRESS_Calc_WH_Rpm_HitCount";

export const STATIONARY_TOOL_CALCULATOR = "Stationary Tool Calculator";
export const STATIONARY_TOOL_CALC_HITCOUNT = "Stationary_Tool_Calc_HitCount";
export const STATIONARY_TOOL_CALC_RPM_HITCOUNT = "S_Tool_Calc_rpm_HitCount";
export const STATIONARY_TOOL_CALC_CONTACT_WIDTH_HITCOUNT = "S_Tool_Calc_Contact_Width_HitCount";
export const STATIONARY_TOOL_CALC_RATE_HITCOUNT = "S_Tool_Calc_Rate_HitCount";
export const STATIONARY_TOOL_CALC_PRODUCTTYPE_CONVENTIONAL_ABRASIVES_HITCOUNT = "S_Tool_Calc_PT_Con_Abr_HitCount";
export const STATIONARY_TOOL_CALC_PRODUCTTYPE_CERAMIC_ABRASIVES_HITCOUNT = "S_Tool_Calc_PT_Cer_Abr_HitCount";
export const STATIONARY_TOOL_CALC_PRODUCTTYPE_VIT_CBN_HITCOUNT = "S_Tool_Calc_PT_Vit_Cbn_HitCount";
export const STATIONARY_TOOL_CALC_INFO_TOP_HITCOUNT = "S_Tool_Calc_Info_Top_HitCount";


export const LENGTH_CONVERTER_CALCULATOR = "Length Converter Calculator";
export const LENGTH_VISIT_HITCOUNT = "Length_Visit_HitCount";
export const VELOCITY_CONVERTER_CALCULATOR = "Velocity Converter Calculator";
export const VELOCITY_VISIT_HITCOUNT = "Velocity_Visit_HitCount";

export const QPRIME_CALCULATOR = "Q-Prime Calculator";
export const QPRIME_CALC_HITCOUNT = "QPrime_Calc_HitCount";
export const QPRIME_CALC_INFO_TOP_HITCOUNT = "QPrime_Calc_Info_Top_HitCount";
export const QPRIME_CALC_INP1_HITCOUNT = "QPrime_Calc_Info_Top_HitCount";
export const QPRIME_CALC_INP2_HITCOUNT = "QPrime_Calc_Info_Top_HitCount";

export const CHIP_THICKNESS_CALCULATOR = "Chip Thickness Calculator";
export const CHIP_THICKNESS_CALC_HITCOUNT = "Chip_Thickness_Calc_HitCount";
export const CHIP_THICKNESS_CALC_INFO_TOP_HITCOUNT = "Chip_Thickness_Calc_Info_Top_HitCount";
export const CHIP_THICKNESS_CALC_INP1_HITCOUNT = "Chip_Thickness_Calc_Inp1_HitCount";
export const CHIP_THICKNESS_CALC_INP2_HITCOUNT = "Chip_Thickness_Calc_Inp2_HitCount";
export const CHIP_THICKNESS_CALC_INP3_HITCOUNT = "Chip_Thickness_Calc_Inp3_HitCount";


export const AREA_CONVERSION_CALCULATOR = "Area Conversion Calculator";
export const AREA_CONVERSION_CALC_INFO_TOP_HITCOUNT = "Area_Conversion_Calc_Info_Top_HitCount";

export const ROUND_BAR_CALC_HITCOUNT = "Round_Tube_Calc_HitCount";
export const ROUND_BAR_CALC_INPUT_TYPE_HITCOUNT = "Round_Tube_Calc_Input_Type_HitCount";
export const ROUND_BAR_CALC_DIAMETER_HITCOUNT = "Round_Tube_Calc_Diameter_HitCount";
export const ROUND_BAR_CALC_INPUTYTPE_IN_HITCOUNT = "Ro_Bar_Calc_IT_In_HitCount";
export const ROUND_BAR_CALC_INPUTYTPE_CM_HITCOUNT = "Ro_Bar_Calc_Cm_HitCount";
export const ROUND_BAR_CALC_INPUTYTPE_MM_HITCOUNT = "Ro_Bar_Calc_Mm_HitCount";
export const ROUND_BAR_CALC_INPUTYTPE_FT_HITCOUNT = "Ro_Bar_Calc_Ft_HitCount";
export const ROUND_BAR_CALC_INPUTYTPE_M_HITCOUNT = "Ro_Bar_Calc_M_HitCount";


export const RECT_BAR_CALC_HITCOUNT = "Rect_Bar_Calc_HitCount";
export const RECT_BAR_CALC_INPUT_TYPE_HITCOUNT = "Rect_Bar_Calc_Input_Type_HitCount";
export const RECT_BAR_CALC_LENGTH_HITCOUNT = "Rect_Bar_Calc_Length_HitCount";
export const RECT_BAR_CALC_WIDTH_HITCOUNT = "Rect_Bar_Calc_Width_HitCount";
export const RECT_BAR_CALC_INPUTYTPE_IN_HITCOUNT = "Re_Bar_Calc_IT_In_HitCount";
export const RECT_BAR_CALC_INPUTYTPE_CM_HITCOUNT = "Re_Bar_Calc_IT_Cm_HitCount";
export const RECT_BAR_CALC_INPUTYTPE_MM_HITCOUNT = "Re_Bar_Calc_IT_Mm_HitCount";
export const RECT_BAR_CALC_INPUTYTPE_FT_HITCOUNT = "Re_Bar_Calc_IT_Ft_HitCount";
export const RECT_BAR_CALC_INPUTYTPE_M_HITCOUNT = "Re_Bar_Calc_IT_M_HitCount";

export const ROUND_TUBE_CALC_HITCOUNT = "Round_Tube_Calc_HitCount";
export const ROUND_TUBE_CALC_INPUT_TYPE_HITCOUNT = "Round_Tube_Calc_Input_Type_HitCount";
export const ROUND_TUBE_CALC_OUT_DIA_HITCOUNT = "Round_Tube_Calc_Out_Dia_HitCount";
export const ROUND_TUBE_CALC_IN_DIA_HITCOUNT = "Round_Tube_Calc_In_Dia_HitCount";
export const ROUND_TUBE_CALC_INPUTYTPE_IN_HITCOUNT = "Re_Tube_Calc_IT_IT_In_HitCount";
export const ROUND_TUBE_CALC_INPUTYTPE_CM_HITCOUNT = "Re_Tube_Calc_IT_Cm_HitCount";
export const ROUND_TUBE_CALC_INPUTYTPE_MM_HITCOUNT = "Re_Tube_Calc_IT_Mm_HitCount";
export const ROUND_TUBE_CALC_INPUTYTPE_FT_HITCOUNT = "Re_Tube_Calc_IT_Ft_HitCount";
export const ROUND_TUBE_CALC_INPUTYTPE_M_HITCOUNT = "Re_Tube_Calc_IT_M_HitCount";

export const RECT_TUBE_CALC_HITCOUNT = "Rect_Tube_Calc_HitCount";
export const RECT_TUBE_CALC_INPUT_TYPE_HITCOUNT = "Rect_Tube_Calc_Input_Type_HitCount";
export const RECT_TUBE_CALC_OUT_LEN_HITCOUNT = "Rect_Tube_Calc_Out_Len_HitCount";
export const RECT_TUBE_CALC_OUT_WID_HITCOUNT = "Rect_Tube_Calc_Out_Wid_HitCount";
export const RECT_TUBE_CALC_IN_LEN_HITCOUNT = "Rect_Tube_Calc_In_Len_HitCount";
export const RECT_TUBE_CALC_IN_WID_HITCOUNT = "Rect_Tube_Calc_In_Wid_HitCount";
export const RECT_TUBE_CALC_INPUTYTPE_IN_HITCOUNT = "Re_Tube_Calc_IT_In_HitCount";
export const RECT_TUBE_CALC_INPUTYTPE_CM_HITCOUNT = "Re_Tube_Calc_IT_Cm_HitCount";
export const RECT_TUBE_CALC_INPUTYTPE_MM_HITCOUNT = "Re_Tube_Calc_IT_Mm_HitCount";
export const RECT_TUBE_CALC_INPUTYTPE_FT_HITCOUNT = "Re_Tube_Calc_IT_Ft_HitCount";
export const RECT_TUBE_CALC_INPUTYTPE_M_HITCOUNT = "Re_Tube_Calc_IT_M_HitCount";












