import * as Constants from './constants';
import { FormGroup } from '@angular/forms';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import * as FirebaseConstants from '../utils/analytics-constants';
export class areaConversionModel {
    PI: number = 3.14159265358979;
    constructor(private storage: any, private firebaseAnalytics: FirebaseAnalytics) {
    }

    roundBarAreaValidator(fg: FormGroup) {
        let inputType = fg.get('inputRoundBarType').value;
        let roundBarDia = fg.get('roundBarDia').value;

        if (roundBarDia != "" && roundBarDia !== undefined && (isNaN(roundBarDia)) || roundBarDia == 0) {
            fg.get('roundBarDia').setErrors({ 'roundBarDia_empty': true });
        }
        if (inputType === undefined || inputType.length == 0 || inputType === "") {
            console.log("true input type error");
            fg.get('inputRoundBarType').setErrors({ 'inputRoundBarType_empty': true });
        }
        return null;
    }
    rectangularBarAreaValidator(fg: FormGroup) {
        // console.log(fg);

        let inputType = fg.get('inputRectangularBarType').value;
        let rectBarLength = fg.get('rectBarLength').value;
        let rectBarWidth = fg.get('rectBarWidth').value;
        console.log(inputType);
        console.log(rectBarLength);
        console.log(rectBarWidth);
        if (rectBarLength != "" && rectBarLength !== undefined && (isNaN(rectBarLength)) || rectBarLength == 0) {
            fg.get('rectBarLength').setErrors({ 'rectBarLength_empty': true });
        }
        if (rectBarWidth != "" && rectBarWidth !== undefined && (isNaN(rectBarWidth)) || rectBarWidth == 0) {
            fg.get('rectBarWidth').setErrors({ 'rectBarWidth_empty': true });
        }
        if (inputType === undefined || inputType.length == 0 || inputType === "") {
            console.log("error rect");
            fg.get('inputRectangularBarType').setErrors({ 'inputRectangularBarType_empty': true });
        }
        return null;
    }
    roundTubeAreaValidator(fg: FormGroup) {
        // console.log(fg);

        let inputType = fg.get('inputRoundTubeType').value;
        // roundTubeOutDia 
        let roundTubeOutDia = fg.get('roundTubeOutDia').value;
        let roundTubeInDia = fg.get('roundTubeInDia').value;
        console.log(inputType);
        console.log(roundTubeOutDia);
        console.log(roundTubeInDia);
        if (roundTubeOutDia != "" && roundTubeOutDia !== undefined && (isNaN(roundTubeOutDia)) || roundTubeOutDia == 0) {
            fg.get('roundTubeOutDia').setErrors({ 'roundTubeOutDia_empty': true });
        }
        if (roundTubeInDia != "" && roundTubeInDia !== undefined && (isNaN(roundTubeInDia)) || roundTubeInDia == 0) {
            fg.get('roundTubeInDia').setErrors({ 'roundTubeInDia_empty': true });
        }
        if (inputType === undefined || inputType.length == 0 || inputType === "") {
            fg.get('inputRoundTubeType').setErrors({ 'inputRoundTubeType_empty': true });
        }
        return null;
    }
    rectangularTubeAreaValidator(fg: FormGroup) {

        let inputType = fg.get('inputRectangularTubeType').value;
        let rectTubeOutLength = fg.get('rectTubeOutLength').value;
        let rectTubeOutWidth = fg.get('rectTubeOutWidth').value;
        let rectTubeInLength = fg.get('rectTubeInLength').value;
        let rectTubeInWidth = fg.get('rectTubeInWidth').value;

        console.log(inputType);
        console.log(rectTubeOutLength);
        console.log(rectTubeOutWidth);
        console.log(rectTubeInLength);
        console.log(rectTubeInWidth);

        if (rectTubeOutLength != "" && rectTubeOutLength !== undefined && (isNaN(rectTubeOutLength)) || rectTubeOutLength == 0) {
            fg.get('rectTubeOutLength').setErrors({ 'rectTubeOutLength_empty': true });
        }
        if (rectTubeOutWidth != "" && rectTubeOutWidth !== undefined && (isNaN(rectTubeOutWidth)) || rectTubeOutWidth == 0) {
            fg.get('rectTubeOutWidth').setErrors({ 'rectTubeOutWidth_empty': true });
        }
        if (rectTubeInLength != "" && rectTubeInLength !== undefined && (isNaN(rectTubeInLength)) || rectTubeInLength == 0) {
            fg.get('rectTubeInLength').setErrors({ 'rectTubeInLength_empty': true });
        }
        if (rectTubeInWidth != "" && rectTubeInWidth !== undefined && (isNaN(rectTubeInWidth)) || rectTubeInWidth == 0) {
            fg.get('rectTubeInWidth').setErrors({ 'rectTubeInWidth_empty': true });
        }

        if (inputType === undefined || inputType.length == 0 || inputType === "") {
            fg.get('inputRectangularTubeType').setErrors({ 'inputRectangularTubeType_empty': true });
        }
        return null;
    }

    triggerFirebaseEventRoundBar(keyError) {
        console.log(keyError);
        if (keyError === "inputRoundBarType_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_BAR_CALC_INPUT_TYPE_HITCOUNT, {});
        } else if (keyError === "roundBarDia_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_BAR_CALC_DIAMETER_HITCOUNT, {});
        }
    }
    triggerFirebaseEventRectBar(keyError) {
        console.log(keyError);
        if (keyError === "inputRectangularBarType_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_BAR_CALC_INPUT_TYPE_HITCOUNT, {});
        } else if (keyError === "rectBarLength_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_BAR_CALC_LENGTH_HITCOUNT, {});
        } else if (keyError === "rectBarWidth_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_BAR_CALC_WIDTH_HITCOUNT, {});
        }
    }
    triggerFirebaseEventRoundTube(keyError) {
        console.log(keyError);
        if (keyError === "inputRoundTubeType_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_TUBE_CALC_INPUT_TYPE_HITCOUNT, {});
        } else if (keyError === "roundTubeOutDia_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_TUBE_CALC_OUT_DIA_HITCOUNT, {});
        } else if (keyError === "roundTubeInDia_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_TUBE_CALC_IN_DIA_HITCOUNT, {});
        }
    }
    triggerFirebaseEventRectTube(keyError) {
        console.log(keyError);
        if (keyError === "inputRectangularTubeType_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_INPUT_TYPE_HITCOUNT, {});
        } else if (keyError === "rectTubeOutLength_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_OUT_LEN_HITCOUNT, {});
        } else if (keyError === "rectTubeOutWidth_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_OUT_WID_HITCOUNT, {});
        } else if (keyError === "rectTubeInLength_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_IN_LEN_HITCOUNT, {});
        } else if (keyError === "rectTubeInWidth_empty") {
            this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_IN_WID_HITCOUNT, {});
        }
    }

    productTypeEventsRoundBar(inputType) {
        console.log(inputType);
        switch (inputType) {
            case Constants.KEY_IN:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_BAR_CALC_INPUTYTPE_IN_HITCOUNT, {});
                break;
            case Constants.KEY_CM:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_BAR_CALC_INPUTYTPE_CM_HITCOUNT, {});
                break;
            case Constants.KEY_MM:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_BAR_CALC_INPUTYTPE_MM_HITCOUNT, {});
                break;
            case Constants.KEY_FT:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_BAR_CALC_INPUTYTPE_FT_HITCOUNT, {});
                break;
            case Constants.KEY_M:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_BAR_CALC_INPUTYTPE_M_HITCOUNT, {});
                break;
            default:
                break;
        }
    }
    productTypeEventsRectBar(inputType) {
        console.log(inputType);
        switch (inputType) {
            case Constants.KEY_IN:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_BAR_CALC_INPUTYTPE_IN_HITCOUNT, {});
                break;
            case Constants.KEY_CM:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_BAR_CALC_INPUTYTPE_CM_HITCOUNT, {});
                break;
            case Constants.KEY_MM:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_BAR_CALC_INPUTYTPE_MM_HITCOUNT, {});
                break;
            case Constants.KEY_FT:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_BAR_CALC_INPUTYTPE_FT_HITCOUNT, {});
                break;
            case Constants.KEY_M:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_BAR_CALC_INPUTYTPE_M_HITCOUNT, {});
                break;
            default:
                break;
        }
    }
    productTypeEventsRoundTube(inputType) {
        console.log(inputType);
        switch (inputType) {
            case Constants.KEY_IN:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_TUBE_CALC_INPUTYTPE_IN_HITCOUNT, {});
                break;
            case Constants.KEY_CM:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_TUBE_CALC_INPUTYTPE_CM_HITCOUNT, {});
                break;
            case Constants.KEY_MM:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_TUBE_CALC_INPUTYTPE_MM_HITCOUNT, {});
                break;
            case Constants.KEY_FT:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_TUBE_CALC_INPUTYTPE_FT_HITCOUNT, {});
                break;
            case Constants.KEY_M:
                this.firebaseAnalytics.logEvent(FirebaseConstants.ROUND_TUBE_CALC_INPUTYTPE_M_HITCOUNT, {});
                break;
            default:
                break;
        }
    }
    productTypeEventsRectTube(inputType) {
        console.log(inputType);

        switch (inputType) {
            case Constants.KEY_IN:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_INPUTYTPE_IN_HITCOUNT, {});
                break;
            case Constants.KEY_CM:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_INPUTYTPE_CM_HITCOUNT, {});
                break;
            case Constants.KEY_MM:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_INPUTYTPE_MM_HITCOUNT, {});
                break;
            case Constants.KEY_FT:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_INPUTYTPE_FT_HITCOUNT, {});
                break;
            case Constants.KEY_M:
                this.firebaseAnalytics.logEvent(FirebaseConstants.RECT_TUBE_CALC_INPUTYTPE_M_HITCOUNT, {});
                break;
            default:
                break;
        }
    }

    //RoundBarCalculation
    calculateRoundBar(inputType, roundBarDia) {
        var resultAreaRoundBar: {} = this.fetchAreaRoundBar(inputType, roundBarDia);
        var resultToLoadDbRoundBar: {} = {
            "inputType": inputType,
            "roundBarDia": roundBarDia
        }
        this.storage.set("roundbardb", resultToLoadDbRoundBar);
        return resultAreaRoundBar;
    }
    fetchAreaRoundBar(inputType, roundBarDia) {
        return this.switchInputTypeRoundBar(inputType, ["mm", "cm", "in", "m", "ft"], roundBarDia);
    }
    switchInputTypeRoundBar(opt, outPutLabel, roundBarDia) {
        switch (opt) {
            case "in":
                return this.inToOtherRoundBar(outPutLabel, roundBarDia);
            case "cm":
                return this.cmToOtherRoundBar(outPutLabel, roundBarDia);
            case "mm":
                return this.mmToOtherRoundBar(outPutLabel, roundBarDia);
            case "ft":
                return this.ftToOtherRoundBar(outPutLabel, roundBarDia);
            case "m":
                return this.mToOtherRoundBar(outPutLabel, roundBarDia);
        }
    }
    inToOtherRoundBar(outPutLabel, roundBarDia) {
        var b = [];
        var formula;
        outPutLabel.forEach(data => {
            if (data == "mm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2) * 645.16;
            } else if (data == "cm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2) * 6.452;
            } else if (data == "in") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2);
            } else if (data == "m") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) / 1550.003;
            } else if (data == "ft") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2) / 144;
            }
            var resultObjectRoundBar = {
                "label": data,
                "value": formula.toFixed(3)
            };
            b.push(resultObjectRoundBar)
        });
        return b;
    }
    cmToOtherRoundBar(outPutLabel, roundBarDia) {
        var b = [];
        var formula;
        outPutLabel.forEach(data => {
            if (data == "mm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2) * 100;
            } else if (data == "cm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2);
            } else if (data == "in") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) / 6.452;
            } else if (data == "m") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) / 10000;
            } else if (data == "ft") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) / 929.03;
            }
            var resultObjectRoundBar = {
                "label": data,
                "value": formula.toFixed(3)
            };
            b.push(resultObjectRoundBar)
        });
        return b;
    }
    mmToOtherRoundBar(outPutLabel, roundBarDia) {
        var b = [];
        var formula;
        outPutLabel.forEach(data => {
            if (data == "mm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2);
            } else if (data == "cm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2) / 100;
            } else if (data == "in") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) / 645.16;
            } else if (data == "m") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) / 1000000;
            } else if (data == "ft") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) / 92903.04;
            }
            var resultObjectRoundBar = {
                "label": data,
                "value": formula.toFixed(3)
            };
            b.push(resultObjectRoundBar)
        });
        return b;
    }
    ftToOtherRoundBar(outPutLabel, roundBarDia) {
        var b = [];
        var formula;
        outPutLabel.forEach(data => {
            if (data == "mm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2) * 92903.04;
            } else if (data == "cm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2) * 929.03;
            } else if (data == "in") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) * 144;
            } else if (data == "m") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) / 10.764;
            } else if (data == "ft") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2));
            }
            var resultObjectRoundBar = {
                "label": data,
                "value": formula.toFixed(3)
            };
            b.push(resultObjectRoundBar)
        });
        return b;
    }
    mToOtherRoundBar(outPutLabel, roundBarDia) {
        var b = [];
        var formula: any;
        outPutLabel.forEach(data => {
            if (data == "mm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2) * 1000000;
            } else if (data == "cm") {
                formula = this.PI * Math.pow(roundBarDia / 2, 2) * 10000;
            } else if (data == "in") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) / 1550.003;
            } else if (data == "m") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2));
            } else if (data == "ft") {
                formula = (this.PI * Math.pow(roundBarDia / 2, 2)) * 10.764;
            }
            var resultObjectRoundBar = {
                "label": data,
                "value": formula.toFixed(3)
            };
            b.push(resultObjectRoundBar)
        });
        return b;
    }

    //RectangularBarCalculation
    calculateRectangularBar(inputType, rectBarLength, rectBarWidth) {
        var resultAreaRectangularBar: {} = this.fetchAreaRectangularBar(inputType, rectBarLength, rectBarWidth);
        var resultToLoadDbRectBar: {} = {
            "inputType": inputType,
            "rectBarLength": rectBarLength,
            "rectBarWidth": rectBarWidth
        }
        this.storage.set("rectbardb", resultToLoadDbRectBar);
        return resultAreaRectangularBar;
    }
    fetchAreaRectangularBar(inputType, rectBarLength, rectBarWidth) {
        return this.switchInputTypeRectBar(inputType, ["cm", "mm", "in", "ft", "m"], rectBarLength, rectBarWidth);
    }
    switchInputTypeRectBar(opt, outPutLabel, rectBarLength, rectBarWidth) {
        switch (opt) {
            case "in":
                return this.inToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth);
            case "cm":
                return this.cmToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth);
            case "mm":
                return this.mmToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth);
            case "ft":
                return this.ftToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth);
            case "m":
                return this.mToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth);
        }
    }
    inToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth) {
        var b = [];
        var formula;
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (rectBarLength * rectBarWidth) * 6.452;
            } else if (data == "mm") {
                formula = (rectBarLength * rectBarWidth) * 645.16;
            } else if (data == "in") {
                formula = (rectBarLength * rectBarWidth);
            } else if (data == "ft") {
                formula = (rectBarLength * rectBarWidth) / 144;
            } else if (data == "m") {
                formula = (rectBarLength * rectBarWidth) / 1550.003;
            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    cmToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth) {
        var b = [];
        var formula;
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (rectBarLength * rectBarWidth);
            } else if (data == "mm") {
                formula = (rectBarLength * rectBarWidth) * 100;
            } else if (data == "in") {
                formula = (rectBarLength * rectBarWidth) / 6.452;
            } else if (data == "ft") {
                formula = (rectBarLength * rectBarWidth) / 929.03;
            } else if (data == "m") {
                formula = (rectBarLength * rectBarWidth) / 10000;
            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    mmToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth) {
        var b = [];
        var formula;
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (rectBarLength * rectBarWidth) / 100;
            } else if (data == "mm") {
                formula = (rectBarLength * rectBarWidth);
            } else if (data == "in") {
                formula = (rectBarLength * rectBarWidth) / 645.16;
            } else if (data == "ft") {
                formula = (rectBarLength * rectBarWidth) / 92903.04;
            } else if (data == "m") {
                formula = (rectBarLength * rectBarWidth) / 1000000;
            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    ftToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth) {
        var b = [];
        var formula;
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (rectBarLength * rectBarWidth) * 929.03;
            } else if (data == "mm") {
                formula = (rectBarLength * rectBarWidth) * 92903.04;
            } else if (data == "in") {
                formula = (rectBarLength * rectBarWidth) * 144;
            } else if (data == "ft") {
                formula = (rectBarLength * rectBarWidth);
            } else if (data == "m") {
                formula = (rectBarLength * rectBarWidth) / 10.764;
            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    mToOtherRectBar(outPutLabel, rectBarLength, rectBarWidth) {
        var b = [];
        var formula: any;
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (rectBarLength * rectBarWidth) * 10000;
            } else if (data == "mm") {
                formula = (rectBarLength * rectBarWidth) * 1000000;
            } else if (data == "in") {
                formula = (rectBarLength * rectBarWidth) * 1550.003;
            } else if (data == "ft") {
                formula = (rectBarLength * rectBarWidth) * 10.764;
            } else if (data == "m") {
                formula = (rectBarLength * rectBarWidth) ;
            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    //Round Tube
    calculateRoundTube(inputType, roundTubeOutDia, roundTubeInDia) {
        var resultAreaRectangularBar: {} = this.fetchAreaRoundTube(inputType, roundTubeOutDia, roundTubeInDia);
        var resultToLoadDbRoundTube: {} = {
            "inputType": inputType,
            "roundTubeOutDia": roundTubeOutDia,
            "roundTubeInDia": roundTubeInDia
        }
        this.storage.set("roundtubedb", resultToLoadDbRoundTube);
        return resultAreaRectangularBar;
    }
    fetchAreaRoundTube(inputType, roundTubeOutDia, roundTubeInDia) {
        return this.switchInputTypeRoundTube(inputType, ["cm", "in", "mm", "m", "ft"], roundTubeOutDia, roundTubeInDia);
    }
    switchInputTypeRoundTube(opt, outPutLabel, roundTubeOutDia, roundTubeInDia) {
        switch (opt) {
            case "in":
                return this.inToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia);
            case "cm":
                return this.cmToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia);
            case "mm":
                return this.mmToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia);
            case "ft":
                return this.ftToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia);
            case "m":
                return this.mToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia);
        }
    }
    inToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia) {
        var b = [];
        var formula;
        var commonFormula = (this.PI * Math.pow(roundTubeOutDia / 2, 2)) - (this.PI * Math.pow(roundTubeInDia / 2, 2));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula * 6.452);
            } else if (data == "mm") {
                formula = (commonFormula * 645.16);
            } else if (data == "in") {
                formula = (commonFormula);
            } else if (data == "ft") {
                formula = (commonFormula / 144);
            } else if (data == "m") {
                formula = (commonFormula / 1550.003);
            }
            var resultObjectRoundTube = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRoundTube)
        });
        return b;
    }
    cmToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia) {
        var b = [];
        var formula;
        var commonFormula = (this.PI * Math.pow(roundTubeOutDia / 2, 2)) - (this.PI * Math.pow(roundTubeInDia / 2, 2));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula);
            } else if (data == "mm") {
                formula = (commonFormula * 100);
            } else if (data == "in") {
                formula = (commonFormula / 6.452);
            } else if (data == "ft") {
                formula = (commonFormula / 929.03);
            } else if (data == "m") {
                formula = (commonFormula / 10000);
            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    mmToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia) {
        var b = [];
        var formula;
        var commonFormula = (this.PI * Math.pow(roundTubeOutDia / 2, 2)) - (this.PI * Math.pow(roundTubeInDia / 2, 2));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula / 100);

            } else if (data == "mm") {
                formula = (commonFormula);

            } else if (data == "in") {
                formula = (commonFormula / 645.16);

            } else if (data == "ft") {
                formula = (commonFormula / 92903.04);

            } else if (data == "m") {
                formula = (commonFormula / 1000000);

            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    ftToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia) {
        var b = [];
        var formula;
        var commonFormula = (this.PI * Math.pow(roundTubeOutDia / 2, 2)) - (this.PI * Math.pow(roundTubeInDia / 2, 2));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula * 929.03);

            } else if (data == "mm") {
                formula = (commonFormula * 92903.04);

            } else if (data == "in") {
                formula = (commonFormula * 144);

            } else if (data == "ft") {
                formula = (commonFormula);

            } else if (data == "m") {
                formula = (commonFormula / 10.764);

            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    mToOtherRoundTube(outPutLabel, roundTubeOutDia, roundTubeInDia) {
        var b = [];
        var formula: any;
        var commonFormula = (this.PI * Math.pow(roundTubeOutDia / 2, 2)) - (this.PI * Math.pow(roundTubeInDia / 2, 2));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula * 10000);
            } else if (data == "mm") {
                formula = (commonFormula * 1000000);
            } else if (data == "in") {
                formula = (commonFormula * 1550.003);
            } else if (data == "ft") {
                formula = (commonFormula * 10.764);
            } else if (data == "m") {
                formula = (commonFormula);
            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }

    //Rect Tube
    calculateRectTube(inputType, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth) {
        var resultAreaRectangularBar: {} = this.fetchAreaRectTube(inputType, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth);
        var resultToLoadDbRectTube: {} = {
            "inputType": inputType,
            "rectTubeOutLength": rectTubeOutLength,
            "rectTubeOutWidth": rectTubeOutWidth,
            "rectTubeInLength": rectTubeInLength,
            "rectTubeInWidth": rectTubeInWidth
        }
        this.storage.set("recttubedb", resultToLoadDbRectTube);
        return resultAreaRectangularBar;
    }
    fetchAreaRectTube(inputType, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth) {
        console.log(2);

        return this.switchInputTypeRectTube(inputType, ["cm", "in", "mm", "m", "ft"], rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth);
    }
    switchInputTypeRectTube(opt, outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth) {
        console.log(3);

        switch (opt) {
            case "in":
                return this.inToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth);
            case "cm":
                return this.cmToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth);
            case "mm":
                return this.mmToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth);
            case "ft":
                return this.ftToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth);
            case "m":
                return this.mToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth);
        }
    }
    inToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth) {
        var b = [];
        var formula;
        var commonFormula = ((rectTubeOutLength * rectTubeOutWidth) - (rectTubeInLength * rectTubeInWidth));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula * 6.452);
            } else if (data == "mm") {
                formula = (commonFormula * 645.16);
            } else if (data == "in") {
                formula = (commonFormula);
            } else if (data == "ft") {
                formula = (commonFormula / 144);
            } else if (data == "m") {
                formula = (commonFormula / 1550.003);
            }
            var resultObjectRoundTube = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRoundTube)
        });
        return b;
    }
    cmToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth) {
        var b = [];
        var formula;
        var commonFormula = ((rectTubeOutLength * rectTubeOutWidth) - (rectTubeInLength * rectTubeInWidth));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula);
            } else if (data == "mm") {
                formula = (commonFormula * 100);
            } else if (data == "in") {
                formula = (commonFormula / 6.452);
            } else if (data == "ft") {
                formula = (commonFormula / 929.03);
            } else if (data == "m") {
                formula = (commonFormula / 10000);
            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    mmToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth) {
        var b = [];
        var formula;
        var commonFormula = ((rectTubeOutLength * rectTubeOutWidth) - (rectTubeInLength * rectTubeInWidth));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula / 100);

            } else if (data == "mm") {
                formula = (commonFormula);

            } else if (data == "in") {
                formula = (commonFormula / 645.16);

            } else if (data == "ft") {
                formula = (commonFormula / 92903.04);

            } else if (data == "m") {
                formula = (commonFormula / 1000000);

            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    ftToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth) {
        var b = [];
        var formula;
        var commonFormula = ((rectTubeOutLength * rectTubeOutWidth) - (rectTubeInLength * rectTubeInWidth));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula * 929.03);

            } else if (data == "mm") {
                formula = (commonFormula * 92903.04);

            } else if (data == "in") {
                formula = (commonFormula * 144);

            } else if (data == "ft") {
                formula = (commonFormula);

            } else if (data == "m") {
                formula = (commonFormula / 10.764);

            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
    mToOtherRectTube(outPutLabel, rectTubeOutLength, rectTubeOutWidth, rectTubeInLength, rectTubeInWidth) {
        var b = [];
        var formula: any;
        var commonFormula = ((rectTubeOutLength * rectTubeOutWidth) - (rectTubeInLength * rectTubeInWidth));
        outPutLabel.forEach(data => {
            if (data == "cm") {
                formula = (commonFormula * 10000);
            } else if (data == "mm") {
                formula = (commonFormula * 1000000);
            } else if (data == "in") {
                formula = (commonFormula * 1550.003);
            } else if (data == "ft") {
                formula = (commonFormula * 10.764);
            } else if (data == "m") {
                formula = (commonFormula);
            }
            var resultObjectRectBar = {
                "label": data,
                "value": formula.toFixed(2)
            };
            b.push(resultObjectRectBar)
        });
        return b;
    }
}
