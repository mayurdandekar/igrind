import * as Constants from './constants';
import { FormGroup } from '@angular/forms';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import * as FirebaseConstants from '../utils/analytics-constants';
export class diamondRollDressingModel {

  resultObj: { [s: string]: string };

  speedRatioRecommeded: number;
  overlapRatioRecommeded: number;
  maxDressDepthRecommended: number;

  constructor(private storage: any, private firebaseAnalytics: FirebaseAnalytics) {
  }

  diamondRollDressingValidator(fg: FormGroup) {

    let isEnglish = !(fg.get('unitPref').value);
    let productType = fg.get('productType').value
    let diamondRollDiameter = fg.get('diamondRollDiameter').value;
    let diamondRollWidth = fg.get('diamondRollWidth').value;
    let diamondRollRPM = fg.get('diamondRollRPM').value;
    let wheelDiameter = fg.get('wheelDiameter').value;
    let wheelRPM = fg.get('wheelRPM').value;
    let wheelTraverseRate = fg.get('wheelTraverseRate').value;

    if (diamondRollDiameter=="") {
      fg.get('diamondRollDiameter').setErrors({ 'diamondRollDiameter_empty': true });
    }
    else if (diamondRollDiameter!="" && diamondRollDiameter != undefined && (diamondRollDiameter.toString().length > 9)) {
      fg.get('diamondRollDiameter').setErrors({ 'input_length': true });
    }
    if (diamondRollWidth=="") {
      fg.get('diamondRollWidth').setErrors({ 'diamondRollWidth_empty': true });
    }
      else if (diamondRollWidth!="" && diamondRollWidth != undefined && (diamondRollWidth.toString().length > 9)) {
      fg.get('diamondRollWidth').setErrors({ 'input_length': true });
    }
    if (diamondRollRPM=="") {
      fg.get('diamondRollRPM').setErrors({ 'diamondRollRPM_empty': true });
    }
      else if (diamondRollRPM!="" && diamondRollRPM != undefined && (diamondRollRPM.toString().length > 9)) {
      fg.get('diamondRollRPM').setErrors({ 'input_length': true });
    }
    if (wheelDiameter=="") {
      fg.get('wheelDiameter').setErrors({ 'wheelDiameter_empty': true });
    }
      else if (wheelDiameter!="" && wheelDiameter != undefined && (wheelDiameter.toString().length > 9)) {
      fg.get('wheelDiameter').setErrors({ 'input_length': true });
    }
    if (wheelRPM=="") {
      fg.get('wheelRPM').setErrors({ 'wheelRPM_empty': true });
    }
      else if (wheelRPM!="" && wheelRPM != undefined && (wheelRPM.toString().length > 9)) {
      fg.get('wheelRPM').setErrors({ 'input_length': true });
    }
    if (wheelTraverseRate=="") {
      fg.get('wheelTraverseRate').setErrors({ 'wheelTraverseRate_empty': true });
    }
      else if (wheelTraverseRate!="" && wheelTraverseRate != undefined && (wheelTraverseRate.toString().length > 9)) {
      fg.get('wheelTraverseRate').setErrors({ 'input_length': true });
    }

    if (isEnglish) {
     if (diamondRollDiameter=="") {
      fg.get('diamondRollDiameter').setErrors({ 'diamondRollDiameter_empty': true });
    }
    else if (diamondRollDiameter!="" && diamondRollDiameter !== undefined && (isNaN(diamondRollDiameter) || diamondRollDiameter==0)) {
        fg.get('diamondRollDiameter').setErrors({ 'diamondRollDiameter_eng': true });
      }
     
     if (diamondRollWidth=="") {
      fg.get('diamondRollWidth').setErrors({ 'diamondRollWidth_empty': true });
    }
    else if (diamondRollWidth!="" && diamondRollWidth !== undefined && (isNaN(diamondRollWidth)) || diamondRollWidth==0) {
        fg.get('diamondRollWidth').setErrors({ 'diamondRollWidth_eng': true });
      }
      
     if (diamondRollRPM=="") {
      fg.get('diamondRollRPM').setErrors({ 'diamondRollRPM_empty': true });
    }
    else if (diamondRollRPM!="" && diamondRollRPM !== undefined && (isNaN(diamondRollRPM)) || diamondRollRPM==0) {
        fg.get('diamondRollRPM').setErrors({ 'diamondRollRPM_eng': true });
      }
    
     if (wheelDiameter=="") {
      fg.get('wheelDiameter').setErrors({ 'wheelDiameter_empty': true });
    }
    else if (wheelDiameter!="" && wheelDiameter !== undefined && (isNaN(wheelDiameter)) || wheelDiameter==0) {
        fg.get('wheelDiameter').setErrors({ 'wheelDiameter_eng': true });
      }
    
     if (wheelRPM=="") {
      fg.get('wheelRPM').setErrors({ 'wheelRPM_empty': true });
    }
    else if (wheelRPM!="" && wheelRPM !== undefined && (isNaN(wheelRPM)) || wheelRPM==0) {
        fg.get('wheelRPM').setErrors({ 'wheelRPM_eng': true });
      }
    
     if (wheelTraverseRate=="") {
      fg.get('wheelTraverseRate').setErrors({ 'wheelTraverseRate_empty': true });
    }
    else if (wheelTraverseRate!="" && wheelTraverseRate !== undefined && (isNaN(wheelTraverseRate)) || wheelTraverseRate==0) {
        fg.get('wheelTraverseRate').setErrors({ 'wheelTraverseRate_eng': true });
      }
    
    }
     else {//if metric
     if (diamondRollDiameter=="") {
      fg.get('diamondRollDiameter').setErrors({ 'diamondRollDiameter_empty': true });
    }
    else if (diamondRollDiameter!="" && diamondRollDiameter !== undefined && (isNaN(diamondRollDiameter) || diamondRollDiameter==0)) {
        fg.get('diamondRollDiameter').setErrors({ 'diamondRollDiameter_metric': true });
      }
    if (diamondRollWidth=="") {
      fg.get('diamondRollWidth').setErrors({ 'diamondRollWidth_empty': true });
    }
    else if (diamondRollWidth!="" && diamondRollWidth !== undefined && (isNaN(diamondRollWidth)) || diamondRollWidth==0) {
        fg.get('diamondRollWidth').setErrors({ 'diamondRollWidth_metric': true });
      }
    if (diamondRollRPM=="") {
      fg.get('diamondRollRPM').setErrors({ 'diamondRollRPM_empty': true });
    }
    else if (diamondRollRPM!="" && diamondRollRPM !== undefined && (isNaN(diamondRollRPM)) || diamondRollRPM==0) {
        fg.get('diamondRollRPM').setErrors({ 'diamondRollRPM_eng': true });
      }
     if (wheelDiameter=="") {
      fg.get('wheelDiameter').setErrors({ 'wheelDiameter_empty': true });
    }
    else if (wheelDiameter!="" && wheelDiameter !== undefined && (isNaN(wheelDiameter)) || wheelDiameter==0) {
        fg.get('wheelDiameter').setErrors({ 'wheelDiameter_metric': true });
      }
     if (wheelRPM=="") {
      fg.get('wheelRPM').setErrors({ 'wheelRPM_empty': true });
    }
    else if (wheelRPM!="" && wheelRPM !== undefined && (isNaN(wheelRPM)) || wheelRPM==0) {
        fg.get('wheelRPM').setErrors({ 'wheelRPM_eng': true });
      }
     if (wheelTraverseRate=="") {
      fg.get('wheelTraverseRate').setErrors({ 'wheelTraverseRate_empty': true });
    }
    else if (wheelTraverseRate!="" && wheelTraverseRate !== undefined && (isNaN(wheelTraverseRate)) || wheelTraverseRate==0) {
        fg.get('wheelTraverseRate').setErrors({ 'wheelTraverseRate_metric': true });
      }
    }

    if (productType === undefined || productType.length == 0 || productType === "") {
      fg.get('productType').setErrors({ 'productType': true });
    }
    return null;
  }


  calculateDiamondRollDressing(isEnglishNotMetric: boolean, productType: string, diamondRollParameters: {}, wheelParameters: {}, speedRatioRecommendedList: Array<any>, overlapRatioRecommendedList: Array<any>, maxDressDepthRecommendedList: Array<any>) {
    var diamondRollDiameter: number = diamondRollParameters["diamondRollDiameter"];
    var diamondRollWidth: number = diamondRollParameters["diamondRollWidth"];
    var diamondRollRPM: number = diamondRollParameters["diamondRollRPM"];
    var wheelDiameter: number = wheelParameters["wheelDiameter"];
    var wheelTraverseRate: number = wheelParameters["wheelTraverseRate"];
    var wheelRPM: number = wheelParameters["wheelRPM"];


    speedRatioRecommendedList.forEach(speedRatio => {
      if (speedRatio.key == productType) {
        this.speedRatioRecommeded = speedRatio.value;
      }
    });

    overlapRatioRecommendedList.forEach(overlapRatio => {
      if (overlapRatio.key == productType) {
        this.overlapRatioRecommeded = overlapRatio.value;
      }
    });


    maxDressDepthRecommendedList.forEach(maxDressDepth => {

      if (isEnglishNotMetric) {
        if (maxDressDepth.key == productType + "english") {
          this.maxDressDepthRecommended = maxDressDepth.value;
        }
      } else {
        if (maxDressDepth.key == productType + "metric") {
          this.maxDressDepthRecommended = maxDressDepth.value;
        }
      }
    });

    var calculatedSFPMDiamondRoll: number = this.calculateSFPMDiamondRoll(isEnglishNotMetric, diamondRollDiameter, diamondRollRPM);
    var calculatedAndRecommendedSFPMWheel: {} = this.calculateAndFetchSFPMWheel(isEnglishNotMetric, wheelDiameter, wheelRPM, calculatedSFPMDiamondRoll);
    var calculatedAndRecommendedSpeedRatio: {} = this.calculateAndFetchSpeedRatio(isEnglishNotMetric, calculatedSFPMDiamondRoll, calculatedAndRecommendedSFPMWheel["resultCalculatedWheelSFPM"]);
    var calculatedAndRecommendedTraverseRate: {} = this.calculateAndFetchTraverseRate(isEnglishNotMetric, diamondRollWidth, 12, wheelRPM);
    var calculatedAndRecommendedOverlapRatio: {} = this.calculateAndFetchOverlapRatio(isEnglishNotMetric, diamondRollWidth, wheelTraverseRate, wheelRPM);
    var calculatedAndRecommendedMaxDressDepth: {} = this.fetchMaxDressDepth(isEnglishNotMetric);


    var resultDiamondRollDressing: {} = {
      "calculatedSFPMDiamondRoll": calculatedSFPMDiamondRoll, "calculatedAndRecommendedSFPMWheel": calculatedAndRecommendedSFPMWheel,
      "calculatedAndRecommendedSpeedRatio": calculatedAndRecommendedSpeedRatio, "calculatedAndRecommendedTraverseRate": calculatedAndRecommendedTraverseRate,
      "calculatedAndRecommendedOverlapRatio": calculatedAndRecommendedOverlapRatio, "calculatedAndRecommendedMaxDressDepth": calculatedAndRecommendedMaxDressDepth,
      "productType": productType, "inputDiamondRollParameters": diamondRollParameters, "inputWheelParameters": wheelParameters
    };

    this.storage.set(Constants.KEY_DIAMOND,resultDiamondRollDressing);
    this.storage.set(Constants.KEY_UNIT_PREF_DIAMOND_ROLL_DRESSING, !isEnglishNotMetric);

    return resultDiamondRollDressing;
  }

  calculateSFPMDiamondRoll(isEnglishNotMetric: boolean, diameterDiamondRoll: number, rpmDiamondRoll: number) {

    var sfpmDiamondRoll: number;

    if (isEnglishNotMetric) {
      // 1. Calculated SFPM diamond roll = ((input diameter for diamond roll)*(Input RPM for diamond roll))*0.2618
      // 2. Recommended SFPM diamond roll = NA
      sfpmDiamondRoll =(diameterDiamondRoll * rpmDiamondRoll) * 0.2618;
    } else {
      // 1. Calculated M/S diamond roll = =(3.14159*Diamond roll input diameter/25.4)/12*Diamond roll input RPM*12*0.0254/60
      // 2. Recommended SFPM diamond roll = NA
      sfpmDiamondRoll = (3.14159 * diameterDiamondRoll / 25.4) / 12 * rpmDiamondRoll * 12 * 0.0254 / 60
    }

    return sfpmDiamondRoll;
  }

  calculateAndFetchSFPMWheel(isEnglishNotMetric: boolean, inputWheelDiameter: number, inputWheelRPM: number, calculatedDiamondRollSFPM: number) {
    var resultCalculatedAndRecommendedSFPMWheel: {};
    var recommendedSpeedRatio: number = this.speedRatioRecommeded; //for conventional vitrified for English
    var resultCalculatedWheelSFPM: number;
    var resultRecommendedWheelSFPM: number;

    if (isEnglishNotMetric) {
      // 1. Calculated SFPM = (input diameter for selected product type) * (input RPM for selected product type) * 0.2618
      // 2. Recommended SFPM = (Calculated SFPM for diamond roll) / (recommended speed ratio for selected product type)

      resultCalculatedWheelSFPM = inputWheelDiameter * inputWheelRPM * 0.2618;
      resultRecommendedWheelSFPM = calculatedDiamondRollSFPM / recommendedSpeedRatio;


    } else {
      // 1. Calculated M/S = =(3.14159*wheel input diameter/25.4)/12*wheel input RPM*12*0.0254/60
      // 2. Recommended M/S = (Calculated M/S)/ (Recommended Wheel Speed ratio)
      resultCalculatedWheelSFPM = (3.14159 * inputWheelDiameter / 25.4) / 12 * inputWheelRPM * 12 * 0.0254 / 60;
      resultRecommendedWheelSFPM = calculatedDiamondRollSFPM / recommendedSpeedRatio;

    }
    resultCalculatedAndRecommendedSFPMWheel = {
      "resultCalculatedWheelSFPM": resultCalculatedWheelSFPM,
      "resultRecommendedWheelSFPM": Math.round(resultRecommendedWheelSFPM)
    };
    return resultCalculatedAndRecommendedSFPMWheel;
  }



  calculateAndFetchSpeedRatio(isEnglishNotMetric: boolean, calculatedDiamondRollSFPM: number, calculatedWheelSFPM: number) {

    var resultCalculatedSpeedRatio: number;
    var resultRecommendedSpeedRatio: number;
    var resultCalculatedAndRecommendedSpeedRatio: {};

    if (isEnglishNotMetric) {
      // 1. Calculated Speed ratio = (calculated diamond roll SFPM) / (calculated SFPM for a selected product type)
      // 2. Recommended speed ratio = Display the value from a set of pre-defined values
      resultRecommendedSpeedRatio = this.speedRatioRecommeded;

    } else {
      // 1. Calculated Speed ratio = (Diamond roll Calculated M/S)/(Wheel calculated M/S)
      // 2. Recommended speed ratio = Display the value from a set of pre-defined values
      resultRecommendedSpeedRatio = this.speedRatioRecommeded;
    }

    resultCalculatedSpeedRatio = calculatedDiamondRollSFPM / calculatedWheelSFPM;
    resultCalculatedAndRecommendedSpeedRatio = {
      "resultCalculatedSpeedRatio": resultCalculatedSpeedRatio,
      "resultRecommendedSpeedRatio": resultRecommendedSpeedRatio
    };

    return resultCalculatedAndRecommendedSpeedRatio;
  }

  calculateAndFetchTraverseRate(isEnglishNotMetric: boolean, inputDiamondRollWidth: number, recommendedWheelOverlapRatio: number, inputWheelRPM: number) {

    var resultCalculatedTraverseRate: number;
    var resultRecommendedTraverseRate: number;
    var resultCalculatedAndRecommendedTraverseRate: {};

    if (isEnglishNotMetric) {
      // 1. traverse rate = ((diamond roll width input) / (recommended overlap ratio for selected product type))* (RPM input for selected product type)
      // 2. Display recommended traverse rate
      recommendedWheelOverlapRatio = this.overlapRatioRecommeded;
      resultRecommendedTraverseRate = 0;
    } else {
      // 1. traverse rate = (Diamond roll  input width / recommended overlap ratio) * Wheel Input RPM
      // 2. Display recommended traverse rate =((input diamond roll width)/(recommended overlap ratio))*(input wheel RPM)
      recommendedWheelOverlapRatio = this.overlapRatioRecommeded;
      resultRecommendedTraverseRate = 0;
    }
    resultCalculatedTraverseRate = (inputDiamondRollWidth / recommendedWheelOverlapRatio) * inputWheelRPM;
    resultCalculatedAndRecommendedTraverseRate = {
      "resultCalculatedTraverseRate": resultCalculatedTraverseRate,
      "resultRecommendedTraverseRate": resultRecommendedTraverseRate
    };

    return resultCalculatedAndRecommendedTraverseRate;
  }

  calculateAndFetchOverlapRatio(isEnglishNotMetric: boolean, inputDiamondRollWidth: number, inputWheelTraverseRate: number, inputWheelRPM: number) {

    var resultCalculatedOverlapRatio: number;
    var resultRecommendedWheelOverlapRatio: number;
    var resultCalculatedAndRecommendedOverlapRatio: {};

    if (isEnglishNotMetric) {
      // 1. Calculated overlap ratio = (input diamond roll width)/((input traverse rate as per selected unit for selected product type)/ (input RPM for selected product type))
      // 2. Recommended overlap ratio = Recommended overlap ratio from a list of hardcoded values, for selected product type
      resultRecommendedWheelOverlapRatio = this.overlapRatioRecommeded;
    } else {
      // 1. Calculated overlap ratio = (Diamond roll input width) / (Input traverse rate / Wheel input RPM)
      // 2. Recommended overlap ratio = Recommended overlap ratio from a list of hardcoded values, for selected product type
      resultRecommendedWheelOverlapRatio = this.overlapRatioRecommeded;
    }
    resultCalculatedOverlapRatio = (inputDiamondRollWidth) / ((inputWheelTraverseRate) / (inputWheelRPM));
    resultCalculatedAndRecommendedOverlapRatio = {
      "resultCalculatedOverlapRatio": resultCalculatedOverlapRatio,
      "resultRecommendedWheelOverlapRatio": resultRecommendedWheelOverlapRatio
    };

    return resultCalculatedAndRecommendedOverlapRatio;
  }

  fetchMaxDressDepth(isEnglishNotMetric: boolean) {

    var resultCalculatedMaxDressDepth: number;
    var resultRecommendedMaxDressDepth: number;
    var resultCalculatedAndRecommendedMaxDressDepth: {};

    if (isEnglishNotMetric) {
      resultCalculatedMaxDressDepth = 0;
      resultRecommendedMaxDressDepth = this.maxDressDepthRecommended;
    } else {
      resultCalculatedMaxDressDepth = 0;
      resultRecommendedMaxDressDepth = this.maxDressDepthRecommended;
    }

    resultCalculatedAndRecommendedMaxDressDepth = {
      "resultCalculatedMaxDressDepth": resultCalculatedMaxDressDepth,
      "resultRecommendedMaxDressDepth": resultRecommendedMaxDressDepth
    };

    return resultCalculatedAndRecommendedMaxDressDepth;
  }

  triggerFirebaseEvent(keyError) {
    if(keyError === "diamondRollDiameter_eng" || keyError === "diamondRollDiameter_metric") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_OOR_DIAMONDROLL_DIAMETER_HITCOUNT,{});
    } else if(keyError === "diamondRollWidth_eng" || keyError === "diamondRollWidth_metric") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_OOR_DIAMONDROLL_WIDTH_HITCOUNT,{});
    } else if (keyError === "diamondRollRPM_eng") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_OOR_DIAMONDROLL_SPEED_HITCOUNT,{});
    } else if(keyError === "wheelDiameter_eng" || keyError === "wheelDiameter_metric"){
      this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_OOR_WHEELPARAMETERS_DIAMETER_HITCOUNT,{});
    } else if(keyError === "wheelRPM_eng") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_OOR_WHEELPARAMETERS_SPEED_HITCOUNT,{});
    } else if(keyError === "wheelTraverseRate_eng" || keyError === "wheelTraverseRate_metric") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_OOR_WHEELPARAMETERS_TRAVERSERATE_HITCOUNT,{});
    }
  }

  productTypeEvents(productType) {
    switch (productType) {
      case Constants.KEY_RESIN:
        this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_PRODUCTTYPE_RESIN_HITCOUNT, {});
        break;
      case Constants.KEY_VITRIFIED:
        this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_PRODUCTTYPE_VITRIFIED_HITCOUNT, {});
        break;
      case Constants.KEY_GFORCE_DIAMOND:
        this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_PRODUCTTYPE_GFORCE_HITCOUNT, {});
        break;
      case Constants.KEY_VIT_CBN:
        this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_PRODUCTTYPE_VITCBN_HITCOUNT, {});
        break;
      case Constants.KEY_PARADIGM:
        this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_PRODUCTTYPE_PARADIGM_HITCOUNT, {});
        break;
      case Constants.KEY_VIT_DIAMOND:
        this.firebaseAnalytics.logEvent(FirebaseConstants.DIAMOND_ROLL_CALC_PRODUCTTYPE_VITDIAMOND_HITCOUNT, {});
        break;

      default:
        break;
    }
  }

}
