import * as Constants from "./constants";
import { FormGroup } from "@angular/forms";
export class WheelSpeedConverterModel {
  resultObj: { [s: string]: string };
  constructor(private storage: any) {}

  wheelSpeedValidator(fg: FormGroup) {
    let wheelSpeedType = fg.get("wheelSpeedType").value;
    let wheelSpeed = fg.get("wheelSpeed").value;
    let isEnglish = !fg.get("unitPref").value;
    let diameter = fg.get("diameter").value;

    if (
      diameter != "" &&
      diameter != undefined &&
      diameter.toString().length > 11
    ) {
      fg.get("diameter").setErrors({ dia_eng: true });
    }
    if (
      wheelSpeed != "" &&
      wheelSpeed != undefined &&
      wheelSpeed.toString().length > 11
    ) {
      fg.get("wheelSpeed").setErrors({ wheelLength: true });
    }
    if (isEnglish) {
      if (diameter == "") {
        fg.get("diameter").setErrors({ dia_empty: true });
      } else if (diameter == 0) {
        fg.get("diameter").setErrors({ dia_metric: true });
      }
    } //isEnglish
    else {
      if (diameter == "") {
        fg.get("diameter").setErrors({ dia_empty: true });
      } else if (
        diameter != "" &&
        diameter !== undefined &&
        (isNaN(diameter) || diameter == 0)
      ) {
        fg.get("diameter").setErrors({ dia_metric: true });
      }
    }
    if (wheelSpeedType === undefined || wheelSpeedType.length == 0) {
      fg.get("wheelSpeed").setErrors({ wheelSpeedType: true });
    } else {
      if (wheelSpeed == "") {
        fg.get("wheelSpeed").setErrors({ wheel_empty: true });
      } else if (
        wheelSpeed != "" &&
        wheelSpeed !== undefined &&
        (isNaN(wheelSpeed) || wheelSpeed == 0)
      ) {
        fg.get("wheelSpeed").setErrors({ wheelSpeed: true });
      }
    }

    return null;
  }

  calculatewheelSpeed(
    isEnglish: boolean,
    wheelSpeedType: string,
    wheelDiameter: number,
    wheelSpeedValue: number
  ) {
    var wheelSpeedResult: {} = {};
    wheelSpeedResult["unitPref"] = !isEnglish;
    wheelSpeedResult["wheelSpeedType"] = wheelSpeedType;
    wheelSpeedResult["diameter"] = wheelDiameter;
    wheelSpeedResult["wheelSpeed"] = wheelSpeedValue;
    var calculatedSFPM;
    var calculatedRPM;
    var calculatedMPS;
    switch (wheelSpeedType) {
      case Constants.KEY_RPM:
        // type : RPM
        calculatedSFPM = this.calculateSFPMIfRPMSelected(
          isEnglish,
          wheelDiameter,
          wheelSpeedValue
        );
        calculatedMPS = this.calculateMPSIfRPMSelected(
          isEnglish,
          wheelDiameter,
          wheelSpeedValue
        );
        calculatedRPM = wheelSpeedValue;
        break;
      case Constants.KEY_SFPM:
        // type : SFPM
        calculatedRPM = this.calculateRPMIfSFPMSelected(
          isEnglish,
          wheelDiameter,
          wheelSpeedValue
        );
        calculatedMPS = this.calculateMPSIfSFPMSelected(
          isEnglish,
          wheelDiameter,
          wheelSpeedValue
        );
        calculatedSFPM = wheelSpeedValue;
        break;
      case Constants.KEY_MPS:
        // type : MPS
        calculatedRPM = this.calculateRPMIfMPSSelected(
          isEnglish,
          wheelDiameter,
          wheelSpeedValue
        );
        calculatedSFPM = this.calculateSFPMIfMPSSelected(
          isEnglish,
          wheelSpeedValue
        );
        calculatedMPS = wheelSpeedValue;
        break;

      default:
        break;
    }

    wheelSpeedResult[Constants.KEY_SFPM] = this.round(calculatedSFPM, 0);
    wheelSpeedResult[Constants.KEY_RPM] = this.round(calculatedRPM, 0);
    wheelSpeedResult[Constants.KEY_MPS] = this.round(calculatedMPS, 1);
    this.storage.set(Constants.KEY_WHEEL, wheelSpeedResult);
    this.storage.set(Constants.KEY_UNIT_PREF_WHEEL_SPEED, !isEnglish);
    return wheelSpeedResult;
  }

  calculateSFPMIfRPMSelected(
    isEnglish: boolean,
    wheelDiameter: number,
    wheelSpeedInRPM: number
  ) {
    /**
     *         1. SFPM = If(Wheel diameter(MM) == “ “) {
          (3.14159*(Wheel diameter(IN)))/12*(user inputs in RPM)
        }else {
        (3.14159*(Wheel diameter(MM))/25.4)/12*(user inputs in RPM))
        }
     */
    var sfpmIfRPMSelected;
    if (isEnglish) {
      // English calculations go here
      sfpmIfRPMSelected = ((3.14159 * wheelDiameter) / 12) * wheelSpeedInRPM;
    } else {
      // Metric calculations go here
      sfpmIfRPMSelected =
        ((3.14159 * wheelDiameter) / 25.4 / 12) * wheelSpeedInRPM;
    }
    return sfpmIfRPMSelected;
  }

  calculateMPSIfRPMSelected(
    isEnglish: boolean,
    wheelDiameter: number,
    wheelSpeedInRPM: number
  ) {
    /**
     * if(Wheel diameter(MM) == “ “){
          (3.14159*(Wheel diameter(IN)))/12*(user inputs in RPM)*12*0.0254/60
        } else {
          (3.14159*(Wheel diameter(MM))/25.4)/12*3600*12*0.0254/60)
        }
     *
     */
    var mpsIfRPMSelected;
    if (isEnglish) {
      // English calculations go here
      mpsIfRPMSelected =
        (((3.14159 * wheelDiameter) / 12) * wheelSpeedInRPM * 12 * 0.0254) / 60;
    } else {
      // Metric calculations go here
      mpsIfRPMSelected =
        (((3.14159 * wheelDiameter) / 25.4 / 12) *
          wheelSpeedInRPM *
          12 *
          0.0254) /
        60;
    }
    return mpsIfRPMSelected;
  }

  calculateRPMIfSFPMSelected(
    isEnglish: boolean,
    wheelDiameter: number,
    wheelSpeedInSFPM: number
  ) {
    /**
     *         1. RPM = if(Wheel diameter(MM) == “ “) {
      (user input for SFPM)/((3.1416*(Wheel diameter(IN)))/12)
      }else {
        (user input for SFPM)/((3.1416*(Wheel diameter(MM))/25.4)/12))
      }
     */
    var rpmIfSFPMSelected;
    if (isEnglish) {
      // English calculations go here
      rpmIfSFPMSelected = wheelSpeedInSFPM / ((3.1416 * wheelDiameter) / 12);
    } else {
      // Metric calculations go here
      rpmIfSFPMSelected =
        wheelSpeedInSFPM / ((3.1416 * wheelDiameter) / 25.4 / 12);
    }
    return rpmIfSFPMSelected;
  }

  calculateMPSIfSFPMSelected(
    isEnglish: boolean,
    wheelDiameter: number,
    wheelSpeedInSFPM: number
  ) {
    /**
     * MPS = if(Wheel diameter(MM) == “ “) {
          (3.14159*(Wheel diameter(IN)))/12*(Calculated RPM using user input for SFPM)*12*0.0254/60
        }else {
          (3.14159*(Wheel diameter(MM))/25.4)/12*(Calculated RPM using user input for SFPM)*12*0.0254/60)
        }
     */
    var mpsIfSFPMSelected;
    if (isEnglish) {
      // English calculations go here
      mpsIfSFPMSelected =
        (((3.14159 * wheelDiameter) / 12) *
          this.calculateRPMIfSFPMSelected(
            true,
            wheelDiameter,
            wheelSpeedInSFPM
          ) *
          12 *
          0.0254) /
        60;
    } else {
      // Metric calculations go here
      mpsIfSFPMSelected =
        (((3.14159 * wheelDiameter) / 25.4 / 12) *
          this.calculateRPMIfSFPMSelected(
            false,
            wheelDiameter,
            wheelSpeedInSFPM
          ) *
          12 *
          0.0254) /
        60;
    }
    return mpsIfSFPMSelected;
  }

  calculateSFPMIfMPSSelected(isEnglish: boolean, wheelSpeedInMPS: number) {
    /**
     * SFPM = ((User input in MPS) / 0.0254/12)*60
     */
    var sfpmIfMPSSelected;
    sfpmIfMPSSelected = (wheelSpeedInMPS / 0.0254 / 12) * 60;
    return sfpmIfMPSSelected;
  }

  calculateRPMIfMPSSelected(
    isEnglish: boolean,
    wheelDiameter: number,
    wheelSpeedInMPS: number
  ) {
    /**
     * RPM = If(Wheel diameter(MM) == “ “) {
      ((User input in MPS) / 0.0254/12*60)/((Wheel diameter(IN))*3.1416/12)
}else {
  ((User input in MPS) / 0.0254/12*60)/((Wheel diameter(MM))/25.4*3.1416/12)
}
     */

    var rpmIfMPSSelected;
    if (isEnglish) {
      // English calculations go here
      rpmIfMPSSelected =
        ((wheelSpeedInMPS / 0.0254 / 12) * 60) /
        ((wheelDiameter * 3.1416) / 12);
    } else {
      // Metric calculations go here
      rpmIfMPSSelected =
        ((wheelSpeedInMPS / 0.0254 / 12) * 60) /
        (((wheelDiameter / 25.4) * 3.1416) / 12);
    }
    return rpmIfMPSSelected;
  }

  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  }
}
