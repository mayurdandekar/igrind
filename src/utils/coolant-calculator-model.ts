import { CoolantInput } from '../pages/coolant-calculator/CoolantInput';
import { CoolantOutput } from '../pages/coolant-calculator/CoolantOutput';
import { GWVelocityPressure } from '../pages/coolant-calculator/CoolantOutput';
import { GrindingPowerAndCoolantFlowrate } from '../pages/coolant-calculator/CoolantOutput';
import { NozzleArea } from '../pages/coolant-calculator/CoolantOutput';
import { GrindingWheelContactWidth } from '../pages/coolant-calculator/CoolantOutput';
import { GrindingWheelContactHeight } from '../pages/coolant-calculator/CoolantOutput';
import { NozzleDiameter } from '../pages/coolant-calculator/CoolantOutput';
import { PumpPower } from '../pages/coolant-calculator/CoolantOutput';
import { Storage } from '@ionic/storage';
import * as Constants from '../utils/constants';

export class CoolantCalculatorModel {

  constructor(private storage: Storage) {
    // initialization code goes here
  }

  /**
   *
   * @param coolantParam "coolantType": 0,
                  "isGrindingWheelVelocityNotPressure": true,
                  "isSfpmNotMps":true,
                  "grindingWheelVelocityValue": 7500,
                  "isGrindingPowerNotFlowrate": true,
                  "isKwNotHp": false,
                  "grindingPowerValue": 35.0,
                  "grindingWheelContactWidth": 0.25,
                  "isMmNotIn":
   */
  calculateCoolantRequirements(coolantParam: CoolantInput) {
    // take specific gravity value depending upon the coolant type from enum
    var specificGravity = coolantParam.values.coolantType;
    var resultObj: CoolantOutput = <any>{};

    var grindingWheelVelocityAndPressure = this.calculateGrindingWheelVelocityAndPressure(coolantParam, specificGravity, coolantParam.unitPrefs.isGrindingWheelVelocityNotPressure);

    var grindingPowerAndCoolantFlowrate = this.calculateGrindingPowerAndCoolantFlowrate(coolantParam, coolantParam.unitPrefs.isGrindingPowerNotFlowrate);

    var nozzleArea = this.calculateNozzleArea(grindingWheelVelocityAndPressure["grindingWheelVelocityMPS"], grindingWheelVelocityAndPressure["grindingWheelVelocitySFPM"], grindingPowerAndCoolantFlowrate["coolantFlowrateIMin"], grindingPowerAndCoolantFlowrate["coolantFlowrateGPM"]);

    var nozzleWidth;
    if (coolantParam.unitPrefs["isMmNotIn"]) {
      nozzleWidth = this.calculateGrindingWheelContactWidth(0, coolantParam.values["grindingWheelContactWidth"]);
    } else {
      nozzleWidth = this.calculateGrindingWheelContactWidth(coolantParam.values["grindingWheelContactWidth"], 0);
    }

    var nozzleHeight = this.calculateGrindingWheelContactHeight(nozzleWidth["grindingWheelContactWidthIn"], nozzleArea["nozzleAreaIn"]);

    var nozzleDiameter = this.calculateNozzleDiameter(nozzleArea["nozzleAreaIn"], nozzleArea["nozzleAreaMM"]);

    var powerRequirements = this.calculatePumpPower(grindingPowerAndCoolantFlowrate["coolantFlowrateIMin"], grindingPowerAndCoolantFlowrate["coolantFlowrateGPM"], grindingWheelVelocityAndPressure["pressurePsi"], grindingWheelVelocityAndPressure["pressureBar"], specificGravity);

    resultObj["resultGWVelocityPressure"] = grindingWheelVelocityAndPressure;
    resultObj["resultGrindingPowerAndCoolantFlowrate"] = grindingPowerAndCoolantFlowrate;
    resultObj["resultGrindingWheelContactWidth"] = nozzleWidth;
    resultObj["resultGrindingWheelContactHeight"] = nozzleHeight;
    resultObj["resultNozzleDiameter"] = nozzleDiameter;
    resultObj["resultNozzleArea"] = nozzleArea;
    resultObj["resultPowerRequirements"] = powerRequirements;
    this.storage.set(Constants.COOLANT_INPUT, coolantParam);
    this.storage.set(Constants.COOLANT_OUTPUT, resultObj);
    return resultObj;
  }


  calculateGrindingWheelVelocityAndPressure(coolantParams: CoolantInput, specificGravity: number, isGrindingWheelVelocityNotPressure: boolean) {
    var grindingWheelVelocitySFPM;
    var grindingWheelVelocityMPS;
    var pressureBar;
    var pressurePsi;
    var isSfpmNotMps: boolean;
    var isBarNotPsi: boolean;
    //var resultGWVelocityPressure: {};
    var resultGWVelocityPressure: GWVelocityPressure;
    // check if isGrindingWheelVelocityNotPressure
    if (isGrindingWheelVelocityNotPressure) {
      // GrindingWheelVelocity is selected

      //check if SFPM or MPS
      isSfpmNotMps = coolantParams.unitPrefs["isSfpmNotMps"];
      if (isSfpmNotMps) {
        // SFPM input is available, hence calculate MPS, pressure bar and pressure psi
        grindingWheelVelocitySFPM = coolantParams.values["grindingWheelVelocityValue"];

        // IF((Input Jet speed (sfpm))>0 then (25.4/1000)*12*(Input Jet speed (sfpm))/60
        grindingWheelVelocityMPS = (25.4 / 1000) * 12 * grindingWheelVelocitySFPM / 60;

        // IF((Input Jet speed (sfpm))>0 then (specific gravity)*((Input Jet speed (sfpm))*12*0.0254/60)^2/200,
        pressureBar = specificGravity * Math.pow((grindingWheelVelocitySFPM * 12 * 0.0254 / 60), 2) / 200;

        //C6*(C9*12*0.0254/60)^2/200

        //IF((Input Jet speed (sfpm))>0 then (specific gravity)*(Input Jet speed (sfpm))^2/535824
        pressurePsi = specificGravity * Math.pow(grindingWheelVelocitySFPM, 2) / 535824;

        // make result dict
        resultGWVelocityPressure = {
          "grindingWheelVelocitySFPM": grindingWheelVelocitySFPM,
          "grindingWheelVelocityMPS": grindingWheelVelocityMPS,
          "pressureBar": pressureBar,
          "pressurePsi": pressurePsi
        };
      } else {
        // MPS input is available, hence calculate SFPM, pressure bar and pressure psi
        grindingWheelVelocityMPS = coolantParams.values["grindingWheelVelocityValue"];

        //IF((Input Jet speed (MPS))>0 then (Input Jet speed (MPS))*(1000*60/25.4)/12,
        grindingWheelVelocitySFPM = grindingWheelVelocityMPS * (1000 * 60 / 25.4) / 12;

        //IF((Input Jet speed (MPS))>0 then (specific gravity)*(Input Jet speed (MPS))^2/200,
        pressureBar = specificGravity * Math.pow(grindingWheelVelocityMPS, 2) / 200;

        // IF((Input Jet speed (MPS))>0 then (specific gravity)*((Input Jet speed)(MPS))*1/0.0254/12*60)^2/535824
        pressurePsi = specificGravity * Math.pow((grindingWheelVelocityMPS * 1 / 0.0254 / 12 * 60), 2) / 535824;

        // make result dict
        resultGWVelocityPressure = {
          "grindingWheelVelocitySFPM": grindingWheelVelocitySFPM,
          "grindingWheelVelocityMPS": grindingWheelVelocityMPS,
          "pressureBar": pressureBar,
          "pressurePsi": pressurePsi
        };
      }
    } else {
      // velocity is selected


      // check if bar or psi
      isBarNotPsi = coolantParams.unitPrefs["isBarNotPsi"];
      if (isBarNotPsi) {
        // pressure Bar is available, hence calculate pressure psi, velocity SFPM and velocity MPS
        pressureBar = coolantParams.values["grindingWheelVelocityValue"];

        // IF((Input Pressure (bar))>0 then (Input Pressure (bar))*14.50377
        pressurePsi = pressureBar * 14.50377;

        //IF((Input Pressure (bar))>0 then SQRT((535824*14.50377*(Input Pressure (bar)))/(specific gravity))
        grindingWheelVelocitySFPM = Math.sqrt((535824 * 14.50377 * (pressureBar)) / specificGravity);

        //IF((Input Pressure (bar))>0 then SQRT((200*(Input Pressure (bar)))/(specific gravity))
        grindingWheelVelocityMPS = Math.sqrt((200 * (pressureBar)) / (specificGravity));

        // make result dict
        resultGWVelocityPressure = {
          "grindingWheelVelocitySFPM": grindingWheelVelocitySFPM,
          "grindingWheelVelocityMPS": grindingWheelVelocityMPS,
          "pressureBar": pressureBar,
          "pressurePsi": pressurePsi
        };

      } else {
        // pressure psi is available, hence calculate pressure bar, velocity SFPM and velocity MPS
        pressurePsi = coolantParams.values["grindingWheelVelocityValue"];

        //IF((Input Pressure (psi))>0 then 1/14.50377*(Input Pressure (psi))
        pressureBar = 1 / 14.50377 * (pressurePsi);

        //IF((Input Pressure (psi))>0 then SQRT((535824*(Input Pressure (psi)))/(specific gravity)),
        grindingWheelVelocitySFPM = Math.sqrt((535824 * (pressurePsi)) / (specificGravity));

        //IF((Input Pressure (psi))>0 then SQRT(200*((Input Pressure (psi))/14.50377)/(specific gravity)),
        grindingWheelVelocityMPS = Math.sqrt(200 * pressurePsi / 14.50377 / specificGravity);

        // make result dict
        resultGWVelocityPressure = {
          "grindingWheelVelocitySFPM": grindingWheelVelocitySFPM,
          "grindingWheelVelocityMPS": grindingWheelVelocityMPS,
          "pressureBar": pressureBar,
          "pressurePsi": pressurePsi
        };
      }
    }
    return resultGWVelocityPressure;
  }

  calculateGrindingPowerAndCoolantFlowrate(coolantParams: CoolantInput, isGrindingPowerAndNotCoolantFlowrate: boolean) {

    var isKwNotHp: boolean;
    var isIMinNotGPM: boolean;
    var grindingPowerHP;
    var grindingPowerKW;
    var coolantFlowrateGPM;
    var coolantFlowrateIMin;
    //var resultGrindingPowerAndCoolantFlowrate: {};
    var resultGrindingPowerAndCoolantFlowrate: GrindingPowerAndCoolantFlowrate;
    // check if grinding power or coolant flowrate
    if (isGrindingPowerAndNotCoolantFlowrate) {
      // Grinding power is selected

      // check if KW or HP
      isKwNotHp = coolantParams.unitPrefs["isKwNotHp"];
      if (isKwNotHp) {
        // KW is available,
        //hence calculate Grinding power HP,
        //Coolant flowrate IMin, Coolant flowrate GPM

        grindingPowerKW = coolantParams.values["grindingPowerValue"];

        //IF((Grinding Power (Kw))>0 then (Grinding Power (Kw))/0.7457
        grindingPowerHP = grindingPowerKW / 0.7457;

        //IF((Grinding Power (Kw))>0 then 2*(Grinding Power (Kw))/0.7457,"")))))
        coolantFlowrateGPM = 2 * grindingPowerKW / 0.7457;

        //IF((Grinding Power (Kw))>0 then (Grinding Power (Kw))*10.151535
        coolantFlowrateIMin = grindingPowerKW * 10.151535;

        resultGrindingPowerAndCoolantFlowrate = {
          "grindingPowerKW": grindingPowerKW,
          "grindingPowerHP": grindingPowerHP,
          "coolantFlowrateGPM": coolantFlowrateGPM,
          "coolantFlowrateIMin": coolantFlowrateIMin
        };

      } else {
        // HP is available,
        //hence calculate Grinding power KW,
        //Coolant flowrate IMin, Coolant flowrate GPM

        grindingPowerHP = coolantParams.values["grindingPowerValue"];

        //IF((Grinding Power (Hp))>0 then (Grinding Power (Hp))*0.7457
        grindingPowerKW = grindingPowerHP * 0.7457;

        //IF((Grinding Power (Hp))>0 then (Grinding Power (Hp))*2
        coolantFlowrateGPM = grindingPowerHP * 2;

        //IF((Grinding Power (Hp))>0 then (Grinding Power (Hp))*0.7457*10.151535
        coolantFlowrateIMin = grindingPowerHP * 0.7457 * 10.151535;

        resultGrindingPowerAndCoolantFlowrate = {
          "grindingPowerKW": grindingPowerKW,
          "grindingPowerHP": grindingPowerHP,
          "coolantFlowrateGPM": coolantFlowrateGPM,
          "coolantFlowrateIMin": coolantFlowrateIMin
        };
      }


    } else {
      // coolant flowrate is selected

      // check if IMin or GPM
      isIMinNotGPM = coolantParams.unitPrefs["isIminNotGPM"];

      if (isIMinNotGPM) {
        // IMin is available,
        // hence calculate Coolant flowrate GPM,
        // Grinding power HP & Grinding power KW

        coolantFlowrateIMin = coolantParams.values["grindingPowerValue"];

        //IF((Coolant Flow (L/min))>0 then (Coolant Flow (L/min))/3.785
        coolantFlowrateGPM = coolantFlowrateIMin / 3.785;

        //IF((Coolant Flow (L/min))>0 then (Coolant Flow (L/min))/3.785/2
        grindingPowerHP = coolantFlowrateIMin / 3.785 / 2;

        //IF((Coolant Flow (L/min))>0 then (Coolant Flow (L/min))/10
        grindingPowerKW = coolantFlowrateIMin / 10;

        resultGrindingPowerAndCoolantFlowrate = {
          "grindingPowerKW": grindingPowerKW,
          "grindingPowerHP": grindingPowerHP,
          "coolantFlowrateGPM": coolantFlowrateGPM,
          "coolantFlowrateIMin": coolantFlowrateIMin
        };
      } else {
        // GPM is available,
        // hence calculate Coolant flowrate IMin,
        // Grinding power HP & Grinding power KW

        coolantFlowrateGPM = coolantParams.values["grindingPowerValue"];

        //IF((Coolant Flow (gpm))>0 then (Coolant Flow (gpm))*3.785
        coolantFlowrateIMin = coolantFlowrateGPM * 3.785;

        // IF((Coolant Flow (gpm))>0 then (Coolant Flow (gpm))/2
        grindingPowerHP = coolantFlowrateGPM / 2;

        //IF((Coolant Flow (gpm))>0,(Coolant Flow (gpm))*3.785/10
        grindingPowerKW = coolantFlowrateGPM * 3.785 / 10;

        resultGrindingPowerAndCoolantFlowrate = {
          "grindingPowerKW": grindingPowerKW,
          "grindingPowerHP": grindingPowerHP,
          "coolantFlowrateGPM": coolantFlowrateGPM,
          "coolantFlowrateIMin": coolantFlowrateIMin
        };
      }
    }
    return resultGrindingPowerAndCoolantFlowrate;
  }

  calculateNozzleArea(calculatedGrindingWheelVelocityMPS: number, calculatedGrindingWheelVelocitySFPM: number, calculatedCoolantFlowrateLMin: number, calculatedCoolantFlowrateGPM: number) {
    var nozzleAreaMM;
    var nozzleAreaIn;
    //var resultNozzleArea:{};
    var resultNozzleArea: NozzleArea;

    /**
     * 7. Calculate Nozzle area in mm^2 = If(Calculated Jet speed(M/sec)>0) { If(Calculated Coolant flow(L/min))>0) { 10^3/60*(Calculated Coolant flow(L/min))/(Calculated Jet speed(M/sec)) } else { “ “ } else { “ “ }
     */
    if (calculatedGrindingWheelVelocityMPS > 0) {
      if (calculatedCoolantFlowrateLMin > 0) {
        nozzleAreaMM = Math.pow(10, 3) / 60 * (calculatedCoolantFlowrateLMin) / (calculatedGrindingWheelVelocityMPS);
      } else {
        nozzleAreaMM = 0;
      }
    } else {
      nozzleAreaMM = 0;
    }

    /**
     * 8. calculate area in in^2 = =IF(Calculated Jet speed(sfpm)>0) {IF((coolant flow (gpm))>0 then 19.25*(coolant flow (gpm))/(Calculated Jet speed(sfpm)),””),””)}
     */
    if (calculatedGrindingWheelVelocitySFPM > 0) {
      if (calculatedCoolantFlowrateGPM > 0) {
        nozzleAreaIn = 19.25 * calculatedCoolantFlowrateGPM / calculatedGrindingWheelVelocitySFPM;
      } else {
        nozzleAreaIn = 0;
      }
    }
    else {
      nozzleAreaIn = 0;
    }

    resultNozzleArea = {
      "nozzleAreaMM": nozzleAreaMM,
      "nozzleAreaIn": nozzleAreaIn
    };
    return resultNozzleArea;
  }

  calculateGrindingWheelContactWidth(inputGrindingWheelContactWidthIn: number, inputGrindingWheelContactWidthMM: number) {

    var grindingWheelContactWidthMM;
    var grindingWheelContactWidthIn;
    var resultGrindingWheelConactWidth: GrindingWheelContactWidth;

    // calculate grindingWheelContactWidth in MM
    /**
     *  Nozzle Width(mm) = If(Input Nozzle width(in))>0 { (Input Nozzle width(in)*25.4 } If((Input Nozzle width(mm)>0) {(Input Nozzle width(mm)}
     *
     */
    if (inputGrindingWheelContactWidthIn > 0) {
      grindingWheelContactWidthMM = inputGrindingWheelContactWidthIn * 25.4;
    } else if (inputGrindingWheelContactWidthMM > 0) {

      grindingWheelContactWidthMM = inputGrindingWheelContactWidthMM;
    }

    // calculate grindingWheelContactWidth in In
    /**
     * Nozzle width(in) = If(Input Nozzle width(in)>0) { Input Nozzle width(in) } If((Input Nozzle width(mm))>0) {Input Nozzle width(mm)/25.4}
     */

    if (inputGrindingWheelContactWidthIn > 0) {
      grindingWheelContactWidthIn = inputGrindingWheelContactWidthIn;
    } else if (inputGrindingWheelContactWidthMM > 0) {
      grindingWheelContactWidthIn = inputGrindingWheelContactWidthMM / 25.4;
    }

    resultGrindingWheelConactWidth = {
      "grindingWheelContactWidthIn": grindingWheelContactWidthIn,
      "grindingWheelContactWidthMM": grindingWheelContactWidthMM
    };
    return resultGrindingWheelConactWidth;
  }

  calculateGrindingWheelContactHeight(calculatedGrindingWheelContactWidthIn: number, calculatedNozzleAreaIn: number) {

    var grindingWheelContactHeightIn;
    var grindingWheelContactHeightMM;
    // var resultGrindingWheelContactHeight : {};
    var resultGrindingWheelContactHeight: GrindingWheelContactHeight;
    // FIRST calculate GrindingWheelContactHeight in IN and then in MM

    // calculate GrindingWheelContactHeight in MM
    /**
     * Nozzle height(in) = =If((Nozzle width(in) == “”) { “ ”} else { (Nozzle area(in^2))/(Nozzle width(in)) }
     */
    if (calculatedGrindingWheelContactWidthIn == 0) {
      grindingWheelContactHeightIn = 0;
    } else {
      grindingWheelContactHeightIn = calculatedNozzleAreaIn / calculatedGrindingWheelContactWidthIn;
    }

    /**
     * Nozzle height(mm) = If((Calculated Nozzle width(in))==“”) {””} else {(Calculated Nozzle height(in))*25.4}
     */
    if (calculatedGrindingWheelContactWidthIn == 0) {
      grindingWheelContactHeightMM = 0;
    } else {
      grindingWheelContactHeightMM = grindingWheelContactHeightIn * 25.4;
    }

    resultGrindingWheelContactHeight = {
      "grindingWheelContactHeightMM": grindingWheelContactHeightMM,
      "grindingWheelContactHeightIn": grindingWheelContactHeightIn
    };
    return resultGrindingWheelContactHeight;
  }

  calculateNozzleDiameter(nozzleAreaIn: number, nozzleAreaMM: number) {

    var nozzleDiameterIn;
    // var resultNozzleDiameter: {};
    var resultNozzleDiameter: NozzleDiameter;
    // calculate diameter in In
    /**
     * Nozzle diameter(in) = =2*SQRT((Nozzle area(in^2))/PI())
     */
    nozzleDiameterIn = 2 * Math.sqrt(nozzleAreaIn / Math.PI);

    // calculate diameter in MM
    /**
     * Nozzle diameter(mm) = 2*SQRT((Nozzle area(mm^2))/PI())
     */
    nozzleAreaMM = 2 * Math.sqrt(nozzleAreaMM / Math.PI);

    resultNozzleDiameter = {
      "nozzleDiameterIn": nozzleDiameterIn,
      "nozzleAreaMM": nozzleAreaMM
    };
    return resultNozzleDiameter;
  }

  calculatePumpPower(calculatedCoolantFlowrateLMin: number, calculatedCoolantFlowrateGPM: number, calculatedPressurePsi: number, calculatedPressureBar: number, specificGravity: number) {

    var pumpPowerKW;
    var pumpPowerHP;
    var pumpPowerHeadFeet;
    // var resultPowerRequirements: {};
    var resultPowerRequirements: PumpPower;

    // calculate Pump Power in KW
    /**
     * pump power in kW = (Calculated Coolant flow(L/min))*(Calculated Pressure(Bar))/600/0.8
     */
    pumpPowerKW = (calculatedCoolantFlowrateLMin) * (calculatedPressureBar) / 600 / 0.8;

    // calculate Pump Power in HP
    /**
     * Pump power (HP) = Coolant flow(gpm)*calculated Pressure(psi)/1700/0.8
     */
    pumpPowerHP = calculatedCoolantFlowrateGPM * calculatedPressurePsi / 1700 / 0.8;

    // calculate Pump Power in head feet
    /**
     * Pump power(head feet) = =calculated Pressure(psi)*2.307*Specific gravity
     */

    pumpPowerHeadFeet = calculatedPressurePsi * 2.307 * specificGravity;

    resultPowerRequirements = {
      "pumpPowerKW": pumpPowerKW,
      "pumpPowerHP": pumpPowerHP,
      "pumpPowerHeadFeet": pumpPowerHeadFeet
    };

    return resultPowerRequirements;
  }

  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

}
