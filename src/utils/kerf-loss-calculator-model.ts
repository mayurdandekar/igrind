import * as Constants from './constants';
import { FormGroup } from '@angular/forms';
export class KerfLossCalculatorModel {
  resultObj: { [s: string]: string };
  constructor(private storage: any) {
  }

  kerfLossValidator(fg: FormGroup) {

    // let thicknessType = fg.get('thicknessType').value;
    let thickness = fg.get('thickness').value;

    if (thickness != "" && thickness != undefined && (thickness.toString().length > 9)) {
      fg.get('thickness').setErrors({ 'thicknessLength': true });
    }
    // if (thicknessType === undefined || thicknessType.length == 0) {
    //   fg.get('thickness').setErrors({ 'thicknessType': true });
    // } else {
    if (thickness == "") {
      fg.get('thickness').setErrors({ 'thickness_empty': true });
    }

    else if (thickness != "" && thickness !== undefined && (isNaN(thickness) || thickness == 0)) {
      fg.get('thickness').setErrors({ 'thickness_zero': true });
    }
    // }

    let surfaceArea = fg.get('surfaceArea').value;

    if (surfaceArea != "" && surfaceArea != undefined && (surfaceArea.toString().length > 9)) {
      fg.get('surfaceArea').setErrors({ 'surfaceAreaLength': true });
    }

    if (surfaceArea == "") {
      fg.get('surfaceArea').setErrors({ 'sa_empty': true });
    }

    else if (surfaceArea != "" && surfaceArea !== undefined && (isNaN(surfaceArea) || surfaceArea == 0)) {
      fg.get('surfaceArea').setErrors({ 'sa_zero': true });
    }

    let thicknessType = fg.get('thicknessType').value;

    if (thicknessType === undefined || thicknessType.length == 0 || thicknessType === "") {
      fg.get('thicknessType').setErrors({ 'thicknesstype_empty': true });
    }

    let surfaceAreaType = fg.get('surfaceAreaType').value;

    if (surfaceAreaType === undefined || surfaceAreaType.length == 0 || surfaceAreaType === "") {
      fg.get('surfaceAreaType').setErrors({ 'satype_empty': true });
    }

    return null;
  }

  calculateKerfLoss(thickness, thicknessType, surfaceArea, surfaceAreaType) {
    var result: {} = this.fetchKerfLoss(thickness, thicknessType, surfaceArea, surfaceAreaType);
    var resultToLoadDb: {} = {
      "thickness": thickness,
      "thicknessType": thicknessType,
      "surfaceArea": surfaceArea,
      "surfaceAreaType": surfaceAreaType
    }
    this.storage.set(Constants.KEY_KERF_LOSS, resultToLoadDb);
    return result;
  }

  fetchKerfLoss(thickness, thicknessType, surfaceArea, surfaceAreaType) {
    return this.switchInputTypefetchKerfLoss(thickness, thicknessType, surfaceArea, surfaceAreaType, ["in", "cm", "mm"]);
  }

  switchInputTypefetchKerfLoss(thickness, thicknessType, surfaceArea, surfaceAreaType, outPutLabel) {

    console.log(thickness, thicknessType, surfaceArea, surfaceAreaType);
    console.log(outPutLabel);

    var opt = thicknessType + "_" + surfaceAreaType;

    console.log(opt);

    switch (opt) {
      case "in_in2":
        // console.log("in_in2");
        return this.in_in2_to_other(thickness, surfaceArea, outPutLabel);

      case "in_cm2":
        // console.log("in_cm2");
        return this.in_cm2_to_other(thickness, surfaceArea, outPutLabel);

      case "in_mm2":
        // console.log("in_mm2");
        return this.in_mm2_to_other(thickness, surfaceArea, outPutLabel);

      case "cm_in2":
        // console.log("cm_in2");
        return this.cm_in2_to_other(thickness, surfaceArea, outPutLabel);

      case "cm_cm2":
        // console.log("cm_cm2");
        return this.cm_cm2_to_other(thickness, surfaceArea, outPutLabel);

      case "cm_mm2":
        // console.log("cm_mm2");
        return this.cm_mm2_to_other(thickness, surfaceArea, outPutLabel);

      case "mm_in2":
        // console.log("mm_in2");
        return this.mm_in2_to_other(thickness, surfaceArea, outPutLabel);

      case "mm_cm2":
        // console.log("mm_cm2");
        return this.mm_cm2_to_other(thickness, surfaceArea, outPutLabel);

      case "mm_mm2":
        // console.log("mm_mm2");
        return this.mm_mm2_to_other(thickness, surfaceArea, outPutLabel);

    }
  }

  in_in2_to_other(thickness, surfaceArea, outPutLabel) {
    var b = [];
    // outPutLabel.forEach((data, index) => {
    b.push({
      "label": "in",
      "value": (thickness * surfaceArea).toFixed(3)
    });
    b.push({
      "label": "cm",
      "value": ((thickness * 2.54) * (surfaceArea * 6.452)).toFixed(3)
    });
    b.push({
      "label": "mm",
      "value": ((thickness * 25.4) * (surfaceArea * 645.16)).toFixed(3)
    });
    // });
    return b;
  }

  in_cm2_to_other(thickness, surfaceArea, outPutLabel) {
    var b = [];
    // outPutLabel.forEach((data, index) => {
    b.push({
      "label": "in",
      "value": (thickness * (surfaceArea / 6.452)).toFixed(3)
    });
    b.push({
      "label": "cm",
      "value": ((thickness * 2.54) * (surfaceArea)).toFixed(3)
    });
    b.push({
      "label": "mm",
      "value": ((thickness * 25.4) * (surfaceArea * 100)).toFixed(3)
    });
    // });
    return b;
  }

  in_mm2_to_other(thickness, surfaceArea, outPutLabel) {
    var b = [];
    // outPutLabel.forEach((data, index) => {
    b.push({
      "label": "in",
      "value": (thickness * (surfaceArea / 645.16)).toFixed(3)
    });
    b.push({
      "label": "cm",
      "value": ((thickness * 2.54) * (surfaceArea / 100)).toFixed(3)
    });
    b.push({
      "label": "mm",
      "value": ((thickness * 25.4) * surfaceArea).toFixed(3)
    });
    // });
    return b;
  }

  cm_in2_to_other(thickness, surfaceArea, outPutLabel) {
    var b = [];
    // outPutLabel.forEach((data, index) => {
    b.push({
      "label": "in",
      "value": ((thickness / 2.54) * surfaceArea).toFixed(3)
    });
    b.push({
      "label": "cm",
      "value": (thickness * (surfaceArea * 6.452)).toFixed(3)
    });
    b.push({
      "label": "mm",
      "value": ((thickness * 10) * (surfaceArea * 645.16)).toFixed(3)
    });
    // });
    return b;
  }

  cm_cm2_to_other(thickness, surfaceArea, outPutLabel) {
    var b = [];
    // outPutLabel.forEach((data, index) => {
    b.push({
      "label": "in",
      "value": ((thickness / 2.54) * (surfaceArea / 6.452)).toFixed(3)
    });
    b.push({
      "label": "cm",
      "value": (thickness * surfaceArea).toFixed(3)
    });
    b.push({
      "label": "mm",
      "value": ((thickness * 10) * (surfaceArea * 100)).toFixed(3)
    });
    // });
    return b;
  }

  cm_mm2_to_other(thickness, surfaceArea, outPutLabel) {
    var b = [];
    // outPutLabel.forEach((data, index) => {
    b.push({
      "label": "in",
      "value": ((thickness / 2.54) * (surfaceArea / 645.16)).toFixed(3)
    });
    b.push({
      "label": "cm",
      "value": (thickness * (surfaceArea / 100)).toFixed(3)
    });
    b.push({
      "label": "mm",
      "value": ((thickness * 10) * surfaceArea).toFixed(3)
    });
    // });
    return b;
  }

  mm_in2_to_other(thickness, surfaceArea, outPutLabel) {
    var b = [];
    // outPutLabel.forEach((data, index) => {
    b.push({
      "label": "in",
      "value": ((thickness / 25.4) * surfaceArea).toFixed(3)
    });
    b.push({
      "label": "cm",
      "value": ((thickness / 10) * (surfaceArea * 6.452)).toFixed(3)
    });
    b.push({
      "label": "mm",
      "value": (thickness * (surfaceArea * 645.16)).toFixed(3)
    });
    // });
    return b;
  }

  mm_cm2_to_other(thickness, surfaceArea, outPutLabel) {
    var b = [];
    // outPutLabel.forEach((data, index) => {
    b.push({
      "label": "in",
      "value": ((thickness / 25.4) * (surfaceArea / 6.452)).toFixed(3)
    });
    b.push({
      "label": "cm",
      "value": ((thickness / 10) * (surfaceArea)).toFixed(3)
    });
    b.push({
      "label": "mm",
      "value": (thickness * (surfaceArea * 100)).toFixed(3)
    });
    // });
    return b;
  }

  mm_mm2_to_other(thickness, surfaceArea, outPutLabel) {
    var b = [];
    // outPutLabel.forEach((data, index) => {
    b.push({
      "label": "in",
      "value": ((thickness / 25.4) * (surfaceArea / 645.16)).toFixed(3)
    });
    b.push({
      "label": "cm",
      "value": ((thickness / 10) * (surfaceArea / 100)).toFixed(3)
    });
    b.push({
      "label": "mm",
      "value": (thickness * surfaceArea).toFixed(3)
    });
    // });
    return b;
  }



}
