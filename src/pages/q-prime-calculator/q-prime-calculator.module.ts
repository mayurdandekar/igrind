import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QPrimeCalculatorPage } from './q-prime-calculator';

@NgModule({
  declarations: [
    QPrimeCalculatorPage,
  ],
  imports: [
    IonicPageModule.forChild(QPrimeCalculatorPage),
  ],
})
export class QPrimeCalculatorPageModule {}
