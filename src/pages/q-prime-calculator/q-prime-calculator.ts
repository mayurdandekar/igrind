import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  Content,
} from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { QPrimeModel } from "../../utils/qprime-calculator-model";
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";
import * as Constants from "../../utils/constants";
import { Keyboard } from "@ionic-native/keyboard";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import * as FirebaseConstants from "../../utils/analytics-constants";

/**
 * Generated class for the QPrimeCalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-q-prime-calculator",
  templateUrl: "q-prime-calculator.html",
})
export class QPrimeCalculatorPage {
  @ViewChild(Content) content: Content;

  qPrimeForm: FormGroup;
  qPrimeType: any = "workpiece";
  isEnglish: boolean;
  resultOpacity: string = "1";
  calculateDisable: boolean = false;
  calculateBtnText: string;
  showResult: boolean;
  qPrimeOutputType: any = "qw";
  autoLoad: boolean = false;
  showErrors: boolean = false;
  calculator: QPrimeModel;
  op_1: any;
  op_2: any;
  op_3: any;
  validationMsgs: { inp_1: Array<any>; inp_2: Array<any> };
  unit_VW: any = "";
  unit_AE: any = "";
  unit_DW: any = "";
  unit_VFR: any = "";
  unit_QW: any = "";
  qprimeRange: any;
  mandatoryClass: any;
  maxEllheight: any;

  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public platform: Platform,
    private keyboard: Keyboard,
    public translate: TranslateService,
    public storage: Storage,
    public common: CommonHelperProvider,
    public firebaseAnalytics: FirebaseAnalytics,
    public popOverCtrl: PopoverController
  ) {
    this.calculator = new QPrimeModel(this.storage);
    this.mandatoryClass = '<span style="color:red;">* </span>';

    /**
     * Name :       Load Constants for qprime calculator units
     * Parameters:
     * Description: Loads and initializes qprime calculator units from constants file
     * Return:      --
     */

    this.unit_VW = Constants.KEY_VW;
    this.unit_AE = Constants.KEY_AE;
    this.unit_DW = Constants.KEY_DW;
    this.unit_VFR = Constants.KEY_VFR;
    this.unit_QW = Constants.KEY_QW;
    console.log(Constants.KEY_VW);

    /**
     * Name :       formBuilder.group
     * Parameters:  parameters and validator
     * Description: it specifies if the field is required
     * Return:      --
     */
    this.qPrimeForm = formBuilder.group(
      {
        // diameter: ['', Validators.required],
        unitPref: [null],
        QPType: ["", Validators.required],
        inp_1: ["", Validators.required],
        inp_2: ["", Validators.required],
      },
      { validator: this.calculator.qPrimeValidator.bind(this) }
    );

    this.qPrimeForm.get("unitPref").setValue(false);
    this.isEnglish = true;

    /**
     * Name :       translate.get
     * Parameters:  text value
     * Description: translate the input text
     * Return:      translated text
     */

    translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    // this.loadSwitchFromDb();

    /**
     * Name :       storage.get
     * Parameters:  Qprime Key
     * Description: fetches and sets the values from the DB
     * Return:      --
     */
    this.storage.get(Constants.KEY_QPRIME).then((qpObj) => {
      console.log(qpObj);

      if (qpObj) {
        this.qPrimeForm.get("inp_1").setValue(qpObj.inp_1);
        this.qPrimeForm.get("inp_2").setValue(qpObj.inp_2);
        this.qPrimeForm.get("unitPref").setValue(!qpObj.unitPref);
        this.qPrimeType = qpObj.QPType;
        this.qPrimeOutputType = qpObj.qPrimeOutputType;
        this.isEnglish = qpObj.unitPref;
        this.showResult = true;
        this.resultOpacity = "1";
        this.op_1 = qpObj.inp_1;
        this.op_2 = qpObj.inp_2;
        this.op_3 = qpObj.output;
        this.autoLoad = true;
        this.calculateDisable = true;
        this.translate
          .get("update_btn")
          .toPromise()
          .then((result) => {
            this.calculateBtnText = result;
          });
        this.setElmHTAuto();
        setTimeout(() => {
          this.checkElementHeight();
        }, 10);
        // this.onSubmit(this.chipThicknessForm.value);
      } else {
        this.autoLoad = false;
      }
    });

    var wheelSpeedErrMsg;
    var emptyErrMsg;
    var inputLengthErrmsg;
    this.translate
      .get("empty_invalid_input")
      .toPromise()
      .then((emptyMsg) => {
        emptyErrMsg = emptyMsg;
        this.validationMsgs = {
          inp_1: [{ type: "inp_1_empty", message: emptyErrMsg }],
          inp_2: [{ type: "inp_2_empty", message: emptyErrMsg }],
        };
      }); //input_length_invalid
  }
  /**
   * Name :       loadSwitchFromDb
   * Parameters:  --
   * Description: loads the switch status from DB
   * Return:      void
   */
  loadSwitchFromDb() {
    this.storage.get(Constants.KEY_UNIT_PREF_QPRIME).then((toggle) => {
      if (toggle !== null) {
        this.qPrimeForm.get("unitPref").setValue(toggle);
        this.isEnglish = !toggle;
      }
    });
  }

  /**
   * Name: ionViewDidLoad
   * Parameters: none
   * Description: used to call firebase event for coolant screen and to create the popover dialog
   * Return: none
   */
  ionViewDidLoad() {
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.QPRIME_CALCULATOR
    );
    this.setElmHTAuto();
    setTimeout(() => {
      this.checkElementHeight();
    }, 10);
    // this.qprimeRange = this.popOverCtrl.create('QprimeRangePage', {'isEnglish':this.isEnglish}, { showBackdrop: true});
  }

  setQPrimeOutputType(param) {
    this.qPrimeOutputType = param;
    this.resultOpacity = "0.3";
    if (this.op_3) {
      this.calculateDisable = false;
    }
    this.qPrimeForm.get("inp_1").markAsUntouched();
    this.qPrimeForm.get("inp_2").markAsUntouched();
    this.qPrimeForm.get("inp_1").markAsPristine();
    this.qPrimeForm.get("inp_2").markAsPristine();
    this.qPrimeForm.get("inp_1").setValue("");
    this.qPrimeForm.get("inp_2").setValue("");

    this.setElmHTAuto();

    setTimeout(() => {
      this.checkElementHeight();
    }, 10);
  }

  updateToggle() {
    this.showErrors = false;
    this.qPrimeForm.get("inp_1").setValue("");
    this.qPrimeForm.get("inp_2").setValue("");
    this.qPrimeForm.get("inp_1").markAsUntouched();
    this.qPrimeForm.get("inp_2").markAsUntouched();
    this.qPrimeForm.get("inp_1").markAsPristine();
    this.qPrimeForm.get("inp_2").markAsPristine();
    this.isEnglish = !this.qPrimeForm.get("unitPref").value;
    this.setElmHTAuto();
    setTimeout(() => {
      this.checkElementHeight();
    }, 10);
    // if (this.isEnglish) {
    //   this.op_1 = " (in³/in min)";
    //   this.op_2 = " (in/min)";
    //   this.op_3 = " (in)";
    // } else {
    //   this.op_1 = " (mm³/sec/mm)";
    //   this.op_2 = " (mm/min)";
    //   this.op_3 = " (mm)";
    // }
    console.log(this.isEnglish);
  }

  setQPType(param) {
    console.log(param);
    this.qPrimeType = param;
    this.resultOpacity = "0.3";
    this.qPrimeOutputType = "qw";
    if (this.op_3) {
      this.calculateDisable = false;
    }
    this.qPrimeForm.get("inp_1").markAsUntouched();
    this.qPrimeForm.get("inp_2").markAsUntouched();
    this.qPrimeForm.get("inp_1").markAsPristine();
    this.qPrimeForm.get("inp_2").markAsPristine();
    this.qPrimeForm.get("inp_1").setValue("");
    this.qPrimeForm.get("inp_2").setValue("");

    this.setElmHTAuto();

    setTimeout(() => {
      this.checkElementHeight();
    }, 10);
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    this.common.telkeypress(ev);
  }
  //

  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    var enteredInput = event.target.value;
    var field = this;
    console.log(enteredInput);

    if (enteredInput)
      var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  /**
   * Name :       updateResults
   * Parameters:  --
   * Description: Updates Result area
   * Return:      void
   */
  updateResults() {
    this.calculateDisable = false;
    this.resultOpacity = "0.3";
  }

  /**
   * Name :       onSubmit
   * Parameters:  value  * Description: validates input fields. If it passes validation then call calculate & log Firebase analytics evets.
   * Return:      void
   */
  onSubmit(value: any): void {
    console.log(this.qPrimeForm.get("unitPref").value);
    this.keyboard.hide();
    var inp_1HasErrors: boolean = false;
    var inp_2HasErrors: boolean = false;
    for (let validation of this.validationMsgs.inp_1) {
      if (this.qPrimeForm.get("inp_1").hasError(validation.type)) {
        inp_1HasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.inp_2) {
      if (this.qPrimeForm.get("inp_2").hasError(validation.type)) {
        inp_2HasErrors = true;
      }
    }

    if (!inp_1HasErrors && !inp_2HasErrors) {
      if (!this.autoLoad) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.QPRIME_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoad = false;
      }
      this.calculate(
        value.unitPref,
        value.inp_1,
        value.inp_2,
        value.QPType,
        this.qPrimeOutputType
      );
    } else {
      for (let validation of this.validationMsgs.inp_1) {
        if (this.qPrimeForm.get("inp_1").hasError(validation.type)) {
          this.firebaseAnalytics.logEvent(
            FirebaseConstants.QPRIME_CALC_INP1_HITCOUNT,
            {}
          );
        }
      }
      for (let validation of this.validationMsgs.inp_2) {
        if (this.qPrimeForm.get("inp_2").hasError(validation.type)) {
          this.firebaseAnalytics.logEvent(
            FirebaseConstants.QPRIME_CALC_INP2_HITCOUNT,
            {}
          );
        }
      }
      this.showErrors = true;
      this.qPrimeForm.get("inp_1").markAsTouched();
      this.qPrimeForm.get("inp_2").markAsTouched();
    }
  }

  /**
   * Name :       calculate
   * Parameters:  input values
   * Description: Calculates the result and assigns the result to the variables
   * Return:      void
   */
  calculate(unitPref, inp_1, inp_2, QPType, qPrimeOutputType) {
    this.resultOpacity = "1";
    this.showResult = true;
    let inp_1_num: number = parseFloat(inp_1);
    let inp_2_num: number = parseFloat(inp_2);
    var output = this.calculator.calculateQprime(
      !unitPref,
      inp_1_num,
      inp_2_num,
      QPType,
      qPrimeOutputType
    );
    console.log("Result: ", output);
    this.op_1 = inp_1_num;
    this.op_2 = inp_2_num;
    this.op_3 = output;
    this.calculateDisable = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;

        // setTimeout(() => {
        //   this.scrollToResult('output');
        // }, 300);
      });
  }

  /**
   * Name :       clearResults
   * Parameters:  --
   * Description: clears all the fields and DB
   * Return:      void
   */
  clearResults() {
    this.resultOpacity = "1";
    this.qPrimeForm.get("inp_1").markAsUntouched();
    this.qPrimeForm.get("inp_2").markAsUntouched();
    this.qPrimeForm.get("inp_1").markAsPristine();
    this.qPrimeForm.get("inp_2").markAsPristine();
    this.qPrimeForm.get("inp_1").setValue("");
    this.qPrimeForm.get("inp_2").setValue("");
    this.op_1 = "";
    this.op_2 = "";
    this.op_3 = "";
    this.showResult = false;
    this.qPrimeForm.get("unitPref").setValue(false);
    this.isEnglish = true;
    this.qPrimeType = "workpiece";
    this.qPrimeOutputType = "qw";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
    this.storage
      .remove(Constants.KEY_QPRIME)
      .then((output) => {
        console.log("QPRIME Cleared: " + output);
      })
      .catch((err) => {
        console.log("QPRIME error: " + err);
      });
    this.showErrors = false;
  }

  /**
   * Name: qprimeRangePopUp
   * Parameters: none
   * Description: used to present qprime popover
   * Return: none
   */
  qprimeRangePopUp() {
    // alert("qprimeRangePopUp clicked")
    this.qprimeRange = this.popOverCtrl.create(
      "QprimeRangePage",
      { isEnglish: this.isEnglish },
      { showBackdrop: true }
    );
    this.qprimeRange.present();
  }

  /**
   * Name: scrollToResult
   * Parameters: element class name
   * Description: scroll to result div
   * Return: none
   */
  scrollToResult(elementId: string) {
    console.log(elementId);
    var className = document.getElementById(elementId);
    let y = className.offsetTop;
    console.log(y);

    this.content.scrollTo(10, y);
  }

  /**
   * Name: setElmHTAuto
   * Parameters:
   * Description: Set maxEllHeight to 'auto'
   * Return: none
   */
  setElmHTAuto() {
    this.maxEllheight = "auto";
  }

  /**
   * Name: checkElementHeight
   * Parameters:
   * Description: Checks height of an HTML Element
   * Return: none
   */
  checkElementHeight() {
    var h1;
    var h2;
    function isEllipsisActive(element) {
      return element.offsetWidth < element.scrollWidth;
    }

    Array.from(document.querySelectorAll(".ellHeightWorkpiece")).forEach(
      (element) => {
        h1 = element.clientHeight;
        console.log(h1);
      }
    );
    Array.from(document.querySelectorAll(".ellHeightRadial")).forEach(
      (element) => {
        h2 = element.clientHeight;
        console.log(h2);
      }
    );
    console.log(Math.max(h1, h2));

    // return (Math.max(h1,h2)+'px');
    this.maxEllheight = Math.max(h1, h2) + "px";
  }
}
