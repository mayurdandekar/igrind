import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
} from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
/**
 * Generated class for the SpeedRatioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-speed-ratio",
  templateUrl: "speed-ratio.html",
})
export class SpeedRatioPage {
  src1;
  src2;
  src3;
  src4;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public translate: TranslateService
  ) {}

  ionViewDidLoad() {
    this.src1 =
      "./assets/imgs/unidirection_wheel_" + this.translate.currentLang + ".png";
    this.src2 =
      "./assets/imgs/unidirection_diamond_" +
      this.translate.currentLang +
      ".png";
    this.src3 =
      "./assets/imgs/unidirection_wheel_" + this.translate.currentLang + ".png";
    this.src4 =
      "./assets/imgs/counter_diamondRoll_" +
      this.translate.currentLang +
      ".png";
  }

  ok() {
    this.viewCtrl.dismiss();
  }
}
