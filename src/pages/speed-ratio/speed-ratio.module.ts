import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeedRatioPage } from './speed-ratio';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    SpeedRatioPage,
  ],
  imports: [
    IonicPageModule.forChild(SpeedRatioPage),
    PipesModule,
    TranslateModule.forChild()
  ],
})
export class SpeedRatioPageModule {}
