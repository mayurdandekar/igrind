import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OverlapRatioPage } from './overlap-ratio';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    OverlapRatioPage,
  ],
  imports: [
    IonicPageModule.forChild(OverlapRatioPage),
    PipesModule,
    TranslateModule.forChild()
  ],
})
export class OverlapRatioPageModule {}
