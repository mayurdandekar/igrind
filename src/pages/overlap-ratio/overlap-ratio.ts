import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
} from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
/**
 * Generated class for the OverlapRatioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-overlap-ratio",
  templateUrl: "overlap-ratio.html",
})
export class OverlapRatioPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public translate: TranslateService
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad OverlapRatioPage");
  }

  ok() {
    this.viewCtrl.dismiss();
  }
}
