import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Select,
  Keyboard,
  Platform,
  ModalController,
  PopoverController,
  Slides,
} from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MasterDataProvider } from "../../providers/master-data/master-data";
import { TranslateService } from "@ngx-translate/core";
import { StationaryToolValidator } from "../../validators/StationaryToolValidator";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import { StatusBar } from "@ionic-native/status-bar";
import { Storage } from "@ionic/storage";
import * as Constants from "../../utils/constants";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { plungeDressingCalculator } from "../../utils/plunge-dressing-calculator-model";
import { stationaryToolCalculator } from "../../utils/stationary-tool-calculator-model";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";

/**
 * Generated class for the StationaryDressingCalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-stationary-dressing-calculator",
  templateUrl: "stationary-dressing-calculator.html",
})
export class StationaryDressingCalculatorPage {
  stationaryToolForm: any;
  validator: StationaryToolValidator;
  stationaryToolModel: stationaryToolCalculator;
  calculateBtnText: any;
  validationMsgs: any;
  isImperial: boolean;
  isSinglePoint: boolean;
  isRateTraverse: boolean;
  productTypeListRefs: any;
  productTypeListRefsOne: any;
  productTypeListRefsTwo: any;
  productTypeListRefsTwoLength: any;
  calculateDisable: boolean;
  resultOpacity: string;

  //
  btnArr: any = ["grey", "grey", "grey"];
  autoLoad: any;
  showErrors: boolean;
  requiredFinish: any;
  showResult: any;
  recommendedLead: Promise<any>;
  op_td_rate_calculated: any;
  op_td_rate_recommended: any;
  op_overlap_ratio_calculated: any;
  op_overlap_ratio_recommended: any;
  op_opposite_td_rate_calculated: any;
  op_opposite_td_rate_recommended: any;
  op_max_dress_depth_calculated: any;
  op_max_dress_depth_recommended: any;

  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  constructor(
    private keyboard: Keyboard,
    public platform: Platform,
    public statusBar: StatusBar,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public popOverCtrl: PopoverController,
    public formBuilder: FormBuilder,
    public masterData: MasterDataProvider,
    public translate: TranslateService,
    public storage: Storage,
    public firebaseAnalytics: FirebaseAnalytics,
    public common: CommonHelperProvider
  ) {
    this.validator = new StationaryToolValidator(
      translate,
      this.firebaseAnalytics
    );
    this.stationaryToolModel = new stationaryToolCalculator(
      storage,
      this.firebaseAnalytics
    );

    translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    this.validator.getValidationMsgs().then((msgs) => {
      console.log(msgs);
      this.validationMsgs = msgs;
    });
    this.selectFinish(0);
    this.requiredFinish = 0;
    this.isImperial = true;
    this.isSinglePoint = true;
    this.isRateTraverse = true;

    this.masterData.getProductTypeDropDownsST(this.isImperial).then((val) => {
      this.productTypeListRefs = val;
      this.productTypeListRefsOne = this.productTypeListRefs.slice(0, 2);
      if (!this.isSinglePoint) {
        console.log("ander aya hai");
        this.productTypeListRefsTwo = this.productTypeListRefs.slice(2);
      } else {
        this.productTypeListRefsTwo = this.productTypeListRefs.slice(3);
      }
    });

    this.stationaryToolForm = formBuilder.group(
      {
        unitPref: [null],
        unitPrefType: [null],
        productType: ["", Validators.required],
        rpm: ["", Validators.required],
        contactWidth: ["", Validators.required],
        rate: ["", Validators.required],
        unitPrefRate: [null],
        requiredFinish: ["", Validators.required],
      },
      { validator: this.validator.dressingValidator.bind(this) }
    );

    this.stationaryToolForm.get("unitPref").setValue(false);
    this.isImperial = true;

    this.updateToggle();
    this.updateTypeToggle();
    this.updateRateToggle();

    this.loadSwitchFromDb();

    this.storage.get(Constants.KEY_STATIONARY).then((stationaryObj) => {
      console.log(stationaryObj);
      if (stationaryObj) {
        console.log(stationaryObj["rpm"]);
        this.stationaryToolForm
          .get("unitPref")
          .setValue(!stationaryObj["isImperial"]);
        this.stationaryToolForm
          .get("unitPrefType")
          .setValue(!stationaryObj["isSinglePoint"]);
        this.stationaryToolForm
          .get("unitPrefRate")
          .setValue(!stationaryObj["isRateTraverse"]);

        this.stationaryToolForm.get("rpm").setValue(stationaryObj["rpm"]);
        this.stationaryToolForm
          .get("contactWidth")
          .setValue(stationaryObj["contactWidth"]);
        this.stationaryToolForm.get("rate").setValue(stationaryObj["rate"]);

        this.stationaryToolForm
          .get("productType")
          .setValue(stationaryObj["productTypeKey"]);

        this.selectFinish(stationaryObj["requiredFinish"]);
        // this.stationaryToolForm.get('requiredFinish').setValue(stationaryObj["requiredFinish"]);

        this.autoLoad = true;
        this.onSubmit(this.stationaryToolForm.value);
      } else {
        this.autoLoad = false;
        this.translate
          .get("calculate_btn")
          .toPromise()
          .then((result) => {
            this.calculateBtnText = result;
          });
      }
    });
  }

  ionViewDidLoad() {
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.STATIONARY_TOOL_CALCULATOR
    );
  }

  selectFinish(index) {
    console.log(index);

    this.showErrors = false;

    this.updateResults();

    this.requiredFinish = index;
    this.btnArr = ["grey", "grey", "grey"];
    this.btnArr[index] = "primary";

    this.calculateDisable = false;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
  }

  /**
   * Name: onSubmit
   * Parameters: value
   * Description: used to read data from ui to show validation messages if required and to perform required calculations
   * Return: none
   */
  onSubmit(value: any): void {
    this.keyboard.close();
    var productTypeHasErrors: boolean;
    var rpmHasErrors: boolean;
    var contactWidthHasErrors: boolean;
    var rateHasErrors: boolean;

    console.log(this.validationMsgs);

    if (
      this.stationaryToolForm
        .get("productType")
        .hasError(this.validationMsgs.productType.type)
    ) {
      productTypeHasErrors = true;
    }
    for (let validation of this.validationMsgs.rpm) {
      if (this.stationaryToolForm.get("rpm").hasError(validation.type)) {
        rpmHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.contactWidth) {
      if (
        this.stationaryToolForm.get("contactWidth").hasError(validation.type)
      ) {
        contactWidthHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.rate) {
      console.log(validation);
      if (this.stationaryToolForm.get("rate").hasError(validation.type)) {
        rateHasErrors = true;
      }
    }

    if (
      !productTypeHasErrors &&
      !rpmHasErrors &&
      !contactWidthHasErrors &&
      !rateHasErrors
    ) {
      if (!this.autoLoad) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.STATIONARY_TOOL_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoad = false;
      }
      this.calculate(
        value.unitPref,
        value.unitPrefType,
        value.unitPrefRate,
        value.rpm,
        value.contactWidth,
        value.rate,
        value.productType
      );
    } else {
      this.getFormValidationErrors();
      this.stationaryToolForm.get("rpm").markAsTouched();
      this.stationaryToolForm.get("contactWidth").markAsTouched();
      this.stationaryToolForm.get("rate").markAsTouched();
      this.showErrors = true;
    }
  }

  /**
   * Name: getFormValidationErrors
   * Parameters: none
   * Description: reads validation error messages
   * Return: none
   */
  getFormValidationErrors() {
    Object.keys(this.stationaryToolForm.controls).forEach((key) => {
      const controlErrors = this.stationaryToolForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          this.stationaryToolModel.triggerFirebaseEvent(keyError);
        });
      }
    });
  }

  calculate(
    unitPref,
    unitPrefType,
    unitPrefRate,
    rpm,
    contactWidth,
    rate,
    productType
  ) {
    this.showResult = true;
    this.resultOpacity = "1";
    // this.recommendedLead = "";
    this.masterData.getProductTypeDropDownsST(this.isImperial).then((val) => {
      this.productTypeListRefs = val;
      this.productTypeListRefsOne = this.productTypeListRefs.slice(0, 2);
      if (!this.isSinglePoint) {
        this.productTypeListRefsTwo = this.productTypeListRefs.slice(2);
      } else {
        this.productTypeListRefsTwo = this.productTypeListRefs.slice(3);
      }

      this.masterData
        .getRecommendedLeadST(this.isImperial, this.isSinglePoint)
        .then((res) => {
          var output = this.stationaryToolModel.calculate(
            !unitPref,
            !unitPrefType,
            !unitPrefRate,
            rpm,
            contactWidth,
            rate,
            productType,
            res,
            this.requiredFinish,
            val
          );
          console.log(output);

          this.op_td_rate_calculated =
            output["calculatedAndRecommendedLead"]["resultCalculatedLead"];
          this.op_td_rate_recommended =
            output["calculatedAndRecommendedLead"]["resultRecommendedLead"];

          this.op_overlap_ratio_calculated =
            output["calculatedAndRecommendedOverlapRatio"][
              "resultCalculatedOverlapRatio"
            ];
          this.op_overlap_ratio_recommended =
            output["calculatedAndRecommendedOverlapRatio"][
              "resultRecommendedOverlapRatio"
            ];

          this.op_opposite_td_rate_calculated =
            output["calculatedAndRecommendedTraverse"][
              "resultCalculatedTraverse"
            ];
          this.op_opposite_td_rate_recommended =
            output["calculatedAndRecommendedTraverse"][
              "resultRecommendedTraverse"
            ];

          this.op_max_dress_depth_calculated = output["productType"];
          this.op_max_dress_depth_recommended = "NA";

          this.showResult = true;
          this.resultOpacity = "1";
          this.stationaryToolModel.productTypeEvents(productType);
          this.calculateDisable = true;
          this.translate
            .get("update_btn")
            .toPromise()
            .then((result) => {
              this.calculateBtnText = result;
            });
        });
    });
  }
  recommendedLeadValue(
    arg0: boolean,
    arg1: boolean,
    arg2: boolean,
    rpm: any,
    contactWidth: any,
    rate: any,
    productType: any,
    recommendedLeadValue: any
  ): any {
    throw new Error("Method not implemented.");
  }
  updateToggle() {
    this.isImperial = !this.stationaryToolForm.get("unitPref").value;
    console.log("Imperial :" + this.isImperial);
    console.log("Single :" + this.isSinglePoint);
    this.masterData.getProductTypeDropDownsST(this.isImperial).then((val) => {
      this.productTypeListRefs = val;
      this.productTypeListRefsOne = this.productTypeListRefs.slice(0, 2);
      if (!this.isSinglePoint) {
        this.productTypeListRefsTwo = this.productTypeListRefs.slice(2);
      } else {
        this.productTypeListRefsTwo = this.productTypeListRefs.slice(3);
      }
      console.log("*******11" + this.productTypeListRefsTwo);
    });
    console.log("*******" + this.productTypeListRefsTwo);

    this.showErrors = false;

    this.stationaryToolForm.get("productType").setValue("");

    this.stationaryToolForm.get("rpm").setValue("");
    this.stationaryToolForm.get("contactWidth").setValue("");
    this.stationaryToolForm.get("rate").setValue("");

    this.stationaryToolForm.get("rpm").markAsPristine();
    this.stationaryToolForm.get("contactWidth").markAsPristine();
    this.stationaryToolForm.get("rate").markAsPristine();

    this.stationaryToolForm.get("rpm").markAsUntouched();
    this.stationaryToolForm.get("contactWidth").markAsUntouched();
    this.stationaryToolForm.get("rate").markAsUntouched();

    this.stationaryToolForm.get("unitPrefRate").setValue(false);
    this.stationaryToolForm.get("unitPrefType").setValue(false);
  }

  updateTypeToggle() {
    this.isSinglePoint = !this.stationaryToolForm.get("unitPrefType").value;
    console.log("Imperial :" + this.isImperial);
    console.log("Single :" + this.isSinglePoint);
    this.masterData.getProductTypeDropDownsST(this.isImperial).then((val) => {
      this.productTypeListRefs = val;
      this.productTypeListRefsOne = this.productTypeListRefs.slice(0, 2);
      if (!this.isSinglePoint) {
        console.log("ander aya hai");
        this.productTypeListRefsTwo = this.productTypeListRefs.slice(2);
      } else {
        this.productTypeListRefsTwo = this.productTypeListRefs.slice(3);
      }
    });

    this.showErrors = false;

    this.stationaryToolForm.get("productType").setValue("");

    this.stationaryToolForm.get("rpm").setValue("");
    this.stationaryToolForm.get("contactWidth").setValue("");
    this.stationaryToolForm.get("rate").setValue("");

    this.stationaryToolForm.get("rpm").markAsPristine();
    this.stationaryToolForm.get("contactWidth").markAsPristine();
    this.stationaryToolForm.get("rate").markAsPristine();

    this.stationaryToolForm.get("rpm").markAsUntouched();
    this.stationaryToolForm.get("contactWidth").markAsUntouched();
    this.stationaryToolForm.get("rate").markAsUntouched();

    this.stationaryToolForm.get("unitPrefRate").setValue(false);
  }
  updateRateToggle() {
    this.isRateTraverse = !this.stationaryToolForm.get("unitPrefRate").value;
    this.stationaryToolForm.get("rate").setValue("");
    this.stationaryToolForm.get("rate").markAsPristine();
    this.stationaryToolForm.get("rate").markAsUntouched();
  }
  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    var enteredInput = event.target.value;
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    this.common.telkeypress(ev);
  }
  //

  /**
   * Name : round
   * Parameters: 'value' & 'precision' of type any
   * Description: This method round of the value to the given precision.
   * Return: void
   */
  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  }

  precise_round(num, dec) {
    if (typeof num !== "number" || typeof dec !== "number") return false;

    var num_sign = num >= 0 ? 1 : -1;

    return (
      Math.round(num * Math.pow(10, dec) + num_sign * 0.0001) /
      Math.pow(10, dec)
    ).toFixed(dec);
  }

  /**
   * Name: updateDropDown
   * Parameters: none
   * Description: on update of dropdown value, calculations have to be done
   * Return: none
   */
  updateDropDown() {
    this.updateResults();
  }
  /**
   * Name: updateResults
   * Parameters: none
   * Description: Used to blur the result area and to display update button
   * Return: none
   */
  updateResults() {
    this.calculateDisable = false;
    this.resultOpacity = "0.3";
  }
  clearResults() {
    console.log("hello");
    this.showErrors = false;

    this.stationaryToolForm.get("productType").setValue("");

    this.stationaryToolForm.get("rpm").setValue("");
    this.stationaryToolForm.get("contactWidth").setValue("");
    this.stationaryToolForm.get("rate").setValue("");

    this.stationaryToolForm.get("rpm").markAsPristine();
    this.stationaryToolForm.get("contactWidth").markAsPristine();
    this.stationaryToolForm.get("rate").markAsPristine();

    this.stationaryToolForm.get("rpm").markAsUntouched();
    this.stationaryToolForm.get("contactWidth").markAsUntouched();
    this.stationaryToolForm.get("rate").markAsUntouched();

    this.stationaryToolForm.get("unitPrefRate").setValue(false);
    this.stationaryToolForm.get("unitPrefType").setValue(false);
    this.stationaryToolForm.get("unitPref").setValue(false);

    this.showResult = false;

    this.storage
      .remove(Constants.KEY_STATIONARY)
      .then((output) => {
        console.log("Diamond Dressing Cleared: " + output);
      })
      .catch((err) => {
        console.log("Diamond Dressing Clear error: " + err);
      });
    this.storage
      .remove(Constants.KEY_UNIT_PREF_STATIONARY_TOOL)
      .then((output) => {
        console.log("Diamond Dressing Toggles Cleared: " + output);
      })
      .catch((err) => {
        console.log("Diamond Dressing Toggles Clear error: " + err);
      });

    this.resultOpacity = "1";

    this.showResult = false;

    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    this.selectFinish(0);
  }

  loadSwitchFromDb() {
    this.storage
      .get(Constants.KEY_UNIT_PREF_STATIONARY_TOOL)
      .then((toggles) => {
        console.log(toggles);
        // "isImperial": unitPref,
        // "isSinglePoint": unitPrefType,
        // "isRateTraverse": unitPrefRate
        if (toggles != null && toggles !== undefined) {
          this.stationaryToolForm.get("unitPref").setValue(!toggles.isImperial);
          this.isImperial = !toggles.isImperial;

          this.stationaryToolForm
            .get("unitPrefType")
            .setValue(!toggles.isSinglePoint);
          this.isSinglePoint = !toggles.isSinglePoint;

          this.stationaryToolForm
            .get("unitPrefRate")
            .setValue(!toggles.isRateTraverse);
          this.isRateTraverse = !toggles.isRateTraverse;

          this.updateToggle();
          this.updateTypeToggle();
          this.updateRateToggle();
        }
      });
  }
}
