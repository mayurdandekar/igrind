import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Slides,
  Platform,
} from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { areaConversionModel } from "../../utils/areaConversionModel";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import { Storage } from "@ionic/storage";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";
import { TranslateService } from "@ngx-translate/core";
import { Keyboard } from "@ionic-native/keyboard";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { MasterDataProvider } from "../../providers/master-data/master-data";

/**
 * Generated class for the AreaConversionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-area-conversion",
  templateUrl: "area-conversion.html",
})
export class AreaConversionPage {
  @ViewChild("psslides") slides: Slides;

  //
  psslideactive: any = [];

  validator: areaConversionModel;

  //
  roundBarForm: FormGroup;
  rectangularBarForm: FormGroup;
  roundTubeForm: FormGroup;
  rectangularTubeForm: FormGroup;

  resultOpacity: string = "1";
  calculateDisableRoundBar: boolean = false;
  calculateDisableRectBar: boolean = false;
  calculateDisableRoundTube: boolean = false;
  calculateDisableRectTube: boolean = false;
  calculateBtnText: string;
  showErrors: boolean = false;
  showResult: boolean;
  roundBarOutput: {};
  rectBarOutput: {};
  roundTubeOutput: {};
  rectTubeOutput: {};
  autoLoadRoundBar: boolean;
  autoLoadRectBar: boolean;
  showErrorsRectBar: boolean;
  validationMsgsForRectBar: {
    rectBarLength: { type: string; message: any }[];
    rectBarWidth: { type: string; message: any }[];
    inputRectangularBarType: { type: string; message: any };
  };
  validationMsgs: {
    roundBarDia: { type: string; message: any }[];
    inputRoundBarType: { type: string; message: any };
  };
  autoLoadRoundTube: boolean;
  showErrorsRoundTube: boolean;
  validationMsgsForRoundTube: {
    roundTubeOutDia: { type: string; message: any }[];
    roundTubeInDia: { type: string; message: any }[];
    inputRoundTubeType: { type: string; message: any };
  };
  autoLoadRectTube: boolean;
  showErrorsRectTube: boolean;
  showResultRoundBar: boolean;
  showResultRectTube: boolean;
  showResultRoundTube: boolean;
  showResultRectBar: boolean;
  calculateBtnTextRoundBar: any;
  calculateBtnTextRectBar: any;
  calculateBtnTextRoundTube: any;
  calculateBtnTextRectTube: any;
  validationMsgsForRectTube: {
    rectTubeOutLength: { type: string; message: any }[];
    rectTubeOutWidth: { type: string; message: any }[];
    rectTubeInLength: { type: string; message: any }[];
    rectTubeInWidth: { type: string; message: any }[];
    inputRectangularTubeType: { type: string; message: any };
  };
  inputTypeList: any;
  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  toppx: string = "0";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public storage: Storage,
    public firebaseAnalytics: FirebaseAnalytics,
    public platform: Platform,
    public common: CommonHelperProvider,
    public translate: TranslateService,
    private keyboard: Keyboard,
    public masterData: MasterDataProvider
  ) {
    this.masterData.getInputTypeDropDowns().then((inputList) => {
      this.inputTypeList = inputList;
    });
    //Round Bar Form
    translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnTextRoundBar = result;
        this.calculateBtnTextRectBar = result;
        this.calculateBtnTextRoundTube = result;
        this.calculateBtnTextRectTube = result;
      });

    this.validator = new areaConversionModel(
      this.storage,
      this.firebaseAnalytics
    );

    this.roundBarForm = formBuilder.group(
      {
        inputRoundBarType: ["", Validators.required],
        roundBarDia: ["", Validators.required],
      },
      { validator: this.validator.roundBarAreaValidator.bind(this) }
    );
    var roundBarErrMsg;
    var emptyErrMsg;
    var inputLengthErrmsg;
    this.translate
      .get("zero_input")
      .toPromise()
      .then((errorMsg) => {
        roundBarErrMsg = errorMsg;
        this.translate
          .get("empty_invalid_input")
          .toPromise()
          .then((emptyMsg) => {
            emptyErrMsg = emptyMsg;
            this.translate
              .get("select_type_empty")
              .toPromise()
              .then((roundBarTypeEmptyMsg) => {
                this.translate
                  .get("input_length_invalid")
                  .toPromise()
                  .then((inputLengthInvalidMsg) => {
                    inputLengthErrmsg = inputLengthInvalidMsg;
                    this.validationMsgs = {
                      roundBarDia: [
                        { type: "roundBarDia_eng", message: inputLengthErrmsg },
                        { type: "roundBarDia_empty", message: emptyErrMsg },
                      ],
                      inputRoundBarType: {
                        type: "inputRoundBarType_empty",
                        message: roundBarTypeEmptyMsg,
                      },
                    };
                  }); //empty
              });
          }); //input_length_invalid
      });
    this.storage.get("roundbardb").then((roundBarObj) => {
      console.log(roundBarObj);
      if (roundBarObj) {
        this.roundBarForm
          .get("inputRoundBarType")
          .setValue(roundBarObj["inputType"]);
        this.roundBarForm
          .get("roundBarDia")
          .setValue(roundBarObj["roundBarDia"]);
        this.autoLoadRoundBar = true;
        this.onSubmitRoundBarForm(this.roundBarForm.value);
      } else {
        this.autoLoadRoundBar = false;
        this.translate
          .get("calculate_btn")
          .toPromise()
          .then((result) => {
            this.calculateBtnTextRoundBar = result;
          });
      }
    });

    //Rectangular Bar
    this.rectangularBarForm = formBuilder.group(
      {
        inputRectangularBarType: ["", Validators.required],
        rectBarLength: ["", Validators.required],
        rectBarWidth: ["", Validators.required],
      },
      { validator: this.validator.rectangularBarAreaValidator.bind(this) }
    );
    this.translate
      .get("zero_input")
      .toPromise()
      .then((errorMsg) => {
        roundBarErrMsg = errorMsg;
        this.translate
          .get("empty_invalid_input")
          .toPromise()
          .then((emptyMsg) => {
            emptyErrMsg = emptyMsg;
            this.translate
              .get("select_type_empty")
              .toPromise()
              .then((roundBarTypeEmptyMsg) => {
                this.translate
                  .get("input_length_invalid")
                  .toPromise()
                  .then((inputLengthInvalidMsg) => {
                    inputLengthErrmsg = inputLengthInvalidMsg;
                    this.validationMsgsForRectBar = {
                      rectBarLength: [
                        {
                          type: "rectBarLength_eng",
                          message: inputLengthErrmsg,
                        },
                        { type: "rectBarLength_empty", message: emptyErrMsg },
                      ],
                      rectBarWidth: [
                        {
                          type: "rectBarWidth_eng",
                          message: inputLengthErrmsg,
                        },
                        { type: "rectBarWidth_empty", message: emptyErrMsg },
                      ],
                      inputRectangularBarType: {
                        type: "inputRectangularBarType_empty",
                        message: roundBarTypeEmptyMsg,
                      },
                    };
                  }); //empty
              });
          }); //input_length_invalid
      });
    this.storage.get("rectbardb").then((rectBarObj) => {
      console.log(rectBarObj);
      if (rectBarObj) {
        this.rectangularBarForm
          .get("inputRectangularBarType")
          .setValue(rectBarObj["inputType"]);
        this.rectangularBarForm
          .get("rectBarLength")
          .setValue(rectBarObj["rectBarLength"]);
        this.rectangularBarForm
          .get("rectBarWidth")
          .setValue(rectBarObj["rectBarWidth"]);
        this.autoLoadRectBar = true;
        this.onSubmitRectBarForm(this.rectangularBarForm.value);
      } else {
        this.autoLoadRectBar = false;
        this.translate
          .get("calculate_btn")
          .toPromise()
          .then((result) => {
            this.calculateBtnTextRectBar = result;
          });
      }
    });
    //Round Tube
    this.roundTubeForm = formBuilder.group(
      {
        inputRoundTubeType: ["", Validators.required],
        roundTubeOutDia: ["", Validators.required],
        roundTubeInDia: ["", Validators.required],
      },
      { validator: this.validator.roundTubeAreaValidator.bind(this) }
    );
    this.translate
      .get("zero_input")
      .toPromise()
      .then((errorMsg) => {
        roundBarErrMsg = errorMsg;
        this.translate
          .get("empty_invalid_input")
          .toPromise()
          .then((emptyMsg) => {
            emptyErrMsg = emptyMsg;
            this.translate
              .get("select_type_empty")
              .toPromise()
              .then((roundBarTypeEmptyMsg) => {
                this.translate
                  .get("input_length_invalid")
                  .toPromise()
                  .then((inputLengthInvalidMsg) => {
                    inputLengthErrmsg = inputLengthInvalidMsg;
                    this.validationMsgsForRoundTube = {
                      roundTubeOutDia: [
                        {
                          type: "roundTubeOutDia_eng",
                          message: inputLengthErrmsg,
                        },
                        { type: "roundTubeOutDia_empty", message: emptyErrMsg },
                      ],
                      roundTubeInDia: [
                        {
                          type: "roundTubeInDia_eng",
                          message: inputLengthErrmsg,
                        },
                        { type: "roundTubeInDia_empty", message: emptyErrMsg },
                      ],
                      inputRoundTubeType: {
                        type: "inputRoundTubeType_empty",
                        message: roundBarTypeEmptyMsg,
                      },
                    };
                  }); //empty
              });
          }); //input_length_invalid
      });
    this.storage.get("roundtubedb").then((roundTubeObj) => {
      console.log(roundTubeObj);
      if (roundTubeObj) {
        this.roundTubeForm
          .get("inputRoundTubeType")
          .setValue(roundTubeObj["inputType"]);
        this.roundTubeForm
          .get("roundTubeOutDia")
          .setValue(roundTubeObj["roundTubeOutDia"]);
        this.roundTubeForm
          .get("roundTubeInDia")
          .setValue(roundTubeObj["roundTubeInDia"]);
        this.autoLoadRoundTube = true;
        this.onSubmitRoundTubeForm(this.roundTubeForm.value);
      } else {
        this.autoLoadRoundTube = false;
        this.translate
          .get("calculate_btn")
          .toPromise()
          .then((result) => {
            this.calculateBtnTextRoundTube = result;
          });
      }
    });
    //Rectangular Tube
    //
    this.rectangularTubeForm = formBuilder.group(
      {
        inputRectangularTubeType: ["", Validators.required],
        rectTubeOutLength: ["", Validators.required],
        rectTubeOutWidth: ["", Validators.required],
        rectTubeInLength: ["", Validators.required],
        rectTubeInWidth: ["", Validators.required],
      },
      { validator: this.validator.rectangularTubeAreaValidator.bind(this) }
    );
    this.translate
      .get("zero_input")
      .toPromise()
      .then((errorMsg) => {
        roundBarErrMsg = errorMsg;
        this.translate
          .get("empty_invalid_input")
          .toPromise()
          .then((emptyMsg) => {
            emptyErrMsg = emptyMsg;
            this.translate
              .get("select_type_empty")
              .toPromise()
              .then((roundBarTypeEmptyMsg) => {
                this.translate
                  .get("input_length_invalid")
                  .toPromise()
                  .then((inputLengthInvalidMsg) => {
                    inputLengthErrmsg = inputLengthInvalidMsg;
                    this.validationMsgsForRectTube = {
                      rectTubeOutLength: [
                        {
                          type: "rectTubeOutLength_eng",
                          message: inputLengthErrmsg,
                        },
                        {
                          type: "rectTubeOutLength_empty",
                          message: emptyErrMsg,
                        },
                      ],
                      rectTubeOutWidth: [
                        {
                          type: "rectTubeOutWidth_eng",
                          message: inputLengthErrmsg,
                        },
                        {
                          type: "rectTubeOutWidth_empty",
                          message: emptyErrMsg,
                        },
                      ],
                      rectTubeInLength: [
                        {
                          type: "rectTubeInLength_eng",
                          message: inputLengthErrmsg,
                        },
                        {
                          type: "rectTubeInLength_empty",
                          message: emptyErrMsg,
                        },
                      ],
                      rectTubeInWidth: [
                        {
                          type: "rectTubeInWidth_eng",
                          message: inputLengthErrmsg,
                        },
                        { type: "rectTubeInWidth_empty", message: emptyErrMsg },
                      ],
                      inputRectangularTubeType: {
                        type: "inputRectangularTubeType_empty",
                        message: roundBarTypeEmptyMsg,
                      },
                    };
                  }); //empty
              });
          }); //input_length_invalid
      });
    this.storage.get("recttubedb").then((rectTubeObj) => {
      console.log(rectTubeObj);
      if (rectTubeObj) {
        this.rectangularTubeForm
          .get("inputRectangularTubeType")
          .setValue(rectTubeObj["inputType"]);
        this.rectangularTubeForm
          .get("rectTubeOutLength")
          .setValue(rectTubeObj["rectTubeOutLength"]);
        this.rectangularTubeForm
          .get("rectTubeOutWidth")
          .setValue(rectTubeObj["rectTubeOutWidth"]);
        this.rectangularTubeForm
          .get("rectTubeInLength")
          .setValue(rectTubeObj["rectTubeInLength"]);
        this.rectangularTubeForm
          .get("rectTubeInWidth")
          .setValue(rectTubeObj["rectTubeInWidth"]);
        this.autoLoadRectTube = true;
        this.onSubmitRectTubeForm(this.rectangularTubeForm.value);
      } else {
        this.autoLoadRectTube = false;
        this.translate
          .get("calculate_btn")
          .toPromise()
          .then((result) => {
            this.calculateBtnTextRectTube = result;
          });
      }
    });
    sessionStorage.removeItem("actslideno");
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.initPSSlides();
    }, 500);
  }
  ionViewDidLoad() {
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.AREA_CONVERSION_CALCULATOR
    );

    var x = document.getElementById("area-conversion-header");

    this.toppx = x ? (x.clientHeight ? x.clientHeight.toString() : "0") : "0";
  }

  /**
   * Name: onSubmit
   * Parameters: value
   * Description: used to read data from ui to show validation messages if required and to perform required calculations
   * Return: none
   */
  onSubmitRoundBarForm(value: any): void {
    this.keyboard.hide();
    var roundBarDiaHasErrors: boolean = false;
    var inputRoundBarTypeHasErrors: boolean = false;
    for (let validation of this.validationMsgs.roundBarDia) {
      if (this.roundBarForm.get("roundBarDia").hasError(validation.type)) {
        roundBarDiaHasErrors = true;
      }
    }
    if (
      this.roundBarForm
        .get("inputRoundBarType")
        .hasError(this.validationMsgs.inputRoundBarType.type)
    ) {
      inputRoundBarTypeHasErrors = true;
    }
    if (!roundBarDiaHasErrors && !inputRoundBarTypeHasErrors) {
      if (!this.autoLoadRoundBar) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.ROUND_BAR_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoadRoundBar = false;
      }
      this.calculateRoundBar(value.inputRoundBarType, value.roundBarDia);
    } else {
      this.getFormValidationErrorsRoundBar();
      this.showErrors = true;
      this.roundBarForm.get("inputRoundBarType").markAsTouched();
      this.roundBarForm.get("roundBarDia").markAsTouched();
    }

    //
  }

  /**
   * Name: getFormValidationErrors
   * Parameters: none
   * Description: reads validation error messages
   * Return: none
   */
  getFormValidationErrorsRoundBar() {
    Object.keys(this.roundBarForm.controls).forEach((key) => {
      const controlErrors = this.roundBarForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          this.validator.triggerFirebaseEventRoundBar(keyError);
        });
      }
    });
  }
  getFormValidationErrorsRectBar() {
    Object.keys(this.rectangularBarForm.controls).forEach((key) => {
      const controlErrors = this.roundBarForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          this.validator.triggerFirebaseEventRectBar(keyError);
        });
      }
    });
  }
  getFormValidationErrorsRoundTube() {
    Object.keys(this.roundTubeForm.controls).forEach((key) => {
      const controlErrors = this.roundBarForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          this.validator.triggerFirebaseEventRoundTube(keyError);
        });
      }
    });
  }
  getFormValidationErrorsRectTube() {
    Object.keys(this.rectangularTubeForm.controls).forEach((key) => {
      const controlErrors = this.roundBarForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          this.validator.triggerFirebaseEventRectTube(keyError);
        });
      }
    });
  }
  calculateRoundBar(inputType, roundBarDia) {
    this.roundBarOutput = this.validator.calculateRoundBar(
      inputType,
      roundBarDia
    );
    this.showResultRoundBar = true;
    this.resultOpacity = "1";
    this.calculateDisableRoundBar = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnTextRoundBar = result;
      });

    this.validator.productTypeEventsRoundBar(inputType);
  }
  onSubmitRectBarForm(value: any): void {
    this.keyboard.hide();
    var rectBarLengthHasErrors: boolean = false;
    var rectBarWidthHasErrors: boolean = false;
    var inputRectangularBarTypeHasErrors: boolean = false;
    for (let validation of this.validationMsgsForRectBar.rectBarLength) {
      console.log(
        this.rectangularBarForm.get("rectBarLength").hasError(validation.type)
      );

      if (
        this.rectangularBarForm.get("rectBarLength").hasError(validation.type)
      ) {
        rectBarLengthHasErrors = true;
      }
    }
    for (let validation of this.validationMsgsForRectBar.rectBarWidth) {
      console.log(
        this.rectangularBarForm.get("rectBarWidth").hasError(validation.type)
      );

      if (
        this.rectangularBarForm.get("rectBarWidth").hasError(validation.type)
      ) {
        rectBarWidthHasErrors = true;
      }
    }
    if (
      this.rectangularBarForm
        .get("inputRectangularBarType")
        .hasError(this.validationMsgsForRectBar.inputRectangularBarType.type)
    ) {
      inputRectangularBarTypeHasErrors = true;
    }
    if (
      !rectBarLengthHasErrors &&
      !rectBarWidthHasErrors &&
      !inputRectangularBarTypeHasErrors
    ) {
      if (!this.autoLoadRectBar) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.RECT_BAR_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoadRectBar = false;
      }
      this.calculateRectBar(
        value.inputRectangularBarType,
        value.rectBarLength,
        value.rectBarWidth
      );
    } else {
      this.getFormValidationErrorsRoundBar();
      this.showErrorsRectBar = true;
      this.rectangularBarForm.get("inputRectangularBarType").markAsTouched();
      this.rectangularBarForm.get("rectBarLength").markAsTouched();
      this.rectangularBarForm.get("rectBarWidth").markAsTouched();
    }

    //
  }
  calculateRectBar(inputType, rectBarLength, rectBarWidth) {
    this.rectBarOutput = this.validator.calculateRectangularBar(
      inputType,
      rectBarLength,
      rectBarWidth
    );
    this.showResultRectBar = true;
    this.resultOpacity = "1";
    this.calculateDisableRectBar = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnTextRectBar = result;
      });

    this.validator.productTypeEventsRectBar(inputType);
  }
  onSubmitRoundTubeForm(value: any): void {
    this.keyboard.hide();
    var roundTubeOutDiaHasErrors: boolean = false;
    var roundTubeInDiaHasErrors: boolean = false;
    var inputRoundTubeTypeHasErrors: boolean = false;
    for (let validation of this.validationMsgsForRoundTube.roundTubeOutDia) {
      console.log(
        this.roundTubeForm.get("roundTubeOutDia").hasError(validation.type)
      );

      if (this.roundTubeForm.get("roundTubeOutDia").hasError(validation.type)) {
        roundTubeOutDiaHasErrors = true;
      }
    }
    for (let validation of this.validationMsgsForRoundTube.roundTubeInDia) {
      console.log(
        this.roundTubeForm.get("roundTubeInDia").hasError(validation.type)
      );

      if (this.roundTubeForm.get("roundTubeInDia").hasError(validation.type)) {
        roundTubeInDiaHasErrors = true;
      }
    }
    if (
      this.roundTubeForm
        .get("inputRoundTubeType")
        .hasError(this.validationMsgsForRoundTube.inputRoundTubeType.type)
    ) {
      inputRoundTubeTypeHasErrors = true;
    }
    if (
      !roundTubeOutDiaHasErrors &&
      !roundTubeInDiaHasErrors &&
      !inputRoundTubeTypeHasErrors
    ) {
      if (!this.autoLoadRoundTube) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.ROUND_TUBE_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoadRoundTube = false;
      }
      this.calculateRoundTube(
        value.inputRoundTubeType,
        value.roundTubeOutDia,
        value.roundTubeInDia
      );
    } else {
      this.getFormValidationErrorsRoundBar();
      this.showErrorsRoundTube = true;
      this.roundTubeForm.get("inputRoundTubeType").markAsTouched();
      this.roundTubeForm.get("roundTubeInDia").markAsTouched();
      this.roundTubeForm.get("roundTubeOutDia").markAsTouched();
    }
  }
  calculateRoundTube(inputType, roundTubeOutDia, roundTubeInDia) {
    this.roundTubeOutput = this.validator.calculateRoundTube(
      inputType,
      roundTubeOutDia,
      roundTubeInDia
    );
    this.showResultRoundTube = true;
    this.resultOpacity = "1";
    this.calculateDisableRoundTube = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnTextRoundTube = result;
      });

    this.validator.productTypeEventsRoundTube(inputType);
  }
  onSubmitRectTubeForm(value: any): void {
    this.keyboard.hide();
    var rectTubeOutLengthHasErrors: boolean = false;
    var rectTubeOutWidthHasErrors: boolean = false;
    var rectTubeInLengthHasErrors: boolean = false;
    var rectTubeInWidthHasErrors: boolean = false;
    var inputRectangularTubeTypeHasErrors: boolean = false;
    for (let validation of this.validationMsgsForRectTube.rectTubeOutLength) {
      console.log(
        this.rectangularTubeForm
          .get("rectTubeOutLength")
          .hasError(validation.type)
      );
      if (
        this.rectangularTubeForm
          .get("rectTubeOutLength")
          .hasError(validation.type)
      ) {
        rectTubeOutLengthHasErrors = true;
      }
    }
    for (let validation of this.validationMsgsForRectTube.rectTubeOutWidth) {
      console.log(
        this.rectangularTubeForm
          .get("rectTubeOutWidth")
          .hasError(validation.type)
      );
      if (
        this.rectangularTubeForm
          .get("rectTubeOutWidth")
          .hasError(validation.type)
      ) {
        rectTubeOutWidthHasErrors = true;
      }
    }
    for (let validation of this.validationMsgsForRectTube.rectTubeInLength) {
      console.log(
        this.rectangularTubeForm
          .get("rectTubeInLength")
          .hasError(validation.type)
      );
      if (
        this.rectangularTubeForm
          .get("rectTubeInLength")
          .hasError(validation.type)
      ) {
        rectTubeInLengthHasErrors = true;
      }
    }
    for (let validation of this.validationMsgsForRectTube.rectTubeInWidth) {
      console.log(
        this.rectangularTubeForm
          .get("rectTubeInWidth")
          .hasError(validation.type)
      );
      if (
        this.rectangularTubeForm
          .get("rectTubeInWidth")
          .hasError(validation.type)
      ) {
        rectTubeInWidthHasErrors = true;
      }
    }
    if (
      this.rectangularTubeForm
        .get("inputRectangularTubeType")
        .hasError(this.validationMsgsForRectTube.inputRectangularTubeType.type)
    ) {
      inputRectangularTubeTypeHasErrors = true;
    }
    if (
      !rectTubeOutLengthHasErrors &&
      !rectTubeOutWidthHasErrors &&
      !rectTubeInLengthHasErrors &&
      !rectTubeInWidthHasErrors &&
      !inputRectangularTubeTypeHasErrors
    ) {
      if (!this.autoLoadRectTube) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.RECT_TUBE_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoadRectTube = false;
      }
      this.calculateRectTube(
        value.inputRectangularTubeType,
        value.rectTubeOutLength,
        value.rectTubeOutWidth,
        value.rectTubeInLength,
        value.rectTubeInWidth
      );
    } else {
      this.getFormValidationErrorsRoundBar();
      this.showErrorsRectTube = true;
      this.rectangularTubeForm.get("inputRectangularTubeType").markAsTouched();
      this.rectangularTubeForm.get("rectTubeOutLength").markAsTouched();
      this.rectangularTubeForm.get("rectTubeOutWidth").markAsTouched();
      this.rectangularTubeForm.get("rectTubeInLength").markAsTouched();
      this.rectangularTubeForm.get("rectTubeInWidth").markAsTouched();
    }
  }
  calculateRectTube(
    inputType,
    rectTubeOutLength,
    rectTubeOutWidth,
    rectTubeInLength,
    rectTubeInWidth
  ) {
    this.rectTubeOutput = this.validator.calculateRectTube(
      inputType,
      rectTubeOutLength,
      rectTubeOutWidth,
      rectTubeInLength,
      rectTubeInWidth
    );
    this.showResultRectTube = true;
    this.resultOpacity = "1";
    this.calculateDisableRectTube = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnTextRectTube = result;
      });

    this.validator.productTypeEventsRectTube(inputType);
  }
  initPSSlides() {
    this.slides.effect = "slide";
    this.slides.loop = false;
    this.slides.paginationType = "bullets";
    this.slides.zoom = false;
    this.slides.spaceBetween = 0;

    // additional part
    var sactslideno = sessionStorage.getItem("actslideno");
    var isactslideno = sactslideno ? parseInt(sactslideno) : 0;

    this.slides.slideTo(isactslideno);
    this.psslideactive = [];
    this.psslideactive[this.slides.getActiveIndex()] = "divbtnpssheaderactive";
  }

  //
  psSlideGoto(inm) {
    this.slides.slideTo(inm);
  }

  //
  onPSSlideChange(ev) {
    this.keyboard.hide();

    this.psslideactive = [];
    console.log(this.slides.getActiveIndex());
    if (this.slides.getActiveIndex() > 3) {
      this.psslideactive[3] = "divbtnpssheaderactive";
    } else {
      this.psslideactive[this.slides.getActiveIndex()] =
        "divbtnpssheaderactive";
    }
    sessionStorage.setItem(
      "actslideno",
      this.slides.getActiveIndex().toString()
    );
  }
  clearResultsRoundBar() {
    this.showResultRoundBar = false;
    this.showErrors = false;

    this.roundBarForm.get("inputRoundBarType").setValue("");

    this.roundBarForm.get("roundBarDia").setValue("");
    this.roundBarForm.get("roundBarDia").markAsUntouched();
    this.roundBarForm.get("roundBarDia").markAsPristine();

    this.resultOpacity = "1";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnTextRoundBar = result;
      });
    this.storage
      .remove("roundbardb")
      .then((output) => {})
      .catch((err) => {});
  }
  clearResultsRectBar() {
    this.showResultRectBar = false;
    this.showErrorsRectBar = false;

    this.rectangularBarForm.get("inputRectangularBarType").setValue("");

    this.rectangularBarForm.get("rectBarLength").setValue("");
    this.rectangularBarForm.get("rectBarLength").markAsUntouched();
    this.rectangularBarForm.get("rectBarLength").markAsPristine();

    this.rectangularBarForm.get("rectBarWidth").setValue("");
    this.rectangularBarForm.get("rectBarWidth").markAsUntouched();
    this.rectangularBarForm.get("rectBarWidth").markAsPristine();

    this.resultOpacity = "1";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnTextRectBar = result;
      });
    this.storage
      .remove("rectbardb")
      .then((output) => {})
      .catch((err) => {});
  }
  clearResultsRoundTube() {
    this.showResultRoundTube = false;
    this.showErrorsRoundTube = false;

    this.roundTubeForm.get("inputRoundTubeType").setValue("");

    this.roundTubeForm.get("roundTubeOutDia").setValue("");
    this.roundTubeForm.get("roundTubeOutDia").markAsUntouched();
    this.roundTubeForm.get("roundTubeOutDia").markAsPristine();

    this.roundTubeForm.get("roundTubeInDia").setValue("");
    this.roundTubeForm.get("roundTubeInDia").markAsUntouched();
    this.roundTubeForm.get("roundTubeInDia").markAsPristine();

    this.resultOpacity = "1";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnTextRoundTube = result;
      });
    this.storage
      .remove("roundtubedb")
      .then((output) => {})
      .catch((err) => {});
  }
  clearResultsRectTube() {
    this.showResultRectTube = false;
    this.showErrorsRectTube = false;

    this.rectangularTubeForm.get("inputRectangularTubeType").setValue("");

    this.rectangularTubeForm.get("rectTubeOutLength").setValue("");
    this.rectangularTubeForm.get("rectTubeOutLength").markAsUntouched();
    this.rectangularTubeForm.get("rectTubeOutLength").markAsPristine();

    this.rectangularTubeForm.get("rectTubeOutWidth").setValue("");
    this.rectangularTubeForm.get("rectTubeOutWidth").markAsUntouched();
    this.rectangularTubeForm.get("rectTubeOutWidth").markAsPristine();

    this.rectangularTubeForm.get("rectTubeInLength").setValue("");
    this.rectangularTubeForm.get("rectTubeInLength").markAsUntouched();
    this.rectangularTubeForm.get("rectTubeInLength").markAsPristine();

    this.rectangularTubeForm.get("rectTubeInWidth").setValue("");
    this.rectangularTubeForm.get("rectTubeInWidth").markAsUntouched();
    this.rectangularTubeForm.get("rectTubeInWidth").markAsPristine();

    this.resultOpacity = "1";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnTextRectTube = result;
      });
    this.storage
      .remove("recttubedb")
      .then((output) => {})
      .catch((err) => {});
  }
  /**
   * Name: updateResults
   * Parameters: none
   * Description: Used to blur the result area and to display update button
   * Return: none
   */
  updateResultsRoundBar() {
    this.calculateDisableRoundBar = false;
    this.resultOpacity = "0.3";
  }
  updateResultsRectBar() {
    this.calculateDisableRectBar = false;
    this.resultOpacity = "0.3";
  }
  updateResultsRoundTube() {
    this.calculateDisableRoundTube = false;
    this.resultOpacity = "0.3";
  }
  updateResultsRectTube() {
    this.calculateDisableRectTube = false;
    this.resultOpacity = "0.3";
  }
  /**
   * Name: round
   * Parameters: value, precision
   * Description: used to perform rounding to decimal numbers based on precision
   * Return: float number
   */
  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  }

  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    var enteredInput = event.target.value;
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    // var inp = ev.key;
    // if (ev.target.value.length >= 9 || !(this.common.floatnoValidationRunTime(inp))) {
    //   ev.preventDefault();
    // }
    this.common.telkeypress(ev);
  }
  //
}
