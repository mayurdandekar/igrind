import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AreaConversionPage } from './area-conversion';

@NgModule({
  declarations: [
    AreaConversionPage,
  ],
  imports: [
    IonicPageModule.forChild(AreaConversionPage),
  ],
})
export class AreaConversionPageModule {}
