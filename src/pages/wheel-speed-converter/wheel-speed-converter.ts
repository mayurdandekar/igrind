import { Component, ViewChild } from "@angular/core";
import { Platform, Select } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { NavController, NavParams } from "ionic-angular";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import { WheelSpeedConverterModel } from "../../utils/wheel-speed-converter-model";
import * as Constants from "../../utils/constants";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { TranslateService } from "@ngx-translate/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { MasterDataProvider } from "../../providers/master-data/master-data";
import { Keyboard } from "@ionic-native/keyboard";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";
/**
 * Generated class for the WheelSpeedConverterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-wheel-speed-converter",
  templateUrl: "wheel-speed-converter.html",
})
export class WheelSpeedConverterPage {
  showResult: boolean;
  private autoLoad: boolean = true;
  calculator: WheelSpeedConverterModel;
  speedListRefs: Array<any>;
  wheelSpeedType: any;
  op_rpm: any;
  op_sfpm: any;
  op_ms: any;
  rpmWeight: string = "normal";
  sfpmWeight: string = "normal";
  mpsWeight: string = "normal";
  wheelSpeedForm: FormGroup;

  validationMsgs: { diameter: Array<any>; wheelSpeed: Array<any> };
  isEnglish: boolean;

  resultOpacity: string = "1";
  calculateDisable: boolean = false;
  calculateBtnText: string;
  showErrors: boolean = false;
  diameterPh = "";
  @ViewChild("wheelSpeedSelect") select: Select;
  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  constructor(
    public platform: Platform,
    private keyboard: Keyboard,
    public statusBar: StatusBar,
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public masterData: MasterDataProvider,
    public translate: TranslateService,
    public storage: Storage,
    public firebaseAnalytics: FirebaseAnalytics,
    public common: CommonHelperProvider
  ) {
    this.calculator = new WheelSpeedConverterModel(this.storage);

    /**
     * Name :       getWheelSpeedDropDowns
     * Parameters:  --
     * Description: get a list of units to show in a dropdown
     * Return:      an array of options
     */
    this.masterData.getWheelSpeedDropDowns().then((speedList) => {
      this.speedListRefs = speedList;
    });

    /**
     * Name :       translate.get
     * Parameters:  text value
     * Description:     translate the input text
     * Return:      translated text
     */

    translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    /**
     * Name :       formBuilder.group
     * Parameters:  parameters and validator
     * Description: it specifies if the field is required
     * Return:      --
     */
    this.wheelSpeedForm = formBuilder.group(
      {
        diameter: ["", Validators.required],
        unitPref: [null],
        wheelSpeedType: ["", Validators.required],
        wheelSpeed: ["", Validators.required],
      },
      { validator: this.calculator.wheelSpeedValidator.bind(this) }
    );

    this.wheelSpeedForm.get("unitPref").setValue(false);
    this.isEnglish = true;
    this.translate
      .get("wheel_diameter_ph")
      .toPromise()
      .then((res) => {
        this.diameterPh = res + " (in)";
      });
    this.loadSwitchFromDb();

    /**
     * Name :       storage.get
     * Parameters:  Wheel string
     * Description: fetches and sets the values from the DB
     * Return:      --
     */
    this.storage.get(Constants.KEY_WHEEL).then((wheelObj) => {
      console.log(wheelObj);
      if (wheelObj) {
        this.wheelSpeedForm
          .get("wheelSpeedType")
          .setValue(wheelObj.wheelSpeedType);
        this.wheelSpeedForm.get("wheelSpeed").setValue(wheelObj.wheelSpeed);
        this.wheelSpeedForm.get("diameter").setValue(wheelObj.diameter);
        this.autoLoad = true;
        this.onSubmit(this.wheelSpeedForm.value);
      } else {
        this.autoLoad = false;
      }
    });

    var wheelSpeedErrMsg;
    var emptyErrMsg;
    var inputLengthErrmsg;
    this.translate
      .get("zero_input")
      .toPromise()
      .then((errorMsg) => {
        wheelSpeedErrMsg = errorMsg;
        this.translate
          .get("empty_invalid_input")
          .toPromise()
          .then((emptyMsg) => {
            emptyErrMsg = emptyMsg;
            this.translate
              .get("wheel_speed_empty")
              .toPromise()
              .then((wheelSpeedEmptyMsg) => {
                this.translate
                  .get("input_length_invalid")
                  .toPromise()
                  .then((inputLengthInvalidMsg) => {
                    inputLengthErrmsg = inputLengthInvalidMsg;
                    this.validationMsgs = {
                      diameter: [
                        { type: "dia_eng", message: inputLengthErrmsg },
                        { type: "dia_empty", message: emptyErrMsg },
                        { type: "dia_metric", message: wheelSpeedErrMsg },
                      ],
                      wheelSpeed: [
                        { type: "wheelSpeedType", message: wheelSpeedEmptyMsg },
                        { type: "wheel_empty", message: emptyErrMsg },
                        { type: "wheelSpeed", message: wheelSpeedErrMsg },
                        { type: "wheelLength", message: inputLengthErrmsg },
                      ],
                    };
                  }); //empty
              });
          }); //input_length_invalid
      });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad WheelSpeedConverterPage");
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.WHEEL_SPEED_CONVERTER
    );
  }

  ionViewWillLeave() {
    this.select.close();
  }

  /**
   * Name :       onSubmit
   * Parameters:  value
   * Description: validates input fields. If it passes validation then call calculate & log Firebase analytics evets.
   * Return:      void
   */
  onSubmit(value: any): void {
    this.keyboard.hide();
    var diameterHasErrors: boolean = false;
    var wheelSpeedHasErrors: boolean = false;
    console.log(this.wheelSpeedForm.get("diameter"));
    for (let validation of this.validationMsgs.diameter) {
      if (this.wheelSpeedForm.get("diameter").hasError(validation.type)) {
        diameterHasErrors = true;
      }
    }
    console.log(1);
    for (let validation of this.validationMsgs.wheelSpeed) {
      if (this.wheelSpeedForm.get("wheelSpeed").hasError(validation.type)) {
        wheelSpeedHasErrors = true;
      }
    }

    if (!diameterHasErrors && !wheelSpeedHasErrors) {
      if (!this.autoLoad) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.WHEEL_SPEED_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoad = false;
      }
      this.calculate(
        value.unitPref,
        value.wheelSpeedType,
        value.diameter,
        value.wheelSpeed
      );
    } else {
      for (let validation of this.validationMsgs.diameter) {
        if (this.wheelSpeedForm.get("diameter").hasError(validation.type)) {
          this.firebaseAnalytics.logEvent(
            FirebaseConstants.WHEEL_SPEED_CALC_OOR_DIAMETER_HITCOUNT,
            {}
          );
        }
      }
      for (let validation of this.validationMsgs.wheelSpeed) {
        if (this.wheelSpeedForm.get("wheelSpeed").hasError(validation.type)) {
          this.firebaseAnalytics.logEvent(
            FirebaseConstants.WHEEL_SPEED_CALC_OOR_SPEED_HITCOUNT,
            {}
          );
        }
      }
      this.showErrors = true;

      this.wheelSpeedForm.get("wheelSpeed").markAsTouched();
      this.wheelSpeedForm.get("diameter").markAsTouched();
    }
  }

  /**
   * Name :       calculate
   * Parameters:  input values
   * Description: Calculates the result and assigns the result to the variables
   * Return:      void
   */
  calculate(unitPref, wheelSpeedType, diameter, wheelSpeed) {
    let diameterNum: number = parseFloat(diameter);
    let wheelSpeedNum: number = parseFloat(wheelSpeed);
    var output = this.calculator.calculatewheelSpeed(
      !unitPref,
      wheelSpeedType,
      diameterNum,
      wheelSpeedNum
    );
    this.op_rpm = output[Constants.KEY_RPM];
    this.op_ms = output[Constants.KEY_MPS];
    this.op_sfpm = output[Constants.KEY_SFPM];
    this.showResult = true;
    this.resultOpacity = "1";
    switch (wheelSpeedType) {
      case Constants.KEY_RPM:
        // type : RPM
        this.rpmWeight = "normal";
        this.sfpmWeight = "bold";
        this.mpsWeight = "bold";
        break;
      case Constants.KEY_SFPM:
        // type : SFPM
        this.rpmWeight = "bold";
        this.sfpmWeight = "normal";
        this.mpsWeight = "bold";
        break;
      case Constants.KEY_MPS:
        // type : MPS
        this.rpmWeight = "bold";
        this.sfpmWeight = "bold";
        this.mpsWeight = "normal";
        break;

      default:
        break;
    }
    this.calculateDisable = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
  }

  /**
   * Name :       clearResults
   * Parameters:  --
   * Description: clears all the fields and DB
   * Return:      void
   */
  clearResults() {
    this.showResult = false;
    this.op_rpm = "";
    this.op_ms = "";
    this.op_sfpm = "";
    this.wheelSpeedForm.get("wheelSpeedType").setValue("");

    this.wheelSpeedForm.get("wheelSpeed").reset();
    this.wheelSpeedForm.get("diameter").reset();

    this.wheelSpeedForm.get("wheelSpeed").setValue(" ");
    this.wheelSpeedForm.get("diameter").setValue(" ");

    this.wheelSpeedForm.get("wheelSpeed").setValue("");
    this.wheelSpeedForm.get("diameter").setValue("");

    this.wheelSpeedForm.get("wheelSpeed").markAsPristine();
    this.wheelSpeedForm.get("diameter").markAsPristine();
    this.wheelSpeedForm.get("wheelSpeed").markAsUntouched();
    this.wheelSpeedForm.get("diameter").markAsUntouched();
    this.loadSwitchFromDb();

    this.resultOpacity = "1";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
    this.storage
      .remove(Constants.KEY_WHEEL)
      .then((output) => {
        console.log("Wheel Speed Cleared: " + output);
      })
      .catch((err) => {
        console.log("Wheel Speed Clear error: " + err);
      });
    this.showErrors = false;
  }

  /**
   * Name :       updateResults
   * Parameters:  --
   * Description: Updates Result area
   * Return:      void
   */
  updateResults() {
    this.calculateDisable = false;
    this.resultOpacity = "0.3";
  }

  /**
   * Name :       updateToggle
   * Parameters:  --
   * Description: Updates toggle as per the Unit selected
   * Return:      void
   */
  updateToggle() {
    this.showErrors = false;
    this.wheelSpeedForm.get("diameter").setValue("");
    this.wheelSpeedForm.get("diameter").markAsUntouched();
    this.wheelSpeedForm.get("diameter").markAsPristine();

    this.wheelSpeedForm.get("wheelSpeedType").setValue("");
    this.wheelSpeedForm.get("wheelSpeedType").markAsUntouched();
    this.wheelSpeedForm.get("wheelSpeedType").markAsPristine();

    this.wheelSpeedForm.get("wheelSpeed").setValue("");
    this.wheelSpeedForm.get("wheelSpeed").markAsUntouched();
    this.wheelSpeedForm.get("wheelSpeed").markAsPristine();

    this.isEnglish = !this.wheelSpeedForm.get("unitPref").value;
    this.translate
      .get("wheel_diameter_ph")
      .toPromise()
      .then((res) => {
        if (this.isEnglish) {
          this.diameterPh = res + " (in)";
        } else {
          this.diameterPh = res + " (mm)";
        }
      });
  }

  /**
   * Name :       updateDropDown
   * Parameters:  --
   * Description: Updates dropdown
   * Return:      void
   */
  updateDropDown() {
    this.wheelSpeedForm.get("wheelSpeed").setValue("");
  }

  /**
   * Name :       loadSwitchFromDb
   * Parameters:  --
   * Description: loads the switch status from DB
   * Return:      void
   */
  loadSwitchFromDb() {
    this.storage.get(Constants.KEY_UNIT_PREF_WHEEL_SPEED).then((toggle) => {
      if (toggle !== null) {
        this.wheelSpeedForm.get("unitPref").setValue(toggle);
        this.isEnglish = !toggle;
        this.translate
          .get("wheel_diameter_ph")
          .toPromise()
          .then((res) => {
            if (this.isEnglish) {
              this.diameterPh = res + " (in)";
            } else {
              this.diameterPh = res + " (mm)";
            }
          });
      }
    });
  }

  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    var enteredInput = event.target.value;
    var field = this;
    console.log(field);

    if (enteredInput)
      var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    this.common.telkeypress(ev);
  }
}
