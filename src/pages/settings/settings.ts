import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  Events,
} from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
import { HomePage } from "../home/home";

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-settings",
  templateUrl: "settings.html",
})
export class SettingsPage {
  selectedLanguage: string = "";
  availableLanguages: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public translate: TranslateService,
    public nav: NavController,
    public events: Events
  ) {}

  ionViewDidLoad() {
    this.menuCtrl.close();
    this.initAvailableLanguages();

    var appLanguage = localStorage.getItem("globalLanguage");
    this.selectedLanguage = appLanguage ? appLanguage : "en";
  }

  initAvailableLanguages() {
    this.availableLanguages = [
      { name: "English", value: "en" },
      { name: "Italian", value: "it" },
    ];
  }

  setLanguage() {
    var globalLanguage = this.selectedLanguage;
    this.translate.use(globalLanguage);
    localStorage.setItem("globalLanguage", globalLanguage);

    this.events.publish("setup:launch");

    setTimeout(() => {
      this.nav.setRoot(HomePage);
    }, 1000);
  }
}
