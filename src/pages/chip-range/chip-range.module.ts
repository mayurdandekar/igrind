import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChipRangePage } from './chip-range';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ChipRangePage,
  ],
  imports: [
    IonicPageModule.forChild(ChipRangePage),
    PipesModule,
    TranslateModule.forChild()
    
  ],
})
export class ChipRangePageModule {}
