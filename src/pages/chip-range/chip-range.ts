import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import * as Constants from '../../utils/constants';

/**
 * Generated class for the ChipRangePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chip-range',
  templateUrl: 'chip-range.html',
})
export class ChipRangePage {
  unit_HEC: any;
  chip_range: any = [];
  isEnglish: boolean;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private translate: TranslateService) {
    this.unit_HEC = Constants.KEY_HEC;


    this.isEnglish = this.navParams.get('isEnglish');
    //Initialize Qprime Ranges
    console.log(this.translate.get('precision_finishing'));

    if (this.isEnglish == true) {

      this.chip_range = [
        { 'id': 1, 'description': this.translate.instant('precision_finishing')  , 'range': '0.39 - 4.0' },
        { 'id': 2, 'description': this.translate.instant('precision_grinding') , 'range': '4.0 - 27.0' },
        { 'id': 3, 'description': this.translate.instant('rough_grinding') , 'range': '27.0 - 137.0' }
      ];

    }
    else {
      this.chip_range = [
        { 'id': 1, 'description': this.translate.instant('precision_finishing') , 'range': '0.01 - 0.1' },
        { 'id': 2, 'description': this.translate.instant('precision_grinding') , 'range': '0.1 - 0.7' },
        { 'id': 3, 'description': this.translate.instant('rough_grinding') , 'range': '0.7 - 3.5' }
      ];

    }
  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChipRangePage');
  }

}
