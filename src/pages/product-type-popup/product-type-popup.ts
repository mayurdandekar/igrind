import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MasterDataProvider } from '../../providers/master-data/master-data';
import { ViewController } from 'ionic-angular';
/**
 * Generated class for the ProductTypePopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-type-popup',
  templateUrl: 'product-type-popup.html',
})
export class ProductTypePopupPage {
  productTypeListRefs: Array<any>;
  productTypeListRefsOne: Array<any>;
  productTypeListRefsTwo: Array<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public masterData: MasterDataProvider, public viewCtrl: ViewController) {

    this.masterData.getProductTypeDropDowns().then((productList) => {
      this.productTypeListRefs = productList;
      this.productTypeListRefsOne = this.productTypeListRefs.slice(0, 2);
      this.productTypeListRefsTwo = this.productTypeListRefs.slice(2);
      console.log(this.productTypeListRefsOne, this.productTypeListRefsTwo);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductTypePopupPage');
  }

  ionViewWillLeave() {
  }

  selected(type){
    console.log(JSON.stringify(type));
    // let data = {
    //   value: selValue,
    //   alias: selAlias
    // }
    this.viewCtrl.dismiss(type);
  }
}
