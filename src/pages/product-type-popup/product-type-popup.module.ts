import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductTypePopupPage } from './product-type-popup';

@NgModule({
  declarations: [
    ProductTypePopupPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductTypePopupPage),
  ],
})
export class ProductTypePopupPageModule {}
