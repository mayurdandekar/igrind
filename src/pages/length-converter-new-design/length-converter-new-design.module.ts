import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LengthConverterNewDesignPage } from './length-converter-new-design';

@NgModule({
  declarations: [
    LengthConverterNewDesignPage,
  ],
  imports: [
    IonicPageModule.forChild(LengthConverterNewDesignPage),
  ],
})
export class LengthConverterNewDesignPageModule {}
