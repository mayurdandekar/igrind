import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  ModalController,
  MenuController,
} from "ionic-angular";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";
import { MasterDataProvider } from "../../providers/master-data/master-data";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { CalcComponent } from "../../components/calc/calc";
import { CalcAllPage } from "../calc-all/calc-all";
import { Keyboard } from "@ionic-native/keyboard";

/**
 * Generated class for the LengthConverterNewDesignPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-length-converter-new-design",
  templateUrl: "length-converter-new-design.html",
})
export class LengthConverterNewDesignPage {
  inputValue: any;
  selectedData: string = "uin";
  outputData: any;
  lengthConverterData: any;
  error1: boolean = false;
  margin: string;
  isKeyBoard: boolean = false;
  currentInputRef: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public common: CommonHelperProvider,
    public masterData: MasterDataProvider,
    public modalCtrl: ModalController,
    public firebaseAnalytics: FirebaseAnalytics
  ) {
    this.margin = "marginTop15";

    this.inputValue = "";
    console.log(this.platform.is("ios"));
    this.masterData.getLengthConverterData().then((val) => {
      this.lengthConverterData = val;
      this.selectedData = this.lengthConverterData[0].data;
      console.log(this.lengthConverterData);
      this.onChange(this.selectedData);
    });
    // this.keyboard.onKeyboardWillShow().subscribe((data) => {
    //   this.isKeyBoard = true;
    // });
    // this.keyboard.onKeyboardWillHide().subscribe((data) => {
    //   this.isKeyBoard = false;
    // });
  }

  onChange(event) {
    console.log(event);
    for (var i = 0; i < this.lengthConverterData.length; i++) {
      if (event == this.lengthConverterData[i].data) {
        this.outputData = this.lengthConverterData[i].outp;
        if (!!this.inputValue) this.onChangeInput(this.inputValue);
        return;
      }
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad VelocityConverterPage");
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.LENGTH_CONVERTER_CALCULATOR
    );
    this.firebaseAnalytics.logEvent(
      FirebaseConstants.LENGTH_VISIT_HITCOUNT,
      {}
    );
  }

  ionViewWillEnter() {
    // this.menuCtrl.enable(true, "main_menu");
    console.log(this.navParams.get("result"));

    this.navParams.get("result")
      ? (this.inputValue = this.navParams.get("result"))
      : "";
  }

  onChangeInput(value) {
    console.log(value, "value console");
    // if(parseInt(this.inputValue).toString().length>9)
    console.log(this.outputData);
    for (let j = 0; j < this.outputData.length; j++) {
      this.outputData[j].outp = this.outputData[j].value * value;
    }
  }
  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    var enteredInput = event.target.value;
    var field = this;
    console.log(field);

    if (this.inputValue == 0 || !this.inputValue == undefined) {
      this.error1 = true;
      this.margin = "marginTop25";
      this.reset();
    } else {
      this.error1 = false;
      this.margin = "marginTop15";
    }

    if (this.inputValue.length > 9) {
      this.inputValue = this.inputValue.substring(
        0,
        this.inputValue.length - 1
      );
    }

    // if (enteredInput)
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    this.common.telkeypress(ev);
  }

  /**
   * Name : round
   * Parameters: 'value' & 'precision' of type any
   * Description: This method round of the value to the given precision.
   * Return: void
   */
  round(num, X) {
    return +(Math.round(parseFloat(num + "e+" + X)) + "e-" + X);
  }

  reset() {
    for (let i = 0; i < this.lengthConverterData.length; i++) {
      for (let j = 0; j < this.lengthConverterData[i].outp.length; j++) {
        this.lengthConverterData[i].outp[j].outp = 0;
      }
    }
    for (var i = 0; i < this.lengthConverterData.length; i++) {
      if (this.selectedData == this.lengthConverterData[i].data) {
        this.outputData = this.lengthConverterData[i].outp;
        console.log(this.outputData);
      }
    }
  }
}
