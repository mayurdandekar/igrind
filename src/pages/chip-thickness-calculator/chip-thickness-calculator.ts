import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  Content,
} from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import * as Constants from "../../utils/constants";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { Storage } from "@ionic/storage";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";
import { ChipThicknessModel } from "../../utils/chip-thickness-calculator-model";
import { Keyboard } from "@ionic-native/keyboard";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";

/**
 * Generated class for the ChipThicknessCalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-chip-thickness-calculator",
  templateUrl: "chip-thickness-calculator.html",
})
export class ChipThicknessCalculatorPage {
  @ViewChild(Content) content: Content;

  chipThicknessForm: FormGroup;
  chipThickType: any = "peripheral";
  isEnglish: boolean;
  resultOpacity: string = "1";
  calculateDisable: boolean = false;
  calculateBtnText: string;
  showResult: boolean;
  chipOutputType: string = "hec";
  autoLoad: boolean = false;
  showErrors: boolean = false;
  calculator: ChipThicknessModel;
  maxEllheight: any;

  validationMsgs: { inp_1: Array<any>; inp_2: Array<any>; inp_3: Array<any> };
  op_1: any;
  op_2: any;
  op_3: any;
  op_4: any;
  hecOutp = Constants.KEY_HEC;
  vcOutp = Constants.KEY_VC;
  inp1Label = "";
  inp2Label = "";
  inp3Label = "";
  vwOrVfrOutp = Constants.KEY_VW;
  aeOrDwOutp = Constants.KEY_AE;
  chipRange: any;

  hecOutpUnit = "in";
  vcOutpUnit = "sfm";
  vwOutpUnit = "in/min";
  aeOutpUnit = "in";

  hecWeight: string = "normal";
  vcWeight: string = "normal";
  vwWeight: string = "normal";
  aeWeight: string = "normal";

  hecStdRange = [];
  hecStdRangeArray = ["rough_grind", "precise_finish", "precise_grind"];

  unit_VW: any = "";
  unit_AE: any = "";
  unit_VC: any = "";
  unit_DW: any = "";
  unit_VFR: any = "";
  unit_HEC: any = "";

  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public platform: Platform,
    public translate: TranslateService,
    public storage: Storage,
    public common: CommonHelperProvider,
    private keyboard: Keyboard,
    public popOverCtrl: PopoverController,
    public firebaseAnalytics: FirebaseAnalytics
  ) {
    this.calculator = new ChipThicknessModel(this.storage);
    /**
     * Name :       Load Constants for qprime calculator units
     * Parameters:
     * Description: Loads and initializes qprime calculator units from constants file
     * Return:      --
     */

    this.unit_VW = Constants.KEY_VW;
    this.unit_AE = Constants.KEY_AE;
    this.unit_VC = Constants.KEY_VC;
    this.unit_DW = Constants.KEY_DW;
    this.unit_VFR = Constants.KEY_VFR;
    this.unit_HEC = Constants.KEY_HEC;

    /**
     * Name :       formBuilder.group
     * Parameters:  parameters and validator
     * Description: it specifies if the field is required
     * Return:      --
     */
    this.chipThicknessForm = formBuilder.group(
      {
        // diameter: ['', Validators.required],
        unitPref: [null],
        chipThicknessType: ["", Validators.required],
        inp_1: ["", Validators.required],
        inp_2: ["", Validators.required],
        inp_3: ["", Validators.required],
      },
      { validator: this.calculator.chipThicknessValidator.bind(this) }
    );

    // this.hecStdRangeArray.forEach((rangeConst) => {
    //   this.translate.get(rangeConst).toPromise().then((stdRange) => {
    //     this.hecStdRange.push(stdRange);
    //   })
    // })

    this.chipThicknessForm.get("unitPref").setValue(false);
    this.isEnglish = true;

    /**
     * Name :       translate.get
     * Parameters:  text value
     * Description: translate the input text
     * Return:      translated text
     */
    // this.getInputLables(1);

    translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
    // this.loadSwitchFromDb();
    /**
     * Name :       storage.get
     * Parameters:  Wheel string
     * Description: fetches and sets the values from the DB
     * Return:      --
     */
    this.storage.get(Constants.KEY_CHIPTHICKNESS).then((chipObj) => {
      if (chipObj) {
        console.log(chipObj);

        this.chipThicknessForm.get("inp_1").setValue(chipObj.inp_1);
        this.chipThicknessForm.get("inp_2").setValue(chipObj.inp_2);
        this.chipThicknessForm.get("inp_3").setValue(chipObj.inp_2);
        this.chipThicknessForm.get("unitPref").setValue(!chipObj.unitPref);
        this.chipThickType = chipObj.chipThicknessType;
        this.chipOutputType = chipObj.chipOutputType;
        this.isEnglish = chipObj.unitPref;
        this.showResult = true;
        this.resultOpacity = "1";
        this.op_1 = chipObj.inp_1;
        this.op_2 = chipObj.inp_2;
        this.op_3 = chipObj.inp_3;
        this.op_4 = chipObj.output;
        this.autoLoad = true;
        this.calculateDisable = true;
        this.translate
          .get("update_btn")
          .toPromise()
          .then((result) => {
            this.calculateBtnText = result;
          });
        this.setElmHTAuto();
        setTimeout(() => {
          this.checkElementHeight();
        }, 10);
        // this.onSubmit(this.chipThicknessForm.value);
      } else {
        this.autoLoad = false;
      }
    });

    var wheelSpeedErrMsg;
    var emptyErrMsg;
    var inputLengthErrmsg;
    this.translate
      .get("empty_invalid_input")
      .toPromise()
      .then((emptyMsg) => {
        emptyErrMsg = emptyMsg;
        this.validationMsgs = {
          inp_1: [{ type: "inp_1_empty", message: emptyErrMsg }],
          inp_2: [{ type: "inp_2_empty", message: emptyErrMsg }],
          inp_3: [{ type: "inp_3_empty", message: emptyErrMsg }],
        };
      }); //input_length_invalid
  }

  /**
   * Name: ionViewDidLoad
   * Parameters: none
   * Description: used to call firebase event for qprime screen and to create the popover dialog
   * Return: none
   */
  ionViewDidLoad() {
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.CHIP_THICKNESS_CALCULATOR
    );
    this.setElmHTAuto();
    setTimeout(() => {
      this.checkElementHeight();
    }, 10);

    // this.qprimeRange = this.popOverCtrl.create('QprimeRangePage', {'isEnglish':this.isEnglish}, { showBackdrop: true});
  }

  /**
   * Name :       loadSwitchFromDb
   * Parameters:  --
   * Description: loads the switch status from DB
   * Return:      void
   */
  loadSwitchFromDb() {
    this.storage.get(Constants.KEY_UNIT_PREF_WHEEL_SPEED).then((toggle) => {
      if (toggle !== null) {
        // this.chipThicknessForm.get('unitPref').setValue(toggle);
        // this.isEnglish = !toggle;
        // this.translate.get('wheel_diameter_ph').toPromise().then(res => {
        //   if (this.isEnglish) {
        //     this.diameterPh = res + " (in)";
        //   } else {
        //     this.diameterPh = res + " (mm)";
        //   }
        // });
      }
    });
  }
  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    // var enteredInput = event.target.value;
    // var field = (this)
    // if (enteredInput)
    //   var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    // if (indexx !== "false") {
    //   var inp = enteredInput.slice(indexx, 1);
    //   if (!(this.common.floatnoValidationRunTime(inp))) {
    //     event.target.value = enteredInput.slice(0, indexx);
    //   }
    // }

    var enteredInput = event.target.value;
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    // // var inp = ev.key;
    // // if (ev.keyCode == 13) {
    // //   let activeElement = <HTMLElement>document.activeElement;
    // //   activeElement.blur();
    // // }
    // // if (ev.target.value.length >= 9 || !(this.common.floatnoValidationRunTime(inp))) {
    // //   ev.preventDefault();
    // // }
    //  var inp = ev.key;
    //   if (ev.target.value.length>=9 || !(this.common.floatnoValidationRunTime(inp))) {
    //   ev.preventDefault();
    //   }
    this.common.telkeypress(ev);
  }
  //

  /**
   * Name :       calculate
   * Parameters:  input values
   * Description: Calculates the result and assigns the result to the variables
   * Return:      void
   */
  calculate(unitPref, chipThicknessType, inp_1, inp_2, inp_3) {
    this.resultOpacity = "1";
    this.showResult = true;
    let inp_1_num: number = parseFloat(inp_1);
    let inp_2_num: number = parseFloat(inp_2);
    let inp_3_num: number = parseFloat(inp_3);

    console.log(inp_1_num, inp_2_num, inp_3_num);

    var output = this.calculator.calculateChipThickness(
      !unitPref,
      chipThicknessType,
      this.chipOutputType,
      inp_1_num,
      inp_2_num,
      inp_3_num
    );
    console.log(output, "final outp");
    this.op_1 = inp_1_num;
    this.op_2 = inp_2_num;
    this.op_3 = inp_3_num;
    this.op_4 = output;

    // this.op_Hec = output[Constants.KEY_HEC];
    // this.op_Vc = output[Constants.KEY_VC];
    // this.op_Vw = output[Constants.KEY_VW];
    // this.op_Ae = output[Constants.KEY_AE];

    // switch (this.chipOutputType) {
    //   case Constants.KEY_HEC:
    //     // type : HEC
    //     this.hecWeight = "bold";
    //     this.vcWeight = "normal";
    //     this.vwWeight = "normal";
    //     this.aeWeight = "normal"
    //     break;
    //   case Constants.KEY_VC:
    //     // type : VC
    //     this.hecWeight = "normal";
    //     this.vcWeight = "bold";
    //     this.vwWeight = "normal";
    //     this.aeWeight = "normal"
    //     break;
    //   case Constants.KEY_VW:
    //     // type : VW
    //     this.hecWeight = "normal";
    //     this.vcWeight = "normal";
    //     this.vwWeight = "bold";
    //     this.aeWeight = "normal"
    //     break;
    //   case Constants.KEY_AE:
    //     // type : AE
    //     this.hecWeight = "normal";
    //     this.vcWeight = "normal";
    //     this.vwWeight = "normal";
    //     this.aeWeight = "bold"
    //     break;

    //   default:
    //     break;
    // }
    this.calculateDisable = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    // setTimeout(() => {
    //   this.scrollToResult('output');
    // }, 300);
  }

  /**
   * Name :       updateResults
   * Parameters:  --
   * Description: Updates Result area
   * Return:      void
   */
  updateResults() {
    this.calculateDisable = false;
    this.resultOpacity = "0.3";
  }

  /**
   * Name :       updateToggle
   * Parameters:  --
   * Description: Updates toggle as per the Unit selected
   * Return:      void
   */
  updateToggle() {
    this.showErrors = false;
    this.isEnglish = !this.chipThicknessForm.get("unitPref").value;
    this.chipThicknessForm.get("inp_1").setValue("");
    this.chipThicknessForm.get("inp_2").setValue("");
    this.chipThicknessForm.get("inp_3").setValue("");
    this.chipThicknessForm.get("inp_1").markAsUntouched();
    this.chipThicknessForm.get("inp_2").markAsUntouched();
    this.chipThicknessForm.get("inp_3").markAsUntouched();
    this.chipThicknessForm.get("inp_1").markAsPristine();
    this.chipThicknessForm.get("inp_2").markAsPristine();
    this.chipThicknessForm.get("inp_3").markAsPristine();

    this.setElmHTAuto();

    setTimeout(() => {
      this.checkElementHeight();
    }, 10);

    // if (this.isEnglish) {
    //   this.hecOutpUnit = 'in';
    //   this.vcOutpUnit = 'sfm';
    //   this.vwOutpUnit = 'in/min';
    //   this.aeOutpUnit = 'in';
    // }
    // else {
    //   this.hecOutpUnit = 'm';
    //   this.vcOutpUnit = 'm/s';
    //   this.vwOutpUnit = 'mm/min';
    //   this.aeOutpUnit = 'mm';
    // }
  }

  /**
   * Name :       clearResults
   * Parameters:  --
   * Description: clears all the fields and DB
   * Return:      void
   */
  clearResults() {
    this.resultOpacity = "1";
    this.chipThicknessForm.get("inp_1").markAsUntouched();
    this.chipThicknessForm.get("inp_2").markAsUntouched();
    this.chipThicknessForm.get("inp_3").markAsUntouched();
    this.chipThicknessForm.get("inp_1").markAsPristine();
    this.chipThicknessForm.get("inp_2").markAsPristine();
    this.chipThicknessForm.get("inp_3").markAsPristine();
    this.chipThicknessForm.get("inp_1").setValue("");
    this.chipThicknessForm.get("inp_2").setValue("");
    this.chipThicknessForm.get("inp_3").setValue("");
    this.op_1 = "";
    this.op_2 = "";
    this.op_3 = "";
    this.op_4 = "";
    this.showResult = false;
    this.chipThicknessForm.get("unitPref").setValue(false);
    this.isEnglish = true;
    this.chipThickType = "peripheral";
    this.chipOutputType = "hec";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
    this.storage
      .remove(Constants.KEY_CHIPTHICKNESS)
      .then((output) => {
        console.log("CHIP Cleared: " + output);
      })
      .catch((err) => {
        console.log("CHIP error: " + err);
      });
    this.showErrors = false;
  }

  /**
   * Name :       onSubmit
   * Parameters:  value
   * Description: validates input fields. If it passes validation then call calculate & log Firebase analytics evets.
   * Return:      void
   */
  onSubmit(value: any): void {
    this.keyboard.hide();
    let inp_1HasErrors: boolean = false;
    let inp_2HasErrors: boolean = false;
    let inp_3HasErrors: boolean = false;
    for (let validation of this.validationMsgs.inp_1) {
      if (this.chipThicknessForm.get("inp_1").hasError(validation.type)) {
        inp_1HasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.inp_2) {
      if (this.chipThicknessForm.get("inp_2").hasError(validation.type)) {
        inp_2HasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.inp_3) {
      if (this.chipThicknessForm.get("inp_3").hasError(validation.type)) {
        inp_3HasErrors = true;
      }
    }

    if (!inp_1HasErrors && !inp_2HasErrors && !inp_3HasErrors) {
      if (!this.autoLoad) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.CHIP_THICKNESS_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoad = false;
      }
      this.calculate(
        value.unitPref,
        value.chipThicknessType,
        value.inp_1,
        value.inp_2,
        value.inp_3
      );
    } else {
      for (let validation of this.validationMsgs.inp_1) {
        if (this.chipThicknessForm.get("inp_1").hasError(validation.type)) {
          this.firebaseAnalytics.logEvent(
            FirebaseConstants.CHIP_THICKNESS_CALC_INP1_HITCOUNT,
            {}
          );
        }
      }
      for (let validation of this.validationMsgs.inp_2) {
        if (this.chipThicknessForm.get("inp_2").hasError(validation.type)) {
          this.firebaseAnalytics.logEvent(
            FirebaseConstants.CHIP_THICKNESS_CALC_INP2_HITCOUNT,
            {}
          );
        }
      }
      for (let validation of this.validationMsgs.inp_3) {
        if (this.chipThicknessForm.get("inp_3").hasError(validation.type)) {
          this.firebaseAnalytics.logEvent(
            FirebaseConstants.CHIP_THICKNESS_CALC_INP3_HITCOUNT,
            {}
          );
        }
      }
      this.showErrors = true;
      this.chipThicknessForm.get("inp_1").markAsTouched();
      this.chipThicknessForm.get("inp_2").markAsTouched();
      this.chipThicknessForm.get("inp_3").markAsTouched();
    }
  }

  getChipOutput(selectedOutp) {
    this.chipOutputType = selectedOutp;
    this.getInputLables(0);
    // this.vwOrVfrOutp= Constants.KEY_VFR;
    // this.aeOrDwOutp=Constants.KEY_DW;
  }

  /**
   * Name :       onChangeChipType
   * Parameters:  --
   * Description:
   * Return:      void
   */
  onChangeChipType() {
    if (this.chipThickType == "peripheral") {
      this.vwOrVfrOutp = Constants.KEY_VW;
      this.aeOrDwOutp = Constants.KEY_AE;
    } else {
      this.vwOrVfrOutp = Constants.KEY_VFR;
      this.aeOrDwOutp = Constants.KEY_DW;
    }
    this.getInputLables(0);
  }

  getInputLables(isOnInit) {
    let firstLabel = "chip_wheel_peripheral_speed";
    let secondLabel = "wheel_piece_speed";
    let thirdLabel = "infeed";
    let vcOrHecUnitLabel = Constants.KEY_VC;
    let vwOrVcLabel = Constants.KEY_VW;
    let aeOrDwLabel = Constants.KEY_AE;

    if (!isOnInit) {
      if (this.chipThickType == "peripheral") {
        //for 1st Label
        if (this.chipOutputType != "hec") {
          firstLabel = "chip_eq_chip_thickness";
          vcOrHecUnitLabel = Constants.KEY_HEC;
        }

        //for 2nd Label
        if (this.chipOutputType != "vc" && this.chipOutputType != "hec") {
          secondLabel = "wheel_peripheral_speed";
          vwOrVcLabel = Constants.KEY_VC;
        }

        //for 3rd Label
        if (this.chipOutputType == "ae") {
          thirdLabel = "wheel_piece_speed";
          aeOrDwLabel = Constants.KEY_VW;
        }
      } else {
        //for 1st Label
        if (this.chipOutputType != "hec") {
          firstLabel = "chip_eq_chip_thickness";
          vcOrHecUnitLabel = Constants.KEY_HEC;
        } else {
          firstLabel = "chip_wheel_circumferal_speed";
          vcOrHecUnitLabel = Constants.KEY_VC;
        }

        //for 2nd Label
        if (this.chipOutputType != "vc" && this.chipOutputType != "hec") {
          secondLabel = "chip_wheel_circumferal_speed";
          vwOrVcLabel = Constants.KEY_VC;
        } else {
          secondLabel = "radial_feed_rate";
          vwOrVcLabel = Constants.KEY_VFR;
        }

        //for 3rd Label
        if (this.chipOutputType != "ae") {
          thirdLabel = "work_piece_diameter";
          aeOrDwLabel = Constants.KEY_DW;
        } else {
          thirdLabel = "radial_feed_rate";
          aeOrDwLabel = Constants.KEY_VFR;
        }
      }
    }
    this.translate
      .get(firstLabel)
      .toPromise()
      .then((wheelPeripheralSpeed) => {
        this.inp1Label = wheelPeripheralSpeed + "(" + vcOrHecUnitLabel + ")";
      });
    this.translate
      .get(secondLabel)
      .toPromise()
      .then((wheelSpeed) => {
        this.inp2Label = wheelSpeed + "(" + vwOrVcLabel + ")";
      });
    this.translate
      .get(thirdLabel)
      .toPromise()
      .then((infeed) => {
        this.inp3Label = infeed + "(" + aeOrDwLabel + ")";
      });
  }

  /**
   * Name :       setChipType
   * Parameters:  --
   * Description: Set Type of CHIP (Plunge or Traverse)
   * Return:      void
   */
  setChipType(param) {
    console.log(param);
    this.chipThickType = param;
    this.resultOpacity = "0.3";
    this.chipOutputType = "hec";
    if (this.op_4) {
      this.calculateDisable = false;
    }
    this.chipThicknessForm.get("inp_1").markAsUntouched();
    this.chipThicknessForm.get("inp_2").markAsUntouched();
    this.chipThicknessForm.get("inp_3").markAsUntouched();
    this.chipThicknessForm.get("inp_1").markAsPristine();
    this.chipThicknessForm.get("inp_2").markAsPristine();
    this.chipThicknessForm.get("inp_3").markAsPristine();
    this.chipThicknessForm.get("inp_1").setValue("");
    this.chipThicknessForm.get("inp_2").setValue("");
    this.chipThicknessForm.get("inp_3").setValue("");

    this.setElmHTAuto();

    setTimeout(() => {
      this.checkElementHeight();
    }, 10);
  }

  /**
   * Name :       setChipOutputType
   * Parameters:  --
   * Description: Set Chip Thickness Output Type
   * Return:      void
   */
  setChipOutputType(param) {
    this.chipOutputType = param;
    this.resultOpacity = "0.3";
    if (this.op_4) {
      this.calculateDisable = false;
    }
    this.chipThicknessForm.get("inp_1").markAsUntouched();
    this.chipThicknessForm.get("inp_2").markAsUntouched();
    this.chipThicknessForm.get("inp_3").markAsUntouched();
    this.chipThicknessForm.get("inp_1").markAsPristine();
    this.chipThicknessForm.get("inp_2").markAsPristine();
    this.chipThicknessForm.get("inp_3").markAsPristine();
    this.chipThicknessForm.get("inp_1").setValue("");
    this.chipThicknessForm.get("inp_2").setValue("");
    this.chipThicknessForm.get("inp_3").setValue("");

    this.setElmHTAuto();

    setTimeout(() => {
      this.checkElementHeight();
    }, 10);
  }

  /**
   * Name: chipRangePopUp
   * Parameters: none
   * Description: used to present qprime popover
   * Return: none
   */
  chipRangePopUp() {
    this.chipRange = this.popOverCtrl.create(
      "ChipRangePage",
      { isEnglish: this.isEnglish },
      { showBackdrop: true }
    );
    this.chipRange.present();
  }

  /**
   * Name: scrollToResult
   * Parameters: element class name
   * Description: scroll to result div
   * Return: none
   */
  scrollToResult(elementId: string) {
    console.log(elementId);
    var className = document.getElementById(elementId);
    let y = className.offsetTop;
    console.log(y);

    this.content.scrollTo(10, y);
  }

  /**
   * Name: setElmHTAuto
   * Parameters:
   * Description: Set maxEllHeight to 'auto'
   * Return: none
   */
  setElmHTAuto() {
    this.maxEllheight = "auto";
  }

  /**
   * Name: checkElementHeight
   * Parameters:
   * Description: Checks height of an HTML Element
   * Return: none
   */
  checkElementHeight() {
    var h1;
    var h2;
    function isEllipsisActive(element) {
      return element.offsetWidth < element.scrollWidth;
    }

    Array.from(document.querySelectorAll(".ellHeightPeri")).forEach(
      (element) => {
        h1 = element.clientHeight;
        console.log(h1);
      }
    );
    Array.from(document.querySelectorAll(".ellHeightCircum")).forEach(
      (element) => {
        h2 = element.clientHeight;
        console.log(h2);
      }
    );
    console.log(Math.max(h1, h2));

    // return (Math.max(h1,h2)+'px');
    this.maxEllheight = Math.max(h1, h2) + "px";
  }

  focusin() {
    console.log(12);
  }
  focusout() {
    console.log(34);
  }
}
