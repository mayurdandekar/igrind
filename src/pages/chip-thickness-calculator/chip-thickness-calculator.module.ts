import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChipThicknessCalculatorPage } from './chip-thickness-calculator';

@NgModule({
  declarations: [
    ChipThicknessCalculatorPage,
  ],
  imports: [
    IonicPageModule.forChild(ChipThicknessCalculatorPage),
  ],
})
export class ChipThicknessCalculatorPageModule {}
