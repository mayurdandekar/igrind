import {Component} from '@angular/core';
import {
	IonicPage,
	NavController,
	NavParams,
	MenuController,
	Events,
	ViewController,
} from 'ionic-angular';

/**
 * Generated class for the CalcAllPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-calc-all',
	templateUrl: 'calc-all.html',
})
export class CalcAllPage {
	SwipedTabsIndicator: any = null;
	calcTypeValue: any = 'Arithmetic';
	userInput: any;
	calcType: any = 1;
	currentCal: any = 0;
	// @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides ;

	/************************ Calc Props */

	myPara: any = '';
	myAns: any = 0;

	result: any = '';
	isInverse: boolean = false;
	appendResultString: boolean = false;
	PowArray = [];
	allOperators = ['+', '-', '*', '/'];
	prevHistory = [];
	ansUnit = 'DEG';
	isExp: boolean;
	inputString: string = '';
	isRoot: boolean;
	showEqual: boolean;
	showResult: boolean = true;
	isRestore: boolean;
	calculatedValue: any;
	selectedCalc: any;
	isEqualClicked: boolean;
	constructor(
		public navCtrl: NavController,
		private _navParams: NavParams,
		private menuCtrl: MenuController,
		private events: Events,
		public viewCtrl: ViewController
	) {
		let uinp = this._navParams.get('userInput');
		this.userInput = uinp ? uinp.toString() : '';
		// this.tabs=["Arithmetic","Algebra","Trigo","Calculus","Power","Fraction"];
		// this.tabs=["Arithmetic","Trigo","History"];
	}

	ngOnInit() {
		if (this.userInput) {
			this.myPara = this.inputString = this.result = this.userInput;
			this.appendResultString = true;
		}
		let historyString = localStorage.getItem('myHistory');
		if (historyString && historyString.length) {
			this.prevHistory = JSON.parse(historyString);
		}
	}

	ionViewDidLoad() {
		this.menuCtrl.close();
	}

	ionViewDidEnter() {
		this.SwipedTabsIndicator = document.getElementById('indicator');
		// this.menuCtrl.enable(false, "main_menu");
	}

	selectTab(index) {
		this.SwipedTabsIndicator.style.webkitTransform =
			'translate3d(' + 100 * index + '%,0,0)';
		// this.SwipedTabsSlider.slideTo(index, 500);
	}

	onCalculation(result) {
		this.result = result;
	}
	onSelectcalc(calcType) {
		this.calcType = calcType;
	}

	dismiss(isCalc) {
		if (isCalc) {
			let finalResult = this.showEqual
				? this.result
				: this.getResult(1, 1);
			let data = {result: finalResult};
			this.viewCtrl.dismiss(data);
		} else {
			this.viewCtrl.dismiss();
		}
		// this.navCtrl.pop();
	}

	checkinput(inp) {
		if (
			this.myPara == NaN ||
			this.myPara == 'NaN' ||
			this.myPara.toString().includes('Error!') ||
			this.result.toString().includes('Error!') ||
			this.showEqual
		) {
			if (this.showEqual && !/\d$/.test(inp)) {
				let formatInput = '';
				inp == '+' || inp == '-' || inp == '%'
					? this.inputString.slice(-1) == ' '
						? (formatInput += '' + inp + ' ')
						: (formatInput += ' ' + inp + ' ')
					: (formatInput += inp);

				this.inputString = this.result + formatInput;
				this.myPara = this.result + inp;
				this.appendResultString = this.showEqual = false;
				// this.result +=inp;
			} else {
				this.inputString = inp;
				this.myPara = inp;
				this.result = inp;
			}
			this.showEqual = false;
		} else {
			let lastElem = this.myPara.toString().slice(-1);
			if (this.allOperators.indexOf(inp) !== -1) {
				if (this.allOperators.indexOf(lastElem) == -1) {
					this.input(inp);
				}
			} else {
				this.input(inp);
			}
		}
	}
	input(inp) {
		if (this.isRoot && inp == ')') {
			this.inputString += ')';
			let reqIndex = this.myPara.lastIndexOf(',');
			let rootInp = this.myPara.substr(reqIndex + 1, this.myPara.length);
			let rootInpStr = '1/' + rootInp.toString();
			this.myPara =
				this.myPara.substr(0, reqIndex + 1) + rootInpStr + ')';
			this.isRoot = false;
		} else {
			inp == '+' || inp == '-' || inp == '%'
				? this.inputString.slice(-1) == ' '
					? (this.inputString += '' + inp + ' ')
					: (this.inputString += ' ' + inp + ' ')
				: (this.inputString += inp);
			this.myPara += inp;
		}
		let input2Nums = this.inputString.slice(-2);

		if (
			/\d+.\d*$/.test(this.inputString) ||
			(/\(\d*/.test(input2Nums) && this.appendResultString)
		) {
			// alert(1);
			this.result += inp;
		} else if (
			input2Nums == '+ ' ||
			input2Nums == '- ' ||
			input2Nums == '* ' ||
			input2Nums == '/ ' ||
			input2Nums == '% ' ||
			/\)$/.test(input2Nums)
		) {
			/\((\d+\s*[%+\u00D7\u00F7\(-]+\s)*d*$/.test(this.inputString)
				? /\d$/.test(this.inputString)
					? (this.result += inp)
					: /[%+*/\(-]+\s*$/.test(this.inputString)
					? ''
					: (this.result = inp)
				: this.getResult(0);
			this.appendResultString = false;
		} else {
			if (/\d$/.test(this.inputString.slice(-1)) && this.isRestore) {
				let inputDigits = this.inputString.match(/\d+$/);
				this.result = inputDigits[0];
			} else {
				// alert(3.2);
				this.result = inp;
			}
			this.appendResultString = true;
		}
		this.isRestore = false;
	}
	factorial(shirious) {
		if (parseInt(shirious)) {
			if (parseInt(shirious) < 2) return 1;
			return parseInt(shirious) * this.factorial(parseInt(shirious) - 1);
		}
	}
	sqrt() {
		// this.result+= "sqrt(" ;
		let lastInput = '';
		lastInput = this.showEqual ? this.result : this.inputString.trim();
		this.inputString = lastInput + ' \u221A(';
		this.appendResultString = this.showEqual = false;
		this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
			? '*Math.sqrt('
			: 'Math.sqrt(';
	}
	cbrt() {
		let lastInput = '';
		lastInput = this.showEqual ? this.result : this.inputString.trim();
		this.inputString = lastInput + ' \u221B(';
		this.appendResultString = this.showEqual = false;
		this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
			? '*Math.cbrt('
			: 'Math.cbrt(';
	}
	nthRoot(n) {
		let x = this.getFirstParam(this.myPara.slice(-1));
		let halfResidual = n % 2;
		if (halfResidual == 1 || x < 0) x = -x;
		var res = Math.pow(x, 1 / n);
		// n = Math.pow(r, n);
		this.myPara = this.myPara.replace(x, res);
		// this.formattedInput=
		// if(Math.abs(x - n) < 1 && (x > 0 === n > 0))
		// {
		//     let res=halfResidual ? -r : r
		//
		// }
	}
	leftParen() {
		let lastInput = '';
		lastInput = this.showEqual ? this.result : this.inputString.trim();
		this.inputString = lastInput + ' (';
		this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
			? '*('
			: '(';
		this.appendResultString = this.showEqual = false;
	}
	piOrE(lunar) {
		this.showEqual
			? (this.inputString = this.result)
			: (this.inputString = this.inputString.trim());
		if (lunar == 'pi') {
			this.inputString += '\u03C0';
			this.appendResultString
				? (this.result += '\u03C0')
				: (this.result = '\u03C0');

			this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
				? '*Math.PI'
				: 'Math.PI';
		} else {
			this.inputString += '\u0065';
			this.appendResultString
				? (this.result += '\u0065')
				: (this.result = '\u0065');
			// this.result+= "\u0065" ;
			this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
				? '*Math.E'
				: 'Math.E';
		}
		this.showEqual = false;
	}
	log(jafca) {
		let lastInput = '';
		lastInput = this.showEqual ? this.result : this.inputString.trim();
		if (jafca == 1) {
			this.inputString = lastInput + ' log(';
			// this.result= "log(" ;
			this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
				? '*Math.log10('
				: 'Math.log10(';
		} else {
			this.inputString = lastInput + ' ln(';
			// this.result= "ln(" ;
			this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
				? '*Math.log('
				: 'Math.log(';
		}
		this.appendResultString = this.showEqual = false;
	}
	trigo(funName) {
		let lastInput = '';
		lastInput = this.showEqual ? this.result : this.inputString.trim();
		this.inputString = lastInput + ' ' + funName + '(';
		this.appendResultString = true;
		this.showEqual = false;
		this.result = funName + '(';
		let getDegOrRad = '(';
		if (this.ansUnit == 'DEG') getDegOrRad += 'Math.PI / 180 * ';
		this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
			? '* Math.' + funName + getDegOrRad
			: ' Math.' + funName + getDegOrRad;
	}
	exponential() {
		let lastInput = '';
		lastInput = this.showEqual ? this.result : this.inputString.trim();

		this.inputString = lastInput + ' exp' + '(';
		this.appendResultString = this.showEqual = false;

		// this.result ='exp' + "("
		this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
			? '*Math.exp('
			: 'Math.exp(';
	}
	trigo1(invFunName) {
		let lastInput = '';
		lastInput = this.showEqual ? this.result : this.inputString.trim();
		this.inputString = lastInput + ' ' + invFunName + '\u207B\u00B9(';

		this.appendResultString = true;
		this.showEqual = false;

		this.result = invFunName + '\u207B\u00B9(';
		let getDegOrRad = '';
		if (this.ansUnit == 'DEG') getDegOrRad = '180 / Math.PI*';
		this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
			? '*' + getDegOrRad + 'Math.a' + invFunName + '('
			: getDegOrRad + 'Math.a' + invFunName + '(';
	}
	multOrDiv(type, isPercent) {
		if (
			this.myPara.toString().slice(-1) != '+' &&
			this.myPara.toString().slice(-1) != '-' &&
			this.myPara.toString().slice(-1) != '*' &&
			this.myPara.toString().slice(-1) != '/'
		) {
			this.showEqual ? (this.inputString = this.result.toString()) : '';
			if (type == 'mult') {
				this.inputString.slice(-1) == ''
					? (this.inputString += '\u00D7 ')
					: (this.inputString += ' \u00D7 ');
				// this.result+= "\u00D7" ;
				this.myPara.toString().trim().slice(-1) != '*'
					? (this.myPara += '*')
					: '';
			} else {
				this.inputString.slice(-1) == ''
					? (this.inputString += '\u00F7 ')
					: (this.inputString += ' \u00F7 ');
				// this.result+= "\u00F7" ;
				this.myPara.toString().trim().slice(-1) != '*'
					? (this.myPara += '/')
					: this.myPara.substring(0, this.myPara.length - 1) + '/';
			}
			if (isPercent) {
				this.inputString += '100)';
				this.myPara += '100)';
			}
			/\((\d+\s*[%+\u00D7\u00F7\(-]+\s)*d*$/.test(this.inputString)
				? ''
				: this.getResult(0);
			// this.getResult(0);
			this.showResult = false;
			this.appendResultString = false;
		}
	}

	del() {
		this.isExp = false;
		this.isRoot = false;
		if (this.myPara.toString().slice(-3) == 'Ans') {
			this.myPara = /[\d)IE]/.test(this.myPara.toString().slice(-4, -3))
				? this.myPara.toString().slice(0, -(this.myAns.length + 3))
				: this.myPara.toString().slice(0, -this.myAns.length);
			this.result = this.myPara.toString().slice(0, -3);
		} else if (this.result == 'Error!' || this.myPara == 'Error!') {
			this.ac();
		} else {
			switch (this.myPara.toString().slice(-2)) {
				case '* ': // sin cos tan
					this.myPara = /[\d)*IE]/.test(
						this.myPara.toString().slice(-27, -26)
					)
						? this.myPara.toString().slice(0, -27)
						: this.myPara.toString().slice(0, -26);
					this.inputString = this.inputString.toString().slice(0, -4);
					break;
				case 'n(':
				case 's(': // asin acos atan remove 11 if sin with RAD
					this.myPara = /Math.PI\*/.test(
						this.myPara.toString().slice(-18, -10)
					)
						? /[\d)*IE]/.test(
								this.myPara.toString().slice(-25, -24)
						  )
							? this.myPara.toString().slice(0, -25)
							: this.myPara.toString().slice(0, -24)
						: /[\d)*IE]/.test(
								this.myPara.toString().slice(-11, -10)
						  )
						? this.myPara.toString().slice(0, -11)
						: this.myPara.toString().slice(0, -10);
					console.log(
						/Math.PI\*/.test(
							this.myPara.toString().slice(-18, -10)
						),
						/Math.PI\*/.test(
							this.myPara.toString().slice(-18, -11)
						),
						/Math.PI\*/.test(this.myPara.toString().slice(-18, -9)),
						this.myPara,
						'case n(,s('
					);
					this.inputString =
						this.inputString.toString().slice(-2) == 'n(' ||
						this.inputString.toString().slice(-2) == 's('
							? this.inputString.toString().slice(0, -4)
							: this.inputString.toString().slice(0, -6);
					break;
				case '0(': // log
					this.myPara = /[\d)*IE]/.test(
						this.myPara.toString().slice(-12, -11)
					)
						? this.myPara.toString().slice(0, -12)
						: this.myPara.toString().slice(0, -11);
					this.inputString = this.inputString.toString().slice(0, -4);
					break;
				case 'g(': // ln
					this.myPara = /[\d)*IE]/.test(
						this.myPara.toString().slice(-10, -9)
					)
						? this.myPara.toString().slice(0, -10)
						: this.myPara.toString().slice(0, -9);
					this.inputString = this.inputString.toString().slice(0, -3);
					break;
				case 't(': // sqrt,cuberoot
					this.myPara = /[\d)*IE]/.test(
						this.myPara.toString().slice(-11, -10)
					)
						? this.myPara.toString().slice(0, -11)
						: this.myPara.toString().slice(0, -10);
					this.inputString = this.inputString.toString().slice(0, -2);
					break;
				case 'p(': // exp
					this.myPara = /[\d)*IE]/.test(
						this.myPara.toString().slice(-10, -9)
					)
						? this.myPara.toString().slice(0, -10)
						: this.myPara.toString().slice(0, -9);
					this.inputString = this.inputString.toString().slice(0, -4);
					break;
				case 'PI': // pi
					this.myPara = /[\d)*IE]/.test(
						this.myPara.toString().slice(-8, -7)
					)
						? this.myPara.toString().slice(0, -8)
						: this.myPara.toString().slice(0, -7);
					this.inputString = this.inputString.toString().slice(0, -1);
					this.result = this.result.toString().slice(0, -1);
					break;
				case '.E': // e
					this.myPara = /[\d)*IE]/.test(
						this.myPara.toString().slice(-7, -6)
					)
						? this.myPara.toString().slice(0, -7)
						: this.myPara.toString().slice(0, -6);
					this.inputString = this.inputString.toString().slice(0, -1);
					this.result = this.result.toString().slice(0, -1);
					break;
				case (this.myPara.toString().slice(-2).match(/\d\,/) || {})
					.input: //if any digit and comma
					this.myPara = /(w\(\d+\,\s*$)/.test(this.myPara)
						? this.myPara.replace(
								/(Math.pow\(\d+\,\s*$)/,
								this.myPara.substring(
									this.myPara.lastIndexOf('Math.pow(') + 9,
									this.myPara.length - 1
								)
						  )
						: this.myPara.toString().slice(0, -10);
					this.inputString = this.inputString.toString().slice(0, -2);
					break;
				case (this.myPara.toString().slice(-2).match(/\/\d/) || {})
					.input: //for root
					/(Math.pow\(\d+\,1\/\d+$)/.test(this.myPara)
						? (this.isRoot = true)
						: '';
					this.myPara = /(Math.pow\(\d+\,1\/\d+$)/.test(this.myPara)
						? this.myPara.toString().slice(0, -3)
						: this.myPara.toString().slice(0, -1);
					this.inputString = this.inputString.toString().slice(0, -1);

					break;
				default:
					if (this.showEqual)
						this.result = this.inputString = this.myPara = '';
					else {
						/[+*/%-\)]\s*$/.test(this.myPara.toString().slice(-2))
							? /\)\s*$/.test(this.myPara.toString().slice(-2))
								? (this.result = '')
								: ''
							: (this.result = this.result
									.toString()
									.slice(0, -1));
						this.myPara = this.myPara.toString().slice(0, -1);
						this.inputString =
							this.inputString.slice(-1) == ' '
								? this.inputString.toString().slice(0, -2)
								: this.inputString.toString().slice(0, -1);
						this.inputString = this.inputString.trim();
					}
			}

			/^\d+$/.test(this.inputString.trim())
				? (this.result = this.inputString)
				: '';
			this.appendResultString = true;
		}
	}
	ac() {
		this.result = this.myPara = this.inputString = '';
		this.isExp = false;
		this.isRoot = false;
		this.showResult = true;
		this.appendResultString = this.showEqual = false;
	}
	ans() {
		this.result += 'Ans';
		this.myPara += /[\d)IE]/.test(this.myPara.toString().slice(-1))
			? ' * ' + this.myAns
			: this.myAns;
	}
	getResult(isEqual, isDismiss?) {
		let tempInputstring = this.myPara;
		this.isEqualClicked = true;
		let openBraceCount = tempInputstring.match(/\(/g)
			? tempInputstring.match(/\(/g).length
			: 0;
		let closeBraceCount = tempInputstring.match(/\)/g)
			? tempInputstring.match(/\)/g).length
			: 0;
		if (closeBraceCount < openBraceCount) {
			for (let i = openBraceCount - closeBraceCount; i > 0; i--) {
				tempInputstring += ')';
				this.inputString += ')';
			}
		}

		if (tempInputstring != '' && !tempInputstring.includes('Error!')) {
			//   let factArray=tempInputstring.match(/(\d+\.?\d*)\!/g);
			//   if(factArray && factArray.length)
			//   {
			//     factArray.forEach(factElem => {
			//       let factValue=this.factorial(factElem.slice(0,-1));
			//       tempInputstring=tempInputstring.replace(factElem, factValue);
			//     });
			//   }
			if (
				tempInputstring.endsWith('+') ||
				tempInputstring.endsWith('-') ||
				tempInputstring.endsWith('*') ||
				tempInputstring.endsWith('/')
			) {
				tempInputstring = tempInputstring.slice(0, -1);
			}
			if (tempInputstring.includes('^()')) {
				tempInputstring = tempInputstring.replace('^()', '');
			}
			if (tempInputstring.includes('*)')) {
				// this.inputString=this.inputString.replace(/\*\)/g,'1)')
				this.inputString = this.inputString.replace('*)', '1)');
				tempInputstring = tempInputstring.replace('*)', '1)');
			}
			if (tempInputstring.includes('* )')) {
				this.inputString = this.inputString.replace('* )', '1)');
				tempInputstring = tempInputstring.replace('* )', '1)');
			}
			//   tempInputstring=tempInputstring.replace('\^\(','Math.pow(');

			if (tempInputstring.includes('(Math.PI / 180 * )')) {
				this.result = this.myAns = 'Error!';
			} else {
				// tempInputstring=this.splitPower(tempInputstring);
				try {
					eval(tempInputstring)
						? (this.result = this.myAns =
								eval(tempInputstring)
									.toPrecision(10)
									.replace(/\.0+$/, ''))
						: (this.result = this.myAns = 'Error!');
					this.result = parseFloat(this.result);
					isEqual ? (this.myPara = this.result.toString()) : '';
				} catch (e) {
					if (e instanceof SyntaxError) {
						this.result = this.myAns = 'Error!';
						this.appendResultString = true;
					}
				}
				// isEqual ? this.emit() : "";
			}
			this.showEqual = isEqual ? true : false;
			this.showResult = isEqual ? true : false;
		}

		if (!isFinite(this.result)) this.result = 'Error!';
		if (isEqual) {
			let myHistory = [];
			this.result != 'Error!'
				? myHistory.push({
						input: this.inputString,
						formattedInput: tempInputstring,
						ans: this.myAns,
				  })
				: '';
			this.prevHistory.forEach(history => {
				myHistory.push(history);
			});
			this.prevHistory = myHistory;
			localStorage.setItem('myHistory', JSON.stringify(myHistory));

			if (isDismiss) {
				return this.result;
			}
		}
	}

	restoreHistory(historyExp) {
		this.myPara = historyExp.formattedInput;
		this.result = historyExp.ans;
		this.inputString = historyExp.input;
		this.calcType = 1;
		this.selectedCalc.emit(this.calcType);
		this.calculatedValue.emit(this.result);
		this.showEqual = false;
		this.isRestore = true;
		this.appendResultString = false;
	}
	getPower(pow, calcRoot) {
		let inputArr = this.getFirstParam(this.myPara);
		let indexInputArr = this.myPara.lastIndexOf(inputArr);
		let lastInput = '';
		lastInput = this.showEqual ? this.result : this.inputString.trim();
		!calcRoot ? (this.inputString = lastInput + ' ^(') : '';
		if (pow != 'y' && !calcRoot) {
			!calcRoot ? (this.inputString += '2)') : '';
			this.myPara =
				this.myPara.substring(0, indexInputArr) +
				'Math.pow(' +
				parseFloat(inputArr) +
				',' +
				pow +
				')';
			this.getResult(0);
			// this.myPara=this.myPara.replace( inputArr.lastIndexOf(')'),'Math.pow('+parseInt(inputArr)+','+pow+')' )
		} //if
		else {
			this.myPara =
				this.myPara.substring(0, indexInputArr) +
				'Math.pow(' +
				parseFloat(inputArr) +
				',';
			this.appendResultString = this.showEqual = false;
			setTimeout(() => {
				calcRoot ? (this.isRoot = true) : (this.isExp = true);
			}, 200);
		}
	}
	getRoot() {
		let firstInpParam = this.getFirstParam(this.myPara);
		let indexInputArr = this.myPara.lastIndexOf(firstInpParam);
		let lastInput = '';
		lastInput = this.showEqual ? this.result : this.inputString.trim();
		this.inputString = lastInput + ' (\u221A';
		this.myPara += '(\u221A';
		this.isRoot = true;
		this.appendResultString = this.showEqual = false;
		this.myPara =
			this.myPara.substring(0, indexInputArr) +
			'Math.pow(' +
			parseInt(firstInpParam) +
			',';
	}
	getPercent() {
		let inputArr = this.getFirstParam(this.myPara);
		let lastInput = '';
		let indexInputArr = this.myPara.lastIndexOf(inputArr);
		lastInput = this.showEqual ? this.result : this.inputString.trim();
		this.inputString =
			this.inputString.substring(0, indexInputArr + 1) +
			'(' +
			parseInt(inputArr);

		this.myPara =
			this.myPara.substring(0, indexInputArr) + '(' + parseInt(inputArr);
		this.inputString += '/100)';
		this.myPara += '/100)';

		// this.multOrDiv("div", true);
	}
	getFirstParam(inputArr) {
		if (inputArr.lastIndexOf('(') > 0) {
			inputArr = inputArr.substring(
				inputArr.lastIndexOf('(') + 1,
				inputArr.length
			);
		}
		if (inputArr.lastIndexOf('+') > 0) {
			inputArr = inputArr.substring(
				inputArr.lastIndexOf('+') + 1,
				inputArr.length
			);
		}
		if (inputArr.lastIndexOf('-') > 0) {
			inputArr = inputArr.substring(
				inputArr.lastIndexOf('-') + 1,
				inputArr.length
			);
		}
		if (inputArr.lastIndexOf('*') > 0) {
			inputArr = inputArr.substring(
				inputArr.lastIndexOf('*') + 1,
				inputArr.length
			);
		}
		if (inputArr.lastIndexOf('/') > 0) {
			inputArr = inputArr.substring(
				inputArr.lastIndexOf('/') + 1,
				inputArr.length
			);
		}
		if (inputArr.lastIndexOf('%') > 0) {
			inputArr = inputArr.substring(
				inputArr.lastIndexOf('%') + 1,
				inputArr.length
			);
		}
		if (inputArr.lastIndexOf(')') > 0) {
			inputArr = inputArr.substring(
				inputArr.lastIndexOf(')') + 1,
				inputArr.length
			);
		}
		if (inputArr.lastIndexOf(',') > 0) {
			inputArr = inputArr.substring(
				inputArr.lastIndexOf(',') + 1,
				inputArr.length
			);
		}
		return inputArr;
	}
	getAnsUnit() {
		this.ansUnit = this.ansUnit == 'DEG' ? 'RAD' : 'DEG';
	}

	changeSign() {
		if (this.myPara.substring(0, 1) == '-') {
			this.myPara = this.myPara.substring(1, this.myPara.length);
			this.inputString = this.inputString.substring(
				1,
				this.inputString.length
			);
		} else {
			this.myPara = '-' + this.myPara;
			this.inputString = '-' + this.inputString;
		}
	}

	clearHistory() {
		localStorage.removeItem('myHistory');
		this.prevHistory = [];
	}
}
