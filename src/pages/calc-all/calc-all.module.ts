import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalcAllPage } from './calc-all';

@NgModule({
  declarations: [
    CalcAllPage,
  ],
  imports: [
    IonicPageModule.forChild(CalcAllPage),
  ],
})
export class CalcAllPageModule {}
