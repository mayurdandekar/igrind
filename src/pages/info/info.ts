import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
import * as Constants from "../../utils/constants";
/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-info",
  templateUrl: "info.html",
})
export class InfoPage {
  infoList: Array<any>;
  infoItems: Array<any>;
  calcKey: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private translate: TranslateService
  ) {}

  ionViewDidLoad() {
    // console.log('ionViewDidLoad InfoPage');
    this.infoList = new Array();
    this.infoItems = new Array();
    this.calcKey = this.navParams.get("data");

    this.setupInfoData();
  }

  setupInfoData() {
    this.infoList.push({
      calcType: "coolant",
      imageName: "",
      description: "about_coolant_desc",
      description1: "",
      label: "about_coolant_label",
    });
    this.infoList.push({
      calcType: "coolant",
      imageName: "",
      description: "info_coolant_type_desc",
      description1: "",
      label: "info_coolant_type_label",
    });
    this.infoList.push({
      calcType: "coolant",
      imageName: "",
      description: "info_gwv_desc",
      description1: "",
      label: "info_gwv_label",
    });
    this.infoList.push({
      calcType: "coolant",
      imageName: "Pressure_Formula_" + this.translate.currentLang + ".png",
      description: "info_pressure_desc",
      description1: "",
      label: "info_pressure_label",
    });
    this.infoList.push({
      calcType: "coolant",
      imageName: "",
      description: "info_gp_desc",
      description1: "",
      label: "info_gp_label",
    });
    this.infoList.push({
      calcType: "coolant",
      imageName: "",
      description: "info_cf_desc",
      description1: "",
      label: "info_cf_label",
    });
    this.infoList.push({
      calcType: "coolant",
      imageName: "",
      description: "info_gwcw_desc",
      description1: "",
      label: "info_gwcw_label",
    });

    //Dressing
    this.infoList.push({
      calcType: "dressing",
      imageName: "",
      description: "dia_roll_drs_cal_desc",
      description1: "",
      label: "dia_roll_drs_cal_label",
    });

    this.infoList.push({
      calcType: "dressing",
      imageName: "",
      description: "info_drw_desc",
      description1: "",
      label: "info_drw_label",
    });

    this.infoList.push({
      calcType: "dressing",
      imageName: "",
      description: "info_wptr_desc",
      description1: "",
      label: "info_wptr_label",
    });
    //wheel_speed
    this.infoList.push({
      calcType: "wheel_speed",
      imageName: "",
      description: "wheel_spd_convrtr_desc",
      description1: "",
      label: "wheel_spd_convrtr_label",
    });

    //plunge_dressing
    this.infoList.push({
      calcType: "plunge_dressing",
      imageName: "",
      description: "plunge_dia_roll_drs_desc",
      description1: "",
      label: "plunge_dia_roll_drs_label",
    });
    this.infoList.push({
      calcType: "plunge_dressing",
      imageName: "",
      description: "plunge_dia_roll_drs_recom_desc",
      description1: "",
      label: "plunge_dia_roll_drs_recom_label",
    });
    this.infoList.push({
      calcType: "plunge_dressing",
      imageName: "",
      description: "plunge_dia_roll_drs_drw_desc",
      description1: "",
      label: "plunge_dia_roll_drs_drw_label",
    });
    this.infoList.push({
      calcType: "plunge_dressing",
      imageName: "",
      description: "plunge_dia_roll_drs_ir_desc",
      description1: "",
      label: "plunge_dia_roll_drs_ir_label",
    });
    this.infoList.push({
      calcType: "plunge_dressing",
      imageName: "",
      description: "plunge_dia_roll_drs_cd_desc",
      description1: "",
      label: "plunge_dia_roll_drs_cd_label",
    });
    this.infoList.push({
      calcType: "plunge_dressing",
      imageName: "",
      description: "plunge_dia_roll_drs_ncd_desc",
      description1: "",
      label: "plunge_dia_roll_drs_ncd_label",
    });

    this.infoList.push({
      calcType: "plunge_dressing",
      imageName: "unidirection_wheel_" + this.translate.currentLang + ".png", //unidirection_diamond
      cssimg: {
        height: "68px",
        width: "68px",
      },
      imageName1: "unidirection_diamond_" + this.translate.currentLang + ".png",
      description: "",
      description1: "",
      label: "plunge_dia_roll_drs_unidirectional_label",
    });
    this.infoList.push({
      calcType: "plunge_dressing",
      imageName: "counter_wheel_" + this.translate.currentLang + ".png", //counter_diamondRoll
      cssimg: {
        height: "68px",
        width: "68px",
      },
      imageName1: "counter_diamondRoll_" + this.translate.currentLang + ".png",
      description: "",
      description1: "",
      label: "plunge_dia_roll_drs_counterdirectional_label",
    });

    //stationary
    this.infoList.push({
      calcType: "stationary_dressing",
      imageName: "",
      description: "sta_tool_drs_cal_desc",
      description1: "",
      label: "sta_tool_drs_cal_label",
    });
    this.infoList.push({
      calcType: "stationary_dressing",
      imageName: "",
      description: "sta_tool_drs_tr_cal_desc",
      description1: "sta_tool_drs_tr_cal_desc_for",
      label: "sta_tool_drs_tr_cal_label",
    });
    this.infoList.push({
      calcType: "stationary_dressing",
      imageName: "",
      description: "sta_tool_drs_or_cal_desc",
      description1: "sta_tool_drs_or_cal_desc_for",
      label: "sta_tool_drs_or_cal_label",
    });
    this.infoList.push({
      calcType: "stationary_dressing",
      imageName: "",
      description: "sta_tool_drs_dl_cal_desc",
      description1: "sta_tool_drs_dl_cal_desc_for",
      label: "sta_tool_drs_dl_cal_label",
    });

    //QPRIME
    this.translate.get("qp_desc").subscribe((v) => {
      this.infoList.push({
        calcType: "qprime",
        imageName: "",
        description: "",
        description1: "",
        description2: v,
        label: "about_qprime",
      });
    });

    this.translate.get("qp_range").subscribe((v) => {
      this.infoList.push({
        calcType: "qprime",
        imageName: "",
        description: "",
        description1: "",
        description2: v,
        label: "info_range",
      });
    });

    this.translate.get("qp_note").subscribe((v) => {
      this.infoList.push({
        calcType: "qprime",
        imageName: "",
        description: "",
        description1: "",
        description2: v,
        label: "info_note",
      });
    });

    //CHIP THICKNESS
    this.translate.get("chip_desc").subscribe((v) => {
      this.infoList.push({
        calcType: "chip_thickness",
        imageName: "",
        description: "",
        description1: "",
        description2: v,
        label: "about_chip",
      });
    });

    let chip_imp;
    this.translate.get("chip_imperial").subscribe((v) => {
      chip_imp = v;
    });

    let chip_met;
    this.translate.get("chip_metric").subscribe((v) => (chip_met = v));

    this.infoList.push({
      calcType: "chip_thickness",
      imageName: "",
      description: "",
      description1: "",
      description2: chip_imp + chip_met,
      label: "info_range",
    });

    this.translate.get("chip_info").subscribe((v) => {
      this.infoList.push({
        calcType: "chip_thickness",
        imageName: "",
        description: "",
        description1: "",
        description2: v,
        label: "info_note",
      });
    });

    this.infoList.forEach((element) => {
      if (element.calcType == this.calcKey) {
        let descPromise;
        let descPromise1;
        let descPromise2;
        if (element.description != "")
          descPromise = this.translate.get(element.description).toPromise();
        if (element.description1 != "")
          descPromise1 = this.translate.get(element.description1).toPromise();
        console.log(descPromise1);
        if (element.description2 != "") descPromise2 = element.description2;
        console.log(descPromise2);
        let labelPromise = this.translate.get(element.label).toPromise();
        Promise.all([
          descPromise,
          labelPromise,
          descPromise1,
          descPromise2,
        ]).then((results) => {
          element["description"] = results[0];
          element["label"] = results[1];
          element["description1"] = results[2];
          element["description2"] = results[3];
          this.infoItems.push(element);
        });
      }
    });
  }

  checkImage(imageName) {
    if (imageName) {
      if (imageName.length <= 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
  checkDescription(description) {
    // console.log(description);
    if (!description) {
      return true;
    } else {
      return false;
    }
  }
}
