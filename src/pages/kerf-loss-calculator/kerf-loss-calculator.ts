import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Platform, Select } from "ionic-angular";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";
import { KerfLossCalculatorModel } from "../../utils/kerf-loss-calculator-model";
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";
import { MasterDataProvider } from "../../providers/master-data/master-data";
import * as Constants from "../../utils/constants";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import { Keyboard } from "@ionic-native/keyboard";

/**
 * Generated class for the KerfLossCalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-kerf-loss-calculator",
  templateUrl: "kerf-loss-calculator.html",
})
export class KerfLossCalculatorPage {
  private autoLoad: boolean = true;

  calculator: KerfLossCalculatorModel;

  kerfLossForm: FormGroup;

  validationMsgs: {
    thickness: Array<any>;
    surfaceArea: Array<any>;
    thicknessType: any;
    surfaceAreaType: any;
  };

  resultOpacity: string = "1";
  calculateDisable: boolean = false;
  calculateBtnText: string;

  thicknessPh = "";
  surfaceAreaPh = "";

  thicknessType: any;

  speedListRefs: Array<any>;
  surfaceAreaRefs: Array<any>;

  showResult: boolean = false;
  showErrors: boolean = false;

  resultOutput: {};

  @ViewChild("kerfThickness") select: Select;
  @ViewChild("kerfSA") select2: Select;

  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public common: CommonHelperProvider,
    public formBuilder: FormBuilder,
    public storage: Storage,
    public translate: TranslateService,
    public masterData: MasterDataProvider,
    public firebaseAnalytics: FirebaseAnalytics,
    private keyboard: Keyboard
  ) {
    //
    this.calculator = new KerfLossCalculatorModel(this.storage);

    //
    this.masterData.getWheelThicknessDropDowns().then((speedList) => {
      this.speedListRefs = speedList;
    });

    //
    this.masterData.getSurfaceAreaDropDowns().then((speedList) => {
      this.surfaceAreaRefs = speedList;
    });

    //
    translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    this.translate
      .get("thickness_ph")
      .toPromise()
      .then((res) => {
        this.thicknessPh = res;
      });

    this.translate
      .get("surface_area_ph")
      .toPromise()
      .then((res) => {
        this.surfaceAreaPh = res;
      });

    //
    this.kerfLossForm = formBuilder.group(
      {
        thickness: ["", Validators.required],
        surfaceArea: ["", Validators.required],
        thicknessType: ["", Validators.required],
        surfaceAreaType: ["", Validators.required],
      },
      { validator: this.calculator.kerfLossValidator.bind(this) }
    );

    //
    var thicknessErrMsg;
    var emptyErrMsg;
    var inputLengthErrmsg;
    this.translate
      .get("zero_input")
      .toPromise()
      .then((errorMsg) => {
        thicknessErrMsg = errorMsg;
        this.translate
          .get("empty_invalid_input")
          .toPromise()
          .then((emptyMsg) => {
            emptyErrMsg = emptyMsg;
            this.translate
              .get("unit_empty")
              .toPromise()
              .then((selectEmptyMsg) => {
                this.translate
                  .get("input_length_invalid")
                  .toPromise()
                  .then((inputLengthInvalidMsg) => {
                    inputLengthErrmsg = inputLengthInvalidMsg;
                    this.validationMsgs = {
                      thickness: [
                        { type: "thickness_empty", message: emptyErrMsg },
                        { type: "thickness_zero", message: thicknessErrMsg },
                      ],
                      surfaceArea: [
                        { type: "sa_empty", message: emptyErrMsg },
                        { type: "sa_zero", message: thicknessErrMsg },
                      ],
                      thicknessType: {
                        type: "thicknesstype_empty",
                        message: selectEmptyMsg,
                      },
                      surfaceAreaType: {
                        type: "satype_empty",
                        message: selectEmptyMsg,
                      },
                    };
                  }); //empty
              });
          }); //input_length_invalid
      });

    //
    this.storage.get(Constants.KEY_KERF_LOSS).then((kerfObj) => {
      console.log(kerfObj);
      if (kerfObj) {
        this.kerfLossForm.get("thicknessType").setValue(kerfObj.thicknessType);
        this.kerfLossForm.get("thickness").setValue(kerfObj.thickness);
        this.kerfLossForm
          .get("surfaceAreaType")
          .setValue(kerfObj.surfaceAreaType);
        this.kerfLossForm.get("surfaceArea").setValue(kerfObj.surfaceArea);
        this.autoLoad = true;
        this.onSubmit(this.kerfLossForm.value);
      } else {
        this.autoLoad = false;
      }
    });
  }

  ionViewDidLoad() {
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.KERF_LOSS_CALCULATOR
    );
  }

  ionViewWillLeave() {
    this.select.close();
    this.select2.close();
  }

  /**
   * Name :       onSubmit
   * Parameters:  value
   * Description: validates input fields. If it passes validation then call calculate & log Firebase analytics evets.
   * Return:      void
   */
  onSubmit(value: any): void {
    this.keyboard.hide();
    var thicknessHasErrors: boolean = false;

    for (let validation of this.validationMsgs.thickness) {
      if (this.kerfLossForm.get("thickness").hasError(validation.type)) {
        thicknessHasErrors = true;
      }
    }

    var thicknessTypeHasErrors: boolean = false;

    if (
      this.kerfLossForm
        .get("thicknessType")
        .hasError(this.validationMsgs.thicknessType.type)
    ) {
      thicknessTypeHasErrors = true;
    }

    var saHasErrors: boolean = false;

    for (let validation of this.validationMsgs.surfaceArea) {
      if (this.kerfLossForm.get("surfaceArea").hasError(validation.type)) {
        saHasErrors = true;
      }
    }

    var satypeHasErrors: boolean = false;

    if (
      this.kerfLossForm
        .get("surfaceAreaType")
        .hasError(this.validationMsgs.surfaceAreaType.type)
    ) {
      satypeHasErrors = true;
    }

    if (
      !thicknessHasErrors &&
      !thicknessTypeHasErrors &&
      !saHasErrors &&
      !satypeHasErrors
    ) {
      if (!this.autoLoad) {
        // this.firebaseAnalytics.logEvent(FirebaseConstants.WHEEL_SPEED_CALC_HITCOUNT, {});
      } else {
        this.autoLoad = false;
      }
      this.calculate(
        value.thickness,
        value.thicknessType,
        value.surfaceArea,
        value.surfaceAreaType
      );
    } else {
      for (let validation of this.validationMsgs.thickness) {
        if (this.kerfLossForm.get("thickness").hasError(validation.type)) {
          // this.firebaseAnalytics.logEvent(FirebaseConstants.WHEEL_SPEED_CALC_OOR_DIAMETER_HITCOUNT, {})
        }
      }
      this.showErrors = true;

      this.kerfLossForm.get("thickness").markAsTouched();
      this.kerfLossForm.get("thicknessType").markAsTouched();
      this.kerfLossForm.get("surfaceArea").markAsTouched();
      this.kerfLossForm.get("surfaceAreaType").markAsTouched();
    }
  }

  calculate(thickness, thicknessType, surfaceArea, surfaceAreaType) {
    this.resultOutput = this.calculator.calculateKerfLoss(
      thickness,
      thicknessType,
      surfaceArea,
      surfaceAreaType
    );
    this.showResult = true;
    this.resultOpacity = "1";
    this.calculateDisable = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    // this.validator.productTypeEventsRoundBar(inputType);
  }

  /**
   * Name :       clearResults
   * Parameters:  --
   * Description: clears all the fields and DB
   * Return:      void
   */
  clearResults() {
    this.showResult = false;
    this.kerfLossForm.get("thicknessType").setValue("");
    this.kerfLossForm.get("thickness").reset();
    this.kerfLossForm.get("thickness").setValue(" ");
    this.kerfLossForm.get("thickness").setValue("");

    this.kerfLossForm.get("thickness").markAsPristine();
    this.kerfLossForm.get("thickness").markAsUntouched();

    this.kerfLossForm.get("surfaceAreaType").setValue("");
    this.kerfLossForm.get("surfaceArea").reset();
    this.kerfLossForm.get("surfaceArea").setValue(" ");
    this.kerfLossForm.get("surfaceArea").setValue("");

    this.kerfLossForm.get("surfaceArea").markAsPristine();
    this.kerfLossForm.get("surfaceArea").markAsUntouched();

    this.resultOpacity = "1";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
    this.storage
      .remove(Constants.KEY_KERF_LOSS)
      .then((output) => {
        console.log("Wheel Speed Cleared: " + output);
      })
      .catch((err) => {
        console.log("Wheel Speed Clear error: " + err);
      });
    this.showErrors = false;
  }

  /**
   * Name :       updateResults
   * Parameters:  --
   * Description: Updates Result area
   * Return:      void
   */
  updateResults() {
    this.calculateDisable = false;
    this.resultOpacity = "0.3";
  }

  /**
   * Name: round
   * Parameters: value, precision
   * Description: used to perform rounding to decimal numbers based on precision
   * Return: float number
   */
  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  }

  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    var enteredInput = event.target.value;
    var field = this;
    // console.log(field);

    // if (enteredInput)
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    // console.log(ev);
    // var inp = ev.key;
    // if (ev.keyCode == 13) {
    //   let activeElement = <HTMLElement>document.activeElement;
    //   activeElement.blur();
    // }
    // if (!(this.common.floatnoValidationRunTime(inp))) {
    //   ev.preventDefault();
    // }
    this.common.telkeypress(ev);
  }
  //
}
