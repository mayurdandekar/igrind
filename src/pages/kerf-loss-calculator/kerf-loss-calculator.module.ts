import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KerfLossCalculatorPage } from './kerf-loss-calculator';

@NgModule({
  declarations: [
    KerfLossCalculatorPage,
  ],
  imports: [
    IonicPageModule.forChild(KerfLossCalculatorPage),
  ],
})
export class KerfLossCalculatorPageModule {}
