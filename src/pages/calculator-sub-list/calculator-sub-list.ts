import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import * as FirebaseConstants from '../../utils/analytics-constants';


/**
 * Generated class for the CalculatorSubListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-calculator-sub-list',
  templateUrl: 'calculator-sub-list.html',
})
export class CalculatorSubListPage {
  calculatorType: any;
  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public firebaseAnalytics: FirebaseAnalytics,
  ) {

  }
  ngOnInit() {
    this.calculatorType = this.navParams.get('type')
  }
  ionViewDidLoad() {
    if (this.calculatorType == 'conversion') {
      this.firebaseAnalytics.setCurrentScreen(FirebaseConstants.CONVERSION_CALCULATOR);
    }
    else if (this.calculatorType == 'grind') {
      this.firebaseAnalytics.setCurrentScreen(FirebaseConstants.GRIND_PARAMETERS);
    }
    else if (this.calculatorType == 'dress') {
      this.firebaseAnalytics.setCurrentScreen(FirebaseConstants.DRESSING_PARAMETERS);
    }
    else if (this.calculatorType == 'cut') {
      this.firebaseAnalytics.setCurrentScreen(FirebaseConstants.CUTTING_PARAMETERS);
    }

  }
  showTerms() {
    this.navCtrl.push('TermsPage');
  }
}
