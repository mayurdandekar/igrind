export interface CoolantInput {
  unitPrefs: {
    isGrindingWheelVelocityNotPressure: boolean;
    isSfpmNotMps: boolean;
    isBarNotPsi: boolean;
    isGrindingPowerNotFlowrate: boolean;
    isKwNotHp: boolean;
    isIminNotGPM: boolean;
    isMmNotIn: boolean;
  },
  values: {
    coolantType: number;
    grindingWheelVelocityValue: number;
    grindingPowerValue: number;
    grindingWheelContactWidth: number;
  }
}