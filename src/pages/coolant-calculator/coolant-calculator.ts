import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  ModalController,
  PopoverController,
  Select,
  Platform,
} from "ionic-angular";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import * as Constants from "../../utils/constants";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { TranslateService } from "@ngx-translate/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { CoolantCalculatorModel } from "../../utils/coolant-calculator-model";
import { CoolantValidator } from "../../validators/CoolantValidator";

import { Keyboard } from "@ionic-native/keyboard";
import { CoolantInput } from "./CoolantInput";
import { MasterDataProvider } from "../../providers/master-data/master-data";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";
/**
 * Generated class for the CoolantCalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-coolant-calculator",
  templateUrl: "coolant-calculator.html",
})
export class CoolantCalculatorPage {
  pumpPowerModel: any;
  private autoLoad: boolean = true;
  coolantTypeListRefs: Array<any>;

  switchHolder: any;
  resultObj: any;
  inputFormData: CoolantInput;
  coolantForm: FormGroup;
  calculator: CoolantCalculatorModel;
  gwvPressureLeftLabel: string;
  gwvPressureRightLabel: string;
  gpCfLeftLabel: string;
  gpCfRightLabel: string;
  validator: CoolantValidator;
  validationMsgs: any = {};

  isGwvNotPressure: boolean;
  isBarNotPsi: boolean;
  isSfpmNotMps: boolean;
  isGwvNotPressureResult: boolean;

  isGPNotFlowrate: boolean;
  isKwNotHp: boolean;
  isIminNotGPM: boolean;
  isGPNotFlowrateResult: boolean;

  isMmNotIn: boolean;

  resultOpacity: string = "1";
  calculateDisable: boolean = false;
  calculateBtnText: string;
  showErrors: boolean = false;
  showResult: boolean;
  @ViewChild("coolantTypeSelect") select: Select;

  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  constructor(
    private keyboard: Keyboard,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public popOverCtrl: PopoverController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public masterData: MasterDataProvider,
    public translate: TranslateService,
    public storage: Storage,
    public firebaseAnalytics: FirebaseAnalytics,
    public platform: Platform,
    public common: CommonHelperProvider
  ) {
    this.calculator = new CoolantCalculatorModel(this.storage);
    this.validator = new CoolantValidator(translate, this.firebaseAnalytics);
    translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
    this.validator.getValidationMsgs().then((msgs) => {
      this.validationMsgs = msgs;
    });
    this.coolantForm = formBuilder.group(
      {
        coolantType: ["", Validators.required],
        gwvOrPressure: [null],
        gwvOrPressureValue: ["", Validators.required],
        gwvOrPressureUnitPref: [null, Validators.required],
        gpOrFlowrate: [null, Validators.required],
        gpOrFlowrateValue: ["", Validators.required],
        gpOrFlowrateUnitPref: [null, Validators.required],
        gwcwValue: ["", Validators.required],
        gwcweUnitPref: [null, Validators.required],
      },
      { validator: this.validator.coolantValidator.bind(this) }
    );

    this.setGwvPref(true);
    this.setGwvSubPref(true);
    this.setGpPref(true);
    this.setGpSubPref(true);
    this.setGwcwPref(true);

    this.masterData.getCoolantTypeDropDowns().then((val) => {
      this.coolantTypeListRefs = val;
    });

    this.loadDataFromDb();
  }

  /**
   * Name: ionViewDidLoad
   * Parameters: none
   * Description: used to call firebase event for coolant screen and to create the popover dialog
   * Return: none
   */
  ionViewDidLoad() {
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.COOLANT_CALCULATOR
    );
    this.pumpPowerModel = this.popOverCtrl.create(
      "PumpPowerPage",
      {},
      { showBackdrop: true }
    );
  }

  /**
   * Name: ionViewWillLeave
   * Parameters: none
   * Description: used to dismiss popovers if open
   * Return: none
   */
  ionViewWillLeave() {
    this.pumpPowerModel.dismiss();
    this.select.close();
  }

  /**
   * Name: onSubmit
   * Parameters: value
   * Description: used to read data from ui to show validation messages if required and to perform required calculations
   * Return: none
   */
  onSubmit(value: any): void {
    this.keyboard.hide();
    var coolantTypeHasErrors: boolean;
    var gwpPressureHasErrors: boolean;
    var gpCFlowrateHasErrors: boolean;
    var gwcWidthHasErrors: boolean;

    if (
      this.coolantForm &&
      this.coolantForm
        .get("coolantType")
        .hasError(this.validationMsgs.coolantType.type)
    ) {
      coolantTypeHasErrors = true;
    }
    for (let validation of this.validationMsgs.gwpPressure) {
      if (
        this.coolantForm.get("gwvOrPressureValue").hasError(validation.type)
      ) {
        gwpPressureHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.gpCFlowrate) {
      if (this.coolantForm.get("gpOrFlowrateValue").hasError(validation.type)) {
        gpCFlowrateHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.gwcWidth) {
      if (this.coolantForm.get("gwcwValue").hasError(validation.type)) {
        gwcWidthHasErrors = true;
      }
    }
    if (
      !coolantTypeHasErrors &&
      !gwpPressureHasErrors &&
      !gpCFlowrateHasErrors &&
      !gwcWidthHasErrors
    ) {
      if (!this.autoLoad) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.COOLANT_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoad = false;
      }
      this.calculate();
    } else {
      this.coolantForm.get("gwvOrPressureValue").markAsTouched();
      this.coolantForm.get("gpOrFlowrateValue").markAsTouched();
      this.coolantForm.get("gwcwValue").markAsTouched();
      this.getFormValidationErrors();
      this.showErrors = true;
    }
  }

  /**
   * Name: getFormValidationErrors
   * Parameters: none
   * Description: reads validation error messages
   * Return: none
   */
  getFormValidationErrors() {
    Object.keys(this.coolantForm.controls).forEach((key) => {
      const controlErrors = this.coolantForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          this.validator.triggerFirebaseEvent(keyError);
        });
      }
    });
  }

  /**
   * Name: calculate
   * Parameters: none
   * Description: performs required calculation based on the form input values and toggle buttons. Used to trigger firebase events based on toggle buttons and dropdown values.
   * Return: none
   */
  calculate() {
    this.showResult = true;
    this.resultOpacity = "1";
    this.isGwvNotPressureResult = !this.coolantForm.get("gwvOrPressure").value;
    this.isGPNotFlowrateResult = !this.coolantForm.get("gpOrFlowrate").value;
    let inputObj: CoolantInput = {
      unitPrefs: {
        isGrindingWheelVelocityNotPressure: !this.coolantForm.get(
          "gwvOrPressure"
        ).value,
        isGrindingPowerNotFlowrate: !this.coolantForm.get("gpOrFlowrate").value,
        isSfpmNotMps: !this.coolantForm.get("gwvOrPressureUnitPref").value,
        isBarNotPsi: !this.coolantForm.get("gwvOrPressureUnitPref").value,
        isKwNotHp: !this.coolantForm.get("gpOrFlowrateUnitPref").value,
        isIminNotGPM: !this.coolantForm.get("gpOrFlowrateUnitPref").value,
        isMmNotIn: !this.coolantForm.get("gwcweUnitPref").value,
      },
      values: {
        coolantType: this.coolantForm.get("coolantType").value,
        grindingWheelVelocityValue: this.coolantForm.get("gwvOrPressureValue")
          .value,
        grindingPowerValue: this.coolantForm.get("gpOrFlowrateValue").value,
        grindingWheelContactWidth: this.coolantForm.get("gwcwValue").value,
      },
    };
    this.calculateDisable = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
    this.resultObj = this.calculator.calculateCoolantRequirements(inputObj);

    switch (inputObj.values.coolantType.toString()) {
      case "0.93":
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.COOLANT_CALC_COOLANTTYPE_SYNTHETICOIL_HITCOUNT,
          {}
        );
        break;
      case "0.87":
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.COOLANT_CALC_COOLANTTYPE_MINERALOIL_HITCOUNT,
          {}
        );
        break;
      case "1":
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.COOLANT_CALC_COOLANTTYPE_WATERSYNTHETICOIL_HITCOUNT,
          {}
        );
        break;
      case "1.05":
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.COOLANT_CALC_COOLANTTYPE_WATERSOLUBLESYNTHETIC_HITCOUNT,
          {}
        );
        break;
    }
    if (inputObj.unitPrefs.isGrindingWheelVelocityNotPressure) {
      this.firebaseAnalytics.logEvent(
        FirebaseConstants.COOLANT_CALC_GRINDINGWHEELVELOCITY_HITCOUNT,
        {}
      );
    } else {
      this.firebaseAnalytics.logEvent(
        FirebaseConstants.COOLANT_CALC_PRESSURE_HITCOUNT,
        {}
      );
    }
    if (inputObj.unitPrefs.isGrindingPowerNotFlowrate) {
      this.firebaseAnalytics.logEvent(
        FirebaseConstants.COOLANT_CALC_GRINDINGPOWER_HITCOUNT,
        {}
      );
    } else {
      this.firebaseAnalytics.logEvent(
        FirebaseConstants.COOLANT_CALC_COOLANTFLOWRATE_HITCOUNT,
        {}
      );
    }
  }

  /**
   * Name: clearResults
   * Parameters: none
   * Description: Used to reset all toggles and input field values and to clear coolant data saved in the internal database.
   * Return: none
   */
  clearResults() {
    this.showResult = false;
    this.coolantForm.get("gwvOrPressureValue").reset();
    this.coolantForm.get("gpOrFlowrateValue").reset();
    this.coolantForm.get("gwcwValue").reset();

    this.coolantForm.get("gwvOrPressureValue").setValue(" ");
    this.coolantForm.get("gpOrFlowrateValue").setValue(" ");
    this.coolantForm.get("gwcwValue").setValue(" ");

    this.coolantForm.get("coolantType").setValue("");
    this.coolantForm.get("gwvOrPressureValue").setValue("");
    this.coolantForm.get("gpOrFlowrateValue").setValue("");
    this.coolantForm.get("gwcwValue").setValue("");

    this.resultOpacity = "1";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
    this.coolantForm.get("gwvOrPressureValue").markAsPristine();
    this.coolantForm.get("gpOrFlowrateValue").markAsPristine();
    this.coolantForm.get("gwcwValue").markAsPristine();
    this.coolantForm.get("gwvOrPressureValue").markAsUntouched();
    this.coolantForm.get("gpOrFlowrateValue").markAsUntouched();
    this.coolantForm.get("gwcwValue").markAsUntouched();
    this.showErrors = false;
    this.storage.get(Constants.COOLANT_INPUT).then((coolantObj) => {
      if (coolantObj && coolantObj.values) {
        coolantObj.values = null;
        this.storage.set(Constants.COOLANT_INPUT, coolantObj);
      }
    });
  }

  /**
   * Name: updateResults
   * Parameters: none
   * Description: Used to blur the result area and to display update button
   * Return: none
   */
  updateResults() {
    this.calculateDisable = false;
    this.resultOpacity = "0.3";
  }

  /**
   * Name: setGwvPref
   * Parameters: none
   * Description: used to set grinding wheel velocity or pressure as selected and to display its value respectively
   * Return: none
   */
  setGwvPref(isGwvNotPressure) {
    this.isGwvNotPressure = isGwvNotPressure;
    this.isGwvNotPressureResult = isGwvNotPressure;
    this.coolantForm.get("gwvOrPressure").setValue(!isGwvNotPressure);
    this.updateGwvPToggle();
  }

  /**
   * Name: setGwvPref
   * Parameters: subUnit
   * Description: used to set grinding wheel velocity sub unit as selected and to display its value respectively to the labels
   * Return: none
   */
  setGwvSubPref(subUnit) {
    // isGwvNotPressure: boolean;
    // isBarNotPsi: boolean;
    // isSfpmNotMps: boolean;
    this.coolantForm.get("gwvOrPressureUnitPref").setValue(!subUnit);
    if (this.isGwvNotPressure) {
      this.isSfpmNotMps = subUnit;
    } else {
      this.isBarNotPsi = subUnit;
    }
    this.updateSubGwvPToggle();
  }

  /**
   * Name: updateGwvPToggle
   * Parameters: none
   * Description: called when the toggle is updated, and its respective input field value is set to null and its sub unit labels are updated.
   * Return: none
   */
  updateGwvPToggle() {
    this.showErrors = false;
    this.coolantForm.get("gwvOrPressureValue").setValue("");
    this.coolantForm.get("gwvOrPressureValue").markAsPristine();
    this.coolantForm.get("gwvOrPressureValue").markAsUntouched();
    this.isGwvNotPressure = !this.coolantForm.get("gwvOrPressure").value;

    if (this.isGwvNotPressure) {
      this.gwvPressureLeftLabel = "SFPM";
      this.gwvPressureRightLabel = "m/Sec";
    } else {
      this.gwvPressureLeftLabel = "Bar";
      this.gwvPressureRightLabel = "Psi";
    }
    this.setGwvSubPref(true);
  }

  /**
   * Name: updateSubGwvPToggle
   * Parameters: none
   * Description: Used to hide validation error message, to update the sub unit toggle of grinding wheel velocity and pressure
   * Return: none
   */
  updateSubGwvPToggle() {
    this.showErrors = false;
    this.coolantForm.get("gwvOrPressureValue").setValue("");
    this.coolantForm.get("gwvOrPressureValue").markAsPristine();
    this.coolantForm.get("gwvOrPressureValue").markAsUntouched();
    this.isBarNotPsi = !this.coolantForm.get("gwvOrPressureUnitPref").value;
    this.isSfpmNotMps = !this.coolantForm.get("gwvOrPressureUnitPref").value;
  }

  /**
   * Name: updateDropDown
   * Parameters: none
   * Description: on update of dropdown value, calculations have to be done
   * Return: none
   */
  updateDropDown() {
    this.updateResults();
  }

  /**
   * Name: setGpPref
   * Parameters: isGPNotFlowrate
   * Description: used to set Grinding power or flowrate as selected and to display its value respectively
   * Return: none
   */
  setGpPref(isGPNotFlowrate) {
    this.isGPNotFlowrate = isGPNotFlowrate;
    this.isGPNotFlowrateResult = isGPNotFlowrate;
    this.coolantForm.get("gpOrFlowrate").setValue(!isGPNotFlowrate);
    this.updateGpCfToggle();
  }
  /**
   * Name: updateGpCfToggle
   * Parameters: none
   * Description: called when the toggle is updated, and its respective input field value is set to null and its sub unit labels are updated.
   * Return: none
   */
  updateGpCfToggle() {
    this.showErrors = false;
    this.coolantForm.get("gpOrFlowrateValue").setValue("");
    this.coolantForm.get("gpOrFlowrateValue").markAsPristine();
    this.coolantForm.get("gpOrFlowrateValue").markAsUntouched();
    this.isGPNotFlowrate = !this.coolantForm.get("gpOrFlowrate").value;
    if (this.isGPNotFlowrate) {
      this.gpCfLeftLabel = "KW";
      this.gpCfRightLabel = "HP";
    } else {
      this.gpCfLeftLabel = "l/min";
      this.gpCfRightLabel = "GPM";
    }
    this.setGpSubPref(true);
  }

  /**
   * Name: updateSubGpCfToggle
   * Parameters: none
   * Description: Used to hide validation error message, to update the sub unit toggle of Grinding power and coolant flowrate
   * Return: none
   */
  updateSubGpCfToggle() {
    this.showErrors = false;
    this.coolantForm.get("gpOrFlowrateValue").setValue("");
    this.coolantForm.get("gpOrFlowrateValue").markAsPristine();
    this.coolantForm.get("gpOrFlowrateValue").markAsUntouched();
    this.isKwNotHp = !this.coolantForm.get("gpOrFlowrateUnitPref").value;
    this.isIminNotGPM = !this.coolantForm.get("gpOrFlowrateUnitPref").value;
  }

  /**
   * Name: setGpSubPref
   * Parameters: subUnit
   * Description: used to set Grinding power or flowrate sub units respectively and to set its value
   * Return: none
   */
  setGpSubPref(subUnit) {
    this.coolantForm.get("gpOrFlowrateUnitPref").setValue(!subUnit);
    if (this.isGPNotFlowrate) {
      this.isKwNotHp = subUnit;
    } else {
      this.isIminNotGPM = subUnit;
    }
    this.updateSubGpCfToggle();
  }

  /**
   * Name: setGwcwPref
   * Parameters: isMmNotIn
   * Description: used to set Grinding wheel contact width sub units and its value
   * Return: none
   */
  setGwcwPref(isMmNotIn) {
    this.isMmNotIn = isMmNotIn;
    this.coolantForm.get("gwcweUnitPref").setValue(!isMmNotIn);
    this.updateGwcwToggle();
  }

  /**
   * Name: updateGwcwToggle
   * Parameters: none
   * Description: used to hide validation error messages and to set input field value to null when the grinding wheel contact width sub units toggle is updated
   * Return: none
   */
  updateGwcwToggle() {
    this.showErrors = false;
    this.coolantForm.get("gwcwValue").setValue("");
    this.coolantForm.get("gwcwValue").markAsPristine();
    this.coolantForm.get("gwcwValue").markAsUntouched();
    this.isMmNotIn = !this.coolantForm.get("gwcweUnitPref").value;
  }

  /**
   * Name: loadDataFromDb
   * Parameters: none
   * Description: used to load data from db if available to update the form with the db data.
   * Return: none
   */
  loadDataFromDb() {
    this.storage.get(Constants.COOLANT_INPUT).then((coolantObj) => {
      if (coolantObj) {
        if (coolantObj.unitPrefs) {
          this.setGwvPref(
            coolantObj.unitPrefs.isGrindingWheelVelocityNotPressure
          );
          if (coolantObj.unitPrefs.isGrindingWheelVelocityNotPressure) {
            this.setGwvSubPref(coolantObj.unitPrefs.isSfpmNotMps);
          } else {
            this.setGwvSubPref(coolantObj.unitPrefs.isBarNotPsi);
          }
          this.setGpPref(coolantObj.unitPrefs.isGrindingPowerNotFlowrate);
          if (coolantObj.unitPrefs.isGrindingPowerNotFlowrate) {
            this.setGpSubPref(coolantObj.unitPrefs.isKwNotHp);
          } else {
            this.setGpSubPref(coolantObj.unitPrefs.isIminNotGPM);
          }
          this.setGwcwPref(coolantObj.unitPrefs.isMmNotIn);
        }
        if (coolantObj.values) {
          this.coolantForm
            .get("gwvOrPressureValue")
            .setValue(coolantObj.values.grindingWheelVelocityValue);
          this.coolantForm
            .get("gpOrFlowrateValue")
            .setValue(coolantObj.values.grindingPowerValue);
          this.coolantForm
            .get("gwcwValue")
            .setValue(coolantObj.values.grindingWheelContactWidth);
          this.coolantForm
            .get("coolantType")
            .setValue(coolantObj.values.coolantType);
          this.autoLoad = true;
          this.onSubmit(this.coolantForm.value);
        } else {
          this.autoLoad = false;
        }
      }
    });
  }

  /**
   * Name: pumpPowerPopUp
   * Parameters: none
   * Description: used to present pump power popover
   * Return: none
   */
  pumpPowerPopUp() {
    this.pumpPowerModel.present();
  }

  /**
   * Name: round
   * Parameters: value, precision
   * Description: used to perform rounding to decimal numbers based on precision
   * Return: float number
   */
  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  }

  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    console.log(this.coolantForm);

    var enteredInput = event.target.value;
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    // var inp = ev.key;
    // if (ev.target.value.length>=9 || !(this.common.floatnoValidationRunTime(inp))) {
    // ev.preventDefault();
    // }
    this.common.telkeypress(ev);
  }
  //
}
