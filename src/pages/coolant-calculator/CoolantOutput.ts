export interface GWVelocityPressure {
  grindingWheelVelocitySFPM: number;
  grindingWheelVelocityMPS: number;
  pressureBar: number;
  pressurePsi: number;
}

export interface GrindingPowerAndCoolantFlowrate {
  grindingPowerKW: number;
  grindingPowerHP: number;
  coolantFlowrateGPM: number;
  coolantFlowrateIMin: number;
}

export interface NozzleArea {
  nozzleAreaMM: number;
  nozzleAreaIn: number;
}

export interface GrindingWheelContactWidth {
  grindingWheelContactWidthIn: number;
  grindingWheelContactWidthMM: number;
}

export interface GrindingWheelContactHeight {
  grindingWheelContactHeightMM: number;
  grindingWheelContactHeightIn: number;
}

export interface NozzleDiameter {
  nozzleDiameterIn: number;
  nozzleAreaMM: number;
}

export interface PumpPower {
  pumpPowerKW: number;
  pumpPowerHP: number;
  pumpPowerHeadFeet: number;
}

export interface CoolantOutput {
  resultGWVelocityPressure: GWVelocityPressure;
  resultGrindingPowerAndCoolantFlowrate: GrindingPowerAndCoolantFlowrate;
  resultNozzleArea: NozzleArea;
  resultGrindingWheelContactWidth: GrindingWheelContactWidth;
  resultGrindingWheelContactHeight: GrindingWheelContactHeight;
  resultNozzleDiameter: NozzleDiameter;
  resultPowerRequirements: PumpPower;
}
