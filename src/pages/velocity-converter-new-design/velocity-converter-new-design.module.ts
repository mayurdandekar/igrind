import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VelocityConverterNewDesignPage } from './velocity-converter-new-design';

@NgModule({
  declarations: [
    VelocityConverterNewDesignPage,
  ],
  imports: [
    IonicPageModule.forChild(VelocityConverterNewDesignPage),
  ],
})
export class VelocityConverterNewDesignPageModule {}
