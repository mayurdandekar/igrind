import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";
import { MasterDataProvider } from "../../providers/master-data/master-data";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import * as FirebaseConstants from "../../utils/analytics-constants";

/**
 * Generated class for the VelocityConverterNewDesignPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-velocity-converter-new-design",
  templateUrl: "velocity-converter-new-design.html",
})
export class VelocityConverterNewDesignPage {
  inputRPMValue: any;
  inputValue: any;
  selectedData: string;

  outputData: any;
  velocityConverterData: any;
  isWorkPiece: boolean;
  margin: string = "marginTop15";
  error2: boolean = false;
  error1: boolean = false;

  error3: boolean;
  outpError: boolean;

  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    public common: CommonHelperProvider,
    public masterData: MasterDataProvider,
    public firebaseAnalytics: FirebaseAnalytics
  ) {
    this.inputValue = "";
    this.inputRPMValue = "";
    this.isWorkPiece = false;
    console.log(this.platform.is("ios"));

    this.masterData.getVelocityConverterData().then((val) => {
      this.velocityConverterData = val;
      this.selectedData = this.velocityConverterData[0].data;
      this.onChange(this.selectedData);
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad VelocityConverterPage");
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.VELOCITY_CONVERTER_CALCULATOR
    );
    this.firebaseAnalytics.logEvent(
      FirebaseConstants.VELOCITY_VISIT_HITCOUNT,
      {}
    );
  }
  onChange(event) {
    for (var i = 0; i < this.velocityConverterData.length; i++) {
      if (event == this.velocityConverterData[i].data) {
        this.outputData = this.velocityConverterData[i].outp;
        if (this.velocityConverterData[i].isWorkPiece) {
          this.isWorkPiece = true;
          !this.inputRPMValue ? (this.error3 = true) : (this.error3 = false);

          if (this.error1 && (this.error2 || this.error3)) {
            this.margin = "marginTop120";
          } else if (this.error1 || this.error2 || this.error3) {
            this.margin = "marginTop95";
          } else {
            this.margin = "marginTop75";
          }
        } else {
          this.isWorkPiece = false;
          this.error2 = false;
          this.error3 = false;
          if (this.error1) {
            this.margin = "marginTop25";
          } else {
            this.margin = "marginTop15";
          }
        }
        if (!!this.inputValue) this.onChangeInput(this.inputValue);
        return;
      }
    }
  }
  onChangeInput(value) {
    var rpmValue = !!this.inputRPMValue ? this.inputRPMValue : 0;
    for (let j = 0; j < this.outputData.length; j++) {
      if (this.outputData[j].isWorkPiece) {
        if (this.outputData[j].workPieceType == "*") {
          this.outputData[j].outp = rpmValue * value;
        } else {
          this.outputData[j].outp = value / rpmValue;
        }
      } else {
        this.outputData[j].outp = this.outputData[j].value * value;
      }
    }
  }
  onChangeInput1(value) {
    var inputValue = !!this.inputValue ? this.inputValue : 0;
    for (let j = 0; j < this.outputData.length; j++) {
      if (this.outputData[j].isWorkPiece) {
        if (this.outputData[j].workPieceType == "*") {
          this.outputData[j].outp = inputValue * value;
        } else {
          this.outputData[j].outp = inputValue / value;
        }
      } else {
        this.outputData[j].outp = this.outputData[j].value * inputValue;
      }
      isNaN(this.outputData[j].outp) ? (this.outputData[j].outp = 0) : "";
    }
  }
  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    var enteredInput = event.target.value;
    var field = this;
    if (this.inputValue == 0 || !this.inputValue == undefined) {
      this.error1 = true;
      if (this.isWorkPiece) {
        if (this.error2 || this.error3) {
          this.margin = "marginTop120";
        } else {
          this.margin = "marginTop100";
        }
      } else {
        if (this.error2) {
          this.margin = "marginTop25";
        } else {
          this.margin = "marginTop25";
        }
      }
      this.reset();
    } else {
      this.error1 = false;
      if (this.isWorkPiece) {
        this.margin = "marginTop85";
      } else {
        this.margin = "marginTop15";
      }
    }

    if (this.inputValue.length > 9) {
      this.inputValue = this.inputValue.substring(
        0,
        this.inputValue.length - 1
      );
    }

    // if (enteredInput)
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  isValidNumber1(event) {
    var enteredInput = event.target.value;
    if (this.inputRPMValue == "") {
      this.error3 = true;
      this.error2 = false;
      if (this.error1) {
        this.margin = "marginTop120";
        this.reset();
      } else {
        this.margin = "marginTop100";
      }
    } else if (this.inputRPMValue == 0 || !this.inputRPMValue == undefined) {
      this.error2 = true;
      this.error3 = false;
      if (this.error1) {
        this.margin = "marginTop120";
        this.reset();
      } else {
        this.margin = "marginTop100";
      }
    } else {
      this.error2 = false;
      this.error3 = false;
      if (this.error1) {
        this.margin = "marginTop95";
      } else {
        this.margin = "marginTop85";
      }
    }
    if (this.inputRPMValue.length > 9) {
      this.inputRPMValue = this.inputRPMValue.substring(
        0,
        this.inputRPMValue.length - 1
      );
    }
    // if (enteredInput)
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    this.common.telkeypress(ev);
  }

  /**
   * Name : round
   * Parameters: 'value' & 'precision' of type any
   * Description: This method round of the value to the given precision.
   * Return: void
   */
  // roundToY(value, precision) {
  //   let multiplier = Math.pow(10, precision || 0);
  //   return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  // }

  round(num, X) {
    console.log("round only");

    if (num == "NaN" || num == "Infinity") {
      this.outpError = true;
      return "Enter RPM";
    }
    return +(Math.round(parseFloat(num + "e+" + X)) + "e-" + X);
  }

  roundToNine(num) {
    console.log("round nine");

    if (num == "NaN" || num == "Infinity") {
      this.outpError = true;
      return "Enter RPM";
    }
    let numArray = num.toString().split(".");
    if (numArray[1] && numArray[1].length > 9) {
      return num.toFixed(9).toString();
    }
    return num;
  }

  precisionToSFPM(num) {
    console.log("round precision");

    num = num.toPrecision(10);
    return Number(num);
  }

  reset() {
    for (let i = 0; i < this.velocityConverterData.length; i++) {
      for (let j = 0; j < this.velocityConverterData[i].outp.length; j++) {
        this.velocityConverterData[i].outp[j].outp = 0;
      }
    }
    for (var i = 0; i < this.velocityConverterData.length; i++) {
      if (this.selectedData == this.velocityConverterData[i].data) {
        this.outputData = this.velocityConverterData[i].outp;
        console.log(this.outputData);
      }
    }
  }
}
