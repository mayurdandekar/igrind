import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QprimeRangePage } from './qprime-range';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    QprimeRangePage,
  ],
  imports: [
    IonicPageModule.forChild(QprimeRangePage),
    PipesModule,
    TranslateModule.forChild()
  ],
})
export class QprimeRangePageModule {}
