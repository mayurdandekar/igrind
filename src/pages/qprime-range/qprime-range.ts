import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import * as Constants from "../../utils/constants";
import { TranslateService } from "@ngx-translate/core";
/**
 * Generated class for the QprimeRangePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-qprime-range",
  templateUrl: "qprime-range.html",
})
export class QprimeRangePage {
  unit_QW: any;
  qPrime_range: any = [];
  isEnglish: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translate: TranslateService
  ) {
    this.unit_QW = Constants.KEY_QW;

    this.isEnglish = this.navParams.get("isEnglish");
    //Initialize Qprime Ranges

    if (this.isEnglish == true) {
      this.qPrime_range = [
        {
          id: 1,
          description: this.translate.instant("precision_finishing"),
          range: "0.5 - 2.8",
        },
        {
          id: 2,
          description: this.translate.instant("grinding_sa"),
          range: "0.3 - 2.8",
        },
        {
          id: 3,
          description: this.translate.instant("surface_grinding"),
          range: "0.2 - .09",
        },
        {
          id: 4,
          description: this.translate.instant("id_grinding"),
          range: "05 - .47",
        },
        {
          id: 5,
          description: this.translate.instant("centerless_grinding"),
          range: "5.5 - 9.0",
        },
      ];
    } else {
      this.qPrime_range = [
        {
          id: 1,
          description: this.translate.instant("precision_finishing"),
          range: "5 - 30",
        },
        {
          id: 2,
          description: this.translate.instant("grinding_sa"),
          range: "3 - 30",
        },
        {
          id: 3,
          description: this.translate.instant("surface_grinding"),
          range: "2 - 10",
        },
        {
          id: 4,
          description: this.translate.instant("id_grinding"),
          range: "0.5 - 5",
        },
        {
          id: 5,
          description: this.translate.instant("centerless_grinding"),
          range: "60 - 100",
        },
      ];
    }
  }

  ionViewDidLoad() {}
}
