import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  ModalController,
  Platform,
  PopoverController,
  Select,
} from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import * as Constants from "../../utils/constants";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { Keyboard } from "@ionic-native/keyboard";
import { TranslateService } from "@ngx-translate/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { diamondRollDressingModel } from "../../utils/diamondRollDressingModel";
import { MasterDataProvider } from "../../providers/master-data/master-data";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";

/**

/**
 * Generated class for the DiamondRollDressingCalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-diamond-roll-dressing-calculator",
  templateUrl: "diamond-roll-dressing-calculator.html",
})
export class DiamondRollDressingCalculatorPage {
  speedRatioModel: any;
  overlapRatioModel: any;
  productTypePopup: any;
  private autoLoad: boolean = true;
  showResult: boolean;
  calculator: diamondRollDressingModel;
  productTypeListRefs: Array<any>;
  productTypeListRefsOne: Array<any>;
  productTypeListRefsTwo: Array<any>;
  speedRatioRecommendedValues: Array<any>;
  overlapRatioRecommendedValues: Array<any>;
  maxDressDepthRecommendedValues: Array<any>;
  productType: any;
  op_speed_ratio: any;
  op_speed_ratio_recommended: any;
  op_traverse_rate: any;
  op_traverse_rate_recommended: any;
  op_SFPM_wheel: any;
  op_SFPM_wheel_recommended: any;
  op_overlap_ratio: any;
  op_overlap_ratio_recommended: any;
  op_max_dress_depth: any;
  op_max_dress_depth_recommended: any;
  op_SFPM_diamond_roll: any;
  unit_label: string;
  public selected_value: { selAlias: ""; selValue: "" };
  @ViewChild("productselect") select: Select;
  selectedItem: any;

  diamondRollDressingForm: FormGroup;
  validationMsgs: {
    productType: any;
    diamondRollDiameter: Array<any>;
    diamondRollWidth: Array<any>;
    diamondRollRPM: Array<any>;
    wheelDiameter: Array<any>;
    wheelRPM: Array<any>;
    wheelTraverseRate: Array<any>;
  };
  isEnglish: boolean;
  isEnglishResult: boolean;
  resultOpacity: string = "1";
  calculateDisable: boolean = false;
  calculateBtnText: string;
  showErrors: boolean = false;

  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  constructor(
    private keyboard: Keyboard,
    public platform: Platform,
    public statusBar: StatusBar,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public popOverCtrl: PopoverController,
    public formBuilder: FormBuilder,
    public masterData: MasterDataProvider,
    public translate: TranslateService,
    public storage: Storage,
    public firebaseAnalytics: FirebaseAnalytics,
    public common: CommonHelperProvider
  ) {
    this.calculator = new diamondRollDressingModel(
      this.storage,
      this.firebaseAnalytics
    );
    this.masterData.getProductTypeDropDowns().then((productList) => {
      this.productTypeListRefs = productList;
      this.productTypeListRefsOne = this.productTypeListRefs.slice(0, 2);
      this.productTypeListRefsTwo = this.productTypeListRefs.slice(2);
    });

    // fetch speedRatioRecommendedValues from master data
    this.masterData
      .getSpeedRatioRecommendedValues()
      .then((speedRatioRecommendedList) => {
        this.speedRatioRecommendedValues = speedRatioRecommendedList;
      });

    // fetch overlapRatioRecommendedValues from master data
    this.masterData
      .getOverlapRatioRecommendedValues()
      .then((overlapRatioRecommendedList) => {
        this.overlapRatioRecommendedValues = overlapRatioRecommendedList;
      });

    // fetch maxDressDepthRecommendedValues from master data
    this.masterData
      .getMaxDressDepthRecommendedValues()
      .then((maxDressDepthRecommendedList) => {
        this.maxDressDepthRecommendedValues = maxDressDepthRecommendedList;
      });

    translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    var diamondRollErrMsg;
    var emptyErrMsg;
    var inputLengthErrmsg;
    this.translate
      .get("zero_input")
      .toPromise()
      .then((errorMsg) => {
        diamondRollErrMsg = errorMsg;
        this.translate
          .get("empty_invalid_input")
          .toPromise()
          .then((emptyInputMsg) => {
            emptyErrMsg = emptyInputMsg;
            this.translate
              .get("product_type_empty")
              .toPromise()
              .then((emptyPrdMsg) => {
                this.translate
                  .get("input_length_invalid")
                  .toPromise()
                  .then((inputLengthInvalidMsg) => {
                    inputLengthErrmsg = inputLengthInvalidMsg;
                    this.validationMsgs = {
                      productType: {
                        type: "productType",
                        message: emptyPrdMsg,
                      },
                      diamondRollDiameter: [
                        {
                          type: "diamondRollDiameter_eng",
                          message: diamondRollErrMsg,
                        },
                        {
                          type: "diamondRollDiameter_metric",
                          message: diamondRollErrMsg,
                        },
                        {
                          type: "diamondRollDiameter_empty",
                          message: emptyErrMsg,
                        },
                        { type: "input_length", message: inputLengthErrmsg },
                      ],
                      diamondRollWidth: [
                        {
                          type: "diamondRollWidth_eng",
                          message: diamondRollErrMsg,
                        },
                        {
                          type: "diamondRollWidth_metric",
                          message: diamondRollErrMsg,
                        },
                        {
                          type: "diamondRollWidth_empty",
                          message: emptyErrMsg,
                        },
                        { type: "input_length", message: inputLengthErrmsg },
                      ],
                      diamondRollRPM: [
                        {
                          type: "diamondRollRPM_eng",
                          message: diamondRollErrMsg,
                        },
                        { type: "diamondRollRPM_empty", message: emptyErrMsg },
                        { type: "input_length", message: inputLengthErrmsg },
                      ],
                      wheelDiameter: [
                        {
                          type: "wheelDiameter_eng",
                          message: diamondRollErrMsg,
                        },
                        {
                          type: "wheelDiameter_metric",
                          message: diamondRollErrMsg,
                        },
                        { type: "wheelDiameter_empty", message: emptyErrMsg },
                        { type: "input_length", message: inputLengthErrmsg },
                      ],
                      wheelRPM: [
                        { type: "wheelRPM_eng", message: diamondRollErrMsg },
                        { type: "wheelRPM_empty", message: emptyErrMsg },
                        { type: "input_length", message: inputLengthErrmsg },
                      ],
                      wheelTraverseRate: [
                        {
                          type: "wheelTraverseRate_eng",
                          message: diamondRollErrMsg,
                        },
                        {
                          type: "wheelTraverseRate_metric",
                          message: diamondRollErrMsg,
                        },
                        {
                          type: "wheelTraverseRate_empty",
                          message: emptyErrMsg,
                        },
                        { type: "input_length", message: inputLengthErrmsg },
                      ],
                    };
                  });
              });
          }); //emptyinputvalue
      });

    this.diamondRollDressingForm = formBuilder.group(
      {
        unitPref: [null],
        productType: ["", Validators.required],
        diamondRollDiameter: ["", Validators.required],
        diamondRollWidth: ["", Validators.required],
        diamondRollRPM: ["", Validators.required],
        wheelDiameter: ["", Validators.required],
        wheelRPM: ["", Validators.required],
        wheelTraverseRate: ["", Validators.required],
      },
      { validator: this.calculator.diamondRollDressingValidator.bind(this) }
    );

    this.diamondRollDressingForm.get("unitPref").setValue(false);
    this.isEnglish = true;
    this.isEnglishResult = true;
    this.updateToggle();
    this.loadSwitchFromDb();
    this.storage.get(Constants.KEY_DIAMOND).then((diamondObj) => {
      if (diamondObj) {
        this.diamondRollDressingForm
          .get("productType")
          .setValue(diamondObj["productType"]);
        this.diamondRollDressingForm
          .get("diamondRollDiameter")
          .setValue(
            diamondObj["inputDiamondRollParameters"]["diamondRollDiameter"]
          );
        this.diamondRollDressingForm
          .get("diamondRollWidth")
          .setValue(
            diamondObj["inputDiamondRollParameters"]["diamondRollWidth"]
          );
        this.diamondRollDressingForm
          .get("diamondRollRPM")
          .setValue(diamondObj["inputDiamondRollParameters"]["diamondRollRPM"]);
        this.diamondRollDressingForm
          .get("wheelDiameter")
          .setValue(diamondObj["inputWheelParameters"]["wheelDiameter"]);
        this.diamondRollDressingForm
          .get("wheelRPM")
          .setValue(diamondObj["inputWheelParameters"]["wheelRPM"]);
        this.diamondRollDressingForm
          .get("wheelTraverseRate")
          .setValue(diamondObj["inputWheelParameters"]["wheelTraverseRate"]);
        this.autoLoad = true;
        this.onSubmit(this.diamondRollDressingForm.value);
      } else {
        this.autoLoad = false;
      }
    });
  }

  /**
   * Name : ionViewDidLoad
   * Parameters: null
   * Description: This method trigger a firebase event and load the data from storage if any and to create pop overs.
   * Return: void
   */
  ionViewDidLoad() {
    console.log("ionViewDidLoad DiamondRollDressingCalculatorPage");
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.DIAMOND_DRESSING_CALCULATOR
    );
    this.speedRatioModel = this.popOverCtrl.create("SpeedRatioPage");
    this.overlapRatioModel = this.popOverCtrl.create("OverlapRatioPage");
    this.productTypePopup = this.popOverCtrl.create("ProductTypePopupPage");
    this.productTypePopup.onDidDismiss((data) => {
      if (data) {
        this.select.close();
        this.selectedItem = data.alias;
        this.diamondRollDressingForm.get("productType").setValue(data.alias);
        this.selected_value = data;
        console.log(data);
      }
    });
  }

  /**
   * Name : ionViewWillLeave
   * Parameters: null
   * Description: This method dismiss the popovers if open.
   * Return: void
   */
  ionViewWillLeave() {
    this.speedRatioModel.dismiss();
    this.overlapRatioModel.dismiss();
    this.select.close();
  }

  /**
   * Name : speedRatioClick
   * Parameters: null
   * Description: This method is called when speed ratio info button is clicked. It shows the speed ratio popup and also log the event in firebase
   * Return: void
   */
  speedRatioClick() {
    this.speedRatioModel.present();
    this.firebaseAnalytics.logEvent(
      FirebaseConstants.DIAMOND_ROLL_CALC_INFO_RES_SPEEDRATIO_HITCOUNT,
      {}
    );
  }

  /**
   * Name : overlapRatio
   * Parameters: null
   * Description: This method is called when overlap ratio info button is clicked. It shows the overlap ratio popup and also log the event in firebase
   * Return: void
   */
  overlapRatio() {
    this.overlapRatioModel.present();
    this.firebaseAnalytics.logEvent(
      FirebaseConstants.DIAMOND_ROLL_CALC_INFO_RES_OVERLAPRATIO_HITCOUNT,
      {}
    );
  }

  /**
   * Name : loadSwitchFromDb
   * Parameters: null
   * Description: This method gets the Unit Pref from storage based on toggle.
   * Return: void
   */
  loadSwitchFromDb() {
    this.storage
      .get(Constants.KEY_UNIT_PREF_DIAMOND_ROLL_DRESSING)
      .then((toggle) => {
        if (toggle != null && toggle !== undefined) {
          this.diamondRollDressingForm.get("unitPref").setValue(toggle);
          this.isEnglish = !toggle;
          this.isEnglishResult = !toggle;
          this.updateToggle();
        }
      });
  }

  /**
   * Name : onSubmit
   * Parameters: 'value' of type any
   * Description: This method validates the form and call the calculate method if form is valid else it shows the error.
   * Return: void
   */
  onSubmit(value: any): void {
    this.keyboard.hide();
    var productTypeHasErrors: boolean;
    var diamondRollDiameterHasErrors: boolean;
    var diamondRollWidthHasErrors: boolean;
    var diamondRollRPMHasErrors: boolean;
    var wheelDiameterHasErrors: boolean;
    var wheelRPMHasErrors: boolean;
    var wheelTraverseRateHasErrors: boolean;

    if (
      this.diamondRollDressingForm
        .get("productType")
        .hasError(this.validationMsgs.productType.type)
    ) {
      productTypeHasErrors = true;
    }
    for (let validation of this.validationMsgs.diamondRollDiameter) {
      if (
        this.diamondRollDressingForm
          .get("diamondRollDiameter")
          .hasError(validation.type)
      ) {
        diamondRollDiameterHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.diamondRollWidth) {
      if (
        this.diamondRollDressingForm
          .get("diamondRollWidth")
          .hasError(validation.type)
      ) {
        diamondRollWidthHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.diamondRollRPM) {
      if (
        this.diamondRollDressingForm
          .get("diamondRollRPM")
          .hasError(validation.type)
      ) {
        diamondRollRPMHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.wheelDiameter) {
      if (
        this.diamondRollDressingForm
          .get("wheelDiameter")
          .hasError(validation.type)
      ) {
        wheelDiameterHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.wheelRPM) {
      if (
        this.diamondRollDressingForm.get("wheelRPM").hasError(validation.type)
      ) {
        wheelRPMHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.wheelTraverseRate) {
      if (
        this.diamondRollDressingForm
          .get("wheelTraverseRate")
          .hasError(validation.type)
      ) {
        wheelTraverseRateHasErrors = true;
      }
    }
    if (
      !productTypeHasErrors &&
      !diamondRollDiameterHasErrors &&
      !diamondRollWidthHasErrors &&
      !diamondRollRPMHasErrors &&
      !wheelDiameterHasErrors &&
      !wheelRPMHasErrors &&
      !wheelTraverseRateHasErrors
    ) {
      if (!this.autoLoad) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.DIAMOND_ROLL_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoad = false;
      }
      this.calculate(
        value.unitPref,
        value.productType,
        {
          diamondRollDiameter: value.diamondRollDiameter,
          diamondRollWidth: value.diamondRollWidth,
          diamondRollRPM: value.diamondRollRPM,
        },
        {
          wheelDiameter: value.wheelDiameter,
          wheelRPM: value.wheelRPM,
          wheelTraverseRate: value.wheelTraverseRate,
        }
      );
    } else {
      this.showErrors = true;
      this.diamondRollDressingForm.get("diamondRollDiameter").markAsTouched();
      this.diamondRollDressingForm.get("diamondRollWidth").markAsTouched();
      this.diamondRollDressingForm.get("diamondRollRPM").markAsTouched();
      this.diamondRollDressingForm.get("wheelDiameter").markAsTouched();
      this.diamondRollDressingForm.get("wheelRPM").markAsTouched();
      this.diamondRollDressingForm.get("wheelTraverseRate").markAsTouched();
      this.getFormValidationErrors();
    }
  }

  /**
   * Name : calculate
   * Parameters: 'unitPref' of type boolean
   *             'productType','diamondRollParameters' & 'wheelParameters' of type any
   * Description: This method calculates and shows the result based on user input
   * Return: void
   */
  calculate(
    unitPref: boolean,
    productType,
    diamondRollParameters,
    wheelParameters
  ) {
    var output = this.calculator.calculateDiamondRollDressing(
      !unitPref,
      productType,
      diamondRollParameters,
      wheelParameters,
      this.speedRatioRecommendedValues,
      this.overlapRatioRecommendedValues,
      this.maxDressDepthRecommendedValues
    );
    //get is english here and change in html
    this.isEnglishResult = !this.diamondRollDressingForm.get("unitPref").value;
    this.op_SFPM_diamond_roll = output["calculatedSFPMDiamondRoll"];
    this.op_speed_ratio =
      output["calculatedAndRecommendedSpeedRatio"][
        "resultCalculatedSpeedRatio"
      ];
    this.op_speed_ratio_recommended =
      output["calculatedAndRecommendedSpeedRatio"][
        "resultRecommendedSpeedRatio"
      ];
    this.op_traverse_rate =
      output["calculatedAndRecommendedTraverseRate"][
        "resultCalculatedTraverseRate"
      ];
    this.op_traverse_rate_recommended =
      output["calculatedAndRecommendedTraverseRate"][
        "resultRecommendedTraverseRate"
      ];
    this.op_SFPM_wheel =
      output["calculatedAndRecommendedSFPMWheel"]["resultCalculatedWheelSFPM"];
    this.op_SFPM_wheel_recommended =
      output["calculatedAndRecommendedSFPMWheel"]["resultRecommendedWheelSFPM"];
    this.op_overlap_ratio =
      output["calculatedAndRecommendedOverlapRatio"][
        "resultCalculatedOverlapRatio"
      ];
    this.op_overlap_ratio_recommended =
      output["calculatedAndRecommendedOverlapRatio"][
        "resultRecommendedWheelOverlapRatio"
      ];
    this.op_max_dress_depth =
      output["calculatedAndRecommendedMaxDressDepth"][
        "resultCalculatedMaxDressDepth"
      ];
    this.op_max_dress_depth_recommended =
      output["calculatedAndRecommendedMaxDressDepth"][
        "resultRecommendedMaxDressDepth"
      ];

    this.showResult = true;
    this.resultOpacity = "1";

    this.calculateDisable = true;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    this.calculator.productTypeEvents(productType);
  }

  /**
   * Name : clearResults
   * Parameters: null
   * Description: This method is called when 'Clear Result' button is clicked. It clears all the user input fields.
   * Return: void
   */
  clearResults() {
    this.showResult = false;
    this.op_speed_ratio = "";
    this.op_traverse_rate = "";
    this.op_SFPM_wheel = "";
    this.op_overlap_ratio = "";
    this.op_max_dress_depth = "";
    this.op_SFPM_diamond_roll = "";
    this.diamondRollDressingForm.get("productType").setValue("");

    this.diamondRollDressingForm.get("diamondRollDiameter").reset();
    this.diamondRollDressingForm.get("diamondRollWidth").reset();
    this.diamondRollDressingForm.get("diamondRollRPM").reset();
    this.diamondRollDressingForm.get("wheelDiameter").reset();
    this.diamondRollDressingForm.get("wheelRPM").reset();
    this.diamondRollDressingForm.get("wheelTraverseRate").reset();

    this.diamondRollDressingForm.get("diamondRollDiameter").setValue(" ");
    this.diamondRollDressingForm.get("diamondRollWidth").setValue(" ");
    this.diamondRollDressingForm.get("diamondRollRPM").setValue(" ");
    this.diamondRollDressingForm.get("wheelDiameter").setValue(" ");
    this.diamondRollDressingForm.get("wheelRPM").setValue(" ");
    this.diamondRollDressingForm.get("wheelTraverseRate").setValue(" ");

    this.diamondRollDressingForm.get("diamondRollDiameter").setValue("");
    this.diamondRollDressingForm.get("diamondRollWidth").setValue("");
    this.diamondRollDressingForm.get("diamondRollRPM").setValue("");
    this.diamondRollDressingForm.get("wheelDiameter").setValue("");
    this.diamondRollDressingForm.get("wheelRPM").setValue("");
    this.diamondRollDressingForm.get("wheelTraverseRate").setValue("");
    //this.loadSwitchFromDb();

    this.resultOpacity = "1";
    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
    this.storage
      .remove(Constants.KEY_DIAMOND)
      .then((output) => {
        console.log("Diamond Dressing Cleared: " + output);
      })
      .catch((err) => {
        console.log("Diamond Dressing Clear error: " + err);
      });
    this.diamondRollDressingForm.get("diamondRollDiameter").markAsUntouched();
    this.diamondRollDressingForm.get("diamondRollWidth").markAsUntouched();
    this.diamondRollDressingForm.get("diamondRollRPM").markAsUntouched();
    this.diamondRollDressingForm.get("wheelDiameter").markAsUntouched();
    this.diamondRollDressingForm.get("wheelRPM").markAsUntouched();
    this.diamondRollDressingForm.get("wheelTraverseRate").markAsUntouched();

    this.diamondRollDressingForm.get("diamondRollDiameter").markAsPristine();
    this.diamondRollDressingForm.get("diamondRollWidth").markAsPristine();
    this.diamondRollDressingForm.get("diamondRollRPM").markAsPristine();
    this.diamondRollDressingForm.get("wheelDiameter").markAsPristine();
    this.diamondRollDressingForm.get("wheelRPM").markAsPristine();
    this.diamondRollDressingForm.get("wheelTraverseRate").markAsPristine();
    this.showErrors = false;
  }

  /**
   * Name : updateResults
   * Parameters: null
   * Description: This method is called when any user input field is updated. It enables the 'Update' button and blur the result.
   * Return: void
   */
  updateResults() {
    this.calculateDisable = false;
    this.resultOpacity = "0.3";
  }

  /**
   * Name : updateToggle
   * Parameters: null
   * Description: This method is called when toggle switch is changed. It changes the units according to the selected value.
   * Return: void
   */
  updateToggle() {
    this.showErrors = false;
    this.diamondRollDressingForm.get("diamondRollWidth").setValue("");
    this.diamondRollDressingForm.get("diamondRollRPM").setValue("");
    this.diamondRollDressingForm.get("diamondRollDiameter").setValue("");
    this.diamondRollDressingForm.get("wheelDiameter").setValue("");
    this.diamondRollDressingForm.get("wheelRPM").setValue("");
    this.diamondRollDressingForm.get("wheelTraverseRate").setValue("");

    this.diamondRollDressingForm.get("diamondRollDiameter").markAsUntouched();
    this.diamondRollDressingForm.get("diamondRollWidth").markAsUntouched();
    this.diamondRollDressingForm.get("diamondRollRPM").markAsUntouched();
    this.diamondRollDressingForm.get("wheelDiameter").markAsUntouched();
    this.diamondRollDressingForm.get("wheelRPM").markAsUntouched();
    this.diamondRollDressingForm.get("wheelTraverseRate").markAsUntouched();

    this.diamondRollDressingForm.get("diamondRollDiameter").markAsPristine();
    this.diamondRollDressingForm.get("diamondRollWidth").markAsPristine();
    this.diamondRollDressingForm.get("diamondRollRPM").markAsPristine();
    this.diamondRollDressingForm.get("wheelDiameter").markAsPristine();
    this.diamondRollDressingForm.get("wheelRPM").markAsPristine();
    this.diamondRollDressingForm.get("wheelTraverseRate").markAsPristine();

    this.isEnglish = !this.diamondRollDressingForm.get("unitPref").value;
    if (this.isEnglish) {
      this.unit_label = " (in";
    } else {
      this.unit_label = " (mm";
    }
  }

  updateDropDown() {}

  /**
   * Name : round
   * Parameters: 'value' & 'precision' of type any
   * Description: This method round of the value to the given precision.
   * Return: void
   */
  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  }

  /**
   * Name : getFormValidationErrors
   * Parameters: null
   * Description: This method trigger a firebase event for the errors occured.
   * Return: void
   */
  getFormValidationErrors() {
    Object.keys(this.diamondRollDressingForm.controls).forEach((key) => {
      const controlErrors = this.diamondRollDressingForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          this.calculator.triggerFirebaseEvent(keyError);
        });
      }
    });
  }

  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    var enteredInput = event.target.value;
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    // var inp = ev.key;
    // if (!(this.common.floatnoValidationRunTime(inp))) {
    // ev.preventDefault();
    // }
    this.common.telkeypress(ev);
  }
  //

  openPopup(myEvent) {
    this.productTypePopup.present({
      ev: myEvent,
    });
  }

  openOptions() {
    this.navCtrl.push("ProductTypePopupPage");
  }
}
