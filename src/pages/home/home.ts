import {Component} from '@angular/core';
import {
	NavController,
	ModalController,
	MenuController,
	Platform,
} from 'ionic-angular';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics';
import {WheelSpeedConverterPage} from '../wheel-speed-converter/wheel-speed-converter';
import {DiamondRollDressingCalculatorPage} from '../diamond-roll-dressing-calculator/diamond-roll-dressing-calculator';
import {CoolantCalculatorPage} from '../coolant-calculator/coolant-calculator';
import {TranslateService} from '@ngx-translate/core';
import * as Constants from '../../utils/constants';
import * as FirebaseConstants from '../../utils/analytics-constants';
import {CalcListProvider} from '../../providers/calc-list/calc-list';
import {AlertController, Alert} from 'ionic-angular';
import {PlungeDressingCalculatorPage} from '../plunge-dressing-calculator/plunge-dressing-calculator';
import {StationaryDressingCalculatorPage} from '../stationary-dressing-calculator/stationary-dressing-calculator';
import {CalculatorSubListPage} from '../calculator-sub-list/calculator-sub-list';
import {CalcAllPage} from '../calc-all/calc-all';
@Component({
	selector: 'page-home',
	templateUrl: 'home.html',
})
export class HomePage {
	showedAlert: boolean;
	landingPageItems: Array<{name: any; desc: any; logo: any; key: any}>;
	calcItems: Array<any>;
	confirmAlert: Alert;

	constructor(
		public navCtrl: NavController,
		public platform: Platform,
		public modalCtrl: ModalController,
		public menuCtrl: MenuController,
		private translate: TranslateService,
		private calcModel: CalcListProvider,
		private firebaseAnalytics: FirebaseAnalytics,
		public alertCtrl: AlertController
	) {
		this.landingPageItems = new Array();
		this.calcItems = new Array();
		this.menuCtrl.enable(true, 'main_menu');

		// this.platform.ready().then(() => {
		//   // Okay, so the platform is ready and our plugins are available.
		//   // Here you can do any higher level native things you might need.

		//   this.showedAlert = false;

		//   platform.registerBackButtonAction(() => {
		//     // let nav = this.app._appRoot._getActivePortal() || this.app.getActiveNav();
		//     // let activeView = nav.getActive();
		//     // let element = document.getElementsByTagName('ion-loading')[0];
		//     // let pageName = this.nav.getActive().component.name.toString().toLowerCase();
		//     console.log(this.navCtrl, "Page Name");

		//     if (this.navCtrl.length() == 1) {
		//       if (!this.showedAlert) {
		//         this.presentConfirm();
		//       } else {
		//         this.showedAlert = false;
		//         this.confirmAlert.dismiss();
		//       }
		//     } else {
		//       this.navCtrl.pop();
		//     }

		//   });

		// });
	}

	ionViewWillLeave() {
		this.menuCtrl.enable(true, 'main_menu');
		this.menuCtrl.swipeEnable(true);
	}

	ionViewDidLoad() {
		this.menuCtrl.close();
		console.log('ionViewDidLoad HomePage');
		this.firebaseAnalytics.setCurrentScreen(FirebaseConstants.HOME_SCREEN);
		this.calcModel.getCalculators().then(calcList => {
			console.log(calcList, 'calcList');

			this.landingPageItems = calcList;
		});
	}

	ionViewDidEnter() {
		// this.menuCtrl.swipeEnable(false);
	}

	navigateToCalculator(pageName) {
		console.log(pageName);
		var calcParam: any;
		if (pageName.calcKey == Constants.KEY_CONVERSION) {
			calcParam = {type: 'conversion'};
		} else if (pageName.calcKey == Constants.KEY_GRIND_PARAM) {
			calcParam = {type: 'grind'};
		} else if (pageName.calcKey == Constants.KEY_DRESS_PARAM) {
			calcParam = {type: 'dress'};
		} else {
			calcParam = {type: 'cut'};
		}

		this.calcModel.setActiveCalculator(pageName);
		if (this.platform.is('android')) {
			this.navCtrl.push(CalculatorSubListPage, calcParam);
		} else if (this.platform.is('ios')) {
			this.navCtrl.setRoot(CalculatorSubListPage, calcParam);
		} else {
			this.navCtrl.push(CalculatorSubListPage, calcParam);
		}
	}

	navigateToCalc() {
		this.navCtrl.push(CalcAllPage);
	}

	showTerms() {
		this.navCtrl.push('TermsPage');
	}

	presentConfirm() {
		let confirmPromise = this.translate.get('confirm_exit').toPromise();
		let confirmMsgPromise = this.translate
			.get('confirm_msg_exit')
			.toPromise();
		let cancelPromise = this.translate.get('confirm_cancel').toPromise();
		let yesPromise = this.translate.get('confirm_yes').toPromise();
		Promise.all([
			confirmPromise,
			confirmMsgPromise,
			cancelPromise,
			yesPromise,
		]).then(results => {
			this.confirmAlert = this.alertCtrl.create({
				title: results[0],
				message: results[1],
				buttons: [
					{
						text: results[2],
						role: 'cancel',
						handler: () => {
							this.showedAlert = false;
						},
					},
					{
						text: results[3],
						handler: () => {
							this.platform.exitApp();
						},
					},
				],
			});
			this.showedAlert = true;
			this.confirmAlert.present().then(() => {});
		});
	}
}
