import { Component, ViewChild } from "@angular/core";
import { Content } from "ionic-angular";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import { HomePage } from "../home/home";
import * as Constants from "../../utils/constants";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { TranslateService } from "@ngx-translate/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
/**
 * Generated class for the TermsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-terms",
  templateUrl: "terms.html",
})
export class TermsPage {
  @ViewChild(Content) content: Content;
  showButton: boolean;
  private myTemplate: any = "";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private menuCtrl: MenuController,
    private translate: TranslateService,
    private firebaseAnalytics: FirebaseAnalytics,
    private http: Http
  ) {
    this.storage.get(Constants.KEY_TERMS).then((termsStatus) => {
      if (termsStatus !== null) {
        this.showButton = false;
        this.content.resize();
      } else {
        this.showButton = true;
        this.content.resize();
      }
    });
    // this.translate.get('terms_content').toPromise().then(res => {

    // alert(JSON.stringify(res));

    // remove after italian
    // localStorage.setItem("globalLanguage", "en");

    this.http
      .get(
        "assets/" + "terms_" + localStorage.getItem("globalLanguage") + ".html"
      )
      .map((res) => res.text())
      .subscribe((text) => {
        this.myTemplate = text;
      });
    // });
  }
  processData(data: any) {
    console.log(data);
  }
  ionViewWillLeave() {
    this.menuCtrl.enable(true, "main_menu");
    this.menuCtrl.swipeEnable(true);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad TermsPage");
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.TERMS_AND_CONDITIONS
    );
  }

  ionViewDidEnter() {
    this.menuCtrl.swipeEnable(false);
  }

  acceptTerms() {
    this.storage.set("terms", true);
    this.navCtrl.setRoot(HomePage);
  }
}
