import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
} from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
/**
 * Generated class for the PumpPowerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-pump-power",
  templateUrl: "pump-power.html",
})
export class PumpPowerPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public translate: TranslateService
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad PumpPowerPage");
  }

  ok() {
    this.viewCtrl.dismiss();
  }
}
