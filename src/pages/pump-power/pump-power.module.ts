import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PumpPowerPage } from './pump-power';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    PumpPowerPage,
  ],
  imports: [
    IonicPageModule.forChild(PumpPowerPage),
    PipesModule,
    TranslateModule.forChild()
  ],
})
export class PumpPowerPageModule {}
