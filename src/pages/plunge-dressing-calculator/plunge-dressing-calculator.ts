import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Select,
  Keyboard,
  Platform,
  ModalController,
  PopoverController,
  Slides,
} from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MasterDataProvider } from "../../providers/master-data/master-data";
import { TranslateService } from "@ngx-translate/core";
import { DressingValidator } from "../../validators/DressingValidator";
import { FirebaseAnalytics } from "@ionic-native/firebase-analytics";
import { StatusBar } from "@ionic-native/status-bar";
import { Storage } from "@ionic/storage";
import * as Constants from "../../utils/constants";
import * as FirebaseConstants from "../../utils/analytics-constants";
import { plungeDressingCalculator } from "../../utils/plunge-dressing-calculator-model";
import { CommonHelperProvider } from "../../providers/common-helper/common-helper";

@Component({
  selector: "page-plunge-dressing-calculator",
  templateUrl: "plunge-dressing-calculator.html",
})
export class PlungeDressingCalculatorPage {
  dressingForm: FormGroup;
  wheelTechnTypeListRefs: any;
  @ViewChild("wheelTechTypeSelect") select: Select;
  @ViewChild("pssSlide") slides: Slides;
  @ViewChild("wheelTechTypeCDSRSelect") wheelTechTypeCDSR: Select;
  calculateDisable: boolean;
  resultOpacity: string = "1";
  calculateBtnText: string;
  private autoLoad: boolean = true;
  showErrors: boolean = false;
  validationMsgs: any = {};
  validator: DressingValidator;
  plungeDressModel: plungeDressingCalculator;
  isImperial: boolean;
  unit_label: string;
  unit_label1: string;
  showResult: boolean;
  totalInfeedRateRecommendedValues: any;
  isImperialNCDSR: boolean;
  dressingFormCDSpeedRatio: FormGroup;
  isImperialCDSR: boolean;
  wheelTechnTypeCDSRListRefs: any;
  isUinNotIn: boolean;
  isUinNotIndp: boolean;
  isCDnotNCD: boolean;
  isUDnotCD: boolean;
  unit_labelIR: string;
  unit_labelIR1: string;
  op_infeed_rate_uin_calculated: number;
  op_infeed_rate_uin_recommended: any;
  op_total_infeed_recommended: any;
  op_dress_roll_rpm_calculated: any;
  op_wheel_sfpm_calculated: any;
  op_wheel_sfpm_recommended: any;
  op_dress_roll_sfpm_calculated: any;
  op_dress_roll_sfpm_recommended: any;
  op_speed_ratio_calculated: any;
  op_speed_ratio_recommended: any;
  op_infeed_rate_inmin_calculated: any;
  op_infeed_rate_inmin_recommended: any;
  op_infeed_rate_uinrev_calculated: any;
  op_infeed_rate_uinrev_recommended: any;
  wheelSFPMRecommendedValues: any;
  speedRatioRecommendedValues: any;
  hideTotalInFeedRate: boolean;
  isIRLeft: boolean;
  ir_unit_lable_inmin: string;
  ir_unit_lable_uinrev: string;
  wheelTechnTypeListRefsOne: any;
  wheelTechnTypeListRefsTwo: any;
  wheelTechnTypeListRefsTwoLength: any;

  isKeyBoard: boolean = false;
  currentInputRef: any;
  currentFormRef: any;

  constructor(
    private keyboard: Keyboard,
    public platform: Platform,
    public statusBar: StatusBar,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public popOverCtrl: PopoverController,
    public formBuilder: FormBuilder,
    public masterData: MasterDataProvider,
    public translate: TranslateService,
    public storage: Storage,
    public firebaseAnalytics: FirebaseAnalytics,
    public common: CommonHelperProvider
  ) {
    this.validator = new DressingValidator(translate, this.firebaseAnalytics);
    this.plungeDressModel = new plungeDressingCalculator(
      storage,
      this.firebaseAnalytics
    );

    translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });

    this.validator.getValidationMsgs().then((msgs) => {
      this.validationMsgs = msgs;
    });
    this.isImperial = true;
    this.isImperialCDSR = true;
    this.isCDnotNCD = true;
    this.hideTotalInFeedRate = false;
    this.isUinNotIndp = true;
    this.isUDnotCD = true;

    this.masterData
      .getTotalInfeedRateRecommendedValues(this.isImperial)
      .then((totalInfeedRateRecommendedList) => {
        console.log(totalInfeedRateRecommendedList);
        this.totalInfeedRateRecommendedValues = totalInfeedRateRecommendedList;
      });

    this.masterData
      .getWheelTechTypeDropDowns(this.isImperial, this.isCDnotNCD)
      .then((val) => {
        this.wheelTechnTypeListRefs = val;
        this.wheelTechnTypeListRefsOne = this.wheelTechnTypeListRefs.slice(
          0,
          2
        );
        this.wheelTechnTypeListRefsTwo = this.wheelTechnTypeListRefs.slice(2);
        this.wheelTechnTypeListRefsTwoLength = this.wheelTechnTypeListRefsTwo.length;
      });
    this.masterData
      .getWheelSpeedSFPMRecommendedValues(this.isImperial, this.isCDnotNCD)
      .then((val) => {
        this.wheelSFPMRecommendedValues = val;
      });
    this.masterData.getSpeedRationRecommended(this.isImperial).then((val) => {
      this.speedRatioRecommendedValues = val;
    });
    this.dressingForm = formBuilder.group(
      {
        unitPref: [null],
        unitPrefCD: [null],
        wheelTechType: ["", Validators.required],
        ncdDiameter: ["", Validators.required],
        ncdWPWheel: ["", Validators.required],
        ncdWPDiameter: ["", Validators.required],
        rpmcdncd: ["", Validators.required],
        irUnitPref: [null],
        irsrwpcdValue: ["", Validators.required],
        unitPrefUDCD: [null],
      },
      { validator: this.validator.dressingValidator.bind(this) }
    );

    this.dressingForm.get("unitPref").setValue(false);
    this.isImperial = true;
    this.isImperialCDSR = true;
    this.isCDnotNCD = true;
    this.hideTotalInFeedRate = false;
    this.isUinNotIndp = true;
    this.isUDnotCD = true;

    this.updateToggle();
    this.updateIRToggle();
    this.updateUDCDToggle();
    this.updateToggleCDNCD();
    this.loadSwitchFromDb();

    this.storage.get(Constants.KEY_DRESSING).then((dressObj) => {
      console.log(dressObj);
      if (dressObj) {
        this.dressingForm.get("irUnitPref").setValue(!dressObj["isUinNotIndp"]);
        this.dressingForm
          .get("irsrwpcdValue")
          .setValue(dressObj["inputDressRollParameters"]["dressRollIFR"]);
        this.dressingForm
          .get("ncdDiameter")
          .setValue(dressObj["inputDressRollParameters"]["dressRollDiameter"]);
        this.dressingForm
          .get("ncdWPDiameter")
          .setValue(dressObj["inputWheelParameters"]["wpDiameter"]);
        this.dressingForm
          .get("ncdWPWheel")
          .setValue(dressObj["inputWheelParameters"]["wpWheelSFPM"]);
        this.dressingForm
          .get("rpmcdncd")
          .setValue(dressObj["inputDressRollParameters"]["dressRollRPM"]);
        this.dressingForm
          .get("unitPref")
          .setValue(!dressObj["isImperialNotMetric"]);
        this.dressingForm.get("unitPrefCD").setValue(!dressObj["isunitPrefCD"]);
        this.dressingForm.get("unitPrefUDCD").setValue(!dressObj["isUDnotCD"]);
        this.dressingForm
          .get("wheelTechType")
          .setValue(dressObj["productType"]);

        this.autoLoad = true;
        this.onSubmit(this.dressingForm.value);
      } else {
        this.autoLoad = false;
        this.translate
          .get("calculate_btn")
          .toPromise()
          .then((result) => {
            this.calculateBtnText = result;
          });
      }
    });
  }

  ionViewDidLoad() {
    this.firebaseAnalytics.setCurrentScreen(
      FirebaseConstants.PLUNGE_DRESSING_CALCULATOR
    );
  }
  /**
   * Name: updateDropDown
   * Parameters: none
   * Description: on update of dropdown value, calculations have to be done
   * Return: none
   */
  updateDropDown() {
    this.updateResults();
  }
  /**
   * Name: updateResults
   * Parameters: none
   * Description: Used to blur the result area and to display update button
   * Return: none
   */
  updateResults() {
    this.calculateDisable = false;
    this.resultOpacity = "0.3";
  }
  /**
   * Name: onSubmit
   * Parameters: value
   * Description: used to read data from ui to show validation messages if required and to perform required calculations
   * Return: none
   */
  onSubmit(value: any): void {
    console.log(value);
    this.keyboard.close();
    var wheelTechTypeHasErrors: boolean;
    var ncdDiameterHasErrors: boolean;
    var ncdWPWheelHasErrors: boolean;
    var dncdWPDiameterHasErrors: boolean;
    var dinfeedRateHasErrors: boolean;
    var rpmDiamRollHasErrors: boolean;
    // console.log(this.validationMsgs);
    /* let ncdDiameter = fg.get('ncdDiameter').value;
   let ncdWPWheel = fg.get('ncdDiameter').value;
   let ncdWPDiameter = fg.get('ncdDiameter').value; */
    if (
      this.dressingForm
        .get("wheelTechType")
        .hasError(this.validationMsgs.wheelTechType.type)
    ) {
      wheelTechTypeHasErrors = true;
    }
    for (let validation of this.validationMsgs.ncdDiameter) {
      if (this.dressingForm.get("ncdDiameter").hasError(validation.type)) {
        ncdDiameterHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.ncdWPWheel) {
      if (this.dressingForm.get("ncdWPWheel").hasError(validation.type)) {
        ncdWPWheelHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.ncdWPDiameter) {
      if (this.dressingForm.get("ncdWPDiameter").hasError(validation.type)) {
        dncdWPDiameterHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.infeedRate) {
      if (this.dressingForm.get("irsrwpcdValue").hasError(validation.type)) {
        dinfeedRateHasErrors = true;
      }
    }
    for (let validation of this.validationMsgs.rpmDiamRoll) {
      if (this.dressingForm.get("rpmcdncd").hasError(validation.type)) {
        rpmDiamRollHasErrors = true;
      }
    }

    if (
      !wheelTechTypeHasErrors &&
      !ncdDiameterHasErrors &&
      !ncdWPWheelHasErrors &&
      !dncdWPDiameterHasErrors &&
      !rpmDiamRollHasErrors &&
      !dinfeedRateHasErrors
    ) {
      if (!this.autoLoad) {
        this.firebaseAnalytics.logEvent(
          FirebaseConstants.PLUNGE_DRESSING_CALC_HITCOUNT,
          {}
        );
      } else {
        this.autoLoad = false;
      }
      this.calculate(
        value.unitPref,
        value.wheelTechType,
        {
          dressRollDiameter: value.ncdDiameter,
          dressRollRPM: value.rpmcdncd,
          dressRollIFR: value.irsrwpcdValue,
        },
        { wpWheelSFPM: value.ncdWPWheel, wpDiameter: value.ncdWPDiameter },
        value.unitPrefUDCD,
        value.irUnitPref,
        value.unitPrefCD
      );
    } else {
      this.getFormValidationErrors();
      this.dressingForm.get("ncdDiameter").markAsTouched();
      this.dressingForm.get("ncdWPWheel").markAsTouched();
      this.dressingForm.get("ncdWPDiameter").markAsTouched();
      this.dressingForm.get("rpmcdncd").markAsTouched();
      this.dressingForm.get("irsrwpcdValue").markAsTouched();
      this.showErrors = true;
    }
  }

  /**
   * Name: getFormValidationErrors
   * Parameters: none
   * Description: reads validation error messages
   * Return: none
   */
  getFormValidationErrors() {
    Object.keys(this.dressingForm.controls).forEach((key) => {
      const controlErrors = this.dressingForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          this.plungeDressModel.triggerFirebaseEvent(keyError);
        });
      }
    });
  }

  calculate(
    unitPref: boolean,
    productType,
    dressRollParameters,
    wheelParameters,
    unitPrefUDCD: boolean,
    isUinNotIndp: boolean,
    unitPrefCD: boolean
  ) {
    console.log(unitPref);
    console.log(productType);
    console.log(dressRollParameters);
    console.log(wheelParameters);
    this.isImperial = !this.dressingForm.get("unitPref").value;
    this.isUDnotCD = !this.dressingForm.get("unitPrefUDCD").value;
    console.log(!unitPrefUDCD);
    //  if(!unitPrefUDCD){
    //    this.isCDnotNCD = false;
    //  }

    // fetch totalInfeedRate from master data
    this.masterData
      .getTotalInfeedRateRecommendedValues(this.isImperial)
      .then((totalInfeedRateRecommendedList) => {
        console.log(totalInfeedRateRecommendedList);
        this.totalInfeedRateRecommendedValues = totalInfeedRateRecommendedList;
      });

    //fetch correct wheel type
    console.log(this.isImperial);
    console.log(!unitPrefCD);
    this.masterData
      .getWheelTechTypeDropDowns(this.isImperial, !unitPrefCD)
      .then((val) => {
        this.masterData
          .getSpeedRationRecommended(this.isImperial)
          .then((val1) => {
            this.speedRatioRecommendedValues = val1;
            this.wheelTechnTypeListRefs = val;
            this.wheelTechnTypeListRefsOne = this.wheelTechnTypeListRefs.slice(
              0,
              2
            );
            this.wheelTechnTypeListRefsTwo = this.wheelTechnTypeListRefs.slice(
              2
            );
            this.wheelTechnTypeListRefsTwoLength = this.wheelTechnTypeListRefsTwo.length;
            console.log(this.wheelSFPMRecommendedValues);
            console.log(this.speedRatioRecommendedValues);
            var output = this.plungeDressModel.calculate(
              !unitPref,
              productType,
              dressRollParameters,
              wheelParameters,
              this.totalInfeedRateRecommendedValues,
              this.wheelTechnTypeListRefs,
              !unitPrefUDCD,
              !isUinNotIndp,
              this.wheelSFPMRecommendedValues,
              val1,
              !unitPrefCD
            );
            console.log(output);

            this.op_wheel_sfpm_calculated =
              output["calculatedAndRecommendedWheelSFPM"][
                "resultCalculatedWheelSFPM"
              ];
            this.op_wheel_sfpm_recommended =
              output["calculatedAndRecommendedWheelSFPM"][
                "resultRecommendedWheelSFPM"
              ];

            this.op_dress_roll_sfpm_calculated =
              output["calculatedAndRecommendedDressRollSFPM"][
                "resultCalculatedDressRollSFPM"
              ];
            this.op_dress_roll_sfpm_recommended =
              output["calculatedAndRecommendedDressRollSFPM"][
                "resultRecommendedDressRollSFPM"
              ];

            this.op_speed_ratio_calculated =
              output["calculatedAndRecommendedSpeedRatio"][
                "resultCalculatedDressSpeedRatio"
              ];
            this.op_speed_ratio_recommended =
              output["calculatedAndRecommendedSpeedRatio"][
                "resultRecommendedDressSpeedRatio"
              ];

            this.op_infeed_rate_inmin_calculated =
              output["calculatedAndRecommendedInfeedRateinmin"][
                "resultCalculatedInfeedRateinmin"
              ];
            this.op_infeed_rate_inmin_recommended =
              output["calculatedAndRecommendedInfeedRateinmin"][
                "resultRecommendedInfeedRateinmin"
              ];

            this.op_infeed_rate_uinrev_calculated =
              output["calculatedAndRecommendedInfeedRateuinrev"][
                "resultCalculatedInfeedRateuinrev"
              ];
            this.op_infeed_rate_uinrev_recommended =
              output["calculatedAndRecommendedInfeedRateuinrev"][
                "resultRecommendedInfeedRateuinrev"
              ];

            this.op_infeed_rate_uin_calculated = 0;
            this.op_infeed_rate_uin_recommended = output["productType"];
            console.log(output["recommendedTotalInfeedRate"]);
            this.op_total_infeed_recommended =
              output["recommendedTotalInfeedRate"];

            this.showResult = true;
            this.resultOpacity = "1";
            this.plungeDressModel.productTypeEvents(productType);
            this.calculateDisable = true;
            this.translate
              .get("update_btn")
              .toPromise()
              .then((result) => {
                this.calculateBtnText = result;
              });
          });
      });
  }

  updateToggle() {
    this.showErrors = false;
    this.dressingForm.get("wheelTechType").setValue("");

    this.dressingForm.get("ncdDiameter").setValue("");
    this.dressingForm.get("ncdWPWheel").setValue("");
    this.dressingForm.get("ncdWPDiameter").setValue("");
    this.dressingForm.get("rpmcdncd").setValue("");
    this.dressingForm.get("irsrwpcdValue").setValue("");

    this.dressingForm.get("ncdDiameter").markAsUntouched();
    this.dressingForm.get("ncdWPWheel").markAsUntouched();
    this.dressingForm.get("ncdWPDiameter").markAsUntouched();
    this.dressingForm.get("rpmcdncd").markAsUntouched();
    this.dressingForm.get("irsrwpcdValue").markAsUntouched();

    this.dressingForm.get("ncdDiameter").markAsPristine();
    this.dressingForm.get("ncdWPWheel").markAsPristine();
    this.dressingForm.get("ncdWPDiameter").markAsPristine();
    this.dressingForm.get("rpmcdncd").markAsPristine();
    this.dressingForm.get("irsrwpcdValue").markAsPristine();

    this.isImperial = !this.dressingForm.get("unitPref").value;

    console.log(this.isImperial);
    if (this.isImperial) {
      this.unit_label = " (in";
      this.unit_label1 = " (uin/rev";
      this.ir_unit_lable_inmin = "in/min";
      this.ir_unit_lable_uinrev = "in_rev";
    } else {
      this.unit_label = " (mm";
      this.unit_label1 = " (um/rev";
      this.ir_unit_lable_inmin = "mm/s";
      this.ir_unit_lable_uinrev = "mm_rev";
    }
    this.dressingForm.get("irUnitPref").setValue(false);
    this.dressingForm.get("unitPrefCD").setValue(false);
    this.dressingForm.get("unitPrefUDCD").setValue(false);

    this.isUinNotIndp = !this.dressingForm.get("irUnitPref").value;

    this.masterData
      .getWheelTechTypeDropDowns(this.isImperial, this.isCDnotNCD)
      .then((val) => {
        this.wheelTechnTypeListRefs = val;
        this.wheelTechnTypeListRefsOne = this.wheelTechnTypeListRefs.slice(
          0,
          2
        );
        this.wheelTechnTypeListRefsTwo = this.wheelTechnTypeListRefs.slice(2);
        this.wheelTechnTypeListRefsTwoLength = this.wheelTechnTypeListRefsTwo.length;
      });

    this.masterData
      .getWheelSpeedSFPMRecommendedValues(this.isImperial, this.isCDnotNCD)
      .then((val) => {
        this.wheelSFPMRecommendedValues = val;
      });
    this.masterData
      .getTotalInfeedRateRecommendedValues(this.isImperial)
      .then((totalInfeedRateRecommendedList) => {
        this.totalInfeedRateRecommendedValues = totalInfeedRateRecommendedList;
      });
  }
  updateToggleCDNCD() {
    this.showErrors = false;
    this.isCDnotNCD = !this.dressingForm.get("unitPrefCD").value;
    if (!this.isCDnotNCD) {
      this.hideTotalInFeedRate = true;
    } else {
      this.hideTotalInFeedRate = false;
    }
    this.dressingForm.get("wheelTechType").setValue("");

    this.masterData
      .getWheelTechTypeDropDowns(this.isImperial, this.isCDnotNCD)
      .then((val) => {
        this.wheelTechnTypeListRefs = val;
        this.wheelTechnTypeListRefsOne = this.wheelTechnTypeListRefs.slice(
          0,
          2
        );
        this.wheelTechnTypeListRefsTwo = this.wheelTechnTypeListRefs.slice(2);
        this.wheelTechnTypeListRefsTwoLength = this.wheelTechnTypeListRefsTwo.length;
      });
    this.masterData
      .getWheelSpeedSFPMRecommendedValues(this.isImperial, this.isCDnotNCD)
      .then((val) => {
        this.wheelSFPMRecommendedValues = val;
      });
    this.updateResults();

    this.calculateDisable = false;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
  }

  updateIRToggle() {
    this.isUinNotIndp = !this.dressingForm.get("irUnitPref").value;
    this.dressingForm.get("irsrwpcdValue").setValue("");
    this.dressingForm.get("irsrwpcdValue").markAsPristine();
    this.dressingForm.get("irsrwpcdValue").markAsUntouched();
    if (this.isUinNotIndp) {
      this.unit_labelIR1 = " (in/min";
    } else {
      this.unit_labelIR1 = " (uin/rev";
    }
    if (this.isImperial) {
      this.unit_label = " (in";
      this.unit_label1 = " (uin/rev";
      this.ir_unit_lable_inmin = "in/min";
      this.ir_unit_lable_uinrev = "in_rev";
    } else {
      this.unit_label = " (mm";
      this.unit_label1 = " (um/rev";
      this.ir_unit_lable_inmin = "mm/s";
      this.ir_unit_lable_uinrev = "mm_rev";
    }
  }
  updateUDCDToggle() {
    this.isUDnotCD = !this.dressingForm.get("unitPrefUDCD").value;
    console.log(this.isUDnotCD);
    this.calculateDisable = false;
    this.translate
      .get("update_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
  }

  /**
   * Name :       isValidNumber
   * Parameters:  --
   * Description: checks if the input is a number maxlength 10 without +91 etc)
   *              //to prevent commas/brackets/+/- etc & restrict only one '.'
   * Return:      void
   */
  isValidNumber(event) {
    var enteredInput = event.target.value;
    var indexx = this.common.getNonFloatIndexFromNumericStr(enteredInput);
    if (indexx !== "false") {
      var inp = enteredInput.slice(indexx, 1);
      if (!this.common.floatnoValidationRunTime(inp)) {
        event.target.value = enteredInput.slice(0, indexx);
      }
    }
  }

  // to prevent letters/*/# etc
  telkeypress(ev) {
    this.common.telkeypress(ev);
  }
  //
  /**
   * Name : round
   * Parameters: 'value' & 'precision' of type any
   * Description: This method round of the value to the given precision.
   * Return: void
   */
  round(value, precision) {
    let multiplier = Math.pow(10, precision || 0);
    return (Math.round(value * multiplier) / multiplier).toFixed(precision);
  }

  /**
   * Name : clearResults
   * Parameters: null
   * Description: This method is called when 'Clear Result' button is clicked. It clears all the user input fields and resets toggles.
   * Return: void
   */
  clearResults() {
    this.showErrors = false;

    this.dressingForm.get("wheelTechType").setValue("");

    this.dressingForm.get("ncdDiameter").setValue("");
    this.dressingForm.get("ncdWPWheel").setValue("");
    this.dressingForm.get("ncdWPDiameter").setValue("");
    this.dressingForm.get("rpmcdncd").setValue("");
    this.dressingForm.get("irsrwpcdValue").setValue("");

    this.dressingForm.get("ncdDiameter").markAsUntouched();
    this.dressingForm.get("ncdWPWheel").markAsUntouched();
    this.dressingForm.get("ncdWPDiameter").markAsUntouched();
    this.dressingForm.get("rpmcdncd").markAsUntouched();
    this.dressingForm.get("irsrwpcdValue").markAsUntouched();

    this.dressingForm.get("ncdDiameter").markAsPristine();
    this.dressingForm.get("ncdWPWheel").markAsPristine();
    this.dressingForm.get("ncdWPDiameter").markAsPristine();
    this.dressingForm.get("rpmcdncd").markAsPristine();
    this.dressingForm.get("irsrwpcdValue").markAsPristine();

    this.dressingForm.get("irUnitPref").setValue(false);
    this.dressingForm.get("unitPref").setValue(false);
    this.dressingForm.get("unitPrefCD").setValue(false);
    this.dressingForm.get("unitPrefUDCD").setValue(false);
    this.storage
      .remove(Constants.KEY_DRESSING)
      .then((output) => {
        console.log("Diamond Dressing Cleared: " + output);
      })
      .catch((err) => {
        console.log("Diamond Dressing Clear error: " + err);
      });
    this.storage
      .remove(Constants.KEY_UNIT_PREF_PLUNGE_ROLL_DRESSING)
      .then((output) => {
        console.log("Diamond Dressing Toggles Cleared: " + output);
      })
      .catch((err) => {
        console.log("Diamond Dressing Toggles Clear error: " + err);
      });

    this.resultOpacity = "1";

    this.showResult = false;

    this.translate
      .get("calculate_btn")
      .toPromise()
      .then((result) => {
        this.calculateBtnText = result;
      });
  }
  psSlideGoto(pageIndex) {
    this.slides.slideTo(pageIndex, 500);
  }

  updateirdToggle() {
    this.showErrors = false;

    this.isUinNotIn = !this.dressingFormCDSpeedRatio.get("irdUnitPref").value;
  }

  loadSwitchFromDb() {
    this.storage
      .get(Constants.KEY_UNIT_PREF_PLUNGE_ROLL_DRESSING)
      .then((toggles) => {
        console.log(toggles);
        // isImperialNotMetric: true
        // isUDnotCD: false
        // isUinNotIndp: true
        // isunitPrefCD: false
        if (toggles != null && toggles !== undefined) {
          this.dressingForm
            .get("unitPref")
            .setValue(!toggles.isImperialNotMetric);
          this.isImperial = !toggles.isImperialNotMetric;

          this.dressingForm.get("unitPrefCD").setValue(!toggles.isunitPrefCD);
          this.isCDnotNCD = toggles.isunitPrefCD;

          this.dressingForm.get("unitPrefUDCD").setValue(!toggles.isUDnotCD);
          this.isUDnotCD = !toggles.isUDnotCD;

          this.dressingForm.get("irUnitPref").setValue(!toggles.isUinNotIndp);
          this.isUinNotIndp = !toggles.isUinNotIndp;
          this.updateToggle();
          this.updateToggleCDNCD();
          this.updateUDCDToggle();
          this.updateIRToggle();
        }
      });
  }
}
