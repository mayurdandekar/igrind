import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
} from "ionic-angular";

/**
 * Generated class for the PlaceholderTitlePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-placeholder-title",
  templateUrl: "placeholder-title.html",
})
export class PlaceholderTitlePage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad PlaceholderTitlePage");
  }

  ok() {
    this.viewCtrl.dismiss();
  }
}
