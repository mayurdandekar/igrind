import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlaceholderTitlePage } from './placeholder-title';

@NgModule({
  declarations: [
    PlaceholderTitlePage,
  ],
  imports: [
    IonicPageModule.forChild(PlaceholderTitlePage),
  ],
})
export class PlaceholderTitlePageModule {}
