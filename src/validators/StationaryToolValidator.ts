import { TranslateService } from '@ngx-translate/core';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import * as Constants from '../utils/constants';
import { FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';


export class StationaryToolValidator {
    productType: any;

    constructor(private translate: TranslateService,
        public firebaseAnalytics: FirebaseAnalytics) {

    }

    dressingValidator(fg: FormGroup) {
        let isImperial = !(fg.get('unitPref').value);
        this.productType = fg.get('productType').value;
        let rpm = fg.get('rpm').value;
        let contactWidth = fg.get('contactWidth').value;
        let rate = fg.get('rate').value;
        let isRateTraverse = !(fg.get('unitPrefRate').value);

        if (rpm != "" && rpm != undefined && (rpm.toString().length > 9)) {
            fg.get('rpm').setErrors({ 'invalid_input_length': true });
        }
        if (contactWidth != "" && contactWidth != undefined && (contactWidth.toString().length > 9)) {
            fg.get('contactWidth').setErrors({ 'invalid_input_length': true });
        }
        if (rate != "" && rate != undefined && (rate.toString().length > 9)) {
            fg.get('rate').setErrors({ 'invalid_input_length': true });
        }
        if (isImperial) {
            if (rpm == "") {
                fg.get('rpm').setErrors({ 'rpm_empty': true });
            }
            else if (rpm != "" && rpm !== undefined && (isNaN(rpm) || rpm == 0)) {
                fg.get('rpm').setErrors({ 'rpm_eng': true });
            }
            if (contactWidth == "") {
                fg.get('contactWidth').setErrors({ 'contactWidth_empty': true });
            }
            else if (contactWidth != "" && contactWidth !== undefined && (isNaN(contactWidth) || contactWidth == 0)) {
                fg.get('contactWidth').setErrors({ 'contactWidth_eng': true });
            }
            // console.log(isRateTraverse);
            if (isRateTraverse) {
                if (rate == "") {
                    fg.get('rate').setErrors({ 'rateTR_empty': true });
                }
                else if (rate != "" && rate !== undefined && (isNaN(rate) || rate == 0)) {
                    fg.get('rate').setErrors({ 'rateTR_eng': true });
                }
            } else {
                // console.log(rate);
                if (rate == "") {
                    fg.get('rate').setErrors({ 'rateDL_empty': true });
                }
                else if (rate != "" && rate !== undefined && (isNaN(rate) || rate == "" || rate == 0)) {
                    fg.get('rate').setErrors({ 'rateDL_eng': true });
                }
            }

        } else {
            if (rpm == "") {
                fg.get('rpm').setErrors({ 'rpm_empty': true });
            }
            else if (rpm != "" && rpm !== undefined && (isNaN(rpm) || rpm == 0)) {
                fg.get('rpm').setErrors({ 'rpm_metric': true });
            }
            if (contactWidth == "") {
                fg.get('contactWidth').setErrors({ 'contactWidth_empty': true });
            }
            else if (contactWidth != "" && contactWidth !== undefined && (isNaN(contactWidth) || contactWidth == 0)) {
                fg.get('contactWidth').setErrors({ 'contactWidth_metric': true });
            }
            if (isRateTraverse) {
                if (rate == "") {
                    fg.get('rate').setErrors({ 'rateTR_empty': true });
                }
                else if (rate != "" && rate !== undefined && (isNaN(rate) || rate == 0)) {
                    fg.get('rate').setErrors({ 'rateTR_metric': true });
                }
            } else {

                if (rate == "") {
                    fg.get('rate').setErrors({ 'rateDL_empty': true });
                }
                else if (rate != "" && rate !== undefined && (isNaN(rate) || rate == "" || rate == 0)) {
                    fg.get('rate').setErrors({ 'rateDL_metric': true });
                }
            }


        }
        if (this.productType === undefined || this.productType.length == 0 || this.productType === "") {
            let error: any = {};
            error[Constants.VAL_WHEEL_TECHNOLOGY_TYPE_EMPTY] = true;
            fg.get('productType').setErrors(error);
        }
        return null;
    }

    getValidationMsgs(): Promise<any> {
        var validationMsgs: any = {};
        var stationaryErrMsg;
        var emptyErrMsg;
        var inputLengthErrmsg = "Entered Value should be 9 digit long";

        return this.translate.get('zero_input').toPromise().then((errorMsg) => {
            stationaryErrMsg = errorMsg
            return this.translate.get('empty_invalid_input').toPromise().then((emptyInputMsg) => {
                emptyErrMsg = emptyInputMsg;
                return this.translate.get('wheeltech_type_empty').toPromise().then((emptyMsg) => {
                    validationMsgs = {
                        'productType': {
                            type: Constants.VAL_WHEEL_TECHNOLOGY_TYPE_EMPTY,
                            message: emptyMsg
                        },
                        'rpm': [
                            { type: 'rpm_eng', message: stationaryErrMsg },
                            { type: 'rpm_metric', message: stationaryErrMsg },
                            { type: 'rpm_empty', message: emptyErrMsg },
                            { type: 'invalid_input_length', message: inputLengthErrmsg }

                        ],
                        'contactWidth': [
                            { type: 'contactWidth_eng', message: stationaryErrMsg },
                            { type: 'contactWidth_metric', message: stationaryErrMsg },
                            { type: 'contactWidth_empty', message: emptyErrMsg },
                            { type: 'invalid_input_length', message: inputLengthErrmsg }

                        ],
                        'rate': [
                            { type: 'rateTR_eng', message: stationaryErrMsg },
                            { type: 'rateTR_metric', message: stationaryErrMsg },
                            { type: 'rateTR_empty', message: emptyErrMsg },
                            { type: 'rateDL_eng', message: stationaryErrMsg },
                            { type: 'rateDL_metric', message: stationaryErrMsg },
                            { type: 'rateDL_empty', message: emptyErrMsg },
                            { type: 'invalid_input_length', message: inputLengthErrmsg }

                        ]
                    }
                    return new Promise((resolve, reject) => {
                        resolve(validationMsgs);
                    });
                });
            });//emptyErrMsg
        });
    }

}
