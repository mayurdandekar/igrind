import { TranslateService } from '@ngx-translate/core';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import * as Constants from '../utils/constants';
import { FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';


export class DressingValidator {
  coolantType: string;
  resultCalculatedWheelRPM: any;
  totalInfeedRateRecommendedValues: any;
  wheelTechType: any;
  resultCalculatedWheelSFPM: any;
  resultCalculatedDressRollSFPM: number;
  wheelSFPM: any;
  wheelSFPMRecommended: any;
  speedRatioRecommended: any;
  wheelTechTypekey: any;
  constructor(private translate: TranslateService,
    public firebaseAnalytics: FirebaseAnalytics) {

  }

  dressingValidator(fg: FormGroup) {
    let isImperial = !(fg.get('unitPref').value);
    this.coolantType = fg.get('wheelTechType').value;
    let ncdDiameter = fg.get('ncdDiameter').value;
    let ncdWPWheel = fg.get('ncdWPWheel').value;
    let ncdWPDiameter = fg.get('ncdWPDiameter').value;
    let irUnitPref = !(fg.get('irUnitPref').value);
    let infeedRate = fg.get('irsrwpcdValue').value;
    let rpmDiamRoll = fg.get('rpmcdncd').value;

    if (ncdDiameter!="" && ncdDiameter != undefined && (ncdDiameter.toString().length > 9)) {
      fg.get('ncdDiameter').setErrors({ 'invalid_input_length': true });
    }
    if (ncdWPWheel!="" && ncdWPWheel != undefined && (ncdWPWheel.toString().length > 9)) {
      fg.get('ncdWPWheel').setErrors({ 'invalid_input_length': true });
    }
    if (ncdWPDiameter!="" && ncdWPDiameter != undefined && (ncdWPDiameter.toString().length > 9)) {
      fg.get('ncdWPDiameter').setErrors({ 'invalid_input_length': true });
    }
    if (infeedRate!="" && infeedRate != undefined && (infeedRate.toString().length > 9)) {
      fg.get('irsrwpcdValue').setErrors({ 'invalid_input_length': true });
    }
    if (rpmDiamRoll!="" && rpmDiamRoll != undefined && (rpmDiamRoll.toString().length > 9)) {
      fg.get('rpmcdncd').setErrors({ 'invalid_input_length': true });
    }
    if (isImperial) {
      if (ncdDiameter =="") {
        fg.get('ncdDiameter').setErrors({ 'ncdDiameter_empty': true });
      }
     else if (ncdDiameter !="" && ncdDiameter !== undefined && (isNaN(ncdDiameter) || ncdDiameter==0)) {
        fg.get('ncdDiameter').setErrors({ 'ncdDiameter_eng': true });
      }
      if (ncdWPWheel =="") {
        fg.get('ncdWPWheel').setErrors({ 'ncdWPWheel_empty': true });
      }
      else if (ncdWPWheel !="" && ncdWPWheel !== undefined && (isNaN(ncdWPWheel) || ncdWPWheel==0)) {
        fg.get('ncdWPWheel').setErrors({ 'ncdWPWheel_eng': true });
      }
      
      if (ncdWPDiameter =="") {
        fg.get('ncdWPDiameter').setErrors({ 'ncdWPDiameter_empty': true });
      }
      else if (ncdWPDiameter !="" && ncdWPDiameter !== undefined && (isNaN(ncdWPDiameter) || ncdWPDiameter==0)) {
        fg.get('ncdWPDiameter').setErrors({ 'ncdWPDiameter_eng': true });
      }
      if (irUnitPref) {
        if (infeedRate =="") {
          fg.get('irsrwpcdValue').setErrors({ 'infeedRateUmrev_empty': true });
        }
        else if (infeedRate !="" && infeedRate !== undefined && (isNaN(infeedRate) || infeedRate==0)) {
          fg.get('irsrwpcdValue').setErrors({ 'infeedRateInmin_eng': true });
        }
      } else {
        if (infeedRate =="") {
          fg.get('irsrwpcdValue').setErrors({ 'infeedRateUmrev_empty': true });
        }
        else if (infeedRate !="" && infeedRate !== undefined && (isNaN(infeedRate) || infeedRate==0)) {
          fg.get('irsrwpcdValue').setErrors({ 'infeedRateUinrev_eng': true });
        }
      }
      if (rpmDiamRoll =="") {
        fg.get('rpmcdncd').setErrors({ 'rpmDiamRoll_empty': true });
      }
      else if (rpmDiamRoll !="" && rpmDiamRoll !== undefined && (isNaN(rpmDiamRoll) || rpmDiamRoll==0)) {
        fg.get('rpmcdncd').setErrors({ 'rpmDiamRoll_eng': true });
      }
    } else {
      if (ncdDiameter =="") {
        fg.get('ncdDiameter').setErrors({ 'ncdDiameter_empty': true });
      }
     else if (ncdDiameter !="" && ncdDiameter !== undefined && (isNaN(ncdDiameter) || ncdDiameter==0)) {
        fg.get('ncdDiameter').setErrors({ 'ncdDiameter_metric': true });
      }
      if (ncdWPWheel =="") {
        fg.get('ncdWPWheel').setErrors({ 'ncdWPWheel_empty': true });
      }
      else if (ncdWPWheel !="" && ncdWPWheel !== undefined && (isNaN(ncdWPWheel) || ncdWPWheel==0)) {
        fg.get('ncdWPWheel').setErrors({ 'ncdWPWheel_metric': true });
      }
      if (ncdWPDiameter =="") {
        fg.get('ncdWPDiameter').setErrors({ 'ncdWPDiameter_empty': true });
      }
      else if (ncdWPDiameter !="" && ncdWPDiameter !== undefined && (isNaN(ncdWPDiameter) || ncdWPDiameter==0)) {
        fg.get('ncdWPDiameter').setErrors({ 'ncdWPDiameter_metric': true });
      }
      if (irUnitPref) {
        if (infeedRate =="") {
          fg.get('irsrwpcdValue').setErrors({ 'infeedRateUmrev_empty': true });
        }
        else if (infeedRate !="" && infeedRate !== undefined && (isNaN(infeedRate) || infeedRate==0)) {
          fg.get('irsrwpcdValue').setErrors({ 'infeedRateMms_metric': true });
        }
      } else {
        if (infeedRate =="") {
          fg.get('irsrwpcdValue').setErrors({ 'infeedRateUmrev_empty': true });
        }
        else if (infeedRate !="" && infeedRate !== undefined && (isNaN(infeedRate) || infeedRate==0)) {
          fg.get('irsrwpcdValue').setErrors({ 'infeedRateUmrev_metric': true });
        }
      }

      if (rpmDiamRoll =="") {
        fg.get('rpmcdncd').setErrors({ 'rpmDiamRoll_empty': true });
      }
      else if (rpmDiamRoll !="" && rpmDiamRoll !== undefined && (isNaN(rpmDiamRoll) || rpmDiamRoll==0)) {
        fg.get('rpmcdncd').setErrors({ 'rpmDiamRoll_metric': true });
      }
    }
    if (this.coolantType === undefined || this.coolantType.length == 0 || this.coolantType === "") {
      let error: any = {};
      error[Constants.VAL_WHEEL_TECHNOLOGY_TYPE_EMPTY] = true;
      fg.get('wheelTechType').setErrors(error);
    }
    return null;
  }



  getValidationMsgs(): Promise<any> {
    var validationMsgs: any = {};
    var dressingErrMsg;
    var emptyErrMsg;
    var inputLengthErrmsg="Entered Value should be 9 digit long";

    return this.translate.get('zero_input').toPromise().then((errorMsg) => {
      dressingErrMsg=errorMsg
      return this.translate.get('empty_invalid_input').toPromise().then((emptyInputMsg)=>{
        emptyErrMsg=emptyInputMsg;
        
      return this.translate.get('wheeltech_type_empty').toPromise().then((emptyMsg) => {
        validationMsgs = {
          'wheelTechType': {
            type: Constants.VAL_WHEEL_TECHNOLOGY_TYPE_EMPTY,
            message: emptyMsg
          },
          'ncdDiameter': [
            { type: 'ncdDiameter_eng', message: dressingErrMsg },
            { type: 'ncdDiameter_metric', message: dressingErrMsg },
            { type: 'ncdDiameter_empty', message: emptyErrMsg },
            { type: 'invalid_input_length',message: inputLengthErrmsg }
          ],
          'ncdWPWheel': [
            { type: 'ncdWPWheel_eng', message: dressingErrMsg },
            { type: 'ncdWPWheel_metric', message: dressingErrMsg },
            { type: 'ncdWPWheel_empty', message: emptyErrMsg },
            { type: 'invalid_input_length',message: inputLengthErrmsg }

          ],
          'ncdWPDiameter': [
            { type: 'ncdWPDiameter_eng', message: dressingErrMsg },
            { type: 'ncdWPDiameter_metric', message: dressingErrMsg },
            { type: 'ncdWPDiameter_empty', message: emptyErrMsg },
            { type: 'invalid_input_length',message: inputLengthErrmsg }
          ],
          'infeedRate': [
            { type: 'infeedRateInmin_eng', message: dressingErrMsg },
            { type: 'infeedRateMms_metric', message: dressingErrMsg },
            { type: 'infeedRateUinrev_eng', message: dressingErrMsg },
            { type: 'infeedRateUmrev_metric', message: dressingErrMsg },
            { type: 'infeedRateUmrev_empty', message: emptyErrMsg },
            { type: 'invalid_input_length',message: inputLengthErrmsg }

          ],
          'rpmDiamRoll': [
            { type: 'rpmDiamRoll_eng', message: dressingErrMsg },
            { type: 'rpmDiamRoll_metric', message: dressingErrMsg },
            { type: 'rpmDiamRoll_empty', message: emptyErrMsg },
            { type: 'invalid_input_length',message: inputLengthErrmsg }

          ]
        }
        return new Promise((resolve, reject) => {
          resolve(validationMsgs);
        });
      });
    });//emptyErrMsg
    });
  }

}
