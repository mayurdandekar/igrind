import { TranslateService } from '@ngx-translate/core';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics';
import * as Constants from '../utils/constants';
import * as FirebaseConstants from '../utils/analytics-constants';
import { FormGroup } from '@angular/forms';

export class CoolantValidator {
  coolantType: string;

  isGwvNotPressure: boolean;
  gwvOrPressureUnitPref: boolean;
  gwvOrPressureVal: any;

  isGPNotFlowrate: boolean;
  gpOrFlowrateUnitPref: boolean;
  gpOrFlowrateVal: any;

  gwcweUnitPref: boolean;
  gwcwValue: any;

  constructor(private translate: TranslateService,
    public firebaseAnalytics: FirebaseAnalytics) {

  }

  coolantValidator(fg: FormGroup) {
    this.coolantType = fg.get('coolantType').value;
    this.isGwvNotPressure = !fg.get('gwvOrPressure').value;
    this.gwvOrPressureUnitPref = !fg.get('gwvOrPressureUnitPref').value;
    this.gwvOrPressureVal = parseFloat(fg.get('gwvOrPressureValue').value);

    this.isGPNotFlowrate = !fg.get('gpOrFlowrate').value;
    this.gpOrFlowrateUnitPref = !fg.get('gpOrFlowrateUnitPref').value;
    this.gpOrFlowrateVal = parseFloat(fg.get('gpOrFlowrateValue').value);

    this.gwcweUnitPref = !fg.get('gwcweUnitPref').value;
    this.gwcwValue = parseFloat(fg.get('gwcwValue').value);

    if (this.coolantType === undefined || this.coolantType.length == 0 || this.coolantType === "") {
      let error: any = {};
      error[Constants.VAL_COOLANT_TYPE_EMPTY] = true;
      fg.get('coolantType').setErrors(error);
    }
    
    
    if (this.gwvOrPressureVal !=0 &&(this.gwvOrPressureVal == "" || isNaN(this.gwvOrPressureVal)))
    { 

      let error: any = {};
      error[Constants.VAL_GWP_EMPTY] = true;
      fg.get('gwvOrPressureValue').setErrors(error);
    }
   else if (this.isGwvNotPressure) {
      let error: any = {};
      if (this.gwvOrPressureVal!="" && this.gwvOrPressureVal != undefined && (this.gwvOrPressureVal.toString().length > 9)) {
        error['invalid_input_length'] = true;
        fg.get('gwvOrPressureValue').setErrors(error);
      }
      if (this.gwvOrPressureUnitPref) {
        //SFPM
        
        if (this.gwvOrPressureVal !== undefined && (isNaN(this.gwvOrPressureVal) || this.gwvOrPressureVal==0)) {
          error[Constants.VAL_GWP_SFPM] = true;
          fg.get('gwvOrPressureValue').setErrors(error);
        }
      } 
     
      else {
        //m/Sec
        if (this.gwvOrPressureVal !== undefined && (isNaN(this.gwvOrPressureVal) || this.gwvOrPressureVal==0)) {
          error[Constants.VAL_GWP_MSEC] = true;
          fg.get('gwvOrPressureValue').setErrors(error);
        }
      }
    } 
    else {
      let error: any = {};
      if (this.gwvOrPressureVal!="" && this.gwvOrPressureVal != undefined && (this.gwvOrPressureVal.toString().length > 9)) {
        error['invalid_input_length'] = true;
        fg.get('gwvOrPressureValue').setErrors(error);
      }
      if (this.gwvOrPressureUnitPref) {
        //bar
        if (this.gwvOrPressureVal !== undefined && (isNaN(this.gwvOrPressureVal) || this.gwvOrPressureVal==0)) {
          error[Constants.VAL_GWP_BAR] = true;
          fg.get('gwvOrPressureValue').setErrors(error);
        }
      } else {
        //psi
        if (this.gwvOrPressureVal !== undefined && (isNaN(this.gwvOrPressureVal) || this.gwvOrPressureVal==0)) {
          error[Constants.VAL_GWP_PSI] = true;
          fg.get('gwvOrPressureValue').setErrors(error);
        }
      }
    }
    if (this.gpOrFlowrateVal !=0 &&(this.gpOrFlowrateVal == "" || isNaN(this.gpOrFlowrateVal)))
    {
      let error: any = {};
      error[Constants.VAL_GPCF_EMPTY] = true;
      fg.get('gpOrFlowrateValue').setErrors(error);
    }
    else if (this.gpOrFlowrateVal!="" && this.gpOrFlowrateVal != undefined && (this.gpOrFlowrateVal.toString().length > 9)) {
      let error: any = {};
      error['invalid_input_length'] = true;
      fg.get('gpOrFlowrateValue').setErrors(error);
    }
   else if (this.isGPNotFlowrate) {
      let error: any = {};
      
      if (this.gpOrFlowrateUnitPref) {
        //KW
        if (this.gpOrFlowrateVal !== undefined && (isNaN(this.gpOrFlowrateVal) || this.gpOrFlowrateVal==0)) {
          error[Constants.VAL_GPCF_KW] = true;
          fg.get('gpOrFlowrateValue').setErrors(error);
        }
      } else {
        //HP
        if (this.gpOrFlowrateVal !== undefined && (isNaN(this.gpOrFlowrateVal) || this.gpOrFlowrateVal==0)) {
          error[Constants.VAL_GPCF_HP] = true;
          fg.get('gpOrFlowrateValue').setErrors(error);
        }
      }
    } else {
      let error: any = {};
      if (this.gpOrFlowrateUnitPref) {
        //lmin
        if (this.gpOrFlowrateVal !== undefined && (isNaN(this.gpOrFlowrateVal) || this.gpOrFlowrateVal==0)) {
          error[Constants.VAL_GPCF_LMIN] = true;
          fg.get('gpOrFlowrateValue').setErrors(error);
        }
      } else {
        //gpm
        if (this.gpOrFlowrateVal !== undefined && (isNaN(this.gpOrFlowrateVal) || this.gpOrFlowrateVal==0)) {
          error[Constants.VAL_GPCF_GPM] = true;
          fg.get('gpOrFlowrateValue').setErrors(error);
        }
      }
    }

    if (this.gwcwValue !=0 &&(this.gwcwValue == "" || isNaN(this.gwcwValue)))
    {
      let error: any = {};
      error[Constants.VAL_GPCW_EMPTY] = true;
      fg.get('gwcwValue').setErrors(error);
    }
    else if (this.gwcwValue!="" && this.gwcwValue != undefined && (this.gwcwValue.toString().length > 9)) {
      let error: any = {};
      error['invalid_input_length'] = true;
      fg.get('gwcwValue').setErrors(error);
    }
   else if (this.gwcweUnitPref) {
      let error: any = {};//gwcwValue
      //mm
       if (this.gwcwValue !== undefined && (isNaN(this.gwcwValue) || this.gwcwValue==0)) {
        error[Constants.VAL_GPCW_MM] = true;
        fg.get('gwcwValue').setErrors(error);
      }
    } else {
      let error: any = {};
      //in
      if (this.gwcwValue !== undefined && (isNaN(this.gwcwValue) || this.gwcwValue==0)) {
        error[Constants.VAL_GPCW_IN] = true;
        fg.get('gwcwValue').setErrors(error);
      }
    }
    return null;
  }

  getValidationMsgs(): Promise<any> {
    var validationMsgs: any = {};
    var inputLengthErrmsg="Entered Value should be 9 digit long";
    return this.translate.get('zero_input').toPromise().then((errorMsg) => {
     return this.translate.get('empty_invalid_input').toPromise().then((emptyInputMsg)=>{
      return this.translate.get('coolant_type_empty').toPromise().then((emptyMsg) => {
        validationMsgs = {
          'coolantType': {
            type: Constants.VAL_COOLANT_TYPE_EMPTY,
            message: emptyMsg
          },
          'gwpPressure': [
            {
              type: Constants.VAL_GWP_SFPM,
              message: errorMsg
            },
            {
              type: Constants.VAL_GWP_MSEC,
              message: errorMsg
            },
            {
              type: Constants.VAL_GWP_BAR,
              message: errorMsg
            },
            {
              type: Constants.VAL_GWP_PSI,
              message: errorMsg
            },
            {
              type: Constants.VAL_GWP_EMPTY,
              message: emptyInputMsg
            },
            {
              type: 'invalid_input_length',
              message: inputLengthErrmsg
            }
          ],
          'gpCFlowrate': [
            {
              type: Constants.VAL_GPCF_KW,
              message: errorMsg
            },
            {
              type: Constants.VAL_GPCF_HP,
              message: errorMsg
            },
            {
              type: Constants.VAL_GPCF_LMIN,
              message: errorMsg
            },
            {
              type: Constants.VAL_GPCF_GPM,
              message: errorMsg
            },
            {
              type: Constants.VAL_GPCF_EMPTY,
              message: emptyInputMsg
            },
            {
              type: 'invalid_input_length',
              message: inputLengthErrmsg
            }
          ],
          'gwcWidth': [
            {
              type: Constants.VAL_GPCW_MM,
              message: errorMsg
            },
            {
              type: Constants.VAL_GPCW_IN,
              message: errorMsg
            },
            {
              type: Constants.VAL_GPCW_EMPTY,
              message: emptyInputMsg
            },
            {
              type: 'invalid_input_length',
              message: inputLengthErrmsg
            }
          ]
        }
        return new Promise((resolve, reject) => {
          resolve(validationMsgs);
        });
      });
    });//empty input
    });
  }

  triggerFirebaseEvent(keyError) {
    if(keyError === "gwp_sfpm" || keyError === "gwp_msec") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.COOLANT_CALC_OOR_GRINDINGWHEELVELOCITY_VALUE,{});
    } else if(keyError === "gwp_bar" || keyError === "gwp_psi") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.COOLANT_CALC_OOR_PRESSURE_VALUE,{});
    } else if (keyError === "gpcf_kw" || keyError === "gpcf_hp") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.COOLANT_CALC_OOR_GRINDINGPOWER_VALUE,{});
    } else if(keyError === "gpcf_lmin" || keyError === "gpcf_gpm"){
      this.firebaseAnalytics.logEvent(FirebaseConstants.COOLANT_CALC_OOR_COOLANTFLOWRATE_VALUE,{});
    } else if(keyError === "gpcw_mm" || keyError === "gpcw_in") {
      this.firebaseAnalytics.logEvent(FirebaseConstants.COOLANT_CALC_OOR_GRINDINGWHEELCONTACTWIDTH_VALUE,{});
    }
  }
}
